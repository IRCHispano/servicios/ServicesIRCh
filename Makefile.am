## AutoMake top-level Makefile for the ServicesIRCh
##
## Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
## Copyright (C) 2016-2017 Toni Garcia - zoltan <toni@tonigarcia.es>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

EXTRA_DIST = .patches ChangeLog doc include src src/lang autogen.sh cfg-rapido.sh configurador

# Install example.conf and helpfiles
#sysconf_DATA = doc/example.conf doc/helpfiles/*

include src/lang/subdir.am
include src/subdir.am

install:
	@if [ ! -d ${bindir} -a ! -f ${bindir} ]; then \
	    echo "Creating directory ${bindir}"; \
	    ${MKDIR} -p ${bindir}; \
        fi
	@echo `date +%y%m%d%H%M`.`cat .patches | \
		${AWK} -F . '{ if ($$(NF)~/\+$$/) { \
		for(i=1;i<NF;i++) \
			printf("%s_",$$i); \
			gsub("\\\\+","",$$(NF)); \
		}; \
		print $$(NF) }'` > /tmp/services.tag;

	@echo "Installing new Services binary as ${bindir}/services.`cat /tmp/services.tag` :"
	${INSTALL} services ${bindir}/services.`cat /tmp/services.tag`
	@( cd ${bindir}; \
	rm -f services; \
	ln -s services.`cat /tmp/services.tag` services; )
	@rm /tmp/services.tag

	@if [ ! -d ${sysconfdir} -a ! -f ${sysconfdir} ]; then \
	    echo "Creating directory ${sysconfdir}"; \
	    ${MKDIR} -p ${sysconfdir}; \
	fi
	@echo "Installing Example Configuration File as $(sysconfdir)/example.conf"
	${INSTALL} -m 664 doc/example.conf $(sysconfdir)

#	@echo "Installing Scripts"
#	${MKDIR} -p $(datadir)/scripts
#	cp scripts/* $(datadir)/scripts

	@if [ ! -d ${datadir} -a ! -f ${datadir} ]; then \
	    echo "Creating directory ${datadir}"; \
	    ${MKDIR} -p ${datadir}; \
	fi
	${MKDIR} -p $(datadir)/temp
	${MKDIR} -p $(datadir)/logs
	${MKDIR} -p $(datadir)/languages
	@echo "Installing Language modules in $(datadir)/languages"
	cp $(LANGOBJS) $(datadir)/languages
	${MKDIR} -p $(datadir)/helpfiles
	@echo "Installing HelpServ help files in $(datadir)/helpfiles"
	cp doc/helpfiles/* $(datadir)/helpfiles

update:
	@if [ ! -d .git ]; then \
  		echo "Esto no es un repositorio GIT, no puedes actualizar :("; \
		exit 1; \
	fi
	@echo "Obteniendo estado repositorio..."
	${GIT} fetch
	@if [ $$(git rev-parse HEAD) == $$(git rev-parse @{u}) ]; then \
		echo "No hay cambios, ya estaba actualizado"; \
	else \
   		echo "Hay cambios, se procede a actualizar el codigo"; \
		${GIT} pull; \
		echo "Compilando..."; \
		$(MAKE) clean; \
		$(MAKE); \
		$(MAKE) install; \
		echo "Services actualizados :)"; \
	fi

indent:
	$(INDENT) include/*.h
	$(INDENT) src/*.c
