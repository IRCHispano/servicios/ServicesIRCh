#! /bin/sh

echo "Services para IRCh"
echo ""
echo "Configuracion del Services para IRCh..."
echo ""

PREFIX="none"
BINDIR="none"
SYSCONFDIR="none"
DATADIR="none"

if [ x"$1" = x ]
then
        echo "Uso $0 {PRO|DESA|MANUAL} [prefijo ruta]"
        echo "  En PRO se instala en /usr/local/services/ServicesIRCh"
        echo "  En DESA se instala en $HOME/bin/ServicesIRCh"
        echo "  En MANUAL se instala en la ruta especificada"
        exit 1
fi

if [ "$1" = "PRO" ]; then
	PREFIX="/usr/local/services/ServicesIRCh"
	BINDIR="/usr/local/services/ServicesIRCh"
	SYSCONFDIR="/usr/local/services/ServicesIRCh"
	DATADIR="/usr/local/services/ServicesIRCh"

elif [ "$1" = "DESA" ]; then
	if [ $USER = "root" ]; then
		PREFIX="/root"
		BINDIR="/bin"
		SYSCONFDIR="/etc/services"
		DATADIR="/usr/share/services"
	else
		PREFIX="$HOME/bin/ServicesIRCh"
		BINDIR="$HOME/bin/ServicesIRCh"
		SYSCONFDIR="$HOME/bin/ServicesIRCh"
		DATADIR="$HOME/bin/ServicesIRCh"
	fi

elif [ "$1" = "MANUAL" ]; then
	if [ x"$2" = x ]
	then
		echo "Falta la ruta de instalacion";
		exit 1
	else
		PREFIX=$2
		BINDIR=$2
		SYSCONFDIR=$2
		DATADIR=$2
	fi
else
	echo "Opcion $1 desconocida";
	exit 1
fi

#echo Generando Configure y Makefile
if [ ! -f configure ];
then
	echo Generando Configure y Makefile
	sh autogen.sh
fi

echo Configurando Services
./configure --prefix=$PREFIX --bindir=$BINDIR --sysconfdir=$SYSCONFDIR --datadir=$DATADIR \
    --enable-debug \
    --enable-dumpcore \
    --enable-pcre \
    --enable-ddb
echo ""
exit 0
