#!/bin/sh
#
# Configuration script for ServicesIRCh.
#
# ServicesIRCh - Services for IRCh, configurador
#
# Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
# Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

###########################################################################

# Nifty handy functions.

echo2 () {
	$ECHO2 "$*$ECHO2SUF"	# these are defined later
}

run () {
        echo >&3 "$MODE: >>> $*"
        ($*) >&3 2>&3 </dev/null
        xxres=$?
        if [ $xxres -ne 0 ] ; then
                echo >&3 "$MODE: *** Command failed (exit code $xxres)"
        fi
        return $xxres
}

exists () {			# because some shells don't have test -e
	if [ -f $1 -o -d $1 -o -p $1 -o -c $1 -o -b $1 ] ; then
		return 0
	else
		return 1
	fi
}

###########################################################################

# Variable initialization.

BINDEST=/usr/local/sbin
SYSCONFDEST=/etc/services
DATDEST=/usr/local/lib/services
SAMEDIR=0

OPTIONS=
IRCTYPE=1
ENCRYPTION=
USE_PCRE=1
DUMPCORE=1

###########################################################################

# How can we echo something without going to the next line?

ECHO2SUF=''
if [ "`echo -n a ; echo -n b`" = "ab" ] ; then
	ECHO2='echo -n'
elif [ "`echo 'a\c' ; echo 'b\c'`" = "ab" ] ; then
	ECHO2='echo' ; ECHO2SUF='\c'
elif [ "`printf 'a' 2>&1 ; printf 'b' 2>&1`" = "ab" ] ; then
	ECHO2='printf "%s"'
else
	# oh well...
	ECHO2='echo'
fi
export ECHO2 ECHO2SUF

###########################################################################

echo ""
echo "Comezando configurador facil de los Servicios para IRCh."
echo ""

###########################################################################

# Ask the user anything we need to know ahead of time.

export ok INPUT

####

if [ "$SAMEDIR" = 1 ] ; then
        DEF=yes
else
        DEF=no
fi

ok=0
echo "Desea instalar todo en el mismo directorio?"
while [ $ok -eq 0 ] ; do
        echo2 "[$DEF] "
        if read INPUT ; then : ; else echo "" ; exit 1 ; fi
        if [ ! "$INPUT" ] ; then
                INPUT=$DEF
        fi
        case $INPUT in
                n*|N*)
                        SAMEDIR=0
                        ok=1
                        ;;
                y*|Y*|s*|S*)
                        SAMEDIR=1
                        ok=1
                        ;;
        esac
done
echo ""

####

ok=0
if [ $SAMEDIR -eq 1 ] ; then
	echo "En que directorio quieres instalar los archivos?"
else
	echo "En que directorio quieres instalar los binarios?"
fi
echo "Pulsa Return para el valor por defecto, o introduce un nuevo valor."
while [ $ok -eq 0 ] ; do
	echo2 "[$BINDEST] "
	if read INPUT ; then : ; else echo "" ; exit 1 ; fi
	if [ ! "$INPUT" ] ; then
		INPUT=$BINDEST
	fi
	if echo "$INPUT" | grep -q \[\'\"\\\] ; then
		echo 'Por favor escribe una ruta sin los caracteres: '\'' " \'
	elif [ ! "$NO_DIR_CHECK" -a ! -d "$INPUT" ] ; then
		if exists "$INPUT" ; then
			echo "$INPUT existe pero no es un directorio!"
		else
			ok=1
		fi
	elif exists "$INPUT/configure.ac" ; then
		echo "No puedes usar el directorio del codigo fuente como directorio destino."
	else
		ok=1
	fi
done
BINDEST=$INPUT
echo ""

####

if [ $SAMEDIR -eq 0 ] ; then

ok=0
echo "En que directorio quieres instalar los archivos de configuracion?"
echo "Pulsa Return para el valor por defecto, o introduce un nuevo valor."
while [ $ok -eq 0 ] ; do
        echo2 "[$SYSCONFDEST] "
        if read INPUT ; then : ; else echo "" ; exit 1 ; fi
        if [ ! "$INPUT" ] ; then
                INPUT=$BINDEST
        fi
        if echo "$INPUT" | grep -q \[\'\"\\\] ; then
                echo 'Por favor escribe una ruta sin los caracteres: '\'' " \'
        elif [ ! "$NO_DIR_CHECK" -a ! -d "$INPUT" ] ; then
                if exists "$INPUT" ; then
                        echo "$INPUT existe pero no es un directorio!"
                else
                        ok=1
                fi
        elif exists "$INPUT/configure.ac" ; then
                echo "No puedes usar el directorio del codigo fuente como directorio destino."
        else
                ok=1
        fi
done
SYSCONFDEST=$INPUT
echo ""

fi

####

if [ $SAMEDIR -eq 0 ] ; then

ok=0
echo "Donde quieres instalar los archivos de datos?"
while [ $ok -eq 0 ] ; do
	echo2 "[$DATDEST] "
	if read INPUT ; then : ; else echo "" ; exit 1 ; fi
	if [ ! "$INPUT" ] ; then
		INPUT=$DATDEST
	fi
	if echo "$INPUT" | grep -q \[\'\"\\\] ; then
		echo 'Por favor escribe una ruta sin los caracteres: '\'' " \'
	elif [ ! "$NO_DIR_CHECK" -a ! -d "$INPUT" ] ; then
		if exists "$INPUT" ; then
			echo "$INPUT existe pero no es un directorio!"
		else
			ok=1
		fi
	elif exists "$INPUT/configure.ac" ; then
		echo "No puedes usar el directorio del codigo fuente como directorio destino."
	else
		ok=1
	fi
done
DATDEST=$INPUT
echo ""

fi

####

ok=0
echo "Cual de las siguientes opciones es el tipo de red IRC en tu red de IRC?"
echo ""
echo "     1) P10 con soporte DDB"
echo "     2) P10 sin soporte DDB"

while [ $ok -eq 0 ] ; do
        echo2 "[$IRCTYPE] "
        if read INPUT ; then : ; else echo "" ; exit 1 ; fi
        if [ ! "$INPUT" ] ; then
                INPUT=$IRCTYPE
        fi
        case $INPUT in
                no\ default)
                        echo "Debes especificar el tipo de red IRC para que funcione correctamente."
                        ;;
                1)
                        OPTIONS="$OPTIONS --enable-ddb"
                        ok=1
                        ;;
                2)
                        ok=1
                        ;;
                *)
                        echo "Por favor teclea la opcion correcta."
                        ;;
        esac
done
IRCTYPE=$INPUT
echo ""

####

if [ "$ENCRYPTION" = 1 ] ; then
        DEF=yes
else
        DEF=no
fi

ok=0
echo "¿Desea utilizar el algoritmo MD5 para cifrar las contraseñas?"
echo "(Seleccionar "\"yes\"" protege sus contraseñas de ser robadas si alguien"
echo "obtiene acceso a las bases de datos de los Servicios, pero hace imposible la"
echo "recuperacion de contraseñas olvidadas.)"
echo "AVISO: Esto debe considerarse una caracteristica EXPERIMENTAL."
while [ $ok -eq 0 ] ; do
        echo2 "[$DEF] "
        if read INPUT ; then : ; else echo "" ; exit 1 ; fi
        if [ ! "$INPUT" ] ; then
                INPUT=$DEF
        fi
        case $INPUT in
                n*|N*)
                        ok=1
                        ;;
                y*|Y*|s*|S*)
                        OPTIONS="$OPTIONS --enable-encryption"
                        ok=1
                        ;;
                *)
                        echo "Por favor teclea 'si' o 'no'"
                        ;;
        esac
done
echo ""

####

if [ "$USE_PCRE" = 1 ] ; then
        DEF=yes
else
        DEF=no
fi

ok=0
echo "Desea activar el soporte PCRE para comparacion con REGEX?"
while [ $ok -eq 0 ] ; do
        echo2 "[$DEF] "
        if read INPUT ; then : ; else echo "" ; exit 1 ; fi
        if [ ! "$INPUT" ] ; then
                INPUT=$DEF
        fi
        case $INPUT in
                n*|N*)
                        ok=1
                        ;;
                y*|Y*|s*|S*)
                        OPTIONS="$OPTIONS --enable-pcre"
                        ok=1
                        ;;
                *)
                        echo 'Por favor teclea "si" o "no".'
                        ;;
        esac
done
echo ""

###

if [ "$DUMPCORE" = 1 ] ; then
	DEF=yes
else
	DEF=no
fi

ok=0
echo "Desea que los Servicios de IRC-Hispano genere un core cuando ocurre un"
echo "segmetation fault? Los archivos core son muy utiles para reportar bugs"
echo "muestra la linea de codigo que causa el crash de los Servicios."
echo "Se recomienda activar la opcion si experimentas problemas con los Servicios"
echo "y te permite depurar el problema."
while [ $ok -eq 0 ] ; do
	echo2 "[$DEF] "
	if read INPUT ; then : ; else echo "" ; exit 1 ; fi
	if [ ! "$INPUT" ] ; then
		INPUT=$DEF
	fi
	case $INPUT in
		n*|N*)
			ok=1
			;;
		y*|Y*)
			OPTIONS="$OPTIONS --enable-dumpcore"
			ok=1
			;;
		*)
			echo 'Por favor teclea "si" o "no".'
			;;
	esac
done
echo ""

####


echo "Fin del configurador interactivo."
echo ""

###########################################################################

if exists "configure" ; then
	echo "El configure existe, seguimos."
else
	echo "El configure no existe, generando..."
	./autogen.sh
fi

echo "Ejecutando configure..."
RPRE="--prefix=$BINDEST"
RBIN="--bindir=$BINDEST"

if [ $SAMEDIR -eq 1 ] ; then
	RSYS="--sysconfdir=$BINDEST"
	RDAT="--datadir=$BINDEST"
else
	RSYS="--sysconfdir=$SYSCONFDEST"
	RDAT="--datadir=$DATDEST"
fi

./configure $RPRE $RBIN $RSYS $RDAT $OPTIONS

echo "Fin del configurador facil, escriba make para continuar..."

exit 0
