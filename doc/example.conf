# Example configuration file for Services.  After making the appropriate
# changes to this file, place it in the Services data directory (as
# specified in the "configure" script, default /usr/local/lib/services)
# under the name "services.conf".
#
# The format of this file is fairly simple: a line beginning with a # is a
# comment, and any other non-blank line is expected to be a directive and
# parameters, separated by spaces or tabs.  For example:
#
#	Directive Parameter-1 Parameter-2 ...
#
# Directives are case-insensitive.  Note that some directives do not take
# any parameters; these are typically "on-off" directives, for which simply
# including the directive in this file (or removing it) has an effect on
# Services' functionality.
#
# If a parameter's value is a string which includes spaces, enclose the
# string in double quotation marks, like the example below.  Quotes may be
# used around any string at all for clarity.
#
#	"This is a parameter string with spaces in it"
#
# If you need to include a double quote inside a quoted string, precede it
# by a backslash:
#
#	"This string has \"double quotes\" in it"
#
# Time parameters can be specified either as an integer representing a
# number of seconds (e.g. "3600" = 1 hour), or as an integer with a unit
# specifier: "s" = seconds, "m" = minutes, "h" = hours, "d" = days.
# Combinations (such as "1h30m") are not permitted.  Examples (all of which
# represent the same length of time, one day):
#
#	"86400", "86400s", "1440m", "24h", "1d"
#
# In the documentation for each directive, one of the following will be
# included to indicate whether an option is required:
#
# [REQUIRED]
#     Indicates a directive which must be given.  Without it, Services will
#     not start.
#
# [RECOMMENDED]
#     Indicates a directive which may be omitted, but omitting it may cause
#     undesirable side effects.
#
# [OPTIONAL]
#     Indicates a directive which is optional.  If not given, the feature
#     will typically be disabled.  If this is not the case, more
#     information will be given in the documentation.
#
# [DISCOURAGED]
#     Indicates a directive which may cause undesirable side effects if
#     specified.
#
# [DEPRECATED]
#     Indicates a directive which will disappear in a future version of
#     Services, usually because its functionality has been either
#     superseded by that of other directives or incorporated into the main
#     program.

###########################################################################
#
# Remote server configuration
#
###########################################################################

# RemoteServer <hostname> <port> <password>  [REQUIRED]
#     Specifies the remote server hostname and port.  The hostname may
#     either be a standard Internet hostname or dotted-quad numeric
#     address; the port number must be an integer between 1 and 65535
#     inclusive.  The password is a string which should be enclosed in
#     double quotes if it contains any spaces (or just for clarity).
#
#     The remote server and port may be overridden at runtime with the
#     -remote command-line option.  The password may not be set at runtime.

RemoteServer	localhost 6667 "mypass"

# LocalAddress <hostname> [port]  [OPTIONAL]
#     Specifies the local address to bind to before connecting to the
#     remote server.  This may be useful on multihomed hosts.  The hostname
#     and port number are specified the same way as with the RemoteServer
#     directive.  If this is not specified, Services will let the operating
#     system choose the local address.  If only a hostname is specified,
#     Services will bind to that address but let the operating system
#     choose the local port number.
#
#     If you don't know what this means or don't need to use it, just leave
#     the directive commented out.
#
#     This directive may be overridden at runtime by the -local
#     command-line option.

#LocalAddress	nowhere. 0

###########################################################################
#
# Services identification and pseudoclient names
#
###########################################################################

# ServerName <name>  [REQUIRED]
#     Specifies the IRC server name which Services should use.  May be
#     overridden by the -name command-line option.

ServerName	"services.localhost.net"

# ServerDesc <text>  [REQUIRED]
#     Specifies the text which should appear as the server's information in
#     /whois and similar queries.  May be overridden by the -desc
#     command-line option.

ServerDesc	"Services for IRC Networks"

# ServiceUser <usermask>  [REQUIRED]
#     Specifies the user@host mask which should be used by the Services
#     pseudoclients.  May be overridden by the -user and -host command-line
#     options.

ServiceUser	"services@localhost.net"

# ...Name <nick> <string>  [REQUIRED except as noted below]
#     Specify the nicknames (first parameter) and "real" names (second
#     parameter) for the Services pseudoclients.  IrcIIHelp and DevNull may
#     be disabled by commenting out the appropriate lines below.

NickServName	"NickServ"	"Nickname Server"
ChanServName	"ChanServ"	"Channel Server"
MemoServName	"MemoServ"	"Memo Server"
HelpServName	"HelpServ"	"Help Server"
OperServName	"OperServ"	"Operator Server"
GlobalName	"Global"	"Global Noticer"
IrcIIHelpName	"IrcIIHelp"	"ircII Help Server"
DevNullName	"DevNull"	"/dev/null -- message sink"

###########################################################################
#
# Services data filenames
#
###########################################################################

# NOTE: All filenames are relative to the Services data directory.

# PIDFile <filename>  [REQUIRED]
#     Specifies the name of the file containing Services' process ID.

PIDFile		services.pid

# MOTDFile <filename>  [REQUIRED]
#     Specifies the name of the Message of the Day file.

MOTDFile	services.motd

# HelpDir <dirname>  [REQUIRED]
#     Specifies the name of the subdirectory containing help files for
#     HelpServ.

HelpDir		helpfiles

# ...DB <filename>  [REQUIRED]
#     Specifies the filenames for the various Services subsystems' databases.

NickServDB	nick.db
ChanServDB	chan.db
OperServDB	oper.db
AutokillDB	akill.db
NewsDB		news.db
ExceptionDB	exception.db

###########################################################################
#
# Basic functionality
#
###########################################################################

# NoBackupOkay  [DISCOURAGED]
#     Allows Services to continue file write operations (i.e. database
#     saving) even if the original file cannot be backed up.  Enabling this
#     option may allow Services to continue operation under some conditions
#     when it might otherwise fail, such as a nearly-full disk.
#
#     *** NOTE ***
#     Enabling this option can cause irrecoverable data loss under some
#     conditions, so make CERTAIN you know what you're doing when you
#     enable it!

#NoBackupOkay

# NoSplitRecovery [OPTIONAL]
#     Disables Services' recognition of users returning from netsplits.
#     Normally (on networks with some sort of timestamp support in the IRC
#     server), Services will check via the timestamp field whether a user
#     is the same as the last user who identified for the nick, and allow
#     the user access to that nick without requiring identification again
#     if the timestamps match.  Enabling this option will force all users
#     to re-identify after a netsplit.
#
#     Normally, it's easier on users to leave this disabled, but if you
#     suspect one of your servers has been hacked to send false timestamps
#     (or you suspect a bug in Services itself) enabling this directive
#     will eliminate the possibility of one user "stealing" another's nick
#     by pretending to have the same timestamp.
#
#     You may also want to uncomment this directive if your servers' clocks
#     are very far apart; the less synchronized the servers' clocks are,
#     the greater the possibility of someone "taking over" another person's
#     nick when a server with a fast clock splits (though the likelihood of
#     success is relatively small in any case).

#NoSplitRecovery

# ListOpersOnly  [DEPRECATED]
#     When enabled, limits use of the ChanServ and NickServ LIST commands
#     to IRC operators.
#
#     This directive has been superseded by the NSListOpersOnly and
#     CSListOpersOnly directives.

#ListOpersOnly

# StrictPasswords  [RECOMMENDED]
#     When enabled, causes Services to perform more stringent checks on
#     passwords.  If this is disabled, Services will only disallow a
#     password if it is the same as the entity (nickname or channel name)
#     with which it is associated.  When enabled, however, Services will
#     also check that the password is at least five characters long, and
#     in the future will probably check other things as well.

StrictPasswords

# BadPassLimit <count>  [RECOMMENDED]
#     Sets the number of invalid password tries before Services removes a
#     user from the network.  If a user enters <count> invalid passwords
#     for any Services function or combination of functions during a
#     single IRC session (subect to BadPassTimeout, below), Services will
#     issue a /KILL for the user.  If not given, Services will ignore
#     failed password attempts (though they will be logged in any case).

BadPassLimit	5

# BadPassTimeout <time>  [OPTIONAL]
#     Sets the time after which invalid passwords are forgotten about.  If
#     a user does not enter any incorrect passwords in this amount of time,
#     the incorrect password count will reset to zero.  If not given, the
#     timeout will be disabled, and the incorrect password count will never
#     be reset until the user disconnects.

BadPassTimeout	1h

# UpdateTimeout <time>  [REQUIRED]
#     Sets the delay between automatic database updates.  This timer is
#     reset by the OperServ UPDATE command.

UpdateTimeout	5m

# ExpireTimeout <time>  [REQUIRED]
#     Sets the delay between checks for expired nicknames and channels.
#     The OperServ UPDATE command will also cause a check for expiration
#     and reset this timer.

ExpireTimeout	30m

# ReadTimeout <time>  [REQUIRED]
#     Sets the timeout period for reading from the network.

ReadTimeout	5s

# WarningTimeout <time>  [REQUIRED]
#     Sets the interval between sending warning messages for program
#     errors via WALLOPS/GLOBOPS.

WarningTimeout	4h

# TimeoutCheck <time>  [REQUIRED]
#     Sets the (maximum) frequency at which the timeout list is checked.
#     This, combined with ReadTimeout above, determine how accurately timed
#     events, such as nick kills, occur; it also determines how much CPU
#     time Services will use doing this.  Higher values will cause less
#     accurate timing but less CPU usage.
#
#     This shouldn't be set any higher than 10 seconds, and 1 second is
#     best if your system is powerful enough (or your network small enough)
#     to handle it.  0 will cause the timeout list to be checked every time
#     through the main loop, which will probably slow down Services too
#     much to be useful on most networks.
#
#     Note that this value is not an absolute limit on the period between
#     checks of the timeout list; the period may be as great as ReadTimeout
#     (above) during periods of inactivity.

TimeoutCheck	3s

###########################################################################
#
# NickServ configuration
#
###########################################################################

# NSDef...  [OPTIONAL]
#     Sets the default options for newly registered nicks.  Note that
#     changing these options will have no effect on nicks which are already
#     registered.
#
#     If both NSDefKill and NSDefKillQuick are given, the latter takes
#     precedence.  KILL IMMED cannot be specified as a default.
#
#     NOTE:  If you do not enable any of these options, a default of
#     Secure, MemoSignon, and MemoReceive will be used, for backward
#     compatibility.  If you really want no options enabled by default, use
#     NSDefNone.

#NSDefNone

#NSDefKill
#NSDefKillQuick
NSDefSecure
#NSDefPrivate
#NSDefHideEmail
#NSDefHideUsermask
#NSDefHideQuit
NSDefMemoSignon
NSDefMemoReceive

# NSRegDelay <time>  [RECOMMENDED]
#     Sets the minimum length of time between consecutive uses of the
#     REGISTER command.  If not given, this restriction is disabled (note
#     that this allows "registration flooding").

NSRegDelay	30s

# NSExpire <time>  [RECOMMENDED]
#     Sets the length of time before a nick registration expires.

NSExpire	30d

# NSAccessMax <count>  [REQUIRED]
#     Sets the maximum number of entries allowed on a nickname access list.

NSAccessMax	32

# NSEnforcerUser <user>[@<host>]  [REQUIRED]
#     Sets the username (and possibly hostname) used for the fake user
#     created when NickServ collides a user.  Should be in user@host
#     format.  If the host is not given, the one from ServicesUser is
#     used.

NSEnforcerUser	enforcer
#NSEnforcerUser	enforcer@localhost.net

# NSReleaseTimeout <time>  [REQUIRED]
#     Sets the delay before a NickServ-collided nick is released.

NSReleaseTimeout 1m

# NSAllowKillImmed  [OPTIONAL]
#     When enabled, allows the use of the IMMED option with the NickServ
#     SET KILL command.

#NSAllowKillImmed

# NSDisableLinkCommand  [OPTIONAL]
#     When enabled, makes the NickServ LINK command unavailable.  Note that
#     any links that have already been created will continue to function;
#     this only prevents new links from being made.

#NSDisableLinkCommand

# NSListOpersOnly  [OPTIONAL]
#     When enabled, limits use of the NickServ LIST command to IRC
#     operators.

#NSListOpersOnly

# NSListMax <count>  [REQUIRED]
#     Specifies the maximum number of nicks to be returned for a NickServ
#     LIST command.

NSListMax	50

# NSForceNickChange  [OPTIONAL]
#     When enabled, makes NickServ change a user's nick to a "Guest######" 
#     nick instead of killing them when enforcing a "nick kill". This is 
#     accomplished by way of the SVSNICK command. This option is only
#     available if Services have been compiled to work with a DALnet server,
#     version 4.4.15 or above.

#NSForceNickChange

# NSGuestNickPrefix <value>  [REQUIRED]
#     When a user's nick is forcibly changed to enforce a "nick kill", their
#     new nick will start with this value. The rest will be made up of 6 or 7
#     digits. This only applies when NSForceNickChange (see above) is enabled.

NSGuestNickPrefix	"Guest"

# NSSecureAdmins  [RECOMMENDED]
#     When enabled, prevents the use of the DROP, GETPASS, and SET PASSWORD
#     commands by Services admins on other Services admins or the Services
#     root.

NSSecureAdmins

###########################################################################
#
# ChanServ configuration
#
###########################################################################

# CSMaxReg <count>  [RECOMMENDED]
#     Limits the number of channels which may be registered to a single
#     nickname.

CSMaxReg	20

# CSExpire <time>  [RECOMMENDED]
#     Sets the number of days before a channel expires.

CSExpire	14d

# CSAccessMax <count>  [REQUIRED]
#     Sets the maximum number of entries on a channel's access list.
#     Channel access lists may contain only registered nicknames;
#     therefore, checking each entry on the list requires only a single
#     scaler comparison instead of a wildcard match, and this limit may be
#     safely set much higher than (for exmple) the nickname access list
#     size limit without impacting performance significantly.

CSAccessMax	1024

# CSAutokickMax <count>  [REQUIRED]
#     Sets the maximum number of entries on a channel's autokick list.

CSAutokickMax	32

# CSAutokickReason <text>  [REQUIRED]
#     Sets the default reason for an autokick if none is given.

CSAutokickReason "User has been banned from the channel"

# CSInhabit <time>  [REQUIRED]
#     Sets the length of time ChanServ stays in a channel after kicking a
#     user from a channel s/he is not permitted to be in.  This only occurs
#     when the user is the only one in the channel.

CSInhabit	15s

# CSRestrictDelay <time>  [DISCOURAGED]
#     When enabled, causes ChanServ to ignore any RESTRICTED or NOJOIN
#     channel setting for the given time after Services starts up.  This
#     gives users a time to identify to NickServ before being kicked out of
#     restricted channels they would normally be allowed to join.  This
#     setting will also cause channel mode +o's from servers to be passed
#     through for this initial period.
#
#     This option is presently discouraged because it is not properly
#     implemented; any users in channels when Services starts up get a
#     "free ride", though they can of course be deopped/kicked manually.

#CSRestrictDelay	30s

# CSListOpersOnly  [OPTIONAL]
#     When enabled, limits use of the ChanServ LIST command to IRC
#     operators.

#CSListOpersOnly

# CSListMax <count>  [REQUIRED]
#     Specifies the maximum number of channels to be returned for a
#     ChanServ LIST command.

CSListMax	50

###########################################################################
#
# MemoServ configuration
#
###########################################################################

# MSMaxMemos <count>  [RECOMMENDED]
#     Sets the maximum number of memos a user is allowed to keep by
#     default.  Normal users may set the limit anywhere between zero and
#     this value; Services admins can change it to any value or disable it.
#     If not given, the limit is disabled by default, and normal users can
#     set any limit they want.

MSMaxMemos	20

# MSSendDelay <time>  [RECOMMENDED]
#     Sets the delay between consecutive uses of the MemoServ SEND command.
#     This can help prevent spam as well as denial-of-service attacks from
#     sending large numbers of memos and filling up disk space (and
#     memory).  A 3-second wait means a maximum average of 150 bytes of
#     memo per second per user under the current IRC protocol.

MSSendDelay	3s

# MSNotifyAll  [OPTIONAL]
#     Should we notify all appropriate users of a new memo?  This applies
#     in cases where a memo is sent to a nick which either is linked to
#     another nick or has another nick linked to it.  Enabling this option
#     will cause MemoServ to check all users who are currently online to
#     see whether any have nicks which are linked to the target of the
#     memo, and if so, notify all of them.  This can take a good deal of
#     CPU time on larger networks, so you may want to disable it.

MSNotifyAll

###########################################################################
#
# OperServ configuration
#
###########################################################################

# ServicesRoot <nick>  [REQUIRED]
#    Specifies the Services "super-user".  The super-user, or "root" as in
#    Unix terminology, is the only user who can add or delete Services
#    admins.
#
#    This is commented out by default; make sure you insert the correct
#    nick before uncommenting it.

#ServicesRoot	Alcan

# LogMaxUsers  [OPTIONAL]
#    Causes Services to write a message to the log every time a new user
#    maximum is set.

LogMaxUsers

# AutokillExpiry <time>  [REQUIRED]
#     Sets the default expiry time for autokills.

AutoKillExpiry	30d

# KillClonesAkillExpire <time>  [REQUIRED]
#     Sets the expiry time for autokills added for hosts that have been
#     killed using the KILLCLONES command.

KillClonesAkillExpire	30m

# WallOper  [OPTIONAL]
#     Causes Services to send a WALLOPS/GLOBOPS when a user becomes an IRC
#     operator.  Note that this can cause WALLOPS floods when Services
#     first connects to the network.

#WallOper

# WallBadOS  [OPTIONAL]
#     Causes Services to send a WALLOPS/GLOBOPS if a non-IRC-operator tries
#     to use OperServ.

#WallBadOS

# WallOS...  [OPTIONAL]
#     Cause Services to send a WALLOPS/GLOBOPS on use of each of the
#     OperServ commands listed.

#WallOSMode
#WallOSClearmodes
#WallOSKick
#WallOSAkill

# WallAkillExpire  [OPTIONAL]
#     Causes Services to send a WALLOPS/GLOBOPS whenever an autokill
#     expires.

#WallAkillExpire

# WallExceptionExpire  [OPTIONAL]
#     Causes Services to send a WALLOPS/GLOBOPS whenever an session limit
#     exception expires.

#WallExceptionExpire

# WallGetpass  [OPTIONAL]
#     Causes Services to send a WALLOPS/GLOBOPS on use of the NickServ or
#     ChanServ GETPASS command.

WallGetpass

# WallSetpass  [OPTIONAL]
#     Causes Services to send a WALLOPS/GLOBOPS whenever a Services admin
#     sets a password for a nickname or channel s/he does not normally have
#     privileges to set.

WallSetpass


# LimitSessions  [OPTIONAL]
#     Enables session limiting. Session limiting prevents users from 
#     connecting more than a certain number of times from the same host at the
#     same time - thus preventing most types of cloning. Once a host reaches 
#     it's session limit, all clients attempting to connect from that host 
#     will be killed. Exceptions to the default session limit, which are based 
#     on host names, can be defined via the exception list. It should be noted
#     that session limiting, along with a large exception list, can degrade
#     services' performance. See the source and comments in sessions.c and the
#     online help for more information about session limiting.
#
#     Session limiting is meant to replace the CheckClones and KillClones
#     code. It is therefore highly recommened that they are disabled when
#     session limiting has been enabled.
#
#     NOTE:  This option is not available when STREAMLINED is defined in
#     the Makefile.

#LimitSessions

# DefSessionLimit <limit>  [REQUIRED]
#     Default session limit per host. Once a host reaches it's session limit,
#     all clients attempting to connect from that host will be killed. A value
#     of zero means an unlimited session limit.

DefSessionLimit	3

# MaxSessionLimit <limit>  [REQUIRED]
#     The maximum session limit that may be set for a host in an exception.

MaxSessionLimit 100

# ExceptionExpiry <time>  [REQUIRED]
#     Sets the default expiry time for exceptions.

ExceptionExpiry	1d

# SessionLimitExceeded <message>  [OPTIONAL]
#     The message that will be NOTICE'd to a user just before they are removed
#     from the network because their's host session-limit has been exceeded. 
#     It may be used to give a slightly more descriptive reason for the
#     impending kill as apposed to simply "Session limit exceeded". If this is
#     commented out, nothing will be sent.

SessionLimitExceeded "The session limit for your host %s has been exceeded."

# SessionLimitDetailsLoc <message>  [OPTIONAL]
#     Same as above, but should be used to provide a website address where
#     users can find out more about session limits and how to go about 
#     applying for an exception. If this is commented out, nothing will be
#     sent.
#
#     This option has been intentionally commented out in an effort to remind
#     you to change the URL it contains. It is recommended that you supply an
#     address/url where people can get help regarding session limits.

#SessionLimitDetailsLoc "Please visit http://your.website.url/ for more information about session limits."


# CheckClones <minusers> <maxdelay> <warningdelay>  [DEPRICATED]
#     Causes Services to try and detect "clones" connecting to the network.
#     A WALLOPS (or GOPER, if supported on the IRC server) will be sent if
#     Services thinks it has found clones.
#
#     This feature has been superceded by Session Limiting.
#
#     <minusers> sets the minimum number of users which must successively
#     connect to the network before Services will send a clone warning.
#
#     <maxdelay> sets the maximum time that can elapse between successive
#     users before Services decides they are not clones.
#
#     <warningdelay> sets the minimum time between clone warnings for
#     clones from the same host.
#
#     NOTE:  This option is not available when STREAMLINED is defined in
#     the Makefile.

CheckClones	5 10s 30s

# KillClones [DISCOURAGED]	[DEPRICATED]
#     Causes Services to kill users which trigger the clone warnings.  (If
#     CheckClones is disabled, this will have no effect.)
#
#     This feature has been superceded by Session Limiting.
#
#     BEWARE!  The clone checking code is easily fooled; it can be
#     triggered falsely under many conditions, for example:
#
#         - Multiple users connecting from a shell machine.
#
#         - A single user repeatedly connecting and disconnecting.
#
#     Be very sure you know what you're doing before you even think about
#     enabling this option, and remember that Services comes with no
#     warranty.
#
#     If that wasn't enough discouragement:
#
#     ***** DO NOT ENABLE THIS OPTION! *****
#
#     NOTE:  This option is not available when STREAMLINED is defined in
#     the Makefile.

#KillClones
