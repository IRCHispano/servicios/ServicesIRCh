/*
 * ServicesIRCh - Services for IRCh, akill.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(AKILL_H)
#define AKILL_H

#if !defined(CONFIG_H)
#include "config.h"
#endif

#include <time.h>

struct NetClient;

struct Akill {
    char *mask;
    char *reason;
    char who[NICKMAX];
    time_t time;
    time_t expires;     /* or 0 for no expiry */
};

extern int num_akills(void);
extern void load_akill(void);
extern void save_akill(void);
extern void get_akill_stats(long *nrec, long *memuse);
extern int check_akill(const char *yxx, const char *nick, const char *username, const char *host, struct irc_in_addr *ip);
extern void expire_akills(void);

extern void do_akill(struct NetClient *nc);
extern void add_akill(const char *mask, const char *reason, const char *who,
                        const time_t expiry);

#endif /* AKILL_H */
