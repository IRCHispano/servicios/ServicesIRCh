/*
 * ServicesIRCh - Services for IRCh, channels.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(CHANNELS_H)
#define CHANNELS_H

#if !defined(CONFIG_H)
#include "config.h"
#endif
#if !defined(NETWORK_H)
#include "network.h"
#endif

#include <time.h>
#include <sys/types.h>

struct NetClient;

#define TOPICLEN 240

/* Online Channel data. */
struct Channel {
    struct Channel *next;
    struct Channel *prev;
    struct Channel *hnext;

    char name[CHANMAX+1];
    struct ChannelInfo *ci;     /* Corresponding ChannelInfo */
    time_t creation_time;       /* When channel was created */

    char *topic;
    char topic_setter[NICKMAX]; /* Who set the topic */
    time_t topic_time;          /* When topic was set */

    int32_t mode;               /* Binary modes only */
    int32_t limit;              /* 0 if none */
    char *key;              /* NULL if none */

    struct Membership *members;
    struct Ban *banlist;

    int32_t bancount;
    int32_t exceptcount;
    int16_t chan_usercount;

    time_t server_modetime;             /* Time of last server MODE */
    time_t chanserv_modetime;           /* Time of last check_modes() */
    int16_t server_modecount;           /* Number of server MODEs this second */
    int16_t chanserv_modecount;         /* Number of check_mode()'s this sec */
    int16_t bouncy_modes;                       /* Did we fail to set modes here? */
};

struct Membership {
    struct NetClient *user;
    struct Channel *channel;
    struct Membership *next_member;
    struct Membership *prev_member;
    struct Membership *next_channel;
    struct Membership *prev_channel;
    int status;
};

#define BAN_IPMASK    0x01
#define BAN_EXCEPTION 0x02

struct Ban {
    struct Ban *next;           /* next ban in thextern Channel */
    struct irc_in_addr address; /* address for BAN_IPMASK bans */
    time_t when;                /* timestamp when ban was added */
    unsigned short flags;       /* modifier flags for the ban */
    unsigned char nu_len;       /* length of nick!user part of banstr */
    unsigned char addrbits;     /* netmask length for BAN_IPMASK bans */
    char who[NICKMAX];          /* name of client that set the ban */
    char banstr[NICKMAX+12+64+3];  /* hostmask that the ban matches 12 username y 64 hostname */
};

/* Channel modes */
/* NOTA: No se puede editar, depende del mlock */
#define CMODE_i   0x00000001
#define CMODE_m   0x00000002
#define CMODE_n   0x00000004
#define CMODE_p   0x00000008
#define CMODE_s   0x00000010
#define CMODE_t   0x00000020
#define CMODE_k   0x00000040
#define CMODE_l   0x00000080
#define CMODE_r   0x00000100
#define CMODE_R   0x00000200
#define CMODE_A   0x00000400
#define CMODE_S   0x00000800
#define CMODE_M   0x00001000
#define CMODE_c   0x00002000
#define CMODE_C   0x00004000
#define CMODE_u   0x00008000
#define CMODE_N   0x00010000
#define CMODE_D   0x00020000
#define CMODE_z   0x00040000
#define CMODE_W   0x00080000
#define CMODE_O   0x00100000

/* Numero de modos */
#define CMODE_COUNT 21

#define CUMODE_OP    0x01
#define CUMODE_VOICE 0x02
#define CUMODE_OWNER 0x04

#define ShowChannel(v,c)        (PubChannel(c) || find_channel_member((v),(c)))
#define PubChannel(x)           ((!x) || ((x)->mode & \
                                    (CMODE_p | CMODE_s)) == 0)


extern void get_channel_stats(long *nrec, long *memuse);

extern struct Channel *firstchan(void);
extern struct Channel *nextchan(void);
extern struct Channel *get_channel(struct NetClient *nc, char *chname, int create);
extern void destruct_channel(struct Channel *c);

extern struct Membership *find_channel_member(struct NetClient *nc, struct Channel *c);
extern void add_user_to_channel(struct Channel *c, struct NetClient *nc, int flags);
extern void remove_user_from_channel(struct NetClient *nc, struct Channel *c);
extern void remove_user_from_all_channels(struct NetClient *nc);

extern int channel_mode_to_int(char mode);
extern char *channel_modes_to_char(int flags);
extern int is_chan_owner(struct NetClient *nc, struct Channel *c);
extern int is_chan_op(struct NetClient *nc, struct Channel *c);
extern int is_chan_voice(struct NetClient *nc, struct Channel *c);

extern void add_ban(struct Channel *c, struct NetClient *who, const char *banstr, int check);
extern void delete_ban(struct Channel *c, struct Ban *ban, struct Ban *prev);

extern void m_bmode(struct NetClient *nc, int ac, char **av);
extern void m_burst(struct NetClient *nc, int ac, char **av);
extern void m_clearmode(struct NetClient *nc, int ac, char **av);
extern void m_create(struct NetClient *nc, int ac, char **av);
extern void m_invite(struct NetClient *nc, int ac, char **av);
extern void m_join(struct NetClient *nc, int ac, char **av);
extern void m_kick(struct NetClient *nc, int ac, char **av);
extern void m_mode(struct NetClient *nc, int ac, char **av);
extern void m_opmode(struct NetClient *nc, int ac, char **av);
extern void m_part(struct NetClient *nc, int ac, char **av);
extern void m_svsjoin(struct NetClient *nc, int ac, char **av);
extern void m_svsmode(struct NetClient *nc, int ac, char **av);
extern void m_svspart(struct NetClient *nc, int ac, char **av);
extern void m_topic(struct NetClient *nc, int ac, char **av);

#endif /* CHANNELS_H */
