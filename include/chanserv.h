/*
 * ServicesIRCh - Services for IRCh, chanserv.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(CHANSERV_H)
#define CHANSERV_H

#if !defined(CONFIG_H)
#include "config.h"
#endif

#if !defined(MEMOSERV_H)
#include "memoserv.h"
#endif

#include <time.h>
#include <sys/types.h>

struct NickInfo;
struct User;

/* Channel info structures.  Stored similarly to the nicks, except that
 * the second character of the channel name, not the first, is used to
 * determine the list.  (Hashing based on the first character of the name
 * wouldn't get very far. ;) ) */

/* Access levels for users. */
struct ChanAccess {
    int16_t in_use;     /* 1 if this entry is in use, else 0 */
    int16_t level;
    struct NickInfo *ni; /* Guaranteed to be non-NULL if in use, NULL if not */
};

/* Note that these two levels also serve as exclusive boundaries for valid
 * access levels.  ACCESS_FOUNDER may be assumed to be strictly greater
 * than any valid access level, and ACCESS_INVALID may be assumed to be
 * strictly less than any valid access level.
 */
#define ACCESS_FOUNDER  10000   /* Numeric level indicating founder access */
#define ACCESS_INVALID  -10000  /* Used in levels[] for disabled settings */

/* AutoKick data. */
struct AutoKick {
    int16_t in_use;
    int16_t is_nick;    /* 1 if a regged nickname, 0 if a nick!user@host mask */
                        /* Always 0 if not in use */
    union {
        char *mask;     /* Guaranteed to be non-NULL if in use, NULL if not */
        struct NickInfo *ni;    /* Same */
    } u;
    char *reason;
};

struct ChannelInfo {
    struct ChannelInfo *next, *prev;
    char name[CHANMAX];
    struct NickInfo *founder;
    struct NickInfo *successor;         /* Who gets the channel if the founder
                                         * nick is dropped or expires */
    char founderpass[PASSMAX];
    char *desc;
   char *url;
    char *email;

    time_t time_registered;
    time_t last_used;
    char *last_topic;                   /* Last topic on the channel */
    char last_topic_setter[NICKMAX];    /* Who set the last topic */
    time_t last_topic_time;             /* When the last topic was set */

    int32_t flags;                      /* See below */

    int16_t *levels;                    /* Access levels for commands */

    int16_t accesscount;
    struct ChanAccess *access;                  /* List of authorized users */
    int16_t akickcount;
    struct AutoKick *akick;                     /* List of users to kickban */

    int16_t mlock_on, mlock_off;                /* See channel modes below */
    int32_t mlock_limit;                        /* 0 if no limit */
    char *mlock_key;                    /* NULL if no key */

    char *entry_message;                /* Notice sent on entering channel */

    struct MemoInfo memos;

    struct Channel *c;                  /* Pointer to channel record (if   *
                                         *    channel is currently in use) */
};

/*************************************************************************/

/* Channel status flags: */

/* Retain topic even after last person leaves channel */
#define CI_KEEPTOPIC    0x00000001
/* Don't allow non-authorized users to be opped */
#define CI_SECUREOPS    0x00000002
/* Hide channel from ChanServ LIST command */
#define CI_PRIVATE      0x00000004
/* Topic can only be changed by SET TOPIC */
#define CI_TOPICLOCK    0x00000008
/* Those not allowed ops are kickbanned */
#define CI_RESTRICTED   0x00000010
/* Don't auto-deop anyone */
#define CI_LEAVEOPS     0x00000020
/* Don't allow any privileges unless a user is IDENTIFY'd with NickServ */
#define CI_SECURE       0x00000040
/* Don't allow the channel to be registered or used */
#define CI_VERBOTEN     0x00000080
/* Channel password is encrypted */
#define CI_ENCRYPTEDPW  0x00000100
/* Channel does not expire */
#define CI_NO_EXPIRE    0x00000200
/* Channel memo limit may not be changed */
#define CI_MEMO_HARDMAX 0x00000400
/* Send notice to channel on use of OP/DEOP */
#define CI_OPNOTICE     0x00000800


/* Indices for cmd_access[]: */
#define CA_INVITE       0
#define CA_AKICK        1
#define CA_SET          2       /* but not FOUNDER or PASSWORD */
#define CA_UNBAN        3
#define CA_AUTOOP       4
#define CA_AUTODEOP     5       /* Maximum, not minimum */
#define CA_AUTOVOICE    6
#define CA_OPDEOP       7       /* ChanServ commands OP and DEOP */
#define CA_ACCESS_LIST  8
#define CA_CLEAR        9
#define CA_NOJOIN       10      /* Maximum */
#define CA_ACCESS_CHANGE 11
#define CA_MEMO         12

#define CA_SIZE         13

/*************************************************************************/

/* Externs */

extern struct NetClient *ncChanServ;
extern void cs_init(void);

extern void get_chanserv_stats(long *nrec, long *memuse);
extern char *get_mlock(int mlock_on, int mlock_off, char *mlock_key, int mlock_limit, int showkeylimit);

extern void chanserv(struct NetClient *source, char *buf);
extern void load_cs_dbase(void);
extern void save_cs_dbase(void);
extern void check_modes(struct Channel *c);
extern int check_valid_owner(struct NetClient *nc, struct Channel *c, int newchan);
extern int check_valid_op(struct NetClient *nc, struct Channel *c, int newchan);
extern int check_valid_voice(struct NetClient *nc, struct Channel *c, int newchan);
extern int check_should_owner(struct NetClient *nc, struct Channel *c);
extern int check_should_op(struct NetClient *nc, struct Channel *c);
extern int check_should_voice(struct NetClient *nc, struct Channel *c);
extern int check_kick(struct NetClient *nc, struct Channel *c);
extern void record_topic(struct Channel *c);
extern void restore_topic(struct Channel *c);
extern int check_topiclock(struct Channel *c);
extern void expire_chans(void);
extern void cs_remove_nick(const struct NickInfo *ni);

extern struct ChannelInfo *cs_findchan(const char *chan);
extern int check_access(struct NetClient *nc, struct ChannelInfo *ci, int what);

#endif /* CHANSERV_H */
