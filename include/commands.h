/*
 * ServicesIRCh - Services for IRCh, commands.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(COMMANDS_H)
#define COMMANDS_H

struct NetClient;


/*************************************************************************/

/* Structure for information about a *Serv command. */

struct Command {
    const char *name;
    void (*routine)(struct NetClient *nc);
    int (*has_priv)(struct NetClient *nc);	/* Returns 1 if user may use command, else 0 */

    /* Regrettably, these are hard-coded to correspond to current privilege
     * levels (v4.0).  Suggestions for better ways to do this are
     * appreciated.
     */
    int helpmsg_all;	/* Displayed to all users; -1 = no message */
    int helpmsg_reg;	/* Displayed to regular users only */
    int helpmsg_oper;	/* Displayed to Services operators only */
    int helpmsg_admin;	/* Displayed to Services admins only */
    int helpmsg_root;	/* Displayed to Services root only */
    const char *help_param1;
    const char *help_param2;
    const char *help_param3;
    const char *help_param4;
};

/*************************************************************************/

/* Routines for looking up commands.  Command lists are arrays that must be
 * terminated with a NULL name.
 */

extern struct Command *lookup_cmd(struct Command *list, const char *name);
extern void run_cmd(struct NetClient *service, struct NetClient *nc, struct Command *list,
		const char *name);
extern void help_cmd(struct NetClient *service, struct NetClient *nc, struct Command *list,
		const char *name);

/*************************************************************************/

#endif /* COMMANDS_H */
