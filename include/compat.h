/*
 * ServicesIRCh - Services for IRCh, compat.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(COMPAT_H)
#define COMPAT_H

#include "sysconf.h"
#include <stdio.h>
#include <stdarg.h>

#if !HAVE_SNPRINTF
# if BAD_SNPRINTF
#  define snprintf my_snprintf
#  define vsnprintf my_vsnprintf
# endif
//# define vsnprintf my_vsnprintf
extern int vsnprintf(char *buf, size_t size, const char *fmt, va_list args);
extern int snprintf(char *buf, size_t size, const char *fmt, ...);
#endif

#if !HAVE_STRTOK
# undef strtok
extern char *strtok(char *str, const char *delim);
#endif

#if !HAVE_STRICMP && !HAVE_STRCASECMP
# undef stricmp
# undef strnicmp
extern int stricmp(const char *s1, const char *s2);
extern int strnicmp(const char *s1, const char *s2, size_t len);
#endif

#if !HAVE_STRDUP
# undef strdup
extern char *strdup(const char *s);
#endif

#if !HAVE_STRSPN
# undef strspn
# undef strcspn
extern size_t strspn(const char *s, const char *accept);
extern size_t strcspn(const char *s, const char *reject);
#endif

#if !HAVE_STRERROR
# undef strerror
extern char *strerror(int errnum);
#endif

#if !HAVE_STRSIGNAL
# undef strsignal
extern char *strsignal(int signum);
#endif

#ifndef NAME_MAX
# define NAME_MAX 255
#endif

#ifndef BUFSIZ
# define BUFSIZ 256
#else
# if BUFSIZ < 256
#  define BUFSIZ 256
# endif
#endif


/* Length of an array: */
#define lenof(a)        (sizeof(a) / sizeof(*(a)))

/* Telling compilers about printf()-like functions: */
#ifdef __GNUC__
# define FORMAT(type,fmt,start) __attribute__((format(type,fmt,start)))
#else
# define FORMAT(type,fmt,start)
#endif

#ifdef _AIX
/* Does anyone know if/where these are declared?  On my AIX box, they're
 * not anywhere I can find. */
extern int strcasecmp(const char *, const char *);
extern int strncasecmp(const char *, const char *, size_t);
# if 0  /* These break on some AIX boxes (4.3.1 reported). */
extern int gettimeofday(struct timeval *, struct timezone *);
extern int socket(int, int, int);
extern int bind(int, struct sockaddr *, int);
extern int connect(int, struct sockaddr *, int);
extern int shutdown(int, int);
# endif
# undef FD_ZERO
# define FD_ZERO(p) memset((p), 0, sizeof(*(p)))
#endif /* _AIX */

#endif  /* COMPAT_H */

