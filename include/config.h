/*
 * ServicesIRCh - Services for IRCh, config.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2011-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(CONFIG_H)
#define CONFIG_H

/* Note that most of the options which used to be here have been moved to
 * services.conf. */

/*************************************************************************/

/******* General configuration *******/

/* Name of configuration file (in Services directory) */
#define SERVICES_CONF	"services.conf"

/* Name of log file (in Services directory) */
#define LOG_FILENAME	"services.log"

/* Maximum amount of data from/to the network to buffer (bytes). */
#define NET_BUFSIZE	65536*16


/******* NickServ configuration *******/

/* Default language for newly registered nicks (and nicks imported from
 * old databases); see services.h for available languages (search for
 * "LANG_").  Unless you're running a regional network, you should probably
 * leave this at LANG_EN_US. */
#define DEF_LANGUAGE	LANG_ES


/******* OperServ configuration *******/

/* What is the maximum number of Services admins we will allow? */
#define MAX_SERVADMINS	32

/* What is the maximum number of Services operators we will allow? */
#define MAX_SERVOPERS	64

/* How big a hostname list do we keep for clone detection?  On large nets
 * (over 500 simultaneous users or so), you may want to increase this if
 * you want a good chance of catching clones. */
#define CLONE_DETECT_SIZE 16

/* Define this to enable OperServ's debugging commands (Services root
 * only).  These commands are undocumented; "use the source, Luke!" */
/* #define DEBUG_COMMANDS */


/******************* END OF USER-CONFIGURABLE SECTION ********************/


/* Size of input buffer (note: this is different from BUFSIZ)
 * This must be big enough to hold at least one full IRC message, or messy
 * things will happen. */
#define BUFSIZE		1024


/* Extra warning:  If you change these, your data files will be unusable! */

/* Maximum length of a channel name, including the trailing null.  Any
 * channels with a length longer than (CHANMAX-1) including the leading #
 * will not be usable with ChanServ. */
#define CHANMAX		64

/* Maximum length of a nickname, including the trailing null.  This MUST be
 * at least one greater than the maximum allowable nickname length on your
 * network, or people will run into problems using Services!  The default
 * (32) works with all servers I know of. */
#define NICKMAX		32

/* Maximum length of a password */
#define PASSMAX		32

extern char *RemoteServer;
extern int   RemotePort;
extern char *RemotePassword;
extern char *LocalHost;
extern int   LocalPort;

extern char *ServerName;
extern char *ServerDesc;
extern char *ServiceUser;
extern char *ServiceHost;
extern struct irc_in_addr ServiceIP;
extern int   ServerNumeric;

extern char *s_NickServ;
extern char *s_ChanServ;
extern char *s_MemoServ;
extern char *s_HelpServ;
extern char *s_OperServ;
extern char *s_GlobalNoticer;
extern char *s_IrcIIHelp;
extern char *s_DevNull;
extern char *desc_NickServ;
extern char *desc_ChanServ;
extern char *desc_MemoServ;
extern char *desc_HelpServ;
extern char *desc_OperServ;
extern char *desc_GlobalNoticer;
extern char *desc_IrcIIHelp;
extern char *desc_DevNull;

extern char *PIDFilename;
extern char *MOTDFilename;
extern char *HelpDir;
extern char *NickDBName;
extern char *ChanDBName;
extern char *OperDBName;
extern char *AutokillDBName;
extern char *NewsDBName;

extern int   NoBackupOkay;
extern int   NoSplitRecovery;
extern int   StrictPasswords;
extern int   BadPassLimit;
extern int   BadPassTimeout;
extern int   UpdateTimeout;
extern int   ExpireTimeout;
extern int   ReadTimeout;
extern int   WarningTimeout;
extern int   TimeoutCheck;

extern int   NSForceNickChange;
extern char *NSGuestNickPrefix;
extern int   NSDefKill;
extern int   NSDefKillQuick;
extern int   NSDefSecure;
extern int   NSDefPrivate;
extern int   NSDefHideEmail;
extern int   NSDefHideUsermask;
extern int   NSDefHideQuit;
extern int   NSDefMemoSignon;
extern int   NSDefMemoReceive;
extern int   NSRegDelay;
extern int   NSExpire;
extern int   NSAccessMax;
extern char *NSEnforcerUser;
extern char *NSEnforcerHost;
extern int   NSReleaseTimeout;
extern int   NSAllowKillImmed;
extern int   NSDisableLinkCommand;
extern int   NSListOpersOnly;
extern int   NSListMax;
extern int   NSSecureAdmins;

extern int   CSMaxReg;
extern int   CSExpire;
extern int   CSAccessMax;
extern int   CSAutokickMax;
extern char *CSAutokickReason;
extern int   CSInhabit;
extern int   CSRestrictDelay;
extern int   CSListOpersOnly;
extern int   CSListMax;

extern int   MSMaxMemos;
extern int   MSSendDelay;
extern int   MSNotifyAll;

extern char *ServicesRoot;
extern int   LogMaxUsers;
extern int   AutokillExpiry;
extern int   WallNetwork;
extern int   WallOper;
extern int   WallBadOS;
extern int   WallOSMode;
extern int   WallOSClearmodes;
extern int   WallOSKick;
extern int   WallOSAkill;
extern int   WallAkillExpire;
extern int   WallExceptionExpire;
extern int   WallGetpass;
extern int   WallSetpass;
extern int   CheckClones;
extern int   CloneMinUsers;
extern int   CloneMaxDelay;
extern int   CloneWarningDelay;
extern int   KillClones;

extern int   KillClonesAkillExpire;

extern int   LimitSessions;
extern int   DefSessionLimit;
extern int   ExceptionExpiry;
extern int   MaxSessionLimit;
extern char *ExceptionDBName;
extern char *SessionLimitDetailsLoc;
extern char *SessionLimitExceeded;

extern int read_config(void);

/**************************************************************************/

#endif	/* CONFIG_H */
