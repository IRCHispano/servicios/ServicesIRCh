/*
 * ServicesIRCh - Services for IRCh, datafiles.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(DATAFILES_H)
#define DATAFILES_H

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

/*************************************************************************/

/* Version number for data files; if structures below change, increment
 * this.  (Otherwise -very- bad things will happen!) */

#define FILE_VERSION    7

/*************************************************************************/

struct dbFILE {
    int mode;			/* 'r' for reading, 'w' for writing */
    FILE *fp;			/* The normal file descriptor */
    FILE *backupfp;		/* Open file pointer to a backup copy of
				 *    the database file (if non-NULL) */
    char filename[PATH_MAX];	/* Name of the database file */
    char backupname[PATH_MAX];	/* Name of the backup file */
};

/*************************************************************************/

/* Prototypes and macros: */

extern void check_file_version(struct dbFILE *f);
extern int get_file_version(struct dbFILE *f);
extern int write_file_version(struct dbFILE *f);
extern struct dbFILE *open_db(const char *service, const char *filename, const char *mode);
extern void restore_db(struct dbFILE *f);	/* Restore to state before open_db() */
extern void close_db(struct dbFILE *f);
#define read_db(f,buf,len)	(fread((buf),1,(len),(f)->fp))
#define write_db(f,buf,len)	(fwrite((buf),1,(len),(f)->fp))
#define getc_db(f)		(fgetc((f)->fp))

extern int read_int16(u_int16_t *ret, struct dbFILE *f);
extern int write_int16(u_int16_t val, struct dbFILE *f);
extern int read_int32(u_int32_t *ret, struct dbFILE *f);
extern int write_int32(u_int32_t val, struct dbFILE *f);
extern int read_int64(int64_t *ret, struct dbFILE *f);
extern int write_int64(int64_t val, struct dbFILE *f);
extern int read_uint16(u_int16_t *ret, struct dbFILE *f);
extern int write_uint16(u_int16_t val, struct dbFILE *f);
extern int read_uint32(u_int32_t *ret, struct dbFILE *f);
extern int write_uint32(u_int32_t val, struct dbFILE *f);
extern int read_uint64(u_int64_t *ret, struct dbFILE *f);
extern int write_uint64(u_int64_t val, struct dbFILE *f);
extern int read_ptr(void **ret, struct dbFILE *f);
extern int write_ptr(const void *ptr, struct dbFILE *f);
extern int read_string(char **ret, struct dbFILE *f);
extern int write_string(const char *s, struct dbFILE *f);

#define read_int8(ret,f)	((*(ret)=fgetc((f)->fp))==EOF ? -1 : 0)
#define write_int8(val,f)	(fputc((val),(f)->fp)==EOF ? -1 : 0)
#define read_buffer(buf,f)	(read_db((f),(buf),sizeof(buf)) == sizeof(buf))
#define write_buffer(buf,f)	(write_db((f),(buf),sizeof(buf)) == sizeof(buf))
#define read_buflen(buf,len,f)	(read_db((f),(buf),(len)) == (len))
#define write_buflen(buf,len,f)	(write_db((f),(buf),(len)) == (len))
#define read_variable(var,f)	(read_db((f),&(var),sizeof(var)) == sizeof(var))
#define write_variable(var,f)	(write_db((f),&(var),sizeof(var)) == sizeof(var))

/*************************************************************************/

#endif	/* DATAFILES_H */
