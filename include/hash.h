/*
 * ServicesIRCh - Services for IRCh, hash.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(HASH_H)
#define HASH_H

struct NetClient;
struct Channel;

/* Codigo procedente del IRCU */

#define HASHSIZE                32768


/* Macros */
/* Raw calls, expect a core if you pass a NULL or zero-length name */
#define SeekNetClient(name)        hSeekNetClient((name), ~0)
#define SeekUser(name)          hSeekNetClient((name), (NETCLIENT_USER | NETCLIENT_SERVICE))
#define SeekService(name)       hSeekNetClient((name), (NETCLIENT_SERVICE))
#define SeekServer(name)        hSeekNetClient((name), (NETCLIENT_ME | NETCLIENT_SERVER))
#define SeekChannel(name)       hSeekChannel((name))

/* Safer macros with sanity check on name, WARNING: these are _macros_,
   no side effects allowed on <name> ! */
#define FindNetClient(name)        (!(name) ? 0 : SeekNetClient(name))
#define FindUser(name)             (!(name) ? 0 : SeekUser(name))
#define FindService(name)          (!(name) ? 0 : SeekService(name))
#define FindServer(name)           (!(name) ? 0 : SeekServer(name))
#define FindChannel(name)          (!(name) ? 0 : SeekChannel(name))

/*************************************************************************/

extern void init_hash(void);    /* Call me on startup */
extern int hAddNetClient(struct NetClient *nc);
extern int hAddChannel(struct Channel *c);
extern int hRemNetClient(struct NetClient *nc);
extern int hRemChannel(struct Channel *c);
extern struct NetClient *hSeekNetClient(const char *name, int TMask);
extern struct Channel *hSeekChannel(const char *name);

#endif /* HASH_H */
