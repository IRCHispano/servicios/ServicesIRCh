/*
 * ServicesIRCh - Services for IRCh, helpserv.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(HELPSERV_H)
#define HELPSERV_H

struct NetClient;

extern struct NetClient *ncHelpServ;
extern struct NetClient *ncIrcIIHelp;
extern void hs_init();
extern void helpserv(struct NetClient *source, char *buf);
extern void irciihelp(struct NetClient *source, char *buf);

#endif  /* HELPSERV_H */
