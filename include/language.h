/*
 * ServicesIRCh - Services for IRCh, language.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(LANGUAGE_H)
#define LANGUAGE_H

#include <time.h>

struct NetClient;
struct NickInfo;

/*************************************************************************/

/* Languages.  Never insert anything in (or delete anything from) the
 * middle of this list, or everybody will start getting the wrong language!
 * If you want to change the order the languages are displayed in for
 * NickServ HELP SET LANGUAGE, do it in language.c.
 */
#define LANG_ES         0       /* Spanish */
#define LANG_EN_US      1       /* United States English */
#define LANG_IT         2       /* Italian */
#define LANG_PT         3       /* Portugese */
#define LANG_TR         4       /* Turkish */

#define NUM_LANGS       5       /* Number of languages */
#define LANG_DEFAULT    -1      /* "Use the default" setting */

/* Sanity-check on default language value */
#if DEF_LANGUAGE < 0 || DEF_LANGUAGE >= NUM_LANGS
# error Invalid value for DEF_LANGUAGE: must be >= 0 and < NUM_LANGS
#endif

/*************************************************************************/

/* Flags for maketime() `flags' parameter. */

#define MT_DUALUNIT     0x0001  /* Allow two units (e.g. X hours Y mins) */
#define MT_SECONDS      0x0002  /* Allow seconds (default minutes only) */

/*************************************************************************/

/* External symbol declarations (see language.c for documentation). */

extern int langlist[NUM_LANGS+1];

extern int lang_init(void);
extern void lang_cleanup(void);

extern int lookup_language(const char *name);
extern int have_language(int language);
extern int lookup_string(const char *name);
//extern char *getstring(const struct NickInfo *ni, int index);
extern char *getstring_lang(int language, int index);

extern int strftime_lang(char *buf, int size, const struct NickInfo *ni,
                         int format, time_t time);
extern char *maketime(const struct NickInfo *ni, time_t time, int flags);
extern void expires_in_lang(char *buf, int size, const struct NickInfo *ni,
                            time_t seconds);

extern void syntax_error(struct NetClient *service, struct NetClient *nc,
                         const char *command, int msgnum);

extern char **langtexts[NUM_LANGS];
#define getstring(ni,index) \
	(langtexts[((ni)?(ni)->language:DEF_LANGUAGE)][(index+1)])

/*************************************************************************/

/* Definitions of language string constants. */
#include "langstrs.h"

/*************************************************************************/

#endif /* LANGUAGE_H */
