/*
 * ServicesIRCh - Services for IRCh, log.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2019 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(LOG_H)
#define LOG_H

#include "compat.h"

#include <stdarg.h>
#include <time.h>

struct LogInfo {
    struct LogInfo *next;
    char           *name;
    char           *filename;
    FILE           *fdlog;
    time_t         datalog;
};

struct NetClient;

extern int log_init(void);
extern struct LogInfo *logsvc;
extern struct LogInfo *log_add(char *name, char *filename);
extern void log_openall(void);
extern void log_closeall(void);
extern void slog(struct LogInfo *li, const char *fmt, ...)           FORMAT(printf,2,3);
#if defined(DEBUGMODE)
extern void logdebug(int level, const char *fmt, ...)  FORMAT(printf,2,3);
#endif
extern void log_perror(const char *fmt, ...)            FORMAT(printf,1,2);
extern void fatal(const char *fmt, ...)         FORMAT(printf,1,2);
extern void fatal_perror(const char *fmt, ...)  FORMAT(printf,1,2);

extern void rotate_log(struct NetClient *nc);

#if defined(DEBUGMODE)
#define Debug(x)  logdebug x
#else
#define Debug(x)
#endif

#endif /* LOG_H */
