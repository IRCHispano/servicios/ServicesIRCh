/*
 * ServicesIRCh - Services for IRCh, config.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(MEMOSERV_H)
#define MEMOSERV_H

#if !defined(CONFIG_H)
#include "config.h"
#endif

#include <sys/types.h>

struct NetClient;
struct NickInfo;

/* Memo info structures.  Since both nicknames and channels can have memos,
 * we encapsulate memo data in a MemoList to make it easier to handle. */

struct Memo {
    u_int32_t number;   /* Index number -- not necessarily array position! */
    int16_t flags;
    time_t time;        /* When it was sent */
    char sender[NICKMAX];
    char *text;
};

#define MF_UNREAD       0x0001  /* Memo has not yet been read */

struct MemoInfo {
    struct Memo *memos;
    u_int16_t memocount;
    u_int16_t memomax;
};

/*************************************************************************/

extern struct NetClient *ncMemoServ;
extern void ms_init(void);
extern void memoserv(struct NetClient *source, char *buf);
extern void load_old_ms_dbase(void);
extern void check_memos(struct NetClient *nc);

#endif /* MEMOSERV_H */
