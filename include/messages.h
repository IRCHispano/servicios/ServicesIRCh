/*
 * ServicesIRCh - Services for IRCh, messages.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(MESSAGES_H)
#define MESSAGES_H

/*************************************************************************/

struct NetClient;

struct Message {
    const char *name;
    const char *tok;
    void (*func)(struct NetClient *source, int ac, char **av);
};

extern struct Message messages[];

extern struct Message *find_message_name(const char *name);
extern struct Message *find_message_tok(const char *tok);

/*************************************************************************/

#endif /* MESSAGES_H */
