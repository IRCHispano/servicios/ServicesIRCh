/*
 * ServicesIRCh - Services for IRCh, misc.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(MISC_H)
#define MISC_H

#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>

struct NetClient;

extern char *strscpy(char *d, const char *s, size_t len);
extern char *stristr(char *s1, char *s2);
extern char *strupper(char *s);
extern char *strlower(char *s);
extern char *strnrepl(char *s, int32_t size, const char *old, const char *new);

extern char *merge_args(int argc, char **argv);

typedef int (*range_callback_t)(struct NetClient *nc, int num, va_list args);
extern int process_numlist(const char *numstr, int *count_ret,
                range_callback_t callback, struct NetClient *nc, ...);
extern int dotime(const char *s);

#endif  /* MISC_H */
