/*
 * ServicesIRCh - Services for IRCh, netclient.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2012-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(NETCLIENT_H)
#define NETCLIENT_H

#if !defined(CONFIG_H)
#include "config.h"
#endif
#if !defined(NETWORK_H)
#include "network.h"
#endif
#include <time.h>

struct User;

/*
 * Describe la lista de clientes remotos
 * (usuarios+servidores) que entran desde
 * otros servidores de la red.
 */
struct NetClient {
    struct NetClient *next;        /* Puntero al siguiente en la lista de NetClients globales */
    struct NetClient *prev;        /* Puntero al anterior en la lista de NetClients globales */
    struct NetClient *hnext;       /* Puntero al siguiente en la tabla hash */

    struct User      *user;        /* Si esta definido, puntero a la estructura de usuario */
    struct Server    *serv;        /* Si esta definido, puntero a la estructura de servidor */
    struct Service   *service;     /* Si esta definido, puntero a la estructura de service */

    char             *name;        /* Nombre de servidor o nick del usuario sun el caso */
    char             *description; /* Descripcion del servidor o realname del usuario segun el caso */
    time_t           ts_create;    /* Timestamp de creacion del rcliente */
    unsigned int     hopcount;     /* N�mero de hops, local = 0 */

    char             yxx[4];       /* Num�rico, YY si es un servidor o XXX si es un usuario o */
    unsigned int     status;       /* Tipo de Netcliente (Me, Server o User) */
};

extern struct NetClient myService;
extern struct NetClient *myHub;
extern struct NetClient *ncLocalList;
extern struct NetClient *ncGlobalList;

/* Estados de NetClient */
#define NETCLIENT_ME      0x1 /* Este servidor del service */
#define NETCLIENT_SERVER  0x2 /* Es un servidor */
#define NETCLIENT_USER    0x4 /* Es un usuario */
#define NETCLIENT_SERVICE 0x8 /* ES un service */

/* Macros */
#define IsMe(x)      ((x)->status & NETCLIENT_ME)
#define IsServer(x)  ((x)->status & NETCLIENT_SERVER)
#define IsUser(x)    ((x)->status & NETCLIENT_USER)
#define MyService(x) ((x)->status & NETCLIENT_SERVICE)
#define SetMe(x)     ((x)->status |= NETCLIENT_ME)
#define SetServer(x) ((x)->status |= NETCLIENT_SERVER)
#define SetUser(x)   ((x)->status |= NETCLIENT_USER)

#define NumServ(x)  x->yxx
#define NumNick(x)  x->user->server->yxx, x->yxx

extern void get_netclient_stats(struct NetClient *service, struct NetClient *user, long *memuse);
extern void netclient_init(void);
extern void netclient_end(void);
extern struct NetClient *netclient_create(int type);
extern void netclient_delete(struct NetClient *nc);

extern struct NetClient *firstnc(void);
extern struct NetClient *nextnc(void);
extern void netclient_add_list(struct NetClient *nc);
extern void netclient_del_list(struct NetClient *nc);

#endif /* NETCLIENT_H */
