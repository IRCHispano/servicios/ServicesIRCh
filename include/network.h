/*
 * ServicesIRCh - Services for IRCh, network.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(NETWORK_H)
#define NETWORK_H


/** Maximum length of a numeric IP (v4 or v6) address.
 * "ffff:ffff:ffff:ffff:ffff:ffff:255.255.255.255"
 */
#define SOCKIPLEN 45

#define HOSTLEN 64

/* IP, soporta IPv6 */
struct irc_in_addr
{
  unsigned short in6_16[8];
};


/** Evaluate to non-zero if \a ADDR is an unspecified (all zeros) address. */
#define irc_in_addr_unspec(ADDR) (((ADDR)->in6_16[0] == 0) \
                                  && ((ADDR)->in6_16[1] == 0) \
                                  && ((ADDR)->in6_16[2] == 0) \
                                  && ((ADDR)->in6_16[3] == 0) \
                                  && ((ADDR)->in6_16[4] == 0) \
                                  && ((ADDR)->in6_16[6] == 0) \
                                  && ((ADDR)->in6_16[7] == 0) \
                                  && ((ADDR)->in6_16[5] == 0 \
                                      || (ADDR)->in6_16[5] == 65535))

/** Evaluate to non-zero if \a ADDR (of type struct irc_in_addr) is an IPv4 address. */
#define irc_in_addr_is_ipv4(ADDR) (!(ADDR)->in6_16[0] && !(ADDR)->in6_16[1] && !(ADDR)->in6_16[2] \
                                   && !(ADDR)->in6_16[3] && !(ADDR)->in6_16[4] \
                                   && ((!(ADDR)->in6_16[5] && (ADDR)->in6_16[6]) \
                                       || (ADDR)->in6_16[5] == 65535))

#endif /* NETWORK_H */
