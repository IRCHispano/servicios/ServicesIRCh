/*
 * ServicesIRCh - Services for IRCh, newsserv.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2011-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(NEWSSERV_H)
#define NEWSSERV_H

#include <sys/types.h>

struct NetClient;

/* Constants for news types. */

#define NEWS_LOGON      0
#define NEWS_OPER       1

extern struct NetClient *ncNewsServ;
extern void nws_init();
extern void get_news_stats(long *nrec, long *memuse);
extern void load_news(void);
extern void save_news(void);
extern void display_news(struct NetClient *nc, int16_t type);
extern void do_logonnews(struct NetClient *nc);
extern void do_opernews(struct NetClient *nc);

#endif /* NEWSSERV_H */
