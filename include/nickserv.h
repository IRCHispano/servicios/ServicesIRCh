/*
 * ServicesIRCh - Services for IRCh, nickserv.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(NICKSERV_H)
#define NICKSERV_H

#if !defined(NETWORK_H)
#include "network.h"
#endif
#if !defined(MEMOSERV_H)
#include "memoserv.h"
#endif

/* Nickname info structure.  Each nick structure is stored in one of 256
 * lists; the list is determined by the first character of the nick.  Nicks
 * are stored in alphabetical order within lists. */

struct NickInfo {
    struct NickInfo *next, *prev;
    char nick[NICKMAX];
    char pass[PASSMAX];
    char *url;
    char *email;

    char *last_usermask;
    char *last_realname;
    char *last_quit;
    time_t time_registered;
    time_t last_seen;
    int16_t status;     /* See NS_* below */

    struct NickInfo *link;      /* If non-NULL, nick to which this one is linked */
    int16_t linkcount;  /* Number of links to this nick */

    /* All information from this point down is governed by links.  Note,
     * however, that channelcount is always saved, even for linked nicks
     * (thus removing the need to recount channels when unlinking a nick). */

    int32_t flags;      /* See NI_* below */

    int16_t accesscount;        /* # of entries */
    char **access;      /* Array of strings */

    struct MemoInfo memos;

    u_int16_t channelcount;/* Number of channels currently registered */
    u_int16_t channelmax;       /* Maximum number of channels allowed */

    u_int16_t language; /* Language selected by nickname owner (LANG_*) */

    time_t id_timestamp;/* TS8 timestamp of user who last ID'd for nick */

    /* Online-only information: */
    struct NetClient *nc;            /* User using this nick, NULL if not online */
};


/* Nickname status flags: */
#define NS_ENCRYPTEDPW  0x0001      /* Nickname password is encrypted */
#define NS_VERBOTEN     0x0002      /* Nick may not be registered or used */
#define NS_NO_EXPIRE    0x0004      /* Nick never expires */

#define NS_IDENTIFIED   0x8000      /* User has IDENTIFY'd */
#define NS_RECOGNIZED   0x4000      /* ON_ACCESS true && SECURE flag not set */
#define NS_ON_ACCESS    0x2000      /* User comes from a known address */
#define NS_KILL_HELD    0x1000      /* Nick is being held after a kill */
#define NS_GUESTED      0x0100      /* SVSNICK has been sent but nick has not
                                     * yet changed. An enforcer will be
                                     * introduced when it does change. */
#define NS_TEMPORARY    0xFF00      /* All temporary status flags */


/* Nickname setting flags: */
#define NI_KILLPROTECT  0x00000001  /* Kill others who take this nick */
#define NI_SECURE       0x00000002  /* Don't recognize unless IDENTIFY'd */
#define NI_MEMO_HARDMAX 0x00000008  /* Don't allow user to change memo limit */
#define NI_MEMO_SIGNON  0x00000010  /* Notify of memos at signon and un-away */
#define NI_MEMO_RECEIVE 0x00000020  /* Notify of new memos when sent */
#define NI_PRIVATE      0x00000040  /* Don't show in LIST to non-servadmins */
#define NI_HIDE_EMAIL   0x00000080  /* Don't show E-mail in INFO */
#define NI_HIDE_MASK    0x00000100  /* Don't show last seen address in INFO */
#define NI_HIDE_QUIT    0x00000200  /* Don't show last quit message in INFO */
#define NI_KILL_QUICK   0x00000400  /* Kill in 20 seconds instead of 60 */
#define NI_KILL_IMMED   0x00000800  /* Kill immediately instead of in 60 sec */

/*************************************************************************/

/* Externs */

extern struct NetClient *ncNickServ;
extern void ns_init(void);
extern void nickserv(struct NetClient *source, char *buf);
extern void get_nickserv_stats(long *nrec, long *memuse);
extern void load_ns_dbase(void);
extern void save_ns_dbase(void);
extern int validate_user(struct NetClient *nc);
extern void cancel_user(struct NetClient *nc);
extern int nick_identified(struct NetClient *nc);
extern int nick_recognized(struct NetClient *nc);
extern void expire_nicks(void);
extern void set_identified(struct NetClient *nc, int server);

extern struct NickInfo *findnick(const char *nick);
extern struct NickInfo *getlink(struct NickInfo *ni);

#endif /* NICKSERV_H */
