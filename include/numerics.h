/*
 * ServicesIRCh - Services for IRCh, numerics.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(NUMERICS_H)
#define NUMERICS_H

/* Codigo procedente de Undernet */

#if !defined(NETWORK_H)
#include "network.h"
#endif

extern unsigned int base64toint(const char* str);
extern const char* inttobase64(char* buf, unsigned int v, unsigned int count);
extern const char* iptobase64(char* buf, const struct irc_in_addr* addr, unsigned int count, int v6_ok);
extern struct NetClient* FindNServer(const char* numeric);
extern struct NetClient* FindNUser(const char* yxx);
extern void SetYXXMyServer(struct NetClient* nc, unsigned int numeric, unsigned int capacity);
extern void SetServerYXX(struct NetClient *server, const char *yxx);
extern void ClearServerYXX(const struct NetClient *server);
extern int SetLocalNumNick(struct NetClient *nc);
extern void SetRemoteNumNick(struct NetClient* nc, const char *yxx);
extern void RemoveYXXClient(struct NetClient* server, const char* yxx);

extern void base64toip(const char* s, struct irc_in_addr* addr);

#endif /* NUMERICS_H */
