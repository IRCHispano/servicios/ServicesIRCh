/*
 * ServicesIRCh - Services for IRCh, operserv.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2009-2016 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(OPERSERV_H)
#define OPERSERV_H

/* Configuration sanity-checking: */

#if MAX_SERVOPERS > 32767
# undef MAX_SERVOPERS
# define MAX_SERVOPERS 32767
#endif
#if MAX_SERVADMINS > 32767
# undef MAX_SERVADMINS
# define MAX_SERVADMINS 32767
#endif

struct NickInfo;
struct NetClient;

/* Externs */

extern struct NetClient *ncOperServ;
extern struct NetClient *ncDevNull;
extern void os_init();
extern void operserv(struct NetClient *source, char *buf);

extern void load_os_dbase(void);
extern void save_os_dbase(void);
extern int is_services_root(struct NetClient *nc);
extern int is_services_admin(struct NetClient *nc);
extern int is_services_oper(struct NetClient *nc);
extern int nick_is_services_admin(struct NickInfo *ni);
extern void os_remove_nick(const struct NickInfo *ni);

extern void check_clones(struct NetClient *nc);

#endif /* OPERSERV_H */
