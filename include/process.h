/*
 * ServicesIRCh - Services for IRCh, process.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(PROCESS_H)
#define PROCESS_H

#if !defined(CONFIG_H)
#include "config.h"
#endif

#include <time.h>

/* Ignorance list data. */

struct IgnoreData {
    struct IgnoreData *next;
    char who[NICKMAX];
    time_t time;        /* When do we stop ignoring them? */
};

extern int allow_ignore;
extern struct IgnoreData *ignore[];

extern void add_ignore(const char *nick, time_t delta);
extern struct IgnoreData *get_ignore(const char *nick);

extern int split_buf(char *buf, char ***argv, int colon_special);
extern void process(void);

#endif  /* PROCESS_H */
