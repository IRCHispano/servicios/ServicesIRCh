/*
 * ServicesIRCh - Services for IRCh, send.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(SEND_H)
#define SEND_H

#include "compat.h"

#include <stdarg.h>

struct NetClient;

/* Externs */

extern void send_cmd(struct NetClient *source, const char *fmt, ...)
        FORMAT(printf,2,3);
extern void vsend_cmd(struct NetClient *source, const char *fmt, va_list args)
        FORMAT(printf,2,0);
extern void send_numirc(struct NetClient *dest, int numeric, const char *fmt, ...)
        FORMAT(printf,3,4);
extern void wallops(struct NetClient *source, const char *fmt, ...)
        FORMAT(printf,2,3);
extern void notice(struct NetClient *source, struct NetClient *dest, const char *fmt, ...);
extern void msg_chan(struct NetClient *source, const char *channel, const char *fmt, ...)
        FORMAT(printf,3,4);
extern void notice_list(struct NetClient *source, struct NetClient *dest, const char **text);
extern void msg_lang(struct NetClient *source, struct NetClient *dest, int message, ...);
extern void msg_help(struct NetClient *source, struct NetClient *dest, int message, ...);
extern void privmsg(struct NetClient *source, struct NetClient *dest, const char *fmt, ...)
        FORMAT(printf,3,4);

#endif  /* SEND_H */
