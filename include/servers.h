/*
 * ServicesIRCh - Services for IRCh, servers.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(SERVERS_H)
#define SERVERS_H

#include <sys/types.h>
#include <time.h>


/* Describe un servidor en la red */
struct Server {
    struct NetClient  *up;            /* Puntero al esta linkado */
    struct ServerLink *down;          /* Punteros al Cliente Hijo */
    struct ServerLink *updown;        /* Punteros al Cliente Padre */

    struct NetClient **client_list;   /* Lista de punteros de los Clientes que cuelgan del servidor */

    char              *version;       /* Versi�n del servidor */
    char              *options;       /* Opciones del servidor */

    time_t            start_time;     /* Timestamp de arranque del servidor */
    time_t            link_time;      /* Timestamp de la conexion en la red */
    u_int32_t         clients;        /* N�mero de clientes en el servidor */
    u_int16_t         flags;          /* Flags del servidor */

    char              nn_capacity[4]; /* Capacidad del servidor en base64 */
    unsigned int      nn_mask;        /* Capacidad del servidor */
    unsigned int      protocol;       /* Protocolo del servidor */

    /* Variables services */
};

/* Lista doble de servidores */
struct ServerLink {
    struct ServerLink *next;      /* Siguiente elemento */
    struct ServerLink *prev;      /* Elemento anterior */
    struct NetClient  *nc;        /* Puntero al servidor */
};

/* Flags del servidor */
#define SFLAG_HUB       0x01  /* El servidor es un Hub */
#define SFLAG_SERVICE   0x02  /* El servidor es un Uworld */
#define SFLAG_IPV6      0x04  /* El servidor soporta IPv6 */
#define SFLAG_BURST     0x08  /* El servidor esta en burst */
#define SFLAG_BURST_ACK 0x10  /* El servidor esta esperando el ack */

#define IsHub(x)         ((x)->serv->flags & SFLAG_HUB)
#define IsService(x)     ((x)->serv->flags & SFLAG_SERVICE)
#define IsIPv6(x)        ((x)->serv->flags & SFLAG_IPV6)
#define IsBurst(x)       ((x)->serv->flags & SFLAG_BURST)
#define IsBurstAck(x)    ((x)->serv->flags & SFLAG_BURST_ACK)
#define SetHub(x)        ((x)->serv->flags |= SFLAG_HUB)
#define SetService(x)    ((x)->serv->flags |= SFLAG_SERVICE)
#define SetIPv6(x)       ((x)->serv->flags |= SFLAG_IPV6)
#define SetBurst(x)      ((x)->serv->flags |= SFLAG_BURST)
#define SetBurstAck(x)   ((x)->serv->flags |= SFLAG_BURST_ACK)
#define ClearBurst(x)    ((x)->serv->flags &= ~SFLAG_BURST)
#define ClearBurstAck(x) ((x)->serv->flags &= ~SFLAG_BURST_ACK)

extern struct Server *servers_create(void);
extern void servers_delete(struct Server *server);
extern struct ServerLink *serverlink_add_(struct ServerLink **slp, struct NetClient *nc);
extern void serverlink_del(struct ServerLink **slp, struct ServerLink *sl);

extern void m_end_of_burst(struct NetClient *source, int ac, char **av);
extern void m_end_of_burst_ack(struct NetClient *source, int ac, char **av);
extern void m_server(struct NetClient *source, int ac, char **av);
extern void m_squit(struct NetClient *source, int ac, char **av);
extern void m_351(struct NetClient *source, int ac, char **av);

#endif /* SERVERS_H */
