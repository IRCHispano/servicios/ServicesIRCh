/*
 * ServicesIRCh - Services for IRCh, service.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(SERVICE_H)
#define SERVICE_H

/* Describe un usuario en la red */
struct Service {
    struct NetClient *nc;
    void            (*func)(struct NetClient *source, char *buf);
    int             (*has_priv)(struct NetClient *nc);
};

extern struct NetClient *service_create(const char *nick, const char *user, const char *host, const char *realname);
extern void service_remove(struct NetClient *nc);
extern void service_introduce(struct NetClient *nc);
extern void service_join(struct NetClient *nc, char *channels);
extern void services_burst();

#endif /* SERVICE_H */
