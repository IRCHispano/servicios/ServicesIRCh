/*
 * ServicesIRCh - Services for IRCh, services.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(SERVICES_H)
#define SERVICES_H

/*************************************************************************/

#include "sysconf.h"
#include "config.h"

#include <time.h>

/* Some Linux boxes (or maybe glibc includes) require this for the
 * prototype of strsignal(). */
#define _GNU_SOURCE

#define BASE_VERSION "4.3.3"
#define RELEASE ".IRCh-1"


/* We also have our own encrypt(). */
#define encrypt encrypt_

/**** init.c ****/

extern int init(int ac, char **av);


/**** main.c ****/
extern char *services_dir;
extern char *log_filename;
extern int   debug;
extern int   readonly;
extern int   skeleton;
extern int   nofork;
extern int   forceload;

extern int   quitting;
extern int   delayed_quit;
extern char *quitmsg;
extern char  inbuf[BUFSIZE];
extern int   servsock;
extern int   save_data;
extern int   got_alarm;
extern time_t start_time;

/*************************************************************************/
/*************************************************************************/

/* Who sends channel MODE (and KICK) commands? */
#if defined(IRC_DALNET) || (defined(IRC_UNDERNET) && !defined(IRC_UNDERNET_NEW))
# define MODE_SENDER(service) service
#else
# define MODE_SENDER(service) ServerName
#endif

/*************************************************************************/

#endif	/* SERVICES_H */
