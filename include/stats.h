/*
 * ServicesIRCh - Services for IRCh, stats.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(STATS_H)
#define STATS_H

#include <sys/types.h>
#include <time.h>

struct NetClient;

struct IRCStatistics {
    /* Usuarios */
    u_int32_t users;          /* Usuarios en la red */
    u_int32_t max_users;      /* Record de usuarios */
    time_t    ts_max_users;   /* TS del record */
    u_int32_t local_services; /* Services locales */
    u_int32_t services;       /* Services totales (local+remoto) */
    /* Servidores */
    u_int32_t servers;        /* Servidores totales (normales+pseudos) */
    u_int32_t pservers;       /* PseudoServidor de services */
    /* Canales */
    u_int32_t channels;       /* Canales en la red */
    u_int32_t max_chans;      /* Record de canales */
    time_t    ts_max_chans;   /* TS del record */
    /* Misc: */
    u_int32_t ircops;         /* Numero de ircops */
    u_int32_t opers;          /* Numero de opers (autentificados) */
};

extern struct IRCStatistics IRCStats;

struct StatsCmds;

typedef void (*StatsFunc)(struct NetClient *nc, const struct StatsCmds *sc, char *param);

struct StatsCmds {
    char             character;  /* Caracter */
    char            *name;       /* Nombre */
    unsigned int     flags;      /* Flags */
    StatsFunc        func;       /* Funcion */
    int              funcdata;   /* Campo extra */
};

#define STATS_CASESENS 0x01
#define STATS_VARPARAM 0x02

/*************************************************************************/

/*
 * Macros
 */
#define Count_newuser(IRCStats, nc) \
    do { \
        ++nc->user->server->serv->clients; \
        if (!IsService(nc->user->server)) { \
            ++IRCStats.users; \
        } else { \
            ++IRCStats.services; \
        } \
        if (IRCStats.users > IRCStats.max_users) \
            stats_newrecord_users(); \
    } while (0)

#define Count_newlocalservice(IRCStats) (++IRCStats.local_services)

#define Count_quituser(IRCStats, nc) \
    do { \
        if (!IsServer(nc->user->server)) \
            --nc->user->server->serv->clients; \
        if (!IsService(nc->user->server)) { \
            --IRCStats.users; \
        } else { \
            --IRCStats.services; \
        } \
    } while (0)

#define Count_quitlocalservice(IRCStats) (--IRCStats.local_services)

#define Count_newserver(IRCStats, nc) \
    do { \
        if (IsService(nc)) \
            ++IRCStats.pservers; \
        else \
            ++IRCStats.servers; \
    } while (0)

#define Count_quitserver(IRCStats, nc) \
    do { \
        if (IsService(nc)) \
            --IRCStats.pservers; \
        else \
            --IRCStats.servers; \
    } while (0)

#define Count_newchannel(IRCStats) \
    do { \
        ++IRCStats.channels; \
        if (IRCStats.channels > IRCStats.max_chans) \
            stats_newrecord_chans(); \
    } while (0)

#define Count_quitchannel(IRCStats) (--IRCStats.channels)

/*************************************************************************/

extern void stats_init(void);
extern void stats_newrecord_users(void);
extern void stats_newrecord_chans(void);
extern void m_stats(struct NetClient *source, int ac, char **av);

#endif /* STATS_H */
