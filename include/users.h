/*
 * IRC-Hispano Services, users.h
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2012-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(USERS_H)
#define USERS_H

#if !defined(CONFIG_H)
#include "config.h"
#endif
#if !defined(NETWORK_H)
#include "network.h"
#endif

#include <sys/types.h>
#include <time.h>

/* Describe un usuario en la red */
struct User {
    struct NetClient   *server;      /* Puntero al servidor que cuelga */
    struct Membership  *channel;     /* Puntero a la lista de canales */
//    struct Clon        *clon;        /* Puntero a la lista de clones */

    char               *username;    /* Username del usuario */
    char               *host;        /* Host del usuario */
    char               *virtualhost; /* Host virtual */
    struct irc_in_addr ip;           /* IP del usuario (128 bits) */


    int32_t             modes;       /* Modos del usuario */
    int32_t             joined;      /* N�mero de canales que ha entrado */

    time_t              ts_connect;  /* Timestamp de entrada en la red */
    time_t              ts_lastnick; /* Timestamp del �ltimo cambio de nick */
    char               *away;        /* Mensaje de away */

  /* Asociados a los services */
    struct NickInfo    *ni;          /* Effective NickInfo (not a link) */
    struct NickInfo    *real_ni;     /* Real NickInfo (ni.nick==user.nick) */
    time_t my_signon;                        /* When did _we_ see the user with
                                                      *    their current nickname? */
    struct u_chaninfolist {
        struct u_chaninfolist *next, *prev;
        struct ChannelInfo *chan;
    } *founder_chans;              /* Channels user has identified for */

    int16_t  invalid_pw_count;     /* # of invalid password attempts */
    time_t   invalid_pw_time;      /* Time of last invalid password */
    time_t   lastmemosend;         /* Last time MS SEND command used */
    time_t   lastnickreg;          /* Last time NS REGISTER cmd used */
};

/* User modes */
#define UMODE_o  0x00000001
#define UMODE_i  0x00000002
#define UMODE_w  0x00000004
#define UMODE_s  0x00000008
#define UMODE_d  0x00000010
#define UMODE_k  0x00000020
#define UMODE_g  0x00000040
#define UMODE_r  0x00000080
#define UMODE_S  0x00000100
#define UMODE_a  0x00000200
#define UMODE_C  0x00000400
#define UMODE_h  0x00000800
#define UMODE_B  0x00001000
#define UMODE_R  0x00002000
#define UMODE_c  0x00004000
#define UMODE_x  0x00008000
#define UMODE_X  0x00010000
#define UMODE_z  0x00020000
#define UMODE_n  0x00040000
#define UMODE_I  0x00080000
#define UMODE_W  0x00100000
#define UMODE_D  0x00200000
#define UMODE_P  0x00400000
#define UMODE_J  0x00800000
#define UMODE_K  0x01000000

/* Numero de modos */
#define UMODE_COUNT 25

extern void m_away(struct NetClient *source, int ac, char **av);
extern void m_kill(struct NetClient *source, int ac, char **av);
extern void m_nick(struct NetClient *source, int ac, char **av);
extern void m_quit(struct NetClient *source, int ac, char **av);
extern void m_umode(struct NetClient *source, int ac, char **av);
extern void m_436(struct NetClient *source, int ac, char **av);

extern struct User *users_create(void);
extern void users_delete(struct User *user);
extern void user_remove(struct NetClient *nc, char *reason);
extern int user_mode_to_int(char mode);
extern void set_user_modes(struct NetClient *source, struct NetClient *nc, char *modes);

extern char *umode_str(int modes, int show_invisible);
extern int is_oper(struct NetClient *nc);

extern int match_usermask(const char *mask, struct NetClient *nc);
extern void split_usermask(const char *mask, char **nick, char **user, char **host);
extern char *create_mask_ban(struct NetClient *nc);
extern char *create_mask_access(char *username, char *host);

#endif /* USERS_H */
