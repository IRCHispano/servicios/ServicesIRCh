/*
 * ServicesIRCh - Services for IRCh, actions.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "actions.h"

#include "config.h"
#include "netclient.h"
#include "send.h"
#include "svc_memory.h"
#include "users.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*************************************************************************/

/* Remove a user from the IRC network.  `source' is the nick which should
 * generate the kill, or NULL for a server-generated kill.
 */

void kill_user(struct NetClient *source, struct NetClient *victim, const char *reason)
{
    char buf[BUFSIZE];

    if (!victim)
        return;
    if (!source)
        source = &myService;
    if (!reason)
        reason = "";

    snprintf(buf, sizeof(buf), "%s (%s)", source->name, reason);
    send_cmd(source, "D %s%s :%s", NumNick(victim), buf);

    /* Salida del usuario */
    user_remove(victim, buf);
}

/*************************************************************************/

/* Note a bad password attempt for the given user.  If they've used up
 * their limit, toss them off.
 */

void bad_password(struct NetClient *nc)
{
    time_t now = time(NULL);

    if (!BadPassLimit)
        return;

    if (BadPassTimeout > 0 && nc->user->invalid_pw_time > 0
                        && nc->user->invalid_pw_time < now - BadPassTimeout)
        nc->user->invalid_pw_count = 0;
    nc->user->invalid_pw_count++;
    nc->user->invalid_pw_time = now;
    if (nc->user->invalid_pw_count >= BadPassLimit)
        kill_user(NULL, nc, "Too many invalid passwords");
}

/*************************************************************************/
