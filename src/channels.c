/*
 * ServicesIRCh - Services for IRCh, channels.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2012-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "channels.h"

#include "chanserv.h"
#include "hash.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "numerics.h"
#include "send.h"
#include "services.h"
#include "stats.h"
#include "svc_memory.h"
#include "svc_string.h"
#include "users.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define HASHCHANSIZE    1024
#define HASHCHAN(chan)        ((chan)[1] ? ((chan)[1]&31)<<5 | ((chan)[2]&31) : 0)
static struct Channel *chanlist[HASHCHANSIZE];
static struct Membership *find_member_link(struct Channel *c, const struct NetClient *nc);

/*************************************************************************/

/* Return statistics.  Pointers are assumed to be valid. */

void get_channel_stats(long *nrec, long *memuse)
{
    long count = 0, mem = 0;
    struct Channel *chan;
    struct Membership *member;
    struct Ban *ban;
    int chansm = 0;
    int chansb = 0;
    int i;

    for (i = 0; i < 1024; i++) {
        for (chan = chanlist[i]; chan; chan = chan->next) {
            count++;
            mem += sizeof(*chan);
            if (chan->topic)
                mem += strlen(chan->topic)+1;
            if (chan->key)
                mem += strlen(chan->key)+1;

            for (member = chan->members; member; member = member->next_member)
                chansm++;

            for (ban = chan->banlist; ban; ban = ban->next)
                chansb++;

            mem += chansm * sizeof(struct Membership);
            mem += chansb * sizeof(struct Ban);
        }
    }
    *nrec = count;
    *memuse = mem;
}

/*************************************************************************/

/* Iterate over all channels in the channel list.  Return NULL at end of
 * list.
 */

static struct Channel *current;
static int next_index;

struct Channel *firstchan(void)
{
    next_index = 0;
    while (next_index < HASHCHANSIZE && current == NULL)
        current = chanlist[next_index++];
    Debug((3, "firstchan() returning %s",
               current ? current->name : "NULL (end of list)"));
    return current;
}

struct Channel *nextchan(void)
{
    if (current)
        current = current->next;
    if (!current && next_index < HASHCHANSIZE) {
        while (next_index < HASHCHANSIZE && current == NULL)
            current = chanlist[next_index++];
    }
    Debug((3, "nextchan() returning %s",
               current ? current->name : "NULL (end of list)"));
    return current;
}

static void channel_add_list(struct Channel *c)
{
    struct Channel **list;

    list = &chanlist[HASHCHAN(c->name)];
    c->next = *list;
    if (*list)
        (*list)->prev = c;
    *list = c;
}

static void channel_del_list(struct Channel *c)
{
    if (c->next)
        c->next->prev = c->prev;
    if (c->prev)
        c->prev->next = c->next;
    else
        chanlist[HASHCHAN(c->name)] = c->next;
}

/*************************************************************************/
/*********** GESTION DE CANALES  *****************************************/
/*************************************************************************/

/* Get a channel block, creating if necessary. */
struct Channel *get_channel(struct NetClient *nc, char *chname, int create)
{
    struct Channel *c;
    int len;

    len = strlen(chname);
    if (!nc && len > CHANMAX) {
        len = CHANMAX;
        *(chname + CHANMAX) = '\0';
    }

    if ((c = FindChannel(chname)))
        return c;

    if (create) {
        time_t now = time(NULL);

        Debug((1, "Creating channel %s", chname));
        c = smalloc(sizeof(struct Channel));
        assert(0 != c);
        memset(c, 0, sizeof(struct Channel));
        strscpy(c->name, chname, sizeof(c->name));

        c->creation_time = now;

        Count_newchannel(IRCStats);
        channel_add_list(c);
        hAddChannel(c);

        /* Store ChannelInfo pointer in channel record */
        c->ci = cs_findchan(chname);
        /* Store return pointer in ChannelInfo record */
        if (c->ci)
            c->ci->c = c;

        /* Restore locked modes and saved topic */
        check_modes(c);
        restore_topic(c);
    }
    return c;
}

/*************************************************************************/

/* Destroy an empty channel */
void destruct_channel(struct Channel *c)
{
    struct Ban *ban, *next;

    assert(0 == c->members);

    Debug((1, "Deleting channel %s", c->name));

    if (c->ci)
       c->ci->c = NULL;

    if (c->topic)
        sfree(c->topic);
    if (c->key)
        sfree(c->key);

    for (ban = c->banlist; ban; ban = next)
    {
        next = ban->next;
        sfree(ban);
    }

    channel_del_list(c);
    hRemChannel(c);

    Count_quitchannel(IRCStats);

    /*
     * make sure that channel actually got removed from hash table
     */
    assert(c->hnext == c);
    sfree(c);
}

/*************************************************************************/

/* Decrement the count of users, and free if empty. */
int sub1_from_channel(struct Channel *c)
{
    if (c->chan_usercount > 1)
    /* Can be 0, called for an empty channel too */
    {
        assert(0 != c->members);
        --c->chan_usercount;
        return 1;
    }
    c->chan_usercount = 0;

    destruct_channel(c);

    return 0;
}

/*************************************************************************/
/*********** GESTION DE USUARIOS EN CANAL ********************************/
/*************************************************************************/

/* Add a user to a channel. */
void add_user_to_channel(struct Channel *c, struct NetClient *nc, int flags)
{
    assert(0 != c);
    assert(0 != nc);

    if (nc->user)
    {
        struct Membership *member;

        member = smalloc(sizeof(struct Membership));
        assert(0 != member);
        member->user = nc;
        member->channel = c;
        member->status = flags;

        member->next_member  = c->members;
        if (member->next_member)
            member->next_member->prev_member = member;
        member->prev_member  = 0;
        c->members = member;

        member->next_channel = nc->user->channel;
        if (member->next_channel)
            member->next_channel->prev_channel = member;
        member->prev_channel = 0;
        nc->user->channel = member;

        ++c->chan_usercount;
        ++nc->user->joined;
    }
}

/*************************************************************************/

/* Remove a person from a channel, given their Membership* */
static int remove_member_from_channel(struct Membership *member)
{
    struct Channel *c;
    assert(0 != member);
    c = member->channel;

    /*
     * unlink channel member list
     */
    if (member->next_member)
        member->next_member->prev_member = member->prev_member;
    if (member->prev_member)
        member->prev_member->next_member = member->next_member;
    else
        member->channel->members = member->next_member;

    /*
     * unlink client channel list
     */
    if (member->next_channel)
        member->next_channel->prev_channel = member->prev_channel;
    if (member->prev_channel)
        member->prev_channel->next_channel = member->next_channel;
    else
        member->user->user->channel = member->next_channel;

    --member->user->user->joined;

    sfree(member);

    return sub1_from_channel(c);
}

/* Remove a user from a channel */
void remove_user_from_channel(struct NetClient *nc, struct Channel *c)
{
    struct Membership *member;
    assert(0 != c);

    if ((member = find_member_link(c, nc)))
        remove_member_from_channel(member);
}

/* Remove a user from all channels they are on. */
void remove_user_from_all_channels(struct NetClient *nc)
{
    struct Membership *chan;

    assert(0 != nc);
    assert(0 != nc->user);

    while ((chan = nc->user->channel))
        remove_user_from_channel(nc, chan->channel);
}

/*************************************************************************/

/* return the struct Membership* that represents a client on a channel */
static struct Membership *find_member_link(struct Channel *c, const struct NetClient *nc)
 {
    struct Membership *m;
    assert(0 != nc);
    assert(0 != c);

    if (IsServer(nc) || IsMe(nc))
        return 0;

    /* +B users are typically on a LOT of channels. */
    if (nc->user->modes & UMODE_B) {

        m = c->members;
        while (m) {
            assert(m->channel == c);
            if (m->user == nc)
                return m;
            m = m->next_member;
        }
    } else {
        m = nc->user->channel;
        while (m) {
            assert(m->user == nc);
            if (m->channel == c)
                return m;
            m = m->next_channel;
        }
    }
  return 0;
}

/* returns Membership * if a person is joined */
struct Membership *find_channel_member(struct NetClient *nc, struct Channel *c)
{
    assert(0 != c);

    return find_member_link(c, nc);
}

/*************************************************************************/

/* Check if this user is a legitimate chan owner */
int is_chan_owner(struct NetClient *nc, struct Channel *c)
{
    struct Membership* member;

    if ((member = find_member_link(c, nc)))
        return (member->status & CUMODE_OWNER);

    return 0;
}

/* Check if this user is a legitimate chanop */
int is_chan_op(struct NetClient *nc, struct Channel *c)
{
    struct Membership* member;

    if ((member = find_member_link(c, nc)))
        return (member->status & CUMODE_OP);

    return 0;
}

/* Check if this user is a legitimate chanvoice */
int is_chan_voice(struct NetClient *nc, struct Channel *c)
{
    struct Membership* member;

    if ((member = find_member_link(c, nc)))
        return (member->status & CUMODE_VOICE);

    return 0;
}

/*************************************************************************/
/*********** GESTION DE MODOS EN CANAL ************************************/
/*************************************************************************/

static int channel_modes[] = {
    CMODE_p, 'p',
    CMODE_s, 's',
    CMODE_m, 'm',
    CMODE_t, 't',
    CMODE_i, 'i',
    CMODE_n, 'n',
    CMODE_r, 'r',
    CMODE_R, 'R',
    CMODE_A, 'A',
    CMODE_S, 'S',
    CMODE_M, 'M',
    CMODE_C, 'C',
    CMODE_c, 'c',
    CMODE_N, 'N',
    CMODE_u, 'u',
    CMODE_D, 'D',
    CMODE_l, 'l',
    CMODE_k, 'k',
    CMODE_z, 'z',
    CMODE_W, 'W',
    CMODE_O, 'O',
    0,       0
};

int channel_mode_to_int(char mode)
{
    int flag, *s;

    for (s = channel_modes; (flag = *s); s += 2) {
        if (mode == (char)(*(s + 1)))
            return flag;
    }
    return 0;
}

/*************************************************************************/

static char cmode_buf[2 * sizeof(channel_modes) / sizeof(int)];
char *channel_modes_to_char(int flags)
{
    char *m = cmode_buf;
    int flag, *s;

    for (s = channel_modes; (flag = *s); s += 2) {
        if (flags & flag)
            *m++ = *(s + 1);
    }
    *m = 0;
    return cmode_buf;
}

/*************************************************************************/
/*********** GESTION DE BANS EN CANAL ************************************/
/*************************************************************************/

static void set_ban_mask(struct Ban *ban, const char *banstr)
{
    char *sep;
    assert(banstr != NULL);
    strncpy(ban->banstr, banstr, sizeof(ban->banstr) - 1);
    sep = strrchr(banstr, '@');
    if (sep) {
        ban->nu_len = sep - banstr;
        if (ipmask_parse(sep + 1, &ban->address, &ban->addrbits))
            ban->flags |= BAN_IPMASK;
    }
}

static struct Ban *make_ban(const char *banstr)
{
    struct Ban *ban;

    ban = smalloc(sizeof(*ban));
    if (!ban)
        return NULL;
    memset(ban, 0, sizeof(*ban));
    set_ban_mask(ban, banstr);
    return ban;
}

void add_ban(struct Channel *c, struct NetClient *who, const char *banstr, int check)
{
    struct Ban *ban;

    if (check) {
        for (ban = c->banlist; ban; ban = ban->next) {
            if (!irc_strcmp(ban->banstr, banstr))
                return;
        }
    }

    ban = make_ban(banstr);
    strncpy(ban->who, IsUser(who) ? who->name : "*", NICKMAX);
    ban->when = time(NULL);
    ban->next = c->banlist;
    c->banlist = ban;

    c->bancount++;
}

void delete_ban(struct Channel *c, struct Ban *ban, struct Ban *prev)
{
    if (prev)
        prev->next = ban->next;
    else
        c->banlist = ban->next;
        sfree(ban);

    c->bancount--;
}

/*************************************************************************/

static char *last_join0(struct NetClient *nc, char *chanlist)
{
    char *p;
    int join0 = 0;

    for (p = chanlist; p[0]; p++) { /* find last "JOIN 0" */
        if (p[0] == '0' && (p[1] == ',' || p[1] == '\0')) {
            if (p[1] == ',')
                p++;
            chanlist = p + 1;
            join0 = 1;
        } else {
            while (p[0] != ',' && p[0] != '\0') /* skip past channel name */
                p++;

            if (!p[0]) /* hit the end */
                break;
        }
    }

    if (join0)
        remove_user_from_all_channels(nc);

    return chanlist;
}

/*************************************************************************/
/******************* PARSEO DE RED - CANALES *****************************/
/*************************************************************************/

/*************************************************************************/

/* Handle a BURST command
 *
 *  source = Servidor
 *    av[0] = Canal
 *    av[1] = Timestamp del canal
 *  Si av[n] empieza con un '+'
 *    av[n] = Modos del canal
 *    av[n+1] = Parametros modos (key +k o limit +l)
 *    av[n+2] = Parametros modos (limit +l)
 *  Si av[n] empieza por '%', es ac-1
 *    av[n] = Bans (%ban ban ban)
 *  Si av[n] empieza por otro caracter
 *    av[n] = nick:o,nick,nick:v,nick
 *
 */
void m_burst(struct NetClient *source, int ac, char **av)
{
    struct Channel *c;
    char bufmodes[512];
    char bufnums[512];
    time_t timestamp;
    int param;
    int cont = 0;

    if (!IsServer(source))
        return;

    if (ac < 2) {
        send_cmd(NULL, "DS :Protocol Violation from %s: Too few parameters for BURST", source->name);
        Debug((1, "BURST message: expecting 2 or more parameters after "
                  "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    if (!(c = get_channel(source, av[0], 1)))
        return;

    timestamp = atoi(av[1]);
    if (!c->creation_time || c->creation_time > timestamp)
        c->creation_time = timestamp;

    param = 2;
    while(param < ac) {
        switch(*av[param]) {
            case '+':
            {
                /* Mode */
                char *p = av[param];
                while (*(++p))  {
                    switch (*p) {
                        case 'k':
                            if (c->key) {
                                sfree(c->key);
                                c->key = NULL;
                            }
                            c->key = sstrdup(av[++param]);
                            c->mode |= CMODE_k;
                            break;

                        case 'l':
                            c->limit = atoi(av[++param]);
                            c->mode &= ~CMODE_l;
                            break;

                        default:
                            c->mode |= channel_mode_to_int(*p);
                            break;
                    }
                } /* while */
                param++;
                break;
            } /* case + */

            case '%':
            {
                /* Bans */
                char *banlist = av[param] + 1;
                char *p, *bantmp;
                struct Ban *ban;

                for (bantmp = irc_strtok(&p, banlist, " "); bantmp;
                     bantmp = irc_strtok(&p, 0, " ")) {
                    for (ban = c->banlist; ban; ban = ban->next) {
                        if (!irc_strcmp(ban->banstr, bantmp)) {
                            bantmp = 0;
                            break;
                        }
                        /* TODO: mmatch */
                    }
                    if (bantmp)
                        add_ban(c, source, bantmp, 0);
                } /* for */
                param++;
                break;
            }

            default:
            {
                /* Usuarios */
                struct NetClient *nc;
                char *nicklist = av[param], *p = 0, *numeric, *ptr;
                int modes = 0, modesnc = 0;

                *bufmodes = 0;
                *bufnums = 0;

                for (numeric = irc_strtok(&p, nicklist, ","); numeric;
                     numeric = irc_strtok(&p, 0, ",")) {
                    if ((ptr = strchr(numeric, ':'))) {
                        *ptr = '\0';
                        modes = 0;
                        while (*(++ptr)) {
                            if (*ptr == 'q')
                                modes |= CUMODE_OWNER;
                            else if (*ptr == 'o')
                                modes |= CUMODE_OP;
                            else if (*ptr == 'v')
                                modes |= CUMODE_VOICE;
                            else
                                break;
                        }
                    }
                    nc = FindNUser(numeric);
                    if (!nc) {
                        slog(logsvc, "user: BURST for nonexistent user %s on %s", numeric, c->name);
                        continue;
                    }

                    /* Make sure check_kick comes before add_user_to_channel, so banned users
                     * don't get to see things like channel keys. */
                    if (check_kick(nc, c))
                        continue;

                    /* Primero si hay que quitar modos */
                    modesnc = modes;
/*
                    if (modesnc & CUMODE_OWNER && !check_valid_owner(nc, c, 0)) {
                        // No es necesario ajustar el modo, ya lo hace la DDB

                    } else */if (modesnc & CUMODE_OP && !check_valid_op(nc, c, 0)) {
                        modesnc &= ~CUMODE_OP;
                        strcat(bufmodes, "-o");
                        if (*bufnums)
                            strcat(bufnums, " ");
                        strcat(bufnums, nc->user->server->yxx);
                        strcat(bufnums, nc->yxx);
                        cont++;

                        if (cont >= 6) {
                            send_cmd(ncChanServ, "M %s %s %s",
                                     c->name, bufmodes, bufnums);
                            *bufmodes = 0;
                            *bufnums = 0;
                            cont = 0;
                        }
/*
                    } else if (modesnc & CUMODE_VOICE && !check_valid_voice(nc, c, 0)) {
                        modesnc &= ~CUMODE_VOICE;
                        strcat(bufmodes, "-v");
                        if (*bufnums)
                            strcat(bufnums, " ");
                        strcat(bufnums, nc->user->server->yxx);
                        strcat(bufnums, nc->yxx);
                        cont++;

                        if (cont >= 6) {
                            send_cmd(ncChanServ, "M %s %s %s",
                                     c->name, bufmodes, bufnums);
                            *bufmodes = 0;
                            *bufnums = 0;
                            cont = 0;
                        }
*/
                    }

                    /* Ahora si hay que poner modos */
                    /*if ((!(modesnc & CUMODE_OWNER)) && check_should_owner(nc, c)) {
                        // No es necesario ajustar el modo, ya lo hace la DDB

                    } else*/ if ((!(modesnc & CUMODE_OP)) && check_should_op(nc, c)) {
                        modesnc |= CUMODE_OP;
                        strcat(bufmodes, "+o");
                        if (*bufnums)
                            strcat(bufnums, " ");
                        strcat(bufnums, nc->user->server->yxx);
                        strcat(bufnums, nc->yxx);
                        cont++;

                        if (cont >= 6) {
                            send_cmd(ncChanServ, "M %s %s %s",
                                     c->name, bufmodes, bufnums);
                            *bufmodes = 0;
                            *bufnums = 0;
                            cont = 0;
                        }
                    } else if (!modesnc && check_should_voice(nc, c)) {
                        modesnc |= CUMODE_VOICE;
                        strcat(bufmodes, "+v");
                        if (*bufnums)
                            strcat(bufnums, " ");
                        strcat(bufnums, nc->user->server->yxx);
                        strcat(bufnums, nc->yxx);
                        cont++;

                        if (cont >= 6) {
                            send_cmd(ncChanServ, "M %s %s %s",
                                     c->name, bufmodes, bufnums);
                            *bufmodes = 0;
                            *bufnums = 0;
                            cont = 0;
                        }
                    }
                    add_user_to_channel(c, nc, modesnc);
                } /* for */
                param++;
                break;
            }
        } /* switch */

    } /* while */

    if (cont) {
        send_cmd(ncChanServ, "M %s %s %s",
                 c->name, bufmodes, bufnums);
    }

    /* Check modes against ChanServ mode lock */
    check_modes(c);
}

/*************************************************************************/

/* Handle a CREATE command
 *
 *  source = Usuario
 *    av[0] = Canales
 *    av[1] = Timestamp
 *
 */
void m_create(struct NetClient *source, int ac, char **av)
{
    struct Channel *c;
    char *name, *p = 0;

    if (IsServer(source)) {
        send_cmd(&myService, "DS :Protocol Violation from %s: Server tried to CREATE a channel",
                       source->name);
        return;
    }

    if (ac < 2 || *av[1] == '\0') {
        Debug((1, "CREATE message: expecting 2 or more parameters after "
                  "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    for (name = irc_strtok(&p, av[0], ","); name;
         name = irc_strtok(&p, 0, ",")) {

        Debug((1, "%s creates %s", source->name, name));

        if ((c = FindChannel(name))) {
            if (find_member_link(c, source)) {
                send_cmd(NULL, "DS :Protocol Violation from %s: Tried to CREATE a channel already joined",
                                source->name);
                continue;
            }

        } else
            c = get_channel(source, name, 1);

        c->creation_time = atoi(av[1]);

        /* Make sure check_kick comes before add_user_to_channel, so banned users
         * don't get to see things like channel keys. */
        if (check_kick(source, c))
            continue;

        /* Comprobamos si puede tener OP o no */
        if (check_valid_op(source, c, 0)) {
            add_user_to_channel(c, source, CUMODE_OP);
        } else {
            add_user_to_channel(c, source, 0);
            send_cmd(ncChanServ, "M %s -o %s%s", c->name, NumNick(source));
        }
    }
}

/*************************************************************************/

/* Handle a INVITE command
 *
 *  source = Usuario
 *    av[0] = Mensaje de ausencia (Opcional)
 *
 */
void m_invite(struct NetClient *source, int ac, char **av)
{
    /* Lo ignoramos */
}

/*************************************************************************/

/* Handle a JOIN command
 *
 *  source = Usuario
 *    av[0] = Canales
 *    av[1] = Timestamp (opcional)
 *
 */
void m_join(struct NetClient *source, int ac, char **av)
{
    struct Channel *c;
    char *name, *p, *chanlist;
    time_t creation = 0;
    int flags;

    if (IsServer(source)) {
        send_cmd(&myService, "DS :Protocol Violation from %s: Server tried to JOIN a channel",
                              source->name);
        return;
    }

    if (ac < 1 || *av[0] == '\0') {
        Debug((1, "JOIN message: expecting 1 or more parameters after "
                  "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    if (ac > 1 && av[1])
        creation = atoi(av[1]);

    /* Comprobamos el JOIN 0 */
    chanlist = last_join0(source, av[0]);

    for (name = irc_strtok(&p, chanlist, ","); name;
        name = irc_strtok(&p, 0, ",")) {

        Debug((1, "%s joins %s", source->name, name));

        if (!(c = FindChannel(name))) {
            if (!(c = get_channel(source, name, 1))) {
                 slog(logsvc, "Bug en JOIN (create): buf='%s'", inbuf);
                 continue;
            }
            c->creation_time = creation;
        } else {
            if (find_member_link(c, source)) {
                if (!(c = get_channel(source, name, 1))) {
                     slog(logsvc, "Bug en JOIN: buf='%s'", inbuf);
                     continue;
                }
            }
            if (creation && creation < c->creation_time)
                c->creation_time = creation;
        }

        /* Make sure check_kick comes before add_user_to_channel, so banned users
         * don't get to see things like channel keys. */
        if (check_kick(source, c))
            continue;

        flags = 0;

        if (check_should_op(source, c)) {
            flags |= CUMODE_OP;
            send_cmd(ncChanServ, "M %s +o %s%s", c->name, NumNick(source));
        } else if (check_should_voice(source, c)) {
            flags |= CUMODE_VOICE;
            send_cmd(ncChanServ, "M %s +v %s%s", c->name, NumNick(source));
        }
        add_user_to_channel(c, source, flags);

        if (c->ci && !(c->ci->flags & (CI_VERBOTEN)) && c->ci->entry_message) {
            notice(ncChanServ, source, " %s", c->ci->entry_message);
        }
    }
}

/*************************************************************************/

/* Handle a KICK command
 *
 *  source = Usuario
 *    av[0] = Canal
 *    av[1] = Numeric del nick
 *    av[ac-1] = Motivo del kick
 *
 */
void m_kick(struct NetClient *source, int ac, char **av)
{
    struct Channel *c;
    struct NetClient *who;

    if (ac < 1 || av[1][0] == '\0') {
        Debug((1, "KICK message: expecting 1 or more parameters after "
               "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    if (!(c = get_channel(source, av[0], 0)) ||
         !(who = FindNUser(av[1]))) {
         slog(logsvc, "user: KICK for nonexistent user %s on %s", av[1], av[0]);
         return;
    }

    if (!find_member_link(c, who)) {
        slog(logsvc, "Bug en KICK (Zombie): buf='%s'", inbuf);
        return;
    }

    Debug((1, "kicking %s from %s", who->name, c->name));

    remove_user_from_channel(who, c);
}

/*************************************************************************/

/* Handle a MODE command
 *
 *  source = Usuario o Servidor
 *   av[0] = Canal
 *   av[1...n.1] = parametros
 *
 */
static void m_cmode(struct NetClient *source, int ac, char **av, int opmode)
{
    struct Channel *chan;
    struct NetClient *nc;
    struct Membership *member;
    char *s, *nick;
    int flag, add = 1;          /* 1 if adding modes, 0 if deleting */
    char *modestr = av[1];
    char parabuf[256];

    if (ac < 2) {
        Debug((1, "MODE message: expecting 2 or more parameters after "
               "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    if (*av[0] != '#') {
        m_umode(source, ac, av);
        return;
    }

    chan = FindChannel(av[0]);
    if (!chan) {
        slog(logsvc, "channel: MODE %s for nonexistent channel %s",
                merge_args(ac-1, av+1), av[0]);
        return;
    }

    //if (!NoBouncyModes) {
    if (1 == 1) {
        time_t now = time(NULL);
        /* This shouldn't trigger on +o, etc. */
        if (!IsServer(source) && !modestr[strcspn(modestr, "bov")]) {
            if (now != chan->server_modetime) {
                chan->server_modecount = 0;
                chan->server_modetime = now;
            }
            chan->server_modecount++;
        }
    }

    *parabuf = '\0';
    s = modestr;
    ac -= 2;
    av += 2;

    while (*s) {
        char modechar = *s++;

        switch (modechar) {

            case '+':
                add = 1;
                break;

            case '-':
                add = 0;
                break;

            case 'k':
                if (--ac < 0) {
                    slog(logsvc, "channel: MODE %s %s: missing parameter for %ck",
                            chan->name, modestr, add ? '+' : '-');
                    break;
                }
                if (chan->key) {
                    sfree(chan->key);
                    chan->key = NULL;
                }
                if (add) {
                    chan->key = sstrdup(*av++);
                    chan->mode |= CMODE_k;
                    if (opmode) {
                        if (*parabuf != '\0')
                            strcat(parabuf, " ");
                        sprintf(parabuf, "%s%s", parabuf, chan->key);
                    }
                } else {
                    chan->mode &= ~CMODE_k;
                }
                break;

            case 'l':
                if (add) {
                    if (--ac < 0) {
                        slog(logsvc, "channel: MODE %s %s: missing parameter for +l",
                                chan->name, modestr);
                        break;
                    }
                    chan->limit = atoi(*av++);
                    chan->mode |= CMODE_l;
                    if (opmode) {
                        if (*parabuf != '\0')
                            strcat(parabuf, " ");
                        sprintf(parabuf, "%s%d", parabuf, chan->limit);
                    }
                } else {
                    chan->limit = 0;
                    chan->mode &= ~CMODE_l;
                }
                break;

            case 'b':
                {
                char *banstr = *av;

                if (--ac < 0) {
                    slog(logsvc, "channel: MODE %s %s: missing parameter for %cb",
                            chan->name, modestr, add ? '+' : '-');
                    break;
                }
                av++;

                if (add) {
                    add_ban(chan, source, banstr, 0);
                    if (opmode) {
                        if (*parabuf != '\0')
                            strcat(parabuf, " ");
                        sprintf(parabuf, "%s%s", parabuf, banstr);
                    }
                } else {
                    struct Ban *ban, *prev;

                    for (ban = chan->banlist, prev = NULL; ban; prev = ban, ban = ban->next) {
                       if (!irc_strcmp(ban->banstr, banstr)) {
                           delete_ban(chan, ban, prev);
                           break;
                       }
                    }

                    if (opmode) {
                        if (*parabuf != '\0')
                            strcat(parabuf, " ");
                        sprintf(parabuf, "%s%s", parabuf, banstr);
                    }
                }
                break;
                }

            case 'q':
                if (--ac < 0) {
                    slog(logsvc, "channel: MODE %s %s: missing parameter for %co",
                            chan->name, modestr, add ? '+' : '-');
                     break;
                }
                nick = *av++;
                nc = FindNUser(nick);
                if (!nc)
                    nc = FindUser(nick);

                if (!nc) {
                    slog(logsvc, "channel: MODE %s %cq for nonexistent user %s",
                            chan->name, add ? '+' : '-', nick);
                    break;
                }
                member = find_channel_member(nc, chan);
                if (!member) {
                    slog(logsvc, "channel: MODE %s %cq for user %s not in channel",
                            chan->name, add ? '+' : '-', nc->name);
                    break;
                }
                if (add) {
                    Debug((1, "Setting +q on %s for %s", chan->name, nc->name));

/*
                    if (!check_valid_owner(nc, chan, IsServer(source))) {
                        // No necesario, ya lo quita DDB
                        //send_cmd(ncChanServ, "M %s -q %s%s", chan->name, NumNick(nc));
                        break;
                    }
*/
                    member->status |= CUMODE_OWNER;
                } else {
                    member->status &= ~CUMODE_OWNER;
                }
                if (opmode) {
                    if (*parabuf != '\0')
                        strcat(parabuf, " ");
                    sprintf(parabuf, "%s%s", parabuf, nc->name);
                }

                break;

            case 'o':
                if (--ac < 0) {
                    slog(logsvc, "channel: MODE %s %s: missing parameter for %co",
                            chan->name, modestr, add ? '+' : '-');
                     break;
                }
                nick = *av++;
                nc = FindNUser(nick);
                if (!nc)
                    nc = FindUser(nick);

                if (!nc) {
                    slog(logsvc, "channel: MODE %s %co for nonexistent user %s",
                            chan->name, add ? '+' : '-', nick);
                    break;
                }
                member = find_channel_member(nc, chan);
                if (!member) {
                    slog(logsvc, "channel: MODE %s %co for user %s not in channel",
                            chan->name, add ? '+' : '-', nc->name);
                    break;
                }
                if (add) {
                    Debug((1, "Setting +o on %s for %s", chan->name, nc->name));

                    if (!check_valid_op(nc, chan, IsServer(source))) {
                        send_cmd(ncChanServ, "M %s -o %s%s", chan->name, NumNick(nc));
                        break;
                    }
                    member->status |= CUMODE_OP;
                } else {
                    member->status &= ~CUMODE_OP;
                }
                if (opmode) {
                    if (*parabuf != '\0')
                        strcat(parabuf, " ");
                    sprintf(parabuf, "%s%s", parabuf, nc->name);
                }
                break;

            case 'v':
                if (--ac < 0) {
                    slog(logsvc, "channel: MODE %s %s: missing parameter for %cv",
                            chan->name, modestr, add ? '+' : '-');
                    break;
                }
                nick = *av++;
                nc = FindNUser(nick);
                if (!nc)
                    nc = FindUser(nick);

                if (!nc) {
                    slog(logsvc, "channel: MODE %s %cv for nonexistent user %s",
                            chan->name, add ? '+' : '-', nick);
                    break;
                }
                member = find_channel_member(nc, chan);
                if (!member) {
                    slog(logsvc, "channel: MODE %s %cv for user %s not in channel",
                            chan->name, add ? '+' : '-', nc->name);
                    break;
                }
                if (add) {
                    Debug((1, "Setting +v on %s for %s", chan->name, nc->name));

/*
                    if (!check_valid_voice(nc, chan, IsServer(source))) {
                        send_cmd(ncChanServ, "M %s -v %s%s", chan->name, NumNick(nc));
                        break;
                    }
*/
                    member->status |= CUMODE_VOICE;
                } else {
                    member->status &= ~CUMODE_VOICE;
                }
                if (opmode) {
                    if (*parabuf != '\0')
                        strcat(parabuf, " ");
                    sprintf(parabuf, "%s%s", parabuf, nc->name);
                }
                break;

            default:
                flag = channel_mode_to_int(modechar);
                if (add)
                    chan->mode |= flag;
                else
                    chan->mode &= ~flag;
                break;
        } /* switch */

    } /* while (*s) */

    /* Check modes against ChanServ mode lock */
    check_modes(chan);
}

/*************************************************************************/

/* Handle a MODE command
 *
 *  source = Usuario o Servidor
 *   av[0] = Canal
 *   av[1...n.1] = parametros
 *
 */
void m_mode(struct NetClient *source, int ac, char **av)
{
    m_cmode(source, ac, av, 0);
}

/*************************************************************************/

/* Handle a OPMODE command
 *
 *  source = Usuario o Servidor
 *   av[0] = Canal
 *   av[1...n.1] = parametros
 *
 */
void m_opmode(struct NetClient *source, int ac, char **av)
{
    m_cmode(source, ac, av, 1);
}

/*************************************************************************/

/* Handle a PART command
 *
 *  source = Usuario
 *    av[0] = Canales a salir
 *    av[1] = Mensaje de ausencia (Opcional)
 *
 */
void m_part(struct NetClient *source, int ac, char **av)
{
    struct Channel *c;
    char *name, *p = 0;

    if (ac < 1 || av[0][0] == '\0') {
        Debug((1, "KICK message: expecting 1 or more parameters after "
               "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    for (name = irc_strtok(&p, av[0], ","); name;
         name = irc_strtok(&p, 0, ",")) {

        Debug((1, "%s leaves %s", source->name, name));

        c = get_channel(source, name, 0);
        if (!c || !find_member_link(c, source)) {
            /* Undernet es asi */
            //slog(logsvc, "Bug en PART : buf='%s'", inbuf);
            continue;
        }

        remove_user_from_channel(source, c);
    }
}

/*************************************************************************/

/* Handle a SVSJOIN command
 *
 *  source = Usuario
 *    av[0] = Mensaje de ausencia (Opcional)
 *
 */
void m_svsjoin(struct NetClient *source, int ac, char **av)
{
    /* Lo ignoramos */
}

/*************************************************************************/


/* Handle a SVSMODE command
 *
 *  source = Servidor
 *   av[0] = Canal
 *   av[1...n.1] = parametros
 *
 */
void m_svsmode(struct NetClient *source, int ac, char **av)
{
    if (ac < 2) {
        Debug((1, "SVSMODE message: expecting 2 or more parameters after "
               "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    if (*av[0] != '#') {
        m_umode(source, ac, av);
        return;
    }

    /* No hay soporte para canales */
    return;
}

/*************************************************************************/

/* Handle a SVSPART command
 *
 *  source = Usuario
 *    av[0] = Mensaje de ausencia (Opcional)
 *
 */
void m_svspart(struct NetClient *source, int ac, char **av)
{
    /* Lo ignoramos */
}

/*************************************************************************/

/* Handle a TOPIC command
 *
 *  source = Usuario
 *    av[0] = Canales
 *    av[1] = Timestamp canal (opcional)
 *    av[2] = Timestamp topic (opcional)
 *    av[ac-1] = Topic
 *
 * Toni Garc�a  -Zoltan-  1- Septiembre- 2000
 * Adaptacion a P10 Agosto 2018
 */
void m_topic(struct NetClient *source, int ac, char **av)
{
    struct Channel *c;
    char *name, *p = 0;
    time_t ts = 0;

    if (ac < 3) {
        Debug((1, "TOPIC message: expecting 3 or more parameters after "
               "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    for (; (name = irc_strtok(&p, av[0], ",")); av[0] = 0) {
        if (!(c = FindChannel(name))) {
            slog(logsvc, "channel: TOPIC %s for nonexistent channel %s",
                    merge_args(ac-1, av+1), name);
            continue;
        }

        if (check_topiclock(c))
            continue;

        if (ac > 2 && (ts = atoi(av[1])) && c->creation_time < ts)
            continue;

        if (ac > 3 && (ts = atoi(av[2])) && c->topic_time > ts)
            continue;

        strscpy(c->topic_setter, source->name, sizeof(c->topic_setter));
        c->topic_time = ts ? ts : time(NULL);

        if (c->topic) {
            sfree(c->topic);
            c->topic = NULL;
        }

        if (*av[ac - 1])
            c->topic = sstrdup(av[ac - 1]);

        if (IsUser(source))
            record_topic(c);
    }
}
