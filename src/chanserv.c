/*
 * ServicesIRCh - Services for IRCh, chanserv.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "chanserv.h"

#include "actions.h"
#include "channels.h"
#include "commands.h"
#include "datafiles.h"
#include "encrypt.h"
#include "hash.h"
#include "language.h"
#include "log.h"
#include "match.h"
#include "misc.h"
#include "netclient.h"
#include "nickserv.h"
#include "send.h"
#include "service.h"
#include "services.h"
#include "svc_memory.h"
#include "timeout.h"
#include "users.h"

#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/* provisional */
#include "operserv.h"
#include "memoserv.h"

/*************************************************************************/

struct NetClient *ncChanServ;

static struct ChannelInfo *chanlists[256];

static int def_levels[][2] = {
    { CA_AUTOOP,             5 },
    { CA_AUTOVOICE,          3 },
    { CA_AUTODEOP,          -1 },
    { CA_NOJOIN,            -2 },
    { CA_INVITE,             5 },
    { CA_AKICK,             10 },
    { CA_SET,   ACCESS_INVALID },
    { CA_CLEAR, ACCESS_INVALID },
    { CA_UNBAN,              5 },
    { CA_OPDEOP,             5 },
    { CA_ACCESS_LIST,        0 },
    { CA_ACCESS_CHANGE,      1 },
    { CA_MEMO,              10 },
    { -1 }
};

typedef struct {
    int what;
    char *name;
    int desc;
} LevelInfo;
static LevelInfo levelinfo[] = {
    { CA_AUTOOP,        "AUTOOP",     CHAN_LEVEL_AUTOOP },
    { CA_AUTOVOICE,     "AUTOVOICE",  CHAN_LEVEL_AUTOVOICE },
    { CA_AUTODEOP,      "AUTODEOP",   CHAN_LEVEL_AUTODEOP },
    { CA_NOJOIN,        "NOJOIN",     CHAN_LEVEL_NOJOIN },
    { CA_INVITE,        "INVITE",     CHAN_LEVEL_INVITE },
    { CA_AKICK,         "AKICK",      CHAN_LEVEL_AKICK },
    { CA_SET,           "SET",        CHAN_LEVEL_SET },
    { CA_CLEAR,         "CLEAR",      CHAN_LEVEL_CLEAR },
    { CA_UNBAN,         "UNBAN",      CHAN_LEVEL_UNBAN },
    { CA_OPDEOP,        "OPDEOP",     CHAN_LEVEL_OPDEOP },
    { CA_ACCESS_LIST,   "ACC-LIST",   CHAN_LEVEL_ACCESS_LIST },
    { CA_ACCESS_CHANGE, "ACC-CHANGE", CHAN_LEVEL_ACCESS_CHANGE },
    { CA_MEMO,          "MEMO",       CHAN_LEVEL_MEMO },
    { -1 }
};
static int levelinfo_maxwidth = 0;

/*************************************************************************/

static void alpha_insert_chan(struct ChannelInfo *ci);
static struct ChannelInfo *makechan(const char *chan);
static int delchan(struct ChannelInfo *ci);
static void reset_levels(struct ChannelInfo *ci);
static int is_founder(struct NetClient *nc, struct ChannelInfo *ci);
static int is_identified(struct NetClient *nc, struct ChannelInfo *ci);
static int get_access(struct NetClient *nc, struct ChannelInfo *ci);

static void do_help(struct NetClient *nc);
static void do_register(struct NetClient *nc);
static void do_identify(struct NetClient *nc);
static void do_drop(struct NetClient *nc);
static void do_set(struct NetClient *nc);
static void do_set_founder(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_successor(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_password(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_desc(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_url(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_email(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_entrymsg(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_topic(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_mlock(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_keeptopic(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_topiclock(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_private(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_secureops(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_leaveops(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_restricted(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_secure(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_opnotice(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_set_noexpire(struct NetClient *nc, struct ChannelInfo *ci, char *param);
static void do_access(struct NetClient *nc);
static void do_akick(struct NetClient *nc);
static void do_info(struct NetClient *nc);
static void do_list(struct NetClient *nc);
static void do_invite(struct NetClient *nc);
static void do_levels(struct NetClient *nc);
static void do_op(struct NetClient *nc);
static void do_deop(struct NetClient *nc);
static void do_unban(struct NetClient *nc);
static void do_clear(struct NetClient *nc);
static void do_getpass(struct NetClient *nc);
static void do_forbid(struct NetClient *nc);
static void do_status(struct NetClient *nc);

/*************************************************************************/

static struct Command cmds[] = {
    { "HELP",     do_help,     NULL,  -1,                       -1,-1,-1,-1 },
    { "REGISTER", do_register, NULL,  CHAN_HELP_REGISTER,       -1,-1,-1,-1 },
    { "IDENTIFY", do_identify, NULL,  CHAN_HELP_IDENTIFY,       -1,-1,-1,-1 },
    { "DROP",     do_drop,     NULL,  -1,
                CHAN_HELP_DROP, CHAN_SERVADMIN_HELP_DROP,
                CHAN_SERVADMIN_HELP_DROP, CHAN_SERVADMIN_HELP_DROP },
    { "SET",      do_set,      NULL,  CHAN_HELP_SET,
                -1, CHAN_SERVADMIN_HELP_SET,
                CHAN_SERVADMIN_HELP_SET, CHAN_SERVADMIN_HELP_SET },
    { "SET FOUNDER",    NULL,  NULL,  CHAN_HELP_SET_FOUNDER,    -1,-1,-1,-1 },
    { "SET SUCCESSOR",  NULL,  NULL,  CHAN_HELP_SET_SUCCESSOR,  -1,-1,-1,-1 },
    { "SET PASSWORD",   NULL,  NULL,  CHAN_HELP_SET_PASSWORD,   -1,-1,-1,-1 },
    { "SET DESC",       NULL,  NULL,  CHAN_HELP_SET_DESC,       -1,-1,-1,-1 },
    { "SET URL",        NULL,  NULL,  CHAN_HELP_SET_URL,        -1,-1,-1,-1 },
    { "SET EMAIL",      NULL,  NULL,  CHAN_HELP_SET_EMAIL,      -1,-1,-1,-1 },
    { "SET ENTRYMSG",   NULL,  NULL,  CHAN_HELP_SET_ENTRYMSG,   -1,-1,-1,-1 },
    { "SET TOPIC",      NULL,  NULL,  CHAN_HELP_SET_TOPIC,      -1,-1,-1,-1 },
    { "SET KEEPTOPIC",  NULL,  NULL,  CHAN_HELP_SET_KEEPTOPIC,  -1,-1,-1,-1 },
    { "SET TOPICLOCK",  NULL,  NULL,  CHAN_HELP_SET_TOPICLOCK,  -1,-1,-1,-1 },
    { "SET MLOCK",      NULL,  NULL,  CHAN_HELP_SET_MLOCK,      -1,-1,-1,-1 },
    { "SET RESTRICTED", NULL,  NULL,  CHAN_HELP_SET_RESTRICTED, -1,-1,-1,-1 },
    { "SET SECURE",     NULL,  NULL,  CHAN_HELP_SET_SECURE,     -1,-1,-1,-1 },
    { "SET SECUREOPS",  NULL,  NULL,  CHAN_HELP_SET_SECUREOPS,  -1,-1,-1,-1 },
    { "SET LEAVEOPS",   NULL,  NULL,  CHAN_HELP_SET_LEAVEOPS,   -1,-1,-1,-1 },
    { "SET OPNOTICE",   NULL,  NULL,  CHAN_HELP_SET_OPNOTICE,   -1,-1,-1,-1 },
    { "SET NOEXPIRE",   NULL,  NULL,  -1, -1,
                CHAN_SERVADMIN_HELP_SET_NOEXPIRE,
                CHAN_SERVADMIN_HELP_SET_NOEXPIRE,
                CHAN_SERVADMIN_HELP_SET_NOEXPIRE },
    { "ACCESS",   do_access,   NULL,  CHAN_HELP_ACCESS,         -1,-1,-1,-1 },
    { "ACCESS LEVELS",  NULL,  NULL,  CHAN_HELP_ACCESS_LEVELS,  -1,-1,-1,-1 },
    { "AKICK",    do_akick,    NULL,  CHAN_HELP_AKICK,          -1,-1,-1,-1 },
    { "LEVELS",   do_levels,   NULL,  CHAN_HELP_LEVELS,         -1,-1,-1,-1 },
    { "INFO",     do_info,     NULL,  CHAN_HELP_INFO,           
                -1, CHAN_SERVADMIN_HELP_INFO, CHAN_SERVADMIN_HELP_INFO, 
                CHAN_SERVADMIN_HELP_INFO },
    { "LIST",     do_list,     NULL,  -1,
                CHAN_HELP_LIST, CHAN_SERVADMIN_HELP_LIST,
                CHAN_SERVADMIN_HELP_LIST, CHAN_SERVADMIN_HELP_LIST },
    { "OP",       do_op,       NULL,  CHAN_HELP_OP,             -1,-1,-1,-1 },
    { "DEOP",     do_deop,     NULL,  CHAN_HELP_DEOP,           -1,-1,-1,-1 },
    { "INVITE",   do_invite,   NULL,  CHAN_HELP_INVITE,         -1,-1,-1,-1 },
    { "UNBAN",    do_unban,    NULL,  CHAN_HELP_UNBAN,          -1,-1,-1,-1 },
    { "CLEAR",    do_clear,    NULL,  CHAN_HELP_CLEAR,          -1,-1,-1,-1 },
    { "GETPASS",  do_getpass,  is_services_admin,  -1,
                -1, CHAN_SERVADMIN_HELP_GETPASS,
                CHAN_SERVADMIN_HELP_GETPASS, CHAN_SERVADMIN_HELP_GETPASS },
    { "FORBID",   do_forbid,   is_services_admin,  -1,
                -1, CHAN_SERVADMIN_HELP_FORBID,
                CHAN_SERVADMIN_HELP_FORBID, CHAN_SERVADMIN_HELP_FORBID },
    { "STATUS",   do_status,   is_services_admin,  -1,
                -1, CHAN_SERVADMIN_HELP_STATUS,
                CHAN_SERVADMIN_HELP_STATUS, CHAN_SERVADMIN_HELP_STATUS },
    { NULL }
};

/*************************************************************************/
/*************************************************************************/

/* Return information on memory use.  Assumes pointers are valid. */

void get_chanserv_stats(long *nrec, long *memuse)
{
    long count = 0, mem = 0;
    int i, j;
    struct ChannelInfo *ci;

    for (i = 0; i < 256; i++) {
        for (ci = chanlists[i]; ci; ci = ci->next) {
            count++;
            mem += sizeof(*ci);
            if (ci->desc)
                mem += strlen(ci->desc)+1;
            if (ci->url)
                mem += strlen(ci->url)+1;
            if (ci->email)
                mem += strlen(ci->email)+1;
            mem += ci->accesscount * sizeof(struct ChanAccess);
            mem += ci->akickcount * sizeof(struct AutoKick);
            for (j = 0; j < ci->akickcount; j++) {
                if (!ci->akick[j].is_nick && ci->akick[j].u.mask)
                    mem += strlen(ci->akick[j].u.mask)+1;
                if (ci->akick[j].reason)
                    mem += strlen(ci->akick[j].reason)+1;
            }
            if (ci->mlock_key)
                mem += strlen(ci->mlock_key)+1;
            if (ci->last_topic)
                mem += strlen(ci->last_topic)+1;
            if (ci->entry_message)
                mem += strlen(ci->entry_message)+1;
            if (ci->levels)
                mem += sizeof(*ci->levels) * CA_SIZE;
            mem += ci->memos.memocount * sizeof(struct Memo);
            for (j = 0; j < ci->memos.memocount; j++) {
                if (ci->memos.memos[j].text)
                    mem += strlen(ci->memos.memos[j].text)+1;
            }
        }
    }
    *nrec = count;
    *memuse = mem;
}

/*************************************************************************/
/*************************************************************************/

/* ChanServ initialization. */

void cs_init(void)
{
    struct Command *cmd;

    ncChanServ = service_create(s_ChanServ, NULL, NULL, desc_ChanServ);
    ncChanServ->service->has_priv = NULL;
    ncChanServ->service->func = chanserv;
    set_user_modes(&myService, ncChanServ, "+Bd");
    service_introduce(ncChanServ);

    cmd = lookup_cmd(cmds, "REGISTER");
    if (cmd)
        cmd->help_param1 = s_NickServ;
    cmd = lookup_cmd(cmds, "SET SECURE");
    if (cmd)
        cmd->help_param1 = s_NickServ;
    cmd = lookup_cmd(cmds, "SET SUCCESSOR");
    if (cmd)
        cmd->help_param1 = (char *)(long)CSMaxReg;
}

/*************************************************************************/

/* Main ChanServ routine. */

void chanserv(struct NetClient *source, char *buf)
{
    char *cmd, *s;

    cmd = strtok(buf, " ");

    if (!cmd) {
        return;
    } else if (strcasecmp(cmd, "\1PING") == 0) {
        if (!(s = strtok(NULL, "")))
            s = "\1";
        notice(ncChanServ, source, "\1PING %s", s);
    } else if (skeleton) {
        msg_lang(ncChanServ, source, SERVICE_OFFLINE, s_ChanServ);
    } else if (strcasecmp(cmd, "AOP") == 0 || strcasecmp(cmd, "SOP") == 0) {
        msg_lang(ncChanServ, source, CHAN_NO_AOP_SOP, s_ChanServ);
    } else {
        run_cmd(ncChanServ, source, cmds, cmd);
    }
}

/*************************************************************************/

/* Load/save data files. */


#define SAFE(x) do {                                        \
    if ((x) < 0) {                                        \
        if (!forceload)                                        \
            fatal("Read error on %s", ChanDBName);        \
        failed = 1;                                        \
        break;                                                \
    }                                                        \
} while (0)


/* Load v1-v4 files. */
static void load_old_cs_dbase(struct dbFILE *f, int ver)
{
    int i, j, c;
    struct ChannelInfo *ci, **last, *prev;
    struct NickInfo *ni;
    int failed = 0;

    struct {
        short level;
#ifdef COMPATIBILITY_V2
        short is_nick;
#else
        short in_use;
#endif
        char *name;
    } old_chanaccess;

    struct {
        short is_nick;
        short pad;
        char *name;
        char *reason;
    } old_autokick;

    struct {
        struct ChannelInfo *next, *prev;
        char name[CHANMAX];
        char founder[NICKMAX];
        char founderpass[PASSMAX];
        char *desc;
        time_t time_registered;
        time_t last_used;
        long accesscount;
        struct ChanAccess *access;
        long akickcount;
        struct AutoKick *akick;
        short mlock_on, mlock_off;
        long mlock_limit;
        char *mlock_key;
        char *last_topic;
        char last_topic_setter[NICKMAX];
        time_t last_topic_time;
        long flags;
        short *levels;
        char *url;
        char *email;
        struct channel_ *c;
    } old_channelinfo;


    for (i = 33; i < 256 && !failed; i++) {

        last = &chanlists[i];
        prev = NULL;
        while ((c = getc_db(f)) != 0) {
            if (c != 1)
                fatal("Invalid format in %s", ChanDBName);
            SAFE(read_variable(old_channelinfo, f));
            if (debug >= 3)
                slog(logsvc, "debug: load_old_cs_dbase: read channel %s",
                        old_channelinfo.name);
            ci = scalloc(1, sizeof(struct ChannelInfo));
            strscpy(ci->name, old_channelinfo.name, CHANMAX);
            ci->founder = findnick(old_channelinfo.founder);
            strscpy(ci->founderpass, old_channelinfo.founderpass, PASSMAX);
            ci->time_registered = old_channelinfo.time_registered;
            ci->last_used = old_channelinfo.last_used;
            ci->accesscount = old_channelinfo.accesscount;
            ci->akickcount = old_channelinfo.akickcount;
            ci->mlock_on = old_channelinfo.mlock_on;
            ci->mlock_off = old_channelinfo.mlock_off;
            ci->mlock_limit = old_channelinfo.mlock_limit;
            strscpy(ci->last_topic_setter,
                        old_channelinfo.last_topic_setter, NICKMAX);
            ci->last_topic_time = old_channelinfo.last_topic_time;
            ci->flags = old_channelinfo.flags;
#ifdef USE_ENCRYPTION
            if (!(ci->flags & (CI_ENCRYPTEDPW | CI_VERBOTEN))) {
                if (debug)
                    slog(logsvc, "debug: %s: encrypting password for %s on load",
                                s_ChanServ, ci->name);
                if (encrypt_in_place(ci->founderpass, PASSMAX) < 0)
                    fatal("%s: load database: Can't encrypt %s password!",
                                s_ChanServ, ci->name);
                ci->flags |= CI_ENCRYPTEDPW;
            }
#else
            if (ci->flags & CI_ENCRYPTEDPW) {
                /* Bail: it makes no sense to continue with encrypted
                 * passwords, since we won't be able to verify them */
                fatal("%s: load database: password for %s encrypted "
                          "but encryption disabled, aborting",
                          s_ChanServ, ci->name);
            }
#endif
            ni = ci->founder;
            if (ni) {
                if (ni->channelcount+1 > ni->channelcount)
                    ni->channelcount++;
                ni = getlink(ni);
                if (ni != ci->founder && ni->channelcount+1 > ni->channelcount)
                    ni->channelcount++;
            }
            SAFE(read_string(&ci->desc, f));
            if (!ci->desc)
                ci->desc = sstrdup("");
            if (old_channelinfo.url)
                SAFE(read_string(&ci->url, f));
            if (old_channelinfo.email)
                SAFE(read_string(&ci->email, f));
            if (old_channelinfo.mlock_key)
                SAFE(read_string(&ci->mlock_key, f));
            if (old_channelinfo.last_topic)
                SAFE(read_string(&ci->last_topic, f));

            if (ci->accesscount) {
                struct ChanAccess *access;
                char *s;

                access = smalloc(sizeof(struct ChanAccess) * ci->accesscount);
                ci->access = access;
                for (j = 0; j < ci->accesscount; j++, access++) {
                    SAFE(read_variable(old_chanaccess, f));
#ifdef COMPATIBILITY_V2
                    if (old_chanaccess.is_nick < 0)
                        access->in_use = 0;
                    else
                        access->in_use = old_chanaccess.is_nick;
#else
                    access->in_use = old_chanaccess.in_use;
#endif
                    access->level = old_chanaccess.level;
                }
                access = ci->access;
                for (j = 0; j < ci->accesscount; j++, access++) {
                    SAFE(read_string(&s, f));
                    if (s && access->in_use)
                        access->ni = findnick(s);
                    else
                        access->ni = NULL;
                    if (s)
                        free(s);
                    if (access->ni == NULL)
                        access->in_use = 0;
                }
            } else {
                ci->access = NULL;
            } /* if (ci->accesscount) */

            if (ci->akickcount) {
                struct AutoKick *akick;
                char *s;

                akick = smalloc(sizeof(struct AutoKick) * ci->akickcount);
                ci->akick = akick;
                for (j = 0; j < ci->akickcount; j++, akick++) {
                    SAFE(read_variable(old_autokick, f));
                    if (old_autokick.is_nick < 0) {
                        akick->in_use = 0;
                        akick->is_nick = 0;
                    } else {
                        akick->in_use = 1;
                        akick->is_nick = old_autokick.is_nick;
                    }
                    akick->reason = old_autokick.reason;
                }
                akick = ci->akick;
                for (j = 0; j < ci->akickcount; j++, akick++) {
                    SAFE(read_string(&s, f));
                    if (akick->is_nick) {
                        if (!(akick->u.ni = findnick(s)))
                            akick->in_use = akick->is_nick = 0;
                        free(s);
                    } else {
                        if (!(akick->u.mask = s))
                            akick->in_use = 0;
                    }
                    if (akick->reason)
                        SAFE(read_string(&akick->reason, f));
                    if (!akick->in_use) {
                        if (akick->is_nick) {
                            akick->u.ni = NULL;
                        } else {
                            sfree(akick->u.mask);
                            akick->u.mask = NULL;
                        }
                        if (akick->reason) {
                            sfree(akick->reason);
                            akick->reason = NULL;
                        }
                    }
                }
            } else {
                ci->akick = NULL;
            } /* if (ci->akickcount) */

            if (old_channelinfo.levels) {
                int16_t n_entries;
                ci->levels = NULL;
                reset_levels(ci);
                SAFE(read_int16(&n_entries, f));
#ifdef COMPATIBILITY_V2
                /* Ignore earlier, incompatible levels list */
                if (n_entries == 6) {
                    fseek(f, sizeof(short) * n_entries, SEEK_CUR);
                } else
#endif
                for (j = 0; j < n_entries; j++) {
                    short lev;
                    SAFE(read_variable(lev, f));
                    if (j < CA_SIZE)
                        ci->levels[j] = lev;
                }
            } else {
                reset_levels(ci);
            }

            ci->memos.memomax = MSMaxMemos;

            *last = ci;
            last = &ci->next;
            ci->prev = prev;
            prev = ci;

        } /* while (getc_db(f) != 0) */

        *last = NULL;

    } /* for (i) */
}


void load_cs_dbase(void)
{
    struct dbFILE *f;
    int ver, i, j, c;
    struct ChannelInfo *ci, **last, *prev;
    int failed = 0;

    if (!(f = open_db(s_ChanServ, ChanDBName, "r")))
        return;

    switch (ver = get_file_version(f)) {

      case 7:
      case 6:
      case 5:

        for (i = 0; i < 256 && !failed; i++) {
            int16_t tmp16;
            int32_t tmp32;
            int n_levels;
            char *s;

            last = &chanlists[i];
            prev = NULL;
            while ((c = getc_db(f)) != 0) {
                if (c != 1)
                    fatal("Invalid format in %s", ChanDBName);
                ci = smalloc(sizeof(struct ChannelInfo));
                *last = ci;
                last = &ci->next;
                ci->prev = prev;
                prev = ci;
                SAFE(read_buffer(ci->name, f));
                SAFE(read_string(&s, f));
                if (s)
                    ci->founder = findnick(s);
                else
                    ci->founder = NULL;
                if (ver >= 7) {
                    SAFE(read_string(&s, f));
                    if (s)
                        ci->successor = findnick(s);
                    else
                        ci->successor = NULL;
                } else {
                    ci->successor = NULL;
                }
                if (ver == 5 && ci->founder != NULL) {
                    /* Channel count incorrect in version 5 files */
                    ci->founder->channelcount++;
                }
                SAFE(read_buffer(ci->founderpass, f));
                SAFE(read_string(&ci->desc, f));
                if (!ci->desc)
                    ci->desc = sstrdup("");
                SAFE(read_string(&ci->url, f));
                SAFE(read_string(&ci->email, f));
                SAFE(read_int32(&tmp32, f));
                ci->time_registered = tmp32;
                SAFE(read_int32(&tmp32, f));
                ci->last_used = tmp32;
                SAFE(read_string(&ci->last_topic, f));
                SAFE(read_buffer(ci->last_topic_setter, f));
                SAFE(read_int32(&tmp32, f));
                ci->last_topic_time = tmp32;
                SAFE(read_int32(&ci->flags, f));
#ifdef USE_ENCRYPTION
                if (!(ci->flags & (CI_ENCRYPTEDPW | CI_VERBOTEN))) {
                    if (debug)
                        slog(logsvc, "debug: %s: encrypting password for %s on load",
                                s_ChanServ, ci->name);
                    if (encrypt_in_place(ci->founderpass, PASSMAX) < 0)
                        fatal("%s: load database: Can't encrypt %s password!",
                                s_ChanServ, ci->name);
                    ci->flags |= CI_ENCRYPTEDPW;
                }
#else
                if (ci->flags & CI_ENCRYPTEDPW) {
                    /* Bail: it makes no sense to continue with encrypted
                     * passwords, since we won't be able to verify them */
                    fatal("%s: load database: password for %s encrypted "
                          "but encryption disabled, aborting",
                          s_ChanServ, ci->name);
                }
#endif
                SAFE(read_int16(&tmp16, f));
                n_levels = tmp16;
                ci->levels = smalloc(2*CA_SIZE);
                reset_levels(ci);
                for (j = 0; j < n_levels; j++) {
                    if (j < CA_SIZE)
                        SAFE(read_int16(&ci->levels[j], f));
                    else
                        SAFE(read_int16(&tmp16, f));
                }

                SAFE(read_int16(&ci->accesscount, f));
                if (ci->accesscount) {
                    ci->access = scalloc(ci->accesscount, sizeof(struct ChanAccess));
                    for (j = 0; j < ci->accesscount; j++) {
                        SAFE(read_int16(&ci->access[j].in_use, f));
                        if (ci->access[j].in_use) {
                            SAFE(read_int16(&ci->access[j].level, f));
                            SAFE(read_string(&s, f));
                            if (s) {
                                ci->access[j].ni = findnick(s);
                                free(s);
                            }
                            if (ci->access[j].ni == NULL)
                                ci->access[j].in_use = 0;
                        }
                    }
                } else {
                    ci->access = NULL;
                }

                SAFE(read_int16(&ci->akickcount, f));
                if (ci->akickcount) {
                    ci->akick = scalloc(ci->akickcount, sizeof(struct AutoKick));
                    for (j = 0; j < ci->akickcount; j++) {
                        SAFE(read_int16(&ci->akick[j].in_use, f));
                        if (ci->akick[j].in_use) {
                            SAFE(read_int16(&ci->akick[j].is_nick, f));
                            SAFE(read_string(&s, f));
                            if (ci->akick[j].is_nick) {
                                ci->akick[j].u.ni = findnick(s);
                                if (!ci->akick[j].u.ni)
                                    ci->akick[j].in_use = 0;
                                free(s);
                            } else {
                                ci->akick[j].u.mask = s;
                            }
                            SAFE(read_string(&s, f));
                            if (ci->akick[j].in_use)
                                ci->akick[j].reason = s;
                            else if (s)
                                free(s);
                        }
                    }
                } else {
                    ci->akick = NULL;
                }

                SAFE(read_int16(&ci->mlock_on, f));
                SAFE(read_int16(&ci->mlock_off, f));
                SAFE(read_int32(&ci->mlock_limit, f));
                SAFE(read_string(&ci->mlock_key, f));

                SAFE(read_int16(&ci->memos.memocount, f));
                SAFE(read_int16(&ci->memos.memomax, f));
                if (ci->memos.memocount) {
                    struct Memo *memos;
                    memos = smalloc(sizeof(struct Memo) * ci->memos.memocount);
                    ci->memos.memos = memos;
                    for (j = 0; j < ci->memos.memocount; j++, memos++) {
                        SAFE(read_int32(&memos->number, f));
                        SAFE(read_int16(&memos->flags, f));
                        SAFE(read_int32(&tmp32, f));
                        memos->time = tmp32;
                        SAFE(read_buffer(memos->sender, f));
                        SAFE(read_string(&memos->text, f));
                    }
                }

                SAFE(read_string(&ci->entry_message, f));

                ci->c = NULL;
                
            } /* while (getc_db(f) != 0) */

            *last = NULL;

        } /* for (i) */

        break; /* case 5 and up */

      case 4:
      case 3:
      case 2:
      case 1:
        load_old_cs_dbase(f, ver);
        break;

      default:
        fatal("Unsupported version number (%d) on %s", ver, ChanDBName);

    } /* switch (version) */

    close_db(f);

    /* Check for non-forbidden channels with no founder */
    for (i = 0; i < 256; i++) {
        struct ChannelInfo *next;
        for (ci = chanlists[i]; ci; ci = next) {
            next = ci->next;
            if (!(ci->flags & CI_VERBOTEN) && !ci->founder) {
                slog(logsvc, "%s: database load: Deleting founderless channel %s",
                        s_ChanServ, ci->name);
                delchan(ci);
            }
        }
    }

}

#undef SAFE

/*************************************************************************/

#define SAFE(x) do {                                                \
    if ((x) < 0) {                                                \
        restore_db(f);                                                \
        log_perror("Write error on %s", ChanDBName);                \
        if (time(NULL) - lastwarn > WarningTimeout) {                \
            wallops(NULL, "Write error on %s: %s", ChanDBName,        \
                        strerror(errno));                        \
            lastwarn = time(NULL);                                \
        }                                                        \
        return;                                                        \
    }                                                                \
} while (0)

void save_cs_dbase(void)
{
    struct dbFILE *f;
    int i, j;
    struct ChannelInfo *ci;
    struct Memo *memos;
    static time_t lastwarn = 0;

    if (!(f = open_db(s_ChanServ, ChanDBName, "w")))
        return;

    for (i = 0; i < 256; i++) {
        int16_t tmp16;

        for (ci = chanlists[i]; ci; ci = ci->next) {
            SAFE(write_int8(1, f));
            SAFE(write_buffer(ci->name, f));
            if (ci->founder)
                SAFE(write_string(ci->founder->nick, f));
            else
                SAFE(write_string(NULL, f));
            if (ci->successor)
                SAFE(write_string(ci->successor->nick, f));
            else
                SAFE(write_string(NULL, f));
            SAFE(write_buffer(ci->founderpass, f));
            SAFE(write_string(ci->desc, f));
            SAFE(write_string(ci->url, f));
            SAFE(write_string(ci->email, f));
            SAFE(write_int32(ci->time_registered, f));
            SAFE(write_int32(ci->last_used, f));
            SAFE(write_string(ci->last_topic, f));
            SAFE(write_buffer(ci->last_topic_setter, f));
            SAFE(write_int32(ci->last_topic_time, f));
            SAFE(write_int32(ci->flags, f));

            tmp16 = CA_SIZE;
            SAFE(write_int16(tmp16, f));
            for (j = 0; j < CA_SIZE; j++)
                SAFE(write_int16(ci->levels[j], f));

            SAFE(write_int16(ci->accesscount, f));
            for (j = 0; j < ci->accesscount; j++) {
                SAFE(write_int16(ci->access[j].in_use, f));
                if (ci->access[j].in_use) {
                    SAFE(write_int16(ci->access[j].level, f));
                    SAFE(write_string(ci->access[j].ni->nick, f));
                }
            }

            SAFE(write_int16(ci->akickcount, f));
            for (j = 0; j < ci->akickcount; j++) {
                SAFE(write_int16(ci->akick[j].in_use, f));
                if (ci->akick[j].in_use) {
                    SAFE(write_int16(ci->akick[j].is_nick, f));
                    if (ci->akick[j].is_nick)
                        SAFE(write_string(ci->akick[j].u.ni->nick, f));
                    else
                        SAFE(write_string(ci->akick[j].u.mask, f));
                    SAFE(write_string(ci->akick[j].reason, f));
                }
            }

            SAFE(write_int16(ci->mlock_on, f));
            SAFE(write_int16(ci->mlock_off, f));
            SAFE(write_int32(ci->mlock_limit, f));
            SAFE(write_string(ci->mlock_key, f));

            SAFE(write_int16(ci->memos.memocount, f));
            SAFE(write_int16(ci->memos.memomax, f));
            memos = ci->memos.memos;
            for (j = 0; j < ci->memos.memocount; j++, memos++) {
                SAFE(write_int32(memos->number, f));
                SAFE(write_int16(memos->flags, f));
                SAFE(write_int32(memos->time, f));
                SAFE(write_buffer(memos->sender, f));
                SAFE(write_string(memos->text, f));
            }

            SAFE(write_string(ci->entry_message, f));

        } /* for (chanlists[i]) */

        SAFE(write_int8(0, f));

    } /* for (i) */

    close_db(f);
}

#undef SAFE

/*************************************************************************/

/* Check the current modes on a channel; if they conflict with a mode lock,
 * fix them. */

void check_modes(struct Channel *c)
{
    struct ChannelInfo *ci;
    char newmodes[64], *newkey = NULL;
    int32_t newlimit = 0;
    char *end = newmodes;
    int modes;
    int set_limit = 0, set_key = 0;

    if (skeleton)
        return;

    if (!c || c->bouncy_modes)
        return;

    /* Check for mode bouncing */
    if (c->server_modecount >= 3 && c->chanserv_modecount >= 3) {
        wallops(NULL, "Warning: unable to set modes on channel %s.  "
                "Are your servers' U:lines configured correctly?", c->name);
        slog(logsvc, "%s: Bouncy modes on channel %s", s_ChanServ, c->name);
        c->bouncy_modes = 1;
        return;
    }

    if (c->chanserv_modetime != time(NULL)) {
        c->chanserv_modecount = 0;
        c->chanserv_modetime = time(NULL);
    }
    c->chanserv_modecount++;

    if (!(ci = c->ci)) {
        return;
    }

    modes = ~c->mode & (ci->mlock_on | CMODE_r);

    end += snprintf(end, sizeof(newmodes)-(end-newmodes)-2,
                    "+%s", channel_modes_to_char(modes));

    c->mode |= modes;
    if (ci->mlock_limit && ci->mlock_limit != c->limit) {
        *end++ = 'l';
        newlimit = ci->mlock_limit;
        c->limit = newlimit;
        set_limit = 1;
    }
    if (ci->mlock_key) {
        if (c->key && strcmp(c->key, ci->mlock_key) != 0) {
            char *av[3];
            send_cmd(ncChanServ, "M %s -k %s", c->name, c->key);
            av[0] = sstrdup(c->name);
            av[1] = sstrdup("-k");
            av[2] = sstrdup(c->key);
            m_mode(ncChanServ, 3, av);
            sfree(av[0]);
            sfree(av[1]);
            sfree(av[2]);
            sfree(c->key);
            c->key = NULL;
        }
        if (!c->key) {
            *end++ = 'k';
            newkey = ci->mlock_key;
            c->key = sstrdup(newkey);
            set_key = 1;
        }
    }

    if (end[-1] == '+')
        end--;

    modes = c->mode & ci->mlock_off;
    end += snprintf(end, sizeof(newmodes)-(end-newmodes)-1,
                    "-%s", channel_modes_to_char(modes));

    c->mode &= ~modes;
    if (c->limit && (ci->mlock_off & CMODE_l)) {
        *end++ = 'l';
        c->limit = 0;
    }
    if (c->key && (ci->mlock_off & CMODE_k)) {
        *end++ = 'k';
        newkey = sstrdup(c->key);
        sfree(c->key);
        c->key = NULL;
        set_key = 1;
    }

    if (end[-1] == '-')
        end--;

    if (end == newmodes)
        return;
    *end = 0;

    if (set_limit) {
        if (set_key)
            send_cmd(ncChanServ, "M %s %s %d :%s", c->name,
                     newmodes, newlimit, newkey ? newkey : "");
        else
            send_cmd(ncChanServ, "M %s %s %d", c->name,
                     newmodes, newlimit);
    } else if (set_key) {
        send_cmd(ncChanServ, "M %s %s :%s", c->name,
                 newmodes, newkey ? newkey : "");
    } else {
        send_cmd(ncChanServ, "M %s %s", c->name, newmodes);
    }

    if (newkey && !c->key)
        free(newkey);
}

/*************************************************************************/

/* Check whether a user is allowed to be opped on a channel; if they
 * aren't, deop them.  If serverop is 1, the +o was done by a server.
 * Return 1 if the user is allowed to be opped, 0 otherwise. */

int check_valid_op(struct NetClient *nc, struct Channel *c, int serverop)
{
    struct ChannelInfo *ci;

    if (!c || !(ci = c->ci) || (ci->flags & CI_LEAVEOPS))
        return 1;

    if (is_oper(nc) || is_services_admin(nc))
        return 1;

    if (ci->flags & CI_VERBOTEN) {
        /* check_kick() will get them out; we needn't explain. */
        //send_cmd(ncChanServ, "M %s -o %s%s", c->name, NumNick(nc));
        return 0;
    }

    if (serverop && time(NULL)-start_time >= CSRestrictDelay
                                && !check_access(nc, ci, CA_AUTOOP)) {
        msg_lang(ncChanServ, nc, CHAN_IS_REGISTERED, s_ChanServ);
        //send_cmd(ncChanServ, "M %s -o %s%s", c->name, NumNick(nc));
        return 0;
    }

    if (check_access(nc, ci, CA_AUTODEOP)) {
#if 0        /* This is probably a bad idea.  People could flood other people,
         * and stuff. */
        notice(ncChanServ, nc, CHAN_NOT_ALLOWED_OP, chan);
#endif
        //send_cmd(ncChanServ, "M %s -o %s%s", c->name, NumNick(nc));
        return 0;
    }

    return 1;
}

/*************************************************************************/

/* Check whether a user should be opped on a channel, and if so, do it.
 * Return 1 if the user was opped, 0 otherwise.  (Updates the channel's
 * last used time if the user was opped.) */

int check_should_op(struct NetClient *nc, struct Channel *c)
{
    struct ChannelInfo *ci;

    if (!c || !(ci = c->ci))
        return 0;

    if ((ci->flags & CI_VERBOTEN) || *c->name == '+')
        return 0;

    if ((ci->flags & CI_SECURE) && !nick_identified(nc))
        return 0;

    if (check_access(nc, ci, CA_AUTOOP)) {
        //send_cmd(ncChanServ, "M %s +o %s%s", c->name, NumNick(nc));
        ci->last_used = time(NULL);
        return 1;
    }

    return 0;
}

/*************************************************************************/

/* Check whether a user should be voiced on a channel, and if so, do it.
 * Return 1 if the user was voiced, 0 otherwise. */

int check_should_voice(struct NetClient *nc, struct Channel *c)
{
    struct ChannelInfo *ci;

    if (!c || !(ci = c->ci))
        return 0;

    if ((ci->flags & CI_VERBOTEN) || *c->name == '+')
        return 0;

    if ((ci->flags & CI_SECURE) && !nick_identified(nc))
        return 0;

    if (check_access(nc, ci, CA_AUTOVOICE)) {
        //send_cmd(ncChanServ, "M %s +v %s%s", c->name, NumNick(nc));
        return 1;
    }

    return 0;
}

/*************************************************************************/

/* Tiny helper routine to get ChanServ out of a channel after it went in. */

static void timeout_leave(struct Timeout *to)
{
    char *chan = to->data;
    send_cmd(ncChanServ, "L %s", chan);
    sfree(to->data);
}


/* Check whether a user is permitted to be on a channel.  If so, return 0;
 * else, kickban the user with an appropriate message (could be either
 * AKICK or restricted access) and return 1.  Note that this is called
 * _before_ the user is added to internal channel lists (so do_kick() is
 * not called).
 */

int check_kick(struct NetClient *nc, struct Channel *c)
{
    struct ChannelInfo *ci;
    struct AutoKick *akick;
    int i;
    struct NickInfo *ni;
    char *av[3], *mask;
    const char *reason;
    struct Timeout *t;
    int stay;

    if (!c || !(ci = c->ci))
        return 0;

    if (is_oper(nc) || is_services_admin(nc))
        return 0;

    if (ci->flags & CI_VERBOTEN) {
        mask = create_mask_ban(nc);
        reason = getstring(nc->user->ni, CHAN_MAY_NOT_BE_USED);
        goto kick;
    }

    if (nick_recognized(nc))
        ni = nc->user->ni;
    else
        ni = NULL;

    for (akick = ci->akick, i = 0; i < ci->akickcount; akick++, i++) {
        if (!akick->in_use)
            continue;
        if ((akick->is_nick && getlink(akick->u.ni) == ni)
                || (!akick->is_nick && match_usermask(akick->u.mask, nc))
        ) {
            Debug((2, "%s matched akick %s", nc->name,
                      akick->is_nick ? akick->u.ni->nick : akick->u.mask));
            mask = akick->is_nick ? create_mask_ban(nc) : sstrdup(akick->u.mask);
            reason = akick->reason ? akick->reason : CSAutokickReason;
            goto kick;
        }
    }

    if (time(NULL)-start_time >= CSRestrictDelay
                                && check_access(nc, ci, CA_NOJOIN)) {
        mask = create_mask_ban(nc);
        reason = getstring(nc->user->ni, CHAN_NOT_ALLOWED_TO_JOIN);
        goto kick;
    }

    return 0;

kick:
    Debug((1, "channel: AutoKicking %s!%s@%s",
              nc->name, nc->user->username, nc->user->host));

    av[0] = sstrdup(c->name);
    av[1] = sstrdup("+b");
    av[2] = mask;

    send_cmd(ncChanServ, "M %s +b %s", c->name, av[2]);
    m_mode(ncChanServ, 3, av);
    sfree(av[0]);
    sfree(av[1]);
    sfree(av[2]);

    send_cmd(ncChanServ, "K %s %s%s :%s", c->name, NumNick(nc), reason);

    return 1;
}

/*************************************************************************/

/* Record the current channel topic in the ChannelInfo structure. */

void record_topic(struct Channel *c)
{
    struct ChannelInfo *ci;

    if (readonly)
        return;
    if (!c || !(ci = c->ci))
        return;
    if (ci->last_topic)
        sfree(ci->last_topic);
    if (c->topic)
        ci->last_topic = sstrdup(c->topic);
    else
        ci->last_topic = NULL;
    strscpy(ci->last_topic_setter, c->topic_setter, NICKMAX);
    ci->last_topic_time = c->topic_time;
}

/*************************************************************************/

/* Restore the topic in a channel when it's created, if we should. */

void restore_topic(struct Channel *c)
{
    struct ChannelInfo *ci;

    if (!c || !(ci = c->ci) || !(ci->flags & CI_KEEPTOPIC))
        return;
    if (c->topic)
        sfree(c->topic);
    if (ci->last_topic) {
        c->topic = sstrdup(ci->last_topic);
        strscpy(c->topic_setter, ci->last_topic_setter, NICKMAX);
        c->topic_time = ci->last_topic_time;
    } else {
        c->topic = NULL;
        strscpy(c->topic_setter, s_ChanServ, NICKMAX);
    }

    send_cmd(ncChanServ, "T %s %lu %lu :%s", c->name,
             c->creation_time, c->topic_time, c->topic ? c->topic : "");
}

/*************************************************************************/

/* See if the topic is locked on the given channel, and return 1 (and fix
 * the topic) if so. */

int check_topiclock(struct Channel *c)
{
    struct ChannelInfo *ci;

    if (!c || !(ci = c->ci) || !(ci->flags & CI_TOPICLOCK))
        return 0;
    if (c->topic)
        free(c->topic);
    if (ci->last_topic)
        c->topic = sstrdup(ci->last_topic);
    else
        c->topic = NULL;
    strscpy(c->topic_setter, ci->last_topic_setter, NICKMAX);
    c->topic_time = ci->last_topic_time;

    send_cmd(ncChanServ, "T %s %lu %lu :%s", c->name,
             c->creation_time, c->topic_time, c->topic ? c->topic : "");
    return 1;
}

/*************************************************************************/

/* Remove all channels which have expired. */

void expire_chans()
{
    struct ChannelInfo *ci, *next;
    struct Channel *c;
    int i;
    time_t now = time(NULL);

    if (!CSExpire)
        return;

    for (i = 0; i < 256; i++) {
        for (ci = chanlists[i]; ci; ci = next) {
            next = ci->next;
            if (now - ci->last_used >= CSExpire
                        && !(ci->flags & (CI_VERBOTEN | CI_NO_EXPIRE))) {
                slog(logsvc, "Expiring channel %s", ci->name);
                delchan(ci);
            }
        }
    }
}

/*************************************************************************/

/* Remove a (deleted or expired) nickname from all channel lists. */

void cs_remove_nick(const struct NickInfo *ni)
{
    int i, j;
    struct ChannelInfo *ci, *next;
    struct ChanAccess *ca;
    struct AutoKick *akick;

    for (i = 0; i < 256; i++) {
        for (ci = chanlists[i]; ci; ci = next) {
            next = ci->next;
            if (ci->founder == ni) {
                if (ci->successor) {
                    struct NickInfo *ni2 = ci->successor;
                    if (ni2->channelcount >= ni2->channelmax) {
                        slog(logsvc, "%s: Successor (%s) of %s owns too many channels, "
                            "deleting channel",
                            s_ChanServ, ni2->nick, ci->name);
                        delchan(ci);
                    } else {
                        slog(logsvc, "%s: Transferring foundership of %s from deleted "
                            "nick %s to successor %s",
                            s_ChanServ, ci->name, ni->nick, ni2->nick);
                        ci->founder = ni2;
                        ci->successor = NULL;
                        if (ni2->channelcount+1 > ni2->channelcount)
                            ni2->channelcount++;
                    }
                } else {
                    slog(logsvc, "%s: Deleting channel %s owned by deleted nick %s",
                                s_ChanServ, ci->name, ni->nick);
                    delchan(ci);
                }
                continue;
            }
            for (ca = ci->access, j = ci->accesscount; j > 0; ca++, j--) {
                if (ca->in_use && ca->ni == ni) {
                    ca->in_use = 0;
                    ca->ni = NULL;
                }
            }
            for (akick = ci->akick, j = ci->akickcount; j > 0; akick++, j--) {
                if (akick->is_nick && akick->u.ni == ni) {
                    akick->in_use = akick->is_nick = 0;
                    akick->u.ni = NULL;
                    if (akick->reason) {
                        free(akick->reason);
                        akick->reason = NULL;
                    }
                }
            }
        }
    }
}

/*************************************************************************/

/* Return the ChannelInfo structure for the given channel, or NULL if the
 * channel isn't registered. */

struct ChannelInfo *cs_findchan(const char *chan)
{
    struct ChannelInfo *ci;

    for (ci = chanlists[tolower(chan[1])]; ci; ci = ci->next) {
        if (strcasecmp(ci->name, chan) == 0)
            return ci;
    }
    return NULL;
}

/*************************************************************************/

/* Return 1 if the user's access level on the given channel falls into the
 * given category, 0 otherwise.  Note that this may seem slightly confusing
 * in some cases: for example, check_access(..., CA_NOJOIN) returns true if
 * the user does _not_ have access to the channel (i.e. matches the NOJOIN
 * criterion). */

int check_access(struct NetClient *nc, struct ChannelInfo *ci, int what)
{
    int level = get_access(nc, ci);
    int limit = ci->levels[what];

    if (level == ACCESS_FOUNDER)
        return (what==CA_AUTODEOP || what==CA_NOJOIN) ? 0 : 1;
    /* Hacks to make flags work */
    if (what == CA_AUTODEOP && (ci->flags & CI_SECUREOPS) && level == 0)
        return 1;
    if (limit == ACCESS_INVALID)
        return 0;
    if (what == CA_AUTODEOP || what == CA_NOJOIN)
        return level <= ci->levels[what];
    else
        return level >= ci->levels[what];
}

/*************************************************************************/
/*********************** ChanServ private routines ***********************/
/*************************************************************************/

/* Insert a channel alphabetically into the database. */

static void alpha_insert_chan(struct ChannelInfo *ci)
{
    struct ChannelInfo *ptr, *prev;
    char *chan = ci->name;

    for (prev = NULL, ptr = chanlists[tolower(chan[1])];
                        ptr != NULL && strcasecmp(ptr->name, chan) < 0;
                        prev = ptr, ptr = ptr->next)
        ;
    ci->prev = prev;
    ci->next = ptr;
    if (!prev)
        chanlists[tolower(chan[1])] = ci;
    else
        prev->next = ci;
    if (ptr)
        ptr->prev = ci;
}

/*************************************************************************/

/* Add a channel to the database.  Returns a pointer to the new ChannelInfo
 * structure if the channel was successfully registered, NULL otherwise.
 * Assumes channel does not already exist. */

static struct ChannelInfo *makechan(const char *chan)
{
    struct ChannelInfo *ci;

    ci = scalloc(sizeof(struct ChannelInfo), 1);
    strscpy(ci->name, chan, CHANMAX);
    ci->time_registered = time(NULL);
    reset_levels(ci);
    alpha_insert_chan(ci);
    return ci;
}

/*************************************************************************/

/* Remove a channel from the ChanServ database.  Return 1 on success, 0
 * otherwise. */

static int delchan(struct ChannelInfo *ci)
{
    int i;
    struct NickInfo *ni = ci->founder;

    if (ci->c)
        ci->c->ci = NULL;
    if (ci->next)
        ci->next->prev = ci->prev;
    if (ci->prev)
        ci->prev->next = ci->next;
    else
        chanlists[tolower(ci->name[1])] = ci->next;
    if (ci->desc)
        free(ci->desc);
    if (ci->mlock_key)
        free(ci->mlock_key);
    if (ci->last_topic)
        free(ci->last_topic);
    if (ci->access)
        free(ci->access);
    for (i = 0; i < ci->akickcount; i++) {
        if (!ci->akick[i].is_nick && ci->akick[i].u.mask)
            sfree(ci->akick[i].u.mask);
        if (ci->akick[i].reason)
            sfree(ci->akick[i].reason);
    }
    if (ci->akick)
        free(ci->akick);
    if (ci->levels)
        free(ci->levels);
    if (ci->memos.memos) {
        for (i = 0; i < ci->memos.memocount; i++) {
            if (ci->memos.memos[i].text)
                free(ci->memos.memos[i].text);
        }
        free(ci->memos.memos);
    }
    sfree(ci);
    while (ni) {
        if (ni->channelcount > 0)
            ni->channelcount--;
        ni = ni->link;
    }
    return 1;
}

/*************************************************************************/

/* Reset channel access level values to their default state. */

static void reset_levels(struct ChannelInfo *ci)
{
    int i;

    if (ci->levels)
        free(ci->levels);
    ci->levels = smalloc(CA_SIZE * sizeof(*ci->levels));
    for (i = 0; def_levels[i][0] >= 0; i++)
        ci->levels[def_levels[i][0]] = def_levels[i][1];
}

/*************************************************************************/

/* Does the given user have founder access to the channel? */

static int is_founder(struct NetClient *nc, struct ChannelInfo *ci)
{
    if (nc->user->ni == getlink(ci->founder)) {
        if ((nick_identified(nc) ||
                 (nick_recognized(nc) && !(ci->flags & CI_SECURE))))
            return 1;
    }
    if (is_identified(nc, ci))
        return 1;
    return 0;
}

/*************************************************************************/

/* Has the given user password-identified as founder for the channel? */

static int is_identified(struct NetClient *nc, struct ChannelInfo *ci)
{
    struct u_chaninfolist *c;

    for (c = nc->user->founder_chans; c; c = c->next) {
        if (c->chan == ci)
            return 1;
    }
    return 0;
}

/*************************************************************************/

/* Return the access level the given user has on the channel.  If the
 * channel doesn't exist, the user isn't on the access list, or the channel
 * is CS_SECURE and the user hasn't IDENTIFY'd with NickServ, return 0. */

static int get_access(struct NetClient *nc, struct ChannelInfo *ci)
{
    struct NickInfo *ni = nc->user->ni;
    struct ChanAccess *access;
    int i;

    if (!ci || !ni)
        return 0;
    if (is_founder(nc, ci))
        return ACCESS_FOUNDER;
    if (nick_identified(nc)
        || (nick_recognized(nc) && !(ci->flags & CI_SECURE))
    ) {
        for (access = ci->access, i = 0; i < ci->accesscount; access++, i++) {
            if (access->in_use && getlink(access->ni) == ni)
                return access->level;
        }
    }
    return 0;
}

/*************************************************************************/

char *get_mlock(int mlock_on, int mlock_off, char *mlock_key, int mlock_limit, int showkeylimit)
{
    static char modebuf[32];
    char *end;

    end = modebuf;
    *end = 0;

    if (mlock_on || mlock_key || mlock_limit)
        end += sprintf(end, "+%s%s%s", channel_modes_to_char(mlock_on),
                       (mlock_key) ? "k" : "",
                       (mlock_limit) ? "l" : "");

    if (mlock_off)
        end += sprintf(end, "-%s", channel_modes_to_char(mlock_off));

    if (*modebuf) {
        if (showkeylimit) {
            if (mlock_key)
                end += sprintf(end, " %s", mlock_key);
            if (mlock_limit)
                end += sprintf(end, " %d", mlock_limit);
        }

        return modebuf;
    } else {
        *end = 0;
        return modebuf;
    }
}

/*************************************************************************/
/*********************** ChanServ command routines ***********************/
/*************************************************************************/

static void do_help(struct NetClient *nc)
{
    char *cmd = strtok(NULL, "");

    if (!cmd) {
        msg_help(ncChanServ, nc, CHAN_HELP);
        if (CSExpire >= 86400)
            msg_help(ncChanServ, nc, CHAN_HELP_EXPIRES, CSExpire/86400);
        if (is_services_oper(nc))
            msg_help(ncChanServ, nc, CHAN_SERVADMIN_HELP);
    } else if (strcasecmp(cmd, "LEVELS DESC") == 0) {
        int i;
        msg_help(ncChanServ, nc, CHAN_HELP_LEVELS_DESC);
        if (!levelinfo_maxwidth) {
            for (i = 0; levelinfo[i].what >= 0; i++) {
                int len = strlen(levelinfo[i].name);
                if (len > levelinfo_maxwidth)
                    levelinfo_maxwidth = len;
            }
        }
        for (i = 0; levelinfo[i].what >= 0; i++) {
            msg_help(ncChanServ, nc, CHAN_HELP_LEVELS_DESC_FORMAT,
                        levelinfo_maxwidth, levelinfo[i].name,
                        getstring(nc->user->ni, levelinfo[i].desc));
        }
    } else {
        help_cmd(ncChanServ, nc, cmds, cmd);
    }
}

/*************************************************************************/

static void do_register(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    char *desc = strtok(NULL, "");
    struct NickInfo *ni = nc->user->ni;
    struct Channel *c;
    struct ChannelInfo *ci;
    struct u_chaninfolist *uc;
#ifdef USE_ENCRYPTION
    char founderpass[PASSMAX+1];
#endif

    if (readonly) {
        msg_lang(ncChanServ, nc, CHAN_REGISTER_DISABLED);
        return;
    }

    if (!desc) {
        syntax_error(ncChanServ, nc, "REGISTER", CHAN_REGISTER_SYNTAX);
    } else if (*chan == '&') {
        msg_lang(ncChanServ, nc, CHAN_REGISTER_NOT_LOCAL);
    } else if (!ni) {
        msg_lang(ncChanServ, nc, CHAN_MUST_REGISTER_NICK, s_NickServ);
    } else if (!nick_recognized(nc)) {
        msg_lang(ncChanServ, nc, CHAN_MUST_IDENTIFY_NICK,
                s_NickServ, s_NickServ);

    } else if ((ci = cs_findchan(chan)) != NULL) {
        if (ci->flags & CI_VERBOTEN) {
            slog(logsvc, "%s: Attempt to register FORBIDden channel %s by %s!%s@%s",
                        s_ChanServ, ci->name, nc->name, nc->user->username, nc->user->host);
            msg_lang(ncChanServ, nc, CHAN_MAY_NOT_BE_REGISTERED, chan);
        } else {
            msg_lang(ncChanServ, nc, CHAN_ALREADY_REGISTERED, chan);
        }

    } else if (ni->channelmax > 0 && ni->channelcount >= ni->channelmax) {
        msg_lang(ncChanServ, nc,
                ni->channelcount > ni->channelmax
                                        ? CHAN_EXCEEDED_CHANNEL_LIMIT
                                        : CHAN_REACHED_CHANNEL_LIMIT,
                ni->channelmax);

    } else if (!(c = FindChannel(chan))) {
        slog(logsvc, "%s: Channel %s not found for REGISTER", s_ChanServ, chan);
        msg_lang(ncChanServ, nc, CHAN_REGISTRATION_FAILED);

    } else if (!is_chan_op(nc, c)) {
        msg_lang(ncChanServ, nc, CHAN_MUST_BE_CHANOP);

    } else if (!(ci = makechan(chan))) {
        slog(logsvc, "%s: makechan() failed for REGISTER %s", s_ChanServ, chan);
        msg_lang(ncChanServ, nc, CHAN_REGISTRATION_FAILED);

#ifdef USE_ENCRYPTION
    } else if (strscpy(founderpass, pass, PASSMAX+1),
               encrypt_in_place(founderpass, PASSMAX) < 0) {
        slog(logsvc, "%s: Couldn't encrypt password for %s (REGISTER)",
                s_ChanServ, chan);
        msg_lang(ncChanServ, nc, CHAN_REGISTRATION_FAILED);
        delchan(ci);
#endif

    } else {
        c->ci = ci;
        ci->c = c;
        ci->flags = CI_KEEPTOPIC | CI_SECURE;
        ci->mlock_on = CMODE_n | CMODE_t;
        ci->memos.memomax = MSMaxMemos;
        ci->last_used = ci->time_registered;
        ci->founder = nc->user->real_ni;
#ifdef USE_ENCRYPTION
        if (strlen(pass) > PASSMAX)
            msg_lang(ncChanServ, nc, PASSWORD_TRUNCATED, PASSMAX);
        memset(pass, 0, strlen(pass));
        memcpy(ci->founderpass, founderpass, PASSMAX);
        ci->flags |= CI_ENCRYPTEDPW;
#else
        if (strlen(pass) > PASSMAX-1) /* -1 for null byte */
            msg_lang(ncChanServ, nc, PASSWORD_TRUNCATED, PASSMAX-1);
        strscpy(ci->founderpass, pass, PASSMAX);
#endif
        ci->desc = sstrdup(desc);
        if (c->topic) {
            ci->last_topic = sstrdup(c->topic);
            strscpy(ci->last_topic_setter, c->topic_setter, NICKMAX);
            ci->last_topic_time = c->topic_time;
        }
        ni = ci->founder;
        while (ni) {
            if (ni->channelcount+1 > ni->channelcount)  /* Avoid wraparound */
                ni->channelcount++;
            ni = ni->link;
        }
        slog(logsvc, "%s: Channel %s registered by %s!%s@%s", s_ChanServ, chan,
                nc->name, nc->user->username, nc->user->host);
        msg_lang(ncChanServ, nc, CHAN_REGISTERED, chan, nc->name);
#ifndef USE_ENCRYPTION
        msg_lang(ncChanServ, nc, CHAN_PASSWORD_IS, ci->founderpass);
#endif
        uc = smalloc(sizeof(*uc));
        uc->next = nc->user->founder_chans;
        uc->prev = NULL;
        if (nc->user->founder_chans)
            nc->user->founder_chans->prev = uc;
        nc->user->founder_chans = uc;
        uc->chan = ci;
        /* Implement new mode lock */
        check_modes(c);
    }
}

/*************************************************************************/

static void do_identify(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    struct ChannelInfo *ci;
    struct u_chaninfolist *uc;

    if (!pass) {
        syntax_error(ncChanServ, nc, "IDENTIFY", CHAN_IDENTIFY_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else {
        int res;

        if ((res = check_password(pass, ci->founderpass)) == 1) {
            if (!is_identified(nc, ci)) {
                uc = smalloc(sizeof(*uc));
                uc->next = nc->user->founder_chans;
                uc->prev = NULL;
                if (nc->user->founder_chans)
                    nc->user->founder_chans->prev = uc;
                nc->user->founder_chans = uc;
                uc->chan = ci;
                slog(logsvc, "%s: %s!%s@%s identified for %s", s_ChanServ,
                        nc->name, nc->user->username, nc->user->host, ci->name);
            }
            msg_lang(ncChanServ, nc, CHAN_IDENTIFY_SUCCEEDED, chan);
        } else if (res < 0) {
            slog(logsvc, "%s: check_password failed for %s", s_ChanServ, ci->name);
            msg_lang(ncChanServ, nc, CHAN_IDENTIFY_FAILED);
        } else {
            slog(logsvc, "%s: Failed IDENTIFY for %s by %s!%s@%s",
                        s_ChanServ, ci->name, nc->name, nc->user->username, nc->user->host);
            msg_lang(ncChanServ, nc, PASSWORD_INCORRECT);
            bad_password(nc);
        }

    }
}

/*************************************************************************/

static void do_drop(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    struct ChannelInfo *ci;
    struct NickInfo *ni;
    struct Channel *c;
    int is_servadmin = is_services_admin(nc);

    if (readonly && !is_servadmin) {
        msg_lang(ncChanServ, nc, CHAN_DROP_DISABLED);
        return;
    }

    if (!chan) {
        syntax_error(ncChanServ, nc, "DROP", CHAN_DROP_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (!is_servadmin & (ci->flags & CI_VERBOTEN)) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!is_servadmin && !is_identified(nc, ci)) {
        msg_lang(ncChanServ, nc, CHAN_IDENTIFY_REQUIRED, s_ChanServ, chan);
    } else {
        if (readonly)  /* in this case we know they're a Services admin */
            msg_lang(ncChanServ, nc, READ_ONLY_MODE);
        ni = ci->founder;
        if (ni) {   /* This might be NULL (i.e. after FORBID) */
            if (ni->channelcount > 0)
                ni->channelcount--;
            ni = getlink(ni);
            if (ni != ci->founder && ni->channelcount > 0)
                ni->channelcount--;
        }
        slog(logsvc, "%s: Channel %s dropped by %s!%s@%s", s_ChanServ, ci->name,
                        nc->name, nc->user->username, nc->user->host);
        delchan(ci);
        msg_lang(ncChanServ, nc, CHAN_DROPPED, chan);
    }
}

/*************************************************************************/

/* Main SET routine.  Calls other routines as follows:
 *        do_set_command(User *command_sender, struct ChannelInfo *ci, char *param);
 * The parameter passed is the first space-delimited parameter after the
 * option name, except in the case of DESC, TOPIC, and ENTRYMSG, in which
 * it is the entire remainder of the line.  Additional parameters beyond
 * the first passed in the function call can be retrieved using
 * strtok(NULL, toks).
 */
static void do_set(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *cmd  = strtok(NULL, " ");
    char *param;
    struct ChannelInfo *ci;
    int is_servadmin = is_services_admin(nc);

    if (readonly) {
        msg_lang(ncChanServ, nc, CHAN_SET_DISABLED);
        return;
    }

    if (cmd) {
        if (strcasecmp(cmd, "DESC") == 0 || strcasecmp(cmd, "TOPIC") == 0
         || strcasecmp(cmd, "ENTRYMSG") == 0)
            param = strtok(NULL, "");
        else
            param = strtok(NULL, " ");
    } else {
        param = NULL;
    }

    if (!param && (!cmd || (strcasecmp(cmd, "SUCCESSOR") != 0 &&
                            strcasecmp(cmd, "URL") != 0 &&
                            strcasecmp(cmd, "EMAIL") != 0 &&
                            strcasecmp(cmd, "ENTRYMSG") != 0))) {
        syntax_error(ncChanServ, nc, "SET", CHAN_SET_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!is_servadmin && !check_access(nc, ci, CA_SET)) {
        msg_lang(ncChanServ, nc, ACCESS_DENIED);
    } else if (strcasecmp(cmd, "FOUNDER") == 0) {
        if (!is_servadmin && get_access(nc, ci) < ACCESS_FOUNDER) {
            msg_lang(ncChanServ, nc, CHAN_IDENTIFY_REQUIRED, s_ChanServ,chan);
        } else {
            do_set_founder(nc, ci, param);
        }
    } else if (strcasecmp(cmd, "SUCCESSOR") == 0) {
        if (!is_servadmin && get_access(nc, ci) < ACCESS_FOUNDER) {
            msg_lang(ncChanServ, nc, CHAN_IDENTIFY_REQUIRED, s_ChanServ,chan);
        } else {
            do_set_successor(nc, ci, param);
        }
    } else if (strcasecmp(cmd, "PASSWORD") == 0) {
        if (!is_servadmin && get_access(nc, ci) < ACCESS_FOUNDER) {
            msg_lang(ncChanServ, nc, CHAN_IDENTIFY_REQUIRED, s_ChanServ,chan);
        } else {
            do_set_password(nc, ci, param);
        }
    } else if (strcasecmp(cmd, "DESC") == 0) {
        do_set_desc(nc, ci, param);
    } else if (strcasecmp(cmd, "URL") == 0) {
        do_set_url(nc, ci, param);
    } else if (strcasecmp(cmd, "EMAIL") == 0) {
        do_set_email(nc, ci, param);
    } else if (strcasecmp(cmd, "ENTRYMSG") == 0) {
        do_set_entrymsg(nc, ci, param);
    } else if (strcasecmp(cmd, "TOPIC") == 0) {
        do_set_topic(nc, ci, param);
    } else if (strcasecmp(cmd, "MLOCK") == 0) {
        do_set_mlock(nc, ci, param);
    } else if (strcasecmp(cmd, "KEEPTOPIC") == 0) {
        do_set_keeptopic(nc, ci, param);
    } else if (strcasecmp(cmd, "TOPICLOCK") == 0) {
        do_set_topiclock(nc, ci, param);
    } else if (strcasecmp(cmd, "PRIVATE") == 0) {
        do_set_private(nc, ci, param);
    } else if (strcasecmp(cmd, "SECUREOPS") == 0) {
        do_set_secureops(nc, ci, param);
    } else if (strcasecmp(cmd, "LEAVEOPS") == 0) {
        do_set_leaveops(nc, ci, param);
    } else if (strcasecmp(cmd, "RESTRICTED") == 0) {
        do_set_restricted(nc, ci, param);
    } else if (strcasecmp(cmd, "SECURE") == 0) {
        do_set_secure(nc, ci, param);
    } else if (strcasecmp(cmd, "OPNOTICE") == 0) {
        do_set_opnotice(nc, ci, param);
    } else if (strcasecmp(cmd, "NOEXPIRE") == 0) {
        do_set_noexpire(nc, ci, param);
    } else {
        msg_lang(ncChanServ, nc, CHAN_SET_UNKNOWN_OPTION, strupper(cmd));
        msg_lang(ncChanServ, nc, MORE_INFO, s_ChanServ, "SET");
    }
}

/*************************************************************************/

static void do_set_founder(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    struct NickInfo *ni = findnick(param), *ni0 = ci->founder;

    if (!ni) {
        msg_lang(ncChanServ, nc, NICK_X_NOT_REGISTERED, param);
        return;
    }
    if (ni->channelcount >= ni->channelmax && !is_services_admin(nc)) {
        msg_lang(ncChanServ, nc, CHAN_SET_FOUNDER_TOO_MANY_CHANS, param);
        return;
    }
    if (ni0->channelcount > 0)  /* Let's be paranoid... */
        ni0->channelcount--;
    ni0 = getlink(ni0);
    if (ni0 != ci->founder && ni0->channelcount > 0)
        ni0->channelcount--;
    ci->founder = ni;
    if (ni->channelcount+1 > ni->channelcount)
        ni->channelcount++;
    ni = getlink(ni);
    if (ni != ci->founder && ni->channelcount+1 > ni->channelcount)
        ni->channelcount++;
    slog(logsvc, "%s: Changing founder of %s to %s by %s!%s@%s", s_ChanServ,
                ci->name, param, nc->name, nc->user->username, nc->user->host);
    msg_lang(ncChanServ, nc, CHAN_FOUNDER_CHANGED, ci->name, param);
}

/*************************************************************************/

static void do_set_successor(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    struct NickInfo *ni;

    if (param) {
        ni = findnick(param);
        if (!ni) {
            msg_lang(ncChanServ, nc, NICK_X_NOT_REGISTERED, param);
            return;
        }
    } else {
        ni = NULL;
    }
    ci->successor = ni;
    if (ni)
        msg_lang(ncChanServ, nc, CHAN_SUCCESSOR_CHANGED, ci->name, param);
    else
        msg_lang(ncChanServ, nc, CHAN_SUCCESSOR_UNSET, ci->name);
}

/*************************************************************************/

static void do_set_password(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
#ifdef USE_ENCRYPTION
    int len = strlen(param);
    if (len > PASSMAX) {
        len = PASSMAX;
        param[len] = 0;
        msg_lang(ncChanServ, nc, PASSWORD_TRUNCATED, PASSMAX);
    }
    if (encrypt(param, len, ci->founderpass, PASSMAX) < 0) {
        memset(param, 0, strlen(param));
        slog(logsvc, "%s: Failed to encrypt password for %s (set)",
                s_ChanServ, ci->name);
        msg_lang(ncChanServ, nc, CHAN_SET_PASSWORD_FAILED);
        return;
    }
    memset(param, 0, strlen(param));
    msg_lang(ncChanServ, nc, CHAN_PASSWORD_CHANGED, ci->name);
#else /* !USE_ENCRYPTION */
    if (strlen(param) > PASSMAX-1) /* -1 for null byte */
        msg_lang(ncChanServ, nc, PASSWORD_TRUNCATED, PASSMAX-1);
    strscpy(ci->founderpass, param, PASSMAX);
    msg_lang(ncChanServ, nc, CHAN_PASSWORD_CHANGED_TO,
                ci->name, ci->founderpass);
#endif /* USE_ENCRYPTION */
    if (get_access(nc, ci) < ACCESS_FOUNDER) {
        slog(logsvc, "%s: %s!%s@%s set password as Services admin for %s",
                s_ChanServ, nc->name, nc->user->username, nc->user->host, ci->name);
        if (WallSetpass)
            wallops(ncChanServ, "\2%s\2 set password as Services admin for "
                                "channel \2%s\2", nc->name, ci->name);
    }
}

/*************************************************************************/

static void do_set_desc(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (ci->desc)
        free(ci->desc);
    ci->desc = sstrdup(param);
    msg_lang(ncChanServ, nc, CHAN_DESC_CHANGED, ci->name, param);
}

/*************************************************************************/

static void do_set_url(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (ci->url)
        free(ci->url);
    if (param) {
        ci->url = sstrdup(param);
        msg_lang(ncChanServ, nc, CHAN_URL_CHANGED, ci->name, param);
    } else {
        ci->url = NULL;
        msg_lang(ncChanServ, nc, CHAN_URL_UNSET, ci->name);
    }
}

/*************************************************************************/

static void do_set_email(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (ci->email)
        free(ci->email);
    if (param) {
        ci->email = sstrdup(param);
        msg_lang(ncChanServ, nc, CHAN_EMAIL_CHANGED, ci->name, param);
    } else {
        ci->email = NULL;
        msg_lang(ncChanServ, nc, CHAN_EMAIL_UNSET, ci->name);
    }
}

/*************************************************************************/

static void do_set_entrymsg(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (ci->entry_message)
        free(ci->entry_message);
    if (param) {
        ci->entry_message = sstrdup(param);
        msg_lang(ncChanServ, nc, CHAN_ENTRY_MSG_CHANGED, ci->name, param);
    } else {
        ci->entry_message = NULL;
        msg_lang(ncChanServ, nc, CHAN_ENTRY_MSG_UNSET, ci->name);
    }
}

/*************************************************************************/

static void do_set_topic(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    struct Channel *c = ci->c;

    if (!c) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, ci->name);
        return;
    }
    if (ci->last_topic)
        free(ci->last_topic);
    if (*param)
        ci->last_topic = sstrdup(param);
    else
        ci->last_topic = NULL;
    if (c->topic) {
        free(c->topic);
        c->topic_time--;        /* to get around TS8 */
    } else
        c->topic_time = time(NULL);
    if (*param)
        c->topic = sstrdup(param);
    else
        c->topic = NULL;
    strscpy(ci->last_topic_setter, nc->name, NICKMAX);
    strscpy(c->topic_setter, nc->name, NICKMAX);
    ci->last_topic_time = c->topic_time;

    send_cmd(ncChanServ, "T %s %lu %lu :%s", c->name,
             c->creation_time, c->topic_time, param);
}

/*************************************************************************/

static void do_set_mlock(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    char *s, c;
    int add = -1;        /* 1 if adding, 0 if deleting, -1 if neither */
    int newlock_on = 0, newlock_off = 0, newlock_limit = 0, flag;
    char *newlock_key = NULL;
    char *mlock;

    while (*param) {
        if (*param != '+' && *param != '-' && add < 0) {
            param++;
            continue;
        }
        switch ((c = *param++)) {
          case '+':
            add = 1;
            break;
          case '-':
            add = 0;
            break;
          case 'k':
            if (add) {
                if (!(s = strtok(NULL, " "))) {
                    msg_lang(ncChanServ, nc, CHAN_SET_MLOCK_KEY_REQUIRED);
                    return;
                }
                if (newlock_key)
                    sfree(newlock_key);
                newlock_key = sstrdup(s);
                newlock_off &= ~CMODE_k;
            } else {
                if (newlock_key) {
                    sfree(newlock_key);
                    newlock_key = NULL;
                }
                newlock_off |= CMODE_k;
            }
            break;
          case 'l':
            if (add) {
                if (!(s = strtok(NULL, " "))) {
                    msg_lang(ncChanServ, nc, CHAN_SET_MLOCK_LIMIT_REQUIRED);
                    return;
                }
                if (atol(s) <= 0) {
                    msg_lang(ncChanServ, nc, CHAN_SET_MLOCK_LIMIT_POSITIVE);
                    return;
                }
                newlock_limit = atol(s);
                newlock_off &= ~CMODE_l;
            } else {
                newlock_limit = 0;
                newlock_off |= CMODE_l;
            }
            break;
          default:
            flag = channel_mode_to_int(c);
            if (flag) {
                if (add) {
                    newlock_on |= flag;
                    newlock_off &= ~flag;
                } else {
                    newlock_on &= ~flag;
                    newlock_off |= flag;
                }
            } else {
                msg_lang(ncChanServ, nc, CHAN_SET_MLOCK_UNKNOWN_CHAR, c);
            }
            break;
        } /* switch */
    } /* while (*param) */

    /* Now that everything's okay, actually set the new mode lock. */
    ci->mlock_on = newlock_on;
    ci->mlock_off = newlock_off;
    ci->mlock_limit = newlock_limit;
    if (ci->mlock_key)
        free(ci->mlock_key);
    ci->mlock_key = newlock_key;

    mlock = get_mlock(ci->mlock_on, ci->mlock_off, ci->mlock_key, ci->mlock_limit, 0);

    if (mlock[0] != '\0') {
        msg_lang(ncChanServ, nc, CHAN_MLOCK_CHANGED, ci->name, mlock);
    } else {
        msg_lang(ncChanServ, nc, CHAN_MLOCK_REMOVED, ci->name);
    }

    /* Implement the new lock. */
    check_modes(ci->c);
}

/*************************************************************************/

static void do_set_keeptopic(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_KEEPTOPIC;
        msg_lang(ncChanServ, nc, CHAN_SET_KEEPTOPIC_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_KEEPTOPIC;
        msg_lang(ncChanServ, nc, CHAN_SET_KEEPTOPIC_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET KEEPTOPIC", CHAN_SET_KEEPTOPIC_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_topiclock(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_TOPICLOCK;
        msg_lang(ncChanServ, nc, CHAN_SET_TOPICLOCK_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_TOPICLOCK;
        msg_lang(ncChanServ, nc, CHAN_SET_TOPICLOCK_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET TOPICLOCK", CHAN_SET_TOPICLOCK_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_private(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_PRIVATE;
        msg_lang(ncChanServ, nc, CHAN_SET_PRIVATE_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_PRIVATE;
        msg_lang(ncChanServ, nc, CHAN_SET_PRIVATE_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET PRIVATE", CHAN_SET_PRIVATE_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_secureops(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_SECUREOPS;
        msg_lang(ncChanServ, nc, CHAN_SET_SECUREOPS_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_SECUREOPS;
        msg_lang(ncChanServ, nc, CHAN_SET_SECUREOPS_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET SECUREOPS", CHAN_SET_SECUREOPS_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_leaveops(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_LEAVEOPS;
        msg_lang(ncChanServ, nc, CHAN_SET_LEAVEOPS_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_LEAVEOPS;
        msg_lang(ncChanServ, nc, CHAN_SET_LEAVEOPS_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET LEAVEOPS", CHAN_SET_LEAVEOPS_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_restricted(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_RESTRICTED;
        if (ci->levels[CA_NOJOIN] < 0)
            ci->levels[CA_NOJOIN] = 0;
        msg_lang(ncChanServ, nc, CHAN_SET_RESTRICTED_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_RESTRICTED;
        if (ci->levels[CA_NOJOIN] >= 0)
            ci->levels[CA_NOJOIN] = -1;
        msg_lang(ncChanServ, nc, CHAN_SET_RESTRICTED_OFF);
    } else {
        syntax_error(ncChanServ,nc,"SET RESTRICTED",CHAN_SET_RESTRICTED_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_secure(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_SECURE;
        msg_lang(ncChanServ, nc, CHAN_SET_SECURE_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_SECURE;
        msg_lang(ncChanServ, nc, CHAN_SET_SECURE_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET SECURE", CHAN_SET_SECURE_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_opnotice(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_OPNOTICE;
        msg_lang(ncChanServ, nc, CHAN_SET_OPNOTICE_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_OPNOTICE;
        msg_lang(ncChanServ, nc, CHAN_SET_OPNOTICE_OFF);
    } else {
        syntax_error(ncChanServ, nc, "SET OPNOTICE", CHAN_SET_OPNOTICE_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_noexpire(struct NetClient *nc, struct ChannelInfo *ci, char *param)
{
    if (!is_services_admin(nc)) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
        return;
    }
    if (strcasecmp(param, "ON") == 0) {
        ci->flags |= CI_NO_EXPIRE;
        msg_lang(ncChanServ, nc, CHAN_SET_NOEXPIRE_ON, ci->name);
    } else if (strcasecmp(param, "OFF") == 0) {
        ci->flags &= ~CI_NO_EXPIRE;
        msg_lang(ncChanServ, nc, CHAN_SET_NOEXPIRE_OFF, ci->name);
    } else {
        syntax_error(ncChanServ, nc, "SET NOEXPIRE", CHAN_SET_NOEXPIRE_SYNTAX);
    }
}

/*************************************************************************/

/* `last' is set to the last index this routine was called with
 * `perm' is incremented whenever a permission-denied error occurs
 */
static int access_del(struct NetClient *nc, struct ChanAccess *access, int *perm, int uacc)
{
    if (!access->in_use)
        return 0;
    if (uacc <= access->level) {
        (*perm)++;
        return 0;
    }
    access->ni = NULL;
    access->in_use = 0;
    return 1;
}

static int access_del_callback(struct NetClient *nc, int num, va_list args)
{
    struct ChannelInfo *ci = va_arg(args, struct ChannelInfo *);
    int *last = va_arg(args, int *);
    int *perm = va_arg(args, int *);
    int uacc = va_arg(args, int);
    if (num < 1 || num > ci->accesscount)
        return 0;
    *last = num;
    return access_del(nc, &ci->access[num-1], perm, uacc);
}


static int access_list(struct NetClient *nc, int index, struct ChannelInfo *ci, int *sent_header)
{
    struct ChanAccess *access = &ci->access[index];
    struct NickInfo *ni;
    char *s;

    if (!access->in_use)
        return 0;
    if (!*sent_header) {
        msg_lang(ncChanServ, nc, CHAN_ACCESS_LIST_HEADER, ci->name);
        *sent_header = 1;
    }
    if ((ni = findnick(access->ni->nick))
                        && !(getlink(ni)->flags & NI_HIDE_MASK))
        s = ni->last_usermask;
    else
        s = NULL;
    msg_lang(ncChanServ, nc, CHAN_ACCESS_LIST_FORMAT,
                index+1, access->level, access->ni->nick,
                s ? " (" : "", s ? s : "", s ? ")" : "");
    return 1;
}

static int access_list_callback(struct NetClient *nc, int num, va_list args)
{
    struct ChannelInfo *ci = va_arg(args, struct ChannelInfo *);
    int *sent_header = va_arg(args, int *);
    if (num < 1 || num > ci->accesscount)
        return 0;
    return access_list(nc, num-1, ci, sent_header);
}


static void do_access(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *cmd  = strtok(NULL, " ");
    char *nick = strtok(NULL, " ");
    char *s    = strtok(NULL, " ");
    struct ChannelInfo *ci;
    struct NickInfo *ni;
    short level = 0, ulev;
    int i;
    int is_list = (cmd && strcasecmp(cmd, "LIST") == 0);
    struct ChanAccess *access;

    /* If LIST, we don't *require* any parameters, but we can take any.
     * If DEL, we require a nick and no level.
     * Else (ADD), we require a level (which implies a nick). */
    if (!cmd || (is_list ? 0 :
                        (strcasecmp(cmd,"DEL")==0) ? (!nick || s) : !s)) {
        syntax_error(ncChanServ, nc, "ACCESS", CHAN_ACCESS_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (((is_list && !check_access(nc, ci, CA_ACCESS_LIST))
                || (!is_list && !check_access(nc, ci, CA_ACCESS_CHANGE)))
               && !is_services_admin(nc))
    {
        msg_lang(ncChanServ, nc, ACCESS_DENIED);

    } else if (strcasecmp(cmd, "ADD") == 0) {

        if (readonly) {
            msg_lang(ncChanServ, nc, CHAN_ACCESS_DISABLED);
            return;
        }

        level = atoi(s);
        ulev = get_access(nc, ci);
        if (level >= ulev) {
            msg_lang(ncChanServ, nc, PERMISSION_DENIED);
            return;
        }
        if (level == 0) {
            msg_lang(ncChanServ, nc, CHAN_ACCESS_LEVEL_NONZERO);
            return;
        } else if (level <= ACCESS_INVALID || level >= ACCESS_FOUNDER) {
            msg_lang(ncChanServ, nc, CHAN_ACCESS_LEVEL_RANGE,
                        ACCESS_INVALID+1, ACCESS_FOUNDER-1);
            return;
        }
        ni = findnick(nick);
        if (!ni) {
            msg_lang(ncChanServ, nc, CHAN_ACCESS_NICKS_ONLY);
            return;
        }
        for (access = ci->access, i = 0; i < ci->accesscount; access++, i++) {
            if (access->ni == ni) {
                /* Don't allow lowering from a level >= ulev */
                if (access->level >= ulev) {
                    msg_lang(ncChanServ, nc, PERMISSION_DENIED);
                    return;
                }
                if (access->level == level) {
                    msg_lang(ncChanServ, nc, CHAN_ACCESS_LEVEL_UNCHANGED,
                        access->ni->nick, chan, level);
                    return;
                }
                access->level = level;
                msg_lang(ncChanServ, nc, CHAN_ACCESS_LEVEL_CHANGED,
                        access->ni->nick, chan, level);
                return;
            }
        }
        for (i = 0; i < ci->accesscount; i++) {
            if (!ci->access[i].in_use)
                break;
        }
        if (i == ci->accesscount) {
            if (i < CSAccessMax) {
                ci->accesscount++;
                ci->access =
                    srealloc(ci->access, sizeof(struct ChanAccess) * ci->accesscount);
            } else {
                msg_lang(ncChanServ, nc, CHAN_ACCESS_REACHED_LIMIT,
                        CSAccessMax);
                return;
            }
        }
        access = &ci->access[i];
        access->ni = ni;
        access->in_use = 1;
        access->level = level;
        msg_lang(ncChanServ, nc, CHAN_ACCESS_ADDED,
                access->ni->nick, chan, level);

    } else if (strcasecmp(cmd, "DEL") == 0) {

        if (readonly) {
            msg_lang(ncChanServ, nc, CHAN_ACCESS_DISABLED);
            return;
        }

        /* Special case: is it a number/list?  Only do search if it isn't. */
        if (isdigit(*nick) && strspn(nick, "1234567890,-") == strlen(nick)) {
            int count, deleted, last = -1, perm = 0;
            deleted = process_numlist(nick, &count, access_del_callback, nc,
                                        ci, &last, &perm, get_access(nc, ci));
            if (!deleted) {
                if (perm) {
                    msg_lang(ncChanServ, nc, PERMISSION_DENIED);
                } else if (count == 1) {
                    msg_lang(ncChanServ, nc, CHAN_ACCESS_NO_SUCH_ENTRY,
                                last, ci->name);
                } else {
                    msg_lang(ncChanServ, nc, CHAN_ACCESS_NO_MATCH, ci->name);
                }
            } else if (deleted == 1) {
                msg_lang(ncChanServ, nc, CHAN_ACCESS_DELETED_ONE, ci->name);
            } else {
                msg_lang(ncChanServ, nc, CHAN_ACCESS_DELETED_SEVERAL,
                                deleted, ci->name);
            }
        } else {
            ni = findnick(nick);
            if (!ni) {
                msg_lang(ncChanServ, nc, NICK_X_NOT_REGISTERED, nick);
                return;
            }
            for (i = 0; i < ci->accesscount; i++) {
                if (ci->access[i].ni == ni)
                    break;
            }
            if (i == ci->accesscount) {
                msg_lang(ncChanServ, nc, CHAN_ACCESS_NOT_FOUND, nick, chan);
                return;
            }
            access = &ci->access[i];
            if (get_access(nc, ci) <= access->level) {
                msg_lang(ncChanServ, nc, PERMISSION_DENIED);
            } else {
                msg_lang(ncChanServ, nc, CHAN_ACCESS_DELETED,
                                access->ni->nick, ci->name);
                access->ni = NULL;
                access->in_use = 0;
            }
        }

    } else if (strcasecmp(cmd, "LIST") == 0) {
        int sent_header = 0;

        if (ci->accesscount == 0) {
            msg_lang(ncChanServ, nc, CHAN_ACCESS_LIST_EMPTY, chan);
            return;
        }
        if (nick && strspn(nick, "1234567890,-") == strlen(nick)) {
            process_numlist(nick, NULL, access_list_callback, nc, ci,
                                                                &sent_header);
        } else {
            for (i = 0; i < ci->accesscount; i++) {
                if (nick && ci->access[i].ni
                         && !match_wild(nick, ci->access[i].ni->nick))
                    continue;
                access_list(nc, i, ci, &sent_header);
            }
        }
        if (!sent_header)
            msg_lang(ncChanServ, nc, CHAN_ACCESS_NO_MATCH, chan);

    } else {
        syntax_error(ncChanServ, nc, "ACCESS", CHAN_ACCESS_SYNTAX);
    }
}

/*************************************************************************/

/* `last' is set to the last index this routine was called with */
static int akick_del(struct NetClient *nc, struct AutoKick *akick)
{
    if (!akick->in_use)
        return 0;
    if (akick->is_nick) {
        akick->u.ni = NULL;
    } else {
        free(akick->u.mask);
        akick->u.mask = NULL;
    }
    if (akick->reason) {
        free(akick->reason);
        akick->reason = NULL;
    }
    akick->in_use = akick->is_nick = 0;
    return 1;
}

static int akick_del_callback(struct NetClient *nc, int num, va_list args)
{
    struct ChannelInfo *ci = va_arg(args, struct ChannelInfo *);
    int *last = va_arg(args, int *);
    if (num < 1 || num > ci->akickcount)
        return 0;
    *last = num;
    return akick_del(nc, &ci->akick[num-1]);
}


static int akick_list(struct NetClient *nc, int index, struct ChannelInfo *ci, int *sent_header)
{
    struct AutoKick *akick = &ci->akick[index];
    char buf[BUFSIZE], buf2[BUFSIZE];

    if (!akick->in_use)
        return 0;
    if (!*sent_header) {
        msg_lang(ncChanServ, nc, CHAN_AKICK_LIST_HEADER, ci->name);
        *sent_header = 1;
    }
    if (akick->is_nick) {
        if (akick->u.ni->flags & NI_HIDE_MASK)
            strscpy(buf, akick->u.ni->nick, sizeof(buf));
        else
            snprintf(buf, sizeof(buf), "%s (%s)",
                        akick->u.ni->nick, akick->u.ni->last_usermask);
    } else {
        strscpy(buf, akick->u.mask, sizeof(buf));
    }
    if (akick->reason)
        snprintf(buf2, sizeof(buf2), " (%s)", akick->reason);
    else
        *buf2 = 0;
    msg_lang(ncChanServ, nc, CHAN_AKICK_LIST_FORMAT, index+1, buf, buf2);
    return 1;
}

static int akick_list_callback(struct NetClient *nc, int num, va_list args)
{
    struct ChannelInfo *ci = va_arg(args, struct ChannelInfo *);
    int *sent_header = va_arg(args, int *);
    if (num < 1 || num > ci->akickcount)
        return 0;
    return akick_list(nc, num-1, ci, sent_header);
}


static void do_akick(struct NetClient *nc)
{
    char *chan   = strtok(NULL, " ");
    char *cmd    = strtok(NULL, " ");
    char *mask   = strtok(NULL, " ");
    char *reason = strtok(NULL, "");
    struct ChannelInfo *ci;
    int i;
    struct AutoKick *akick;

    if (!cmd || (!mask && 
                (strcasecmp(cmd, "ADD") == 0 || strcasecmp(cmd, "DEL") == 0))) {
        syntax_error(ncChanServ, nc, "AKICK", CHAN_AKICK_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!check_access(nc, ci, CA_AKICK) && !is_services_admin(nc)) {
        if (ci->founder && getlink(ci->founder) == nc->user->ni)
            msg_lang(ncChanServ, nc, CHAN_IDENTIFY_REQUIRED, s_ChanServ,chan);
        else
            msg_lang(ncChanServ, nc, ACCESS_DENIED);

    } else if (strcasecmp(cmd, "ADD") == 0) {

        struct NickInfo *ni = findnick(mask);
        char *nick, *user, *host;

        if (readonly) {
            msg_lang(ncChanServ, nc, CHAN_AKICK_DISABLED);
            return;
        }

        if (!ni) {
            split_usermask(mask, &nick, &user, &host);
            mask = smalloc(strlen(nick)+strlen(user)+strlen(host)+3);
            sprintf(mask, "%s!%s@%s", nick, user, host);
            sfree(nick);
            sfree(nc);
            sfree(host);
        }

        for (akick = ci->akick, i = 0; i < ci->akickcount; akick++, i++) {
            if (!akick->in_use)
                continue;
            if (akick->is_nick ? akick->u.ni == ni
                               : strcasecmp(akick->u.mask,mask) == 0) {
                msg_lang(ncChanServ, nc, CHAN_AKICK_ALREADY_EXISTS,
                        akick->is_nick ? akick->u.ni->nick : akick->u.mask,
                        chan);
                return;
            }
        }

        for (i = 0; i < ci->akickcount; i++) {
            if (!ci->akick[i].in_use)
                break;
        }
        if (i == ci->akickcount) {
            if (ci->akickcount >= CSAutokickMax) {
                msg_lang(ncChanServ, nc, CHAN_AKICK_REACHED_LIMIT,
                        CSAutokickMax);
                return;
            }
            ci->akickcount++;
            ci->akick = srealloc(ci->akick, sizeof(struct AutoKick) * ci->akickcount);
        }
        akick = &ci->akick[i];
        akick->in_use = 1;
        if (ni) {
            akick->is_nick = 1;
            akick->u.ni = ni;
        } else {
            akick->is_nick = 0;
            akick->u.mask = mask;
        }
        if (reason)
            akick->reason = sstrdup(reason);
        else
            akick->reason = NULL;
        msg_lang(ncChanServ, nc, CHAN_AKICK_ADDED, mask, chan);

    } else if (strcasecmp(cmd, "DEL") == 0) {

        if (readonly) {
            msg_lang(ncChanServ, nc, CHAN_AKICK_DISABLED);
            return;
        }

        /* Special case: is it a number/list?  Only do search if it isn't. */
        if (isdigit(*mask) && strspn(mask, "1234567890,-") == strlen(mask)) {
            int count, deleted, last = -1;
            deleted = process_numlist(mask, &count, akick_del_callback, nc,
                                        ci, &last);
            if (!deleted) {
                if (count == 1) {
                    msg_lang(ncChanServ, nc, CHAN_AKICK_NO_SUCH_ENTRY,
                                last, ci->name);
                } else {
                    msg_lang(ncChanServ, nc, CHAN_AKICK_NO_MATCH, ci->name);
                }
            } else if (deleted == 1) {
                msg_lang(ncChanServ, nc, CHAN_AKICK_DELETED_ONE, ci->name);
            } else {
                msg_lang(ncChanServ, nc, CHAN_AKICK_DELETED_SEVERAL,
                                deleted, ci->name);
            }
        } else {
            struct NickInfo *ni = findnick(mask);

            for (akick = ci->akick, i = 0; i < ci->akickcount; akick++, i++) {
                if (!akick->in_use)
                    continue;
                if ((akick->is_nick && akick->u.ni == ni)
                 || (!akick->is_nick && strcasecmp(akick->u.mask, mask) == 0))
                    break;
            }
            if (i == ci->akickcount) {
                msg_lang(ncChanServ, nc, CHAN_AKICK_NOT_FOUND, mask, chan);
                return;
            }
            msg_lang(ncChanServ, nc, CHAN_AKICK_DELETED, mask, chan);
            akick_del(nc, akick);
        }

    } else if (strcasecmp(cmd, "LIST") == 0) {
        int sent_header = 0;

        if (ci->akickcount == 0) {
            msg_lang(ncChanServ, nc, CHAN_AKICK_LIST_EMPTY, chan);
            return;
        }
        if (mask && isdigit(*mask) &&
                        strspn(mask, "1234567890,-") == strlen(mask)) {
            process_numlist(mask, NULL, akick_list_callback, nc, ci,
                                                                &sent_header);
        } else {
            for (akick = ci->akick, i = 0; i < ci->akickcount; akick++, i++) {
                if (!akick->in_use)
                    continue;
                if (mask) {
                    if (!akick->is_nick && !match_wild(mask, akick->u.mask))
                        continue;
                    if (akick->is_nick && !match_wild(mask, akick->u.ni->nick))
                        continue;
                }
                akick_list(nc, i, ci, &sent_header);
            }
        }
        if (!sent_header)
            msg_lang(ncChanServ, nc, CHAN_AKICK_NO_MATCH, chan);

    } else if (strcasecmp(cmd, "ENFORCE") == 0) {
        struct Channel *c = FindChannel(ci->name);
        struct Membership *member, *next;
        int count = 0;

        if (!c) {
            msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, ci->name);
            return;
        }

        for (member = c->members; member; member = next) {
            next = member->next_member;
            if (check_kick(member->user, c)) {
                remove_user_from_channel(member->user, c);
                count++;
            }
        }

        msg_lang(ncChanServ, nc, CHAN_AKICK_ENFORCE_DONE, chan, count);

    } else {
        syntax_error(ncChanServ, nc, "AKICK", CHAN_AKICK_SYNTAX);
    }
}

/*************************************************************************/

static void do_levels(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *cmd  = strtok(NULL, " ");
    char *what = strtok(NULL, " ");
    char *s    = strtok(NULL, " ");
    struct ChannelInfo *ci;
    short level;
    int i;

    /* If SET, we want two extra parameters; if DIS[ABLE], we want only
     * one; else, we want none.
     */
    if (!cmd || ((strcasecmp(cmd,"SET")==0) ? !s :
                        (strncasecmp(cmd,"DIS",3)==0) ? (!what || s) : !!what)) {
        syntax_error(ncChanServ, nc, "LEVELS", CHAN_LEVELS_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!is_founder(nc, ci) && !is_services_admin(nc)) {
        msg_lang(ncChanServ, nc, ACCESS_DENIED);

    } else if (strcasecmp(cmd, "SET") == 0) {
        level = atoi(s);
        if (level <= ACCESS_INVALID || level >= ACCESS_FOUNDER) {
            msg_lang(ncChanServ, nc, CHAN_LEVELS_RANGE,
                        ACCESS_INVALID+1, ACCESS_FOUNDER-1);
            return;
        }
        for (i = 0; levelinfo[i].what >= 0; i++) {
            if (strcasecmp(levelinfo[i].name, what) == 0) {
                ci->levels[levelinfo[i].what] = level;
                msg_lang(ncChanServ, nc, CHAN_LEVELS_CHANGED,
                        levelinfo[i].name, chan, level);
                return;
            }
        }
        msg_lang(ncChanServ, nc, CHAN_LEVELS_UNKNOWN, what, s_ChanServ);

    } else if (strcasecmp(cmd, "DIS") == 0 || strcasecmp(cmd, "DISABLE") == 0) {
        for (i = 0; levelinfo[i].what >= 0; i++) {
            if (strcasecmp(levelinfo[i].name, what) == 0) {
                ci->levels[levelinfo[i].what] = ACCESS_INVALID;
                msg_lang(ncChanServ, nc, CHAN_LEVELS_DISABLED,
                        levelinfo[i].name, chan);
                return;
            }
        }
        msg_lang(ncChanServ, nc, CHAN_LEVELS_UNKNOWN, what, s_ChanServ);

    } else if (strcasecmp(cmd, "LIST") == 0) {
        int i;
        msg_lang(ncChanServ, nc, CHAN_LEVELS_LIST_HEADER, chan);
        if (!levelinfo_maxwidth) {
            for (i = 0; levelinfo[i].what >= 0; i++) {
                int len = strlen(levelinfo[i].name);
                if (len > levelinfo_maxwidth)
                    levelinfo_maxwidth = len;
            }
        }
        for (i = 0; levelinfo[i].what >= 0; i++) {
            int j = ci->levels[levelinfo[i].what];
            if (j == ACCESS_INVALID) {
                j = levelinfo[i].what;
                if (j == CA_AUTOOP || j == CA_AUTODEOP
                                || j == CA_AUTOVOICE || j == CA_NOJOIN)
                {
                    msg_lang(ncChanServ, nc, CHAN_LEVELS_LIST_DISABLED,
                                levelinfo_maxwidth, levelinfo[i].name);
                } else {
                    msg_lang(ncChanServ, nc, CHAN_LEVELS_LIST_DISABLED,
                                levelinfo_maxwidth, levelinfo[i].name);
                }
            } else {
                msg_lang(ncChanServ, nc, CHAN_LEVELS_LIST_NORMAL,
                                levelinfo_maxwidth, levelinfo[i].name, j);
            }
        }

    } else if (strcasecmp(cmd, "RESET") == 0) {
        reset_levels(ci);
        msg_lang(ncChanServ, nc, CHAN_LEVELS_RESET, chan);

    } else {
        syntax_error(ncChanServ, nc, "LEVELS", CHAN_LEVELS_SYNTAX);
    }
}

/*************************************************************************/

/* SADMINS and users, who have identified for a channel, can now cause it's
 * Enstry Message and Successor to be displayed by supplying the ALL parameter.
 * Syntax: INFO channel [ALL]
 * -TheShadow (29 Mar 1999)
 */

static void do_info(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *param = strtok(NULL, " ");
    struct ChannelInfo *ci;
    struct NickInfo *ni;
    char buf[BUFSIZE], *end;
    int need_comma = 0;
    const char *commastr = getstring(nc->user->ni, COMMA_SPACE);
    int is_servadmin = is_services_admin(nc);
    int show_all = 0;

    if (!chan) {
        syntax_error(ncChanServ, nc, "INFO", CHAN_INFO_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!ci->founder) {
        /* Paranoia... this shouldn't be able to happen */
        delchan(ci);
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else {

        /* Should we show all fields? Only for sadmins and identified users */
        if (param && strcasecmp(param, "ALL") == 0 && 
                                            (is_identified(nc, ci) || is_servadmin))
            show_all = 1;

        msg_lang(ncChanServ, nc, CHAN_INFO_HEADER, chan);
        ni = ci->founder;
        if (ni->last_usermask && (is_servadmin || !(ni->flags & NI_HIDE_MASK)))
        {
            msg_lang(ncChanServ, nc, CHAN_INFO_FOUNDER, ni->nick,
                        ni->last_usermask);
        } else {
            msg_lang(ncChanServ, nc, CHAN_INFO_NO_FOUNDER, ni->nick);
        }

        if (show_all) {
            ni = ci->successor;
            if (ni) {
                if (ni->last_usermask && (is_servadmin ||
                                !(ni->flags & NI_HIDE_MASK))) {
                    msg_lang(ncChanServ, nc, CHAN_INFO_SUCCESSOR, ni->nick,
                                ni->last_usermask);
                } else {
                    msg_lang(ncChanServ, nc, CHAN_INFO_NO_SUCCESSOR,
                                ni->nick);
                }
            }
        }

        msg_lang(ncChanServ, nc, CHAN_INFO_DESCRIPTION, ci->desc);
        strftime_lang(buf, sizeof(buf), nc->user->ni, STRFTIME_DATE_TIME_FORMAT,
                      ci->time_registered);
        msg_lang(ncChanServ, nc, CHAN_INFO_TIME_REGGED, buf);
        strftime_lang(buf, sizeof(buf), nc->user->ni, STRFTIME_DATE_TIME_FORMAT,
                      ci->last_used);
        msg_lang(ncChanServ, nc, CHAN_INFO_LAST_USED, buf);
        if (ci->last_topic) {
            msg_lang(ncChanServ, nc, CHAN_INFO_LAST_TOPIC, ci->last_topic);
            msg_lang(ncChanServ, nc, CHAN_INFO_TOPIC_SET_BY,
                        ci->last_topic_setter);
        }

        if (ci->entry_message && show_all)
            msg_lang(ncChanServ, nc, CHAN_INFO_ENTRYMSG, ci->entry_message);
        if (ci->url)
            msg_lang(ncChanServ, nc, CHAN_INFO_URL, ci->url);
        if (ci->email)
            msg_lang(ncChanServ, nc, CHAN_INFO_EMAIL, ci->email);
        end = buf;
        *end = 0;
        if (ci->flags & CI_PRIVATE) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s",
                        getstring(nc->user->ni, CHAN_INFO_OPT_PRIVATE));
            need_comma = 1;
        }
        if (ci->flags & CI_KEEPTOPIC) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, CHAN_INFO_OPT_KEEPTOPIC));
            need_comma = 1;
        }
        if (ci->flags & CI_TOPICLOCK) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, CHAN_INFO_OPT_TOPICLOCK));
            need_comma = 1;
        }
        if (ci->flags & CI_SECUREOPS) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, CHAN_INFO_OPT_SECUREOPS));
            need_comma = 1;
        }
        if (ci->flags & CI_LEAVEOPS) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, CHAN_INFO_OPT_LEAVEOPS));
            need_comma = 1;
        }
        if (ci->flags & CI_RESTRICTED) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, CHAN_INFO_OPT_RESTRICTED));
            need_comma = 1;
        }
        if (ci->flags & CI_SECURE) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, CHAN_INFO_OPT_SECURE));
            need_comma = 1;
        }
        msg_lang(ncChanServ, nc, CHAN_INFO_OPTIONS,
                *buf ? buf : getstring(nc->user->ni, CHAN_INFO_OPT_NONE));
        end = buf;
        *end = 0;
        if (ci->mlock_on || ci->mlock_key || ci->mlock_limit)
            end += snprintf(end, sizeof(buf)-(end-buf), "+%s%s%s%s%s%s%s%s%s",
                                (ci->mlock_on & CMODE_i) ? "i" : "",
                                (ci->mlock_key         ) ? "k" : "",
                                (ci->mlock_limit       ) ? "l" : "",
                                (ci->mlock_on & CMODE_m) ? "m" : "",
                                (ci->mlock_on & CMODE_n) ? "n" : "",
                                (ci->mlock_on & CMODE_p) ? "p" : "",
                                (ci->mlock_on & CMODE_s) ? "s" : "",
                                (ci->mlock_on & CMODE_t) ? "t" : "",
                                    (ci->mlock_on & CMODE_R) ? "R" : "");
        if (ci->mlock_off)
            end += snprintf(end, sizeof(buf)-(end-buf), "-%s%s%s%s%s%s%s%s%s",
                                (ci->mlock_off & CMODE_i) ? "i" : "",
                                (ci->mlock_off & CMODE_k) ? "k" : "",
                                (ci->mlock_off & CMODE_l) ? "l" : "",
                                (ci->mlock_off & CMODE_m) ? "m" : "",
                                (ci->mlock_off & CMODE_n) ? "n" : "",
                                (ci->mlock_off & CMODE_p) ? "p" : "",
                                (ci->mlock_off & CMODE_s) ? "s" : "",
                                (ci->mlock_off & CMODE_t) ? "t" : "",
                                    (ci->mlock_off & CMODE_R) ? "R" : "");

        if (*buf)
            msg_lang(ncChanServ, nc, CHAN_INFO_MODE_LOCK, buf);

        if ((ci->flags & CI_NO_EXPIRE) && show_all)
                        msg_lang(ncChanServ, nc, CHAN_INFO_NO_EXPIRE);
    }
}

/*************************************************************************/

static void do_list(struct NetClient *nc)
{
    char *pattern = strtok(NULL, " ");
    struct ChannelInfo *ci;
    int nchans, i;
    char buf[BUFSIZE];
    int is_servadmin = is_services_admin(nc);

    if (CSListOpersOnly && (!nc || !(nc->user->modes & UMODE_o))) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
        return;
    }

    if (!pattern) {
        syntax_error(ncChanServ, nc, "LIST", CHAN_LIST_SYNTAX);
    } else {
        nchans = 0;
        msg_lang(ncChanServ, nc, CHAN_LIST_HEADER, pattern);
        for (i = 0; i < 256; i++) {
            for (ci = chanlists[i]; ci; ci = ci->next) {
                if (!is_servadmin & (ci->flags & CI_VERBOTEN))
                    continue;
                snprintf(buf, sizeof(buf), "%-20s  %s", ci->name,
                                                ci->desc ? ci->desc : "");
                if (strcasecmp(pattern, ci->name) == 0 ||
                                        match_wild_nocase(pattern, buf)) {
                    if (++nchans <= CSListMax) {
                        char noexpire_char = ' ';
                        if (is_servadmin && (ci->flags & CI_NO_EXPIRE))
                            noexpire_char = '!';
                        notice(ncChanServ, nc, "  %c%s",
                                                noexpire_char, buf);
                    }
                }
            }
        }
        msg_lang(ncChanServ, nc, CHAN_LIST_END,
                        nchans>CSListMax ? CSListMax : nchans, nchans);
    }

}

/*************************************************************************/

static void do_invite(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    struct Channel *c;
    struct ChannelInfo *ci;

    if (!chan) {
        syntax_error(ncChanServ, nc, "INVITE", CHAN_INVITE_SYNTAX);
    } else if (!(c = FindChannel(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (!(ci = c->ci)) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!nc || !check_access(nc, ci, CA_INVITE)) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
    } else {
        send_cmd(ncChanServ, "I %s %s", nc->name, chan);
    }
}

/*************************************************************************/

static void do_op(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *op_params = strtok(NULL, " ");
    struct Channel *c;
    struct ChannelInfo *ci;
    struct NetClient *nc2;

    if (!chan || !op_params) {
        syntax_error(ncChanServ, nc, "OP", CHAN_OP_SYNTAX);
    } else if (!(c = FindChannel(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (!(ci = c->ci)) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!nc || !check_access(nc, ci, CA_OPDEOP)) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
    } else if (!(nc2 = FindUser(op_params))) {
        msg_lang(ncChanServ, nc, NICK_X_NOT_IN_USE, op_params);
    } else {
        char *av[3];
        send_cmd(ncChanServ, "M %s +o %s%s", chan, NumNick(nc2));
        av[0] = chan;
        av[1] = sstrdup("+o");
        av[2] = op_params;
        m_mode(ncChanServ, 3, av);
        free(av[1]);
        if (ci->flags & CI_OPNOTICE) {
            char buf[NICKMAX*2+32];
            snprintf(buf, sizeof(buf), "OP command used for %s by %s",
                        op_params, nc->name);
            msg_chan(ncChanServ, chan, buf);
        }
    }
}

/*************************************************************************/

static void do_deop(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *deop_params = strtok(NULL, " ");
    struct Channel *c;
    struct ChannelInfo *ci;
    struct NetClient *nc2;

    if (!chan || !deop_params) {
        syntax_error(ncChanServ, nc, "DEOP", CHAN_DEOP_SYNTAX);
    } else if (!(c = FindChannel(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (!(ci = c->ci)) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!nc || !check_access(nc, ci, CA_OPDEOP)) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
    } else if (!(nc2 = FindUser(deop_params))) {
        msg_lang(ncChanServ, nc, NICK_X_NOT_IN_USE, deop_params);
    } else {
        char *av[3];
        send_cmd(ncChanServ, "M %s +o %s%s", chan, NumNick(nc2));
        av[0] = chan;
        av[1] = sstrdup("-o");
        av[2] = deop_params;
        m_mode(ncChanServ, 3, av);
        free(av[1]);
        if (ci->flags & CI_OPNOTICE) {
            char buf[NICKMAX*2+32];
            snprintf(buf, sizeof(buf), "DEOP command used for %s by %s",
                        deop_params, nc->name);
            msg_chan(ncChanServ, c->name, buf);
        }
    }
}

/*************************************************************************/

static void do_unban(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    struct Channel *c;
    struct ChannelInfo *ci;
    int i;
    char *av[3];

    if (!chan) {
        syntax_error(ncChanServ, nc, "UNBAN", CHAN_UNBAN_SYNTAX);
    } else if (!(c = FindChannel(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (!(ci = c->ci)) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!nc || !check_access(nc, ci, CA_UNBAN)) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
    } else {
        char nickuser[NICKMAX + 12 + 2];
        char *hostmask;
        struct Ban *ban, *next;
        int bcnt, res;

        bcnt = 0;

        /* Nick!User */
        sprintf(nickuser, "%s!%s", nc->name, nc->user->username);

        for (ban = c->banlist; ban; ban = next) {
            next = ban->next;
            /* Las excepciones no tocan */
            if (ban->flags & BAN_EXCEPTION)
                continue;

            /* Comparamos parte del nick!user */
            ban->banstr[ban->nu_len] = '\0';
            res = match_wild_nocase(ban->banstr, nickuser);
            ban->banstr[ban->nu_len] = '@';
            if (!res)
                continue;

            /* Compare parte de host/IP */
            hostmask = ban->banstr + ban->nu_len + 1;
            if (((ban->flags & BAN_IPMASK)
                && ipmask_check(&nc->user->ip, &ban->address, ban->addrbits))
                || match_wild_nocase(hostmask, nc->user->host))
            {
                char *av[3];
                av[0] = sstrdup(chan);
                av[1] = sstrdup("-b");;
                av[2] = sstrdup(ban->banstr);
                send_cmd(ncChanServ, "M %s %s %s", av[0], av[1], av[2]);
                m_mode(ncChanServ, 3, av);
                bcnt++;
            }
        }
        msg_lang(ncChanServ, nc, CHAN_UNBANNED, chan);
    }
}

/*************************************************************************/

static void do_clear(struct NetClient *nc)
{
    char *chan = strtok(NULL, " ");
    char *what = strtok(NULL, " ");
    struct Channel *c;
    struct ChannelInfo *ci;

    if (!what) {
        syntax_error(ncChanServ, nc, "CLEAR", CHAN_CLEAR_SYNTAX);
    } else if (!(c = FindChannel(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (!(ci = c->ci)) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else if (!nc || !check_access(nc, ci, CA_CLEAR)) {
        msg_lang(ncChanServ, nc, PERMISSION_DENIED);
    } else if (strcasecmp(what, "bans") == 0) {
        struct Ban *ban, *next;
        char *av[3];

        for (ban = c->banlist; ban; ban = next) {
            next = ban->next;
            av[0] = sstrdup(chan);
            av[1] = sstrdup("-b");
            av[2] = sstrdup(ban->banstr);
            send_cmd(ncChanServ, "M %s %s :%s", av[0], av[1], av[2]);

            m_mode(ncChanServ, 3, av);
            sfree(av[0]);
            sfree(av[1]);
            sfree(av[2]);
        }
        msg_lang(ncChanServ, nc, CHAN_CLEARED_BANS, chan);
    } else if (strcasecmp(what, "modes") == 0) {
        char *av[3];

        av[0] = chan;
        av[1] = sstrdup("-mintpslkcrRMCNPuz");
        if (c->key)
            av[2] = sstrdup(c->key);
        else
            av[2] = sstrdup("");
        send_cmd(ncChanServ, "M %s %s :%s", av[0], av[1], av[2]);
        m_mode(ncChanServ, 3, av);
        free(av[2]);
        free(av[1]);
        check_modes(c);
        msg_lang(ncChanServ, nc, CHAN_CLEARED_MODES, chan);
    } else if (strcasecmp(what, "ops") == 0) {
        struct Membership *member;

        for (member = c->members; member; member = member->next_member) {
            if (member->status & CUMODE_OP) {
                member->status &= ~CUMODE_OP;
                send_cmd(ncChanServ, "M %s -o :%s%s", chan, NumNick(member->user));
            }
        }
        msg_lang(ncChanServ, nc, CHAN_CLEARED_OPS, chan);
    } else if (strcasecmp(what, "voices") == 0) {
        struct Membership *member;

        for (member = c->members; member; member = member->next_member) {
            if (member->status & CUMODE_VOICE) {
                member->status &= ~CUMODE_VOICE;
                send_cmd(ncChanServ, "M %s -v :%s%s", chan, NumNick(member->user));
            }
        }
        msg_lang(ncChanServ, nc, CHAN_CLEARED_VOICES, chan);
    } else if (strcasecmp(what, "users") == 0) {
        struct Membership *member, *next;
        char *av[3];
        char buf[256];

        snprintf(buf, sizeof(buf), "CLEAR USERS command from %s", nc->name);

        for (member = c->members; member; member = next) {
            next = member->next_member;
                send_cmd(ncChanServ, "K %s %s%s :%s", chan, NumNick(member->user), buf);
                remove_user_from_channel(member->user, c);
        }
        msg_lang(ncChanServ, nc, CHAN_CLEARED_USERS, chan);
    } else {
        syntax_error(ncChanServ, nc, "CLEAR", CHAN_CLEAR_SYNTAX);
    }
}

/*************************************************************************/

static void do_getpass(struct NetClient *nc)
{
#ifndef USE_ENCRYPTION
    char *chan = strtok(NULL, " ");
    struct ChannelInfo *ci;
#endif

    /* Assumes that permission checking has already been done. */
#ifdef USE_ENCRYPTION
    msg_lang(ncChanServ, nc, CHAN_GETPASS_UNAVAILABLE);
#else
    if (!chan) {
        syntax_error(ncChanServ, nc, "GETPASS", CHAN_GETPASS_SYNTAX);
    } else if (!(ci = cs_findchan(chan))) {
        msg_lang(ncChanServ, nc, CHAN_X_NOT_REGISTERED, chan);
    } else if (ci->flags & CI_VERBOTEN) {
        msg_lang(ncChanServ, nc, CHAN_X_FORBIDDEN, chan);
    } else {
        slog(logsvc, "%s: %s!%s@%s used GETPASS on %s",
                s_ChanServ, nc->name, nc->user->username, nc->user->host, ci->name);
        if (WallGetpass) {
            wallops(ncChanServ, "\2%s\2 used GETPASS on channel \2%s\2",
                nc->name, chan);
        }
        msg_lang(ncChanServ, nc, CHAN_GETPASS_PASSWORD_IS,
                chan, ci->founderpass);
    }
#endif
}

/*************************************************************************/

static void do_forbid(struct NetClient *nc)
{
    struct ChannelInfo *ci;
    char *chan = strtok(NULL, " ");

    /* Assumes that permission checking has already been done. */
    if (!chan) {
        syntax_error(ncChanServ, nc, "FORBID", CHAN_FORBID_SYNTAX);
        return;
    }
    if (readonly)
        msg_lang(ncChanServ, nc, READ_ONLY_MODE);
    if ((ci = cs_findchan(chan)) != NULL)
        delchan(ci);
    ci = makechan(chan);
    if (ci) {
        slog(logsvc, "%s: %s set FORBID for channel %s", s_ChanServ, nc->name,
                ci->name);
        ci->flags |= CI_VERBOTEN;
        msg_lang(ncChanServ, nc, CHAN_FORBID_SUCCEEDED, chan);
    } else {
        slog(logsvc, "%s: Valid FORBID for %s by %s failed", s_ChanServ, ci->name,
                nc->name);
        msg_lang(ncChanServ, nc, CHAN_FORBID_FAILED, chan);
    }
}

/*************************************************************************/

static void do_status(struct NetClient *nc)
{
    struct ChannelInfo *ci;
    struct NetClient *nc2;
    char *nick, *chan;

    chan = strtok(NULL, " ");
    nick = strtok(NULL, " ");
    if (!nick || strtok(NULL, " ")) {
        notice(ncChanServ, nc, "STATUS ERROR Syntax error");
        return;
    }
    if (!(ci = cs_findchan(chan))) {
        char *temp = chan;
        chan = nick;
        nick = temp;
        ci = cs_findchan(chan);
    }
    if (!ci) {
        notice(ncChanServ, nc, "STATUS ERROR Channel %s not registered",
                chan);
    } else if (ci->flags & CI_VERBOTEN) {
        notice(ncChanServ, nc, "STATUS ERROR Channel %s forbidden", chan);
        return;
    } else if ((nc2 = FindUser(nick)) != NULL) {
        notice(ncChanServ, nc, "STATUS %s %s %d",
                chan, nick, get_access(nc2, ci));
    } else { /* !nc2 */
        notice(ncChanServ, nc, "STATUS ERROR Nick %s not online", nick);
    }
}

/*************************************************************************/
