/*
 * ServicesIRCh - Services for IRCh, commands.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "commands.h"

#include "language.h"
#include "send.h"
/*prov */
#include "operserv.h"

#include <limits.h>
#include <string.h>

/*************************************************************************/

/* Return the Command corresponding to the given name, or NULL if no such
 * command exists.
 */

struct Command *lookup_cmd(struct Command *list, const char *cmd)
{
    struct Command *c;

    for (c = list; c->name; c++) {
        if (strcasecmp(c->name, cmd) == 0)
            return c;
    }
    return NULL;
}

/*************************************************************************/

/* Run the routine for the given command, if it exists and the user has
 * privilege to do so; if not, print an appropriate error message.
 */
void run_cmd(struct NetClient *service, struct NetClient *nc, struct Command *list,
             const char *cmd)
{
    struct Command *c = lookup_cmd(list, cmd);
    if (c && c->routine) {
        if ((c->has_priv == NULL) || c->has_priv(nc))
            c->routine(nc);
        else
            msg_lang(service, nc, ACCESS_DENIED);
    } else {
            msg_lang(service, nc, UNKNOWN_COMMAND_HELP, cmd, service);
    }
}

/*************************************************************************/

/* Print a help message for the given command. */

void help_cmd(struct NetClient *service, struct NetClient *nc, struct Command *list,
              const char *cmd)
{
    struct Command *c = lookup_cmd(list, cmd);

    if (c) {
        const char *p1 = c->help_param1,
                   *p2 = c->help_param2,
                   *p3 = c->help_param3,
                   *p4 = c->help_param4;

        if (c->helpmsg_all >= 0) {
            msg_help(service, nc, c->helpmsg_all, p1, p2, p3, p4);
        }

        if (is_services_root(nc)) {
            if (c->helpmsg_root >= 0)
                msg_help(service, nc, c->helpmsg_root, p1, p2, p3, p4);
            else if (c->helpmsg_all < 0)
                msg_lang(service, nc, NO_HELP_AVAILABLE, cmd);
        } else if (is_services_admin(nc)) {
            if (c->helpmsg_admin >= 0)
                msg_help(service, nc, c->helpmsg_admin, p1, p2, p3, p4);
            else if (c->helpmsg_all < 0)
                msg_lang(service, nc, NO_HELP_AVAILABLE, cmd);
        } else if (is_services_oper(nc)) {
            if (c->helpmsg_oper >= 0)
                msg_help(service, nc, c->helpmsg_oper, p1, p2, p3, p4);
            else if (c->helpmsg_all < 0)
                msg_lang(service, nc, NO_HELP_AVAILABLE, cmd);
        } else {
            if (c->helpmsg_reg >= 0)
                msg_help(service, nc, c->helpmsg_reg, p1, p2, p3, p4);
            else if (c->helpmsg_all < 0)
                msg_lang(service, nc, NO_HELP_AVAILABLE, cmd);
        }

    } else {

        msg_lang(service, nc, NO_HELP_AVAILABLE, cmd);

    }
}

/*************************************************************************/
