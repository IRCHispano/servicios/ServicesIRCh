/*
 * ServicesIRCh - Services for IRCh, hash.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "hash.h"

#include "channels.h"
#include "charset.h"
#include "log.h"
#include "netclient.h"
#include "svc_string.h"

#include <limits.h>
#include <sys/time.h>

/* Codigo procedente del IRCD */


struct MD5Context {
    unsigned int buf[4];        /**< Current digest state/value. */
    unsigned int bits[2];       /**< Number of bits hashed so far. */
    unsigned char in[64]; /**< Residual input buffer. */
};

#define HASHSIZE 32768
static struct MD5Context localkey;
static unsigned int localkey_pos;
static unsigned int crc32hash[256];

/* Hash table for clients */
static struct NetClient *ncHashTable[HASHSIZE];
/* Hash table for channels */
static struct Channel *cHashTable[HASHSIZE];

/* The four core functions - F1 is optimized somewhat */
/* #define F1(x, y, z) (x & y | ~x & z) */
/** Helper function for first round of MD5. */
#define F1(x, y, z) (z ^ (x & (y ^ z)))
/** Helper function for second round of MD5. */
#define F2(x, y, z) F1(z, x, y)
/** Helper function for third round of MD5. */
#define F3(x, y, z) (x ^ y ^ z)
/** Helper function for fourth round of MD5. */
#define F4(x, y, z) (y ^ (x | ~z))
/* Step function for each round of MD5 */
#define MD5STEP(f, w, x, y, z, data, s) \
        ( w += f(x, y, z) + data,  w = w<<s | w>>(32-s),  w += x )

static void MD5Transform(unsigned int buf[4], unsigned int const in[16])
{
    register unsigned int a, b, c, d;

    a = buf[0];
    b = buf[1];
    c = buf[2];
    d = buf[3];

    MD5STEP(F1, a, b, c, d, in[0] + 0xd76aa478U, 7);
    MD5STEP(F1, d, a, b, c, in[1] + 0xe8c7b756U, 12);
    MD5STEP(F1, c, d, a, b, in[2] + 0x242070dbU, 17);
    MD5STEP(F1, b, c, d, a, in[3] + 0xc1bdceeeU, 22);
    MD5STEP(F1, a, b, c, d, in[4] + 0xf57c0fafU, 7);
    MD5STEP(F1, d, a, b, c, in[5] + 0x4787c62aU, 12);
    MD5STEP(F1, c, d, a, b, in[6] + 0xa8304613U, 17);
    MD5STEP(F1, b, c, d, a, in[7] + 0xfd469501U, 22);
    MD5STEP(F1, a, b, c, d, in[8] + 0x698098d8U, 7);
    MD5STEP(F1, d, a, b, c, in[9] + 0x8b44f7afU, 12);
    MD5STEP(F1, c, d, a, b, in[10] + 0xffff5bb1U, 17);
    MD5STEP(F1, b, c, d, a, in[11] + 0x895cd7beU, 22);
    MD5STEP(F1, a, b, c, d, in[12] + 0x6b901122U, 7);
    MD5STEP(F1, d, a, b, c, in[13] + 0xfd987193U, 12);
    MD5STEP(F1, c, d, a, b, in[14] + 0xa679438eU, 17);
    MD5STEP(F1, b, c, d, a, in[15] + 0x49b40821U, 22);

    MD5STEP(F2, a, b, c, d, in[1] + 0xf61e2562U, 5);
    MD5STEP(F2, d, a, b, c, in[6] + 0xc040b340U, 9);
    MD5STEP(F2, c, d, a, b, in[11] + 0x265e5a51U, 14);
    MD5STEP(F2, b, c, d, a, in[0] + 0xe9b6c7aaU, 20);
    MD5STEP(F2, a, b, c, d, in[5] + 0xd62f105dU, 5);
    MD5STEP(F2, d, a, b, c, in[10] + 0x02441453U, 9);
    MD5STEP(F2, c, d, a, b, in[15] + 0xd8a1e681U, 14);
    MD5STEP(F2, b, c, d, a, in[4] + 0xe7d3fbc8U, 20);
    MD5STEP(F2, a, b, c, d, in[9] + 0x21e1cde6U, 5);
    MD5STEP(F2, d, a, b, c, in[14] + 0xc33707d6U, 9);
    MD5STEP(F2, c, d, a, b, in[3] + 0xf4d50d87U, 14);
    MD5STEP(F2, b, c, d, a, in[8] + 0x455a14edU, 20);
    MD5STEP(F2, a, b, c, d, in[13] + 0xa9e3e905U, 5);
    MD5STEP(F2, d, a, b, c, in[2] + 0xfcefa3f8U, 9);
    MD5STEP(F2, c, d, a, b, in[7] + 0x676f02d9U, 14);
    MD5STEP(F2, b, c, d, a, in[12] + 0x8d2a4c8aU, 20);

    MD5STEP(F3, a, b, c, d, in[5] + 0xfffa3942U, 4);
    MD5STEP(F3, d, a, b, c, in[8] + 0x8771f681U, 11);
    MD5STEP(F3, c, d, a, b, in[11] + 0x6d9d6122U, 16);
    MD5STEP(F3, b, c, d, a, in[14] + 0xfde5380cU, 23);
    MD5STEP(F3, a, b, c, d, in[1] + 0xa4beea44U, 4);
    MD5STEP(F3, d, a, b, c, in[4] + 0x4bdecfa9U, 11);
    MD5STEP(F3, c, d, a, b, in[7] + 0xf6bb4b60U, 16);
    MD5STEP(F3, b, c, d, a, in[10] + 0xbebfbc70U, 23);
    MD5STEP(F3, a, b, c, d, in[13] + 0x289b7ec6U, 4);
    MD5STEP(F3, d, a, b, c, in[0] + 0xeaa127faU, 11);
    MD5STEP(F3, c, d, a, b, in[3] + 0xd4ef3085U, 16);
    MD5STEP(F3, b, c, d, a, in[6] + 0x04881d05U, 23);
    MD5STEP(F3, a, b, c, d, in[9] + 0xd9d4d039U, 4);
    MD5STEP(F3, d, a, b, c, in[12] + 0xe6db99e5U, 11);
    MD5STEP(F3, c, d, a, b, in[15] + 0x1fa27cf8U, 16);
    MD5STEP(F3, b, c, d, a, in[2] + 0xc4ac5665U, 23);

    MD5STEP(F4, a, b, c, d, in[0] + 0xf4292244U, 6);
    MD5STEP(F4, d, a, b, c, in[7] + 0x432aff97U, 10);
    MD5STEP(F4, c, d, a, b, in[14] + 0xab9423a7U, 15);
    MD5STEP(F4, b, c, d, a, in[5] + 0xfc93a039U, 21);
    MD5STEP(F4, a, b, c, d, in[12] + 0x655b59c3U, 6);
    MD5STEP(F4, d, a, b, c, in[3] + 0x8f0ccc92U, 10);
    MD5STEP(F4, c, d, a, b, in[10] + 0xffeff47dU, 15);
    MD5STEP(F4, b, c, d, a, in[1] + 0x85845dd1U, 21);
    MD5STEP(F4, a, b, c, d, in[8] + 0x6fa87e4fU, 6);
    MD5STEP(F4, d, a, b, c, in[15] + 0xfe2ce6e0U, 10);
    MD5STEP(F4, c, d, a, b, in[6] + 0xa3014314U, 15);
    MD5STEP(F4, b, c, d, a, in[13] + 0x4e0811a1U, 21);
    MD5STEP(F4, a, b, c, d, in[4] + 0xf7537e82U, 6);
    MD5STEP(F4, d, a, b, c, in[11] + 0xbd3af235U, 10);
    MD5STEP(F4, c, d, a, b, in[2] + 0x2ad7d2bbU, 15);
    MD5STEP(F4, b, c, d, a, in[9] + 0xeb86d391U, 21);

    buf[0] += a;
    buf[1] += b;
    buf[2] += c;
    buf[3] += d;

}

static void random_add_entropy(const char *buf, unsigned int count)
{
    while (count--) {
        localkey.in[localkey_pos++] ^= *buf++;
        if (localkey_pos >= sizeof(localkey.in))
            localkey_pos = 0;
    }
}


unsigned int ircrandom(void)
{
    struct timeval tv;
    char usec[3];

    /* Add some randomness to the pool. */
    gettimeofday(&tv, 0);
    usec[0] = tv.tv_usec;
    usec[1] = tv.tv_usec >> 8;
    usec[2] = tv.tv_usec >> 16;
    random_add_entropy(usec, 3);

    /* Perform MD5 step. */
    localkey.buf[0] = 0x67452301;
    localkey.buf[1] = 0xefcdab89;
    localkey.buf[2] = 0x98badcfe;
    localkey.buf[3] = 0x10325476;
    MD5Transform(localkey.buf, (unsigned int*)localkey.in);

    /* Feed back 12 bytes of hash value into randomness pool. */
    random_add_entropy((char*)localkey.buf, 12);

    /* Return the final word of hash, which should not provide any
     * useful insight into current pool contents. */
    return localkey.buf[3];
}

/* Initialize the map used by the hash function. */
void init_hash(void)
{
    unsigned int ii, jj, rand, poly;

    /* First calculate a normal CRC-32 table. */
    for (ii = 0, poly = 0xedb88320; ii < 256; ii++)
    {
        rand = ii;
        for (jj = 0; jj < 8; jj++)
            rand = (rand & 1) ? poly ^ (rand >> 1) : rand >> 1;
        crc32hash[ii] = rand;
    }

    /* Now reorder the hash table. */
    for (ii = 0, rand = 0; ii < 256; ii++)
    {
        if (!rand)
            rand = ircrandom();
        poly = ii + rand % (256 - ii);
        jj = crc32hash[ii];
        crc32hash[ii] = crc32hash[poly];
        crc32hash[poly] = jj;
        rand >>= 8;
    }
}

/* Calculate hash value for a string */
static unsigned int strhash(const char *n)
{
    unsigned int hash = crc32hash[ToLower(*n++) & 255];
    while (*n)
        hash = (hash >> 8) ^ crc32hash[(hash ^ ToLower(*n++)) & 255];
    return hash % HASHSIZE;
}


int hAddNetClient(struct NetClient *nc)
{
  unsigned int hashv = strhash(nc->name);

  nc->hnext = ncHashTable[hashv];
  ncHashTable[hashv] = nc;

  return 0;
}

int hAddChannel(struct Channel *c)
{
  unsigned int hashv = strhash(c->name);

  c->hnext = cHashTable[hashv];
  cHashTable[hashv] = c;

  return 0;
}

int hRemNetClient(struct NetClient *nc)
{
    unsigned int hashv = strhash(nc->name);
    struct NetClient *tmp = ncHashTable[hashv];

    if (tmp == nc) {
        ncHashTable[hashv] = nc->hnext;
        nc->hnext = nc;
        return 0;
    }

    while (tmp) {
        if (tmp->hnext == nc) {
            tmp->hnext = tmp->hnext->hnext;
            nc->hnext = nc;
            return 0;
        }
        tmp = tmp->hnext;
    }

    return -1;
}

int hRemChannel(struct Channel *c)
{
    unsigned int hashv = strhash(c->name);
    struct Channel *tmp = cHashTable[hashv];

    if (tmp == c) {
        cHashTable[hashv] = c->hnext;
        c->hnext = c;
        return 0;
    }

    while (tmp) {
        if (tmp->hnext == c) {
            tmp->hnext = tmp->hnext->hnext;
            c->hnext = c;
            return 0;
        }
        tmp = tmp->hnext;
    }

    return -1;
}

struct NetClient *hSeekNetClient(const char *name, int TMask)
{
    unsigned int hashv      = strhash(name);
    struct NetClient *nc = ncHashTable[hashv];

    Debug((3, "hFindNetClient(%s)", name));

    if (nc) {
        if (0 == (nc->status & TMask) || 0 != irc_strcmp(name, nc->name)) {
            struct NetClient *prev;
            while (prev = nc, nc = nc->hnext) {
                if ((nc->status & TMask) && (0 == irc_strcmp(name, nc->name))) {
                    prev->hnext = nc->hnext;
                    nc->hnext = ncHashTable[hashv];
                    ncHashTable[hashv] = nc;
                    break;
                }
            }
        }
    }
    Debug((3, "hFindNetClient(%s) -> %p", name, nc));

    return nc;
}

struct Channel *hSeekChannel(const char *name)
{
    unsigned int hashv      = strhash(name);
    struct Channel *c = cHashTable[hashv];

    Debug((3, "hFindChannel(%s)", name));

    if (c) {
        if (0 != irc_strcmp(name, c->name)) {
            struct Channel *prev;
            while (prev = c, c = c->hnext) {
                if ((0 == irc_strcmp(name, c->name))) {
                    prev->hnext = c->hnext;
                    c->hnext = cHashTable[hashv];
                    cHashTable[hashv] = c;
                    break;
                }
            }
        }
    }
    Debug((3, "hFindChannel(%s) -> %p", name, c));

    return c;
}

/*************************************************************************/
