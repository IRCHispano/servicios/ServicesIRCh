/*
 * ServicesIRCh - Services for IRCh, helpserv.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "helpserv.h"

#include "config.h"
#include "language.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "send.h"
#include "service.h"
#include "services.h"
#include "svc_memory.h"
#include "users.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct NetClient *ncHelpServ;
struct NetClient *ncIrcIIHelp;

static void do_help(struct NetClient *whoami, struct NetClient *source, char *topic);

/*************************************************************************/

/* helpserv:  Main HelpServ routine.  `whoami' is what nick we should send
 * messages as: this won't necessarily be s_HelpServ, because other
 * routines call this one to display help files. */

static void helpservice(struct NetClient *whoami, struct NetClient *source, char *buf)
{
    char *cmd, *topic, *s;

    topic = buf ? sstrdup(buf) : NULL;
    cmd = strtok(buf, " ");
    if (cmd && strcasecmp(cmd, "\1PING") == 0) {
        if (!(s = strtok(NULL, "")))
            s = "\1";
        notice(whoami, source, "\1PING %s", s);
    } else {
        do_help(whoami, source, topic);
    }
    if (topic)
        free(topic);
}

void helpserv(struct NetClient *source, char *buf)
{
    helpservice(ncHelpServ, source, buf);
}

void irciihelp(struct NetClient *source, char *buf)
{
    helpservice(ncIrcIIHelp, source, buf);
}

/*************************************************************************/
/*********************** HelpServ command routines ***********************/
/*************************************************************************/

/* HelpServ initialization. */

void hs_init(void)
{
    if (s_HelpServ) {
        ncHelpServ = service_create(s_HelpServ, NULL, NULL, desc_HelpServ);
        ncHelpServ->service->has_priv = NULL;
        ncHelpServ->service->func = helpserv;
        set_user_modes(&myService, ncHelpServ, "+Bd");
        service_introduce(ncHelpServ);
    }
    if (s_IrcIIHelp) {
        ncIrcIIHelp = service_create(s_IrcIIHelp, NULL, NULL, desc_IrcIIHelp);
        ncIrcIIHelp->service->has_priv = NULL;
        ncIrcIIHelp->service->func = irciihelp;
        set_user_modes(&myService, ncIrcIIHelp, "+Bd");
        service_introduce(ncIrcIIHelp);
    }
}

/*************************************************************************/

/* Return a help message. */

static void do_help(struct NetClient *whoami, struct NetClient *source, char *topic)
{
    FILE *f;
    struct stat st;
    char buf[256], *ptr, *s;
    char *old_topic;        /* an unclobbered (by strtok) copy */

    if (!topic || !*topic)
        topic = "help";
    old_topic = sstrdup(topic);

    /* As we copy path parts, (1) lowercase everything and (2) make sure
     * we don't let any special characters through -- this includes '.'
     * (which could get parent dir) or '/' (which couldn't _really_ do
     * anything nasty if we keep '.' out, but better to be on the safe
     * side).  Special characters turn into '_'.
     */
    strscpy(buf, HelpDir, sizeof(buf));
    ptr = buf + strlen(buf);
    for (s = strtok(topic, " "); s && ptr-buf < sizeof(buf)-1;
                                                s = strtok(NULL, " ")) {
        *ptr++ = '/';
        while (*s && ptr-buf < sizeof(buf)-1) {
            if (*s == '.' || *s == '/')
                *ptr++ = '_';
            else
                *ptr++ = tolower(*s);
            ++s;
        }
        *ptr = 0;
    }

    /* If we end up at a directory, go for an "index" file/dir if
     * possible.
     */
    while (ptr-buf < sizeof(buf)-1
                && stat(buf, &st) == 0 && S_ISDIR(st.st_mode)) {
        *ptr++ = '/';
        strscpy(ptr, "index", sizeof(buf) - (ptr-buf));
        ptr += strlen(ptr);
    }

    /* Send the file, if it exists.
     */
    if (!(f = fopen(buf, "r"))) {
        Debug((1, "Cannot open help file %s", buf));
        msg_lang(whoami, source, NO_HELP_AVAILABLE, old_topic);
        free(old_topic);
        return;
    }
    while (fgets(buf, sizeof(buf), f)) {
        s = strtok(buf, "\n");
        /* Use this odd construction to prevent any %'s in the text from
         * doing weird stuff to the output.  Also replace blank lines by
         * spaces (see send.c/notice_list() for an explanation of why).
         */
        notice(whoami, source, "%s", s ? s : " ");
    }
    fclose(f);
    free(old_topic);
}

/*************************************************************************/
