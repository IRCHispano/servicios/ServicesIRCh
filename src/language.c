/*
 * ServicesIRCh - Services for IRCh, language.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#define LANGSTR_ARRAY  /* define langstrs[] in langstrs.h, via language.h */

#include "sysconf.h"
#include "language.h"

#include "config.h"
#include "datafiles.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "nickserv.h"
#include "send.h"
#include "svc_memory.h"
#include "users.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*************************************************************************/

/* Indexes of available languages (exported), terminated by -1: */
int langlist[NUM_LANGS+1];

/* Count of available strings: */
int num_strings;

/* The list of lists of messages. */
char **langtexts[NUM_LANGS];

/* Order in which languages should be displayed: (alphabetical) */
static int langorder[] = {
    LANG_ES,        /* Spanish */
    LANG_EN_US,     /* English (US) */
    LANG_IT,        /* Italian */
    LANG_PT,        /* Portugese */
    LANG_TR,        /* Turkish */
};

/* Filenames for language files: */
static struct {
    int num;
    int count;
    const char *filename;
} filenames[] = {
    { LANG_ES,        0,   "es" },
    { LANG_EN_US,     0,   "en_us" },
    { LANG_IT,        0,   "it" },
    { LANG_PT,        0,   "pt" },
    { LANG_TR,        0,   "tr" },
    { -1, 0, NULL }
};

/* Array indicating which languages were actually loaded (needed since NULL
 * langtexts[] pointers are redirected to DEF_LANGUAGE) */
static int is_loaded[NUM_LANGS];

/*************************************************************************/

/* Load a language file. */

static void load_lang(int index, const char *filename)
{
    char buf[256];
    struct dbFILE *f;
    u_int32_t num, size, i;
    char *data = NULL;

    Debug((1, "Loading language %d from file `languages/%s'",
               index, filename));

    snprintf(buf, sizeof(buf), "languages/%s", filename);
    if (!(f = open_db("languages", buf, "r"))) {
        log_perror("Failed to load language %d (%s)", index, filename);
        return;
    } else if (read_uint32(&num, f) < 0) {
        slog(logsvc, "Failed to read number of strings for language %d (%s)",
                index, filename);
        return;
    } else if (read_uint32(&size, f) < 0) {
        slog(logsvc, "Failed to read data size for language %d (%s)",
                index, filename);
        return;
    } else if (num != NUM_BASE_STRINGS) {
        slog(logsvc, "Warning: Bad number of strings (%d, wanted %d) "
                "for language %d (%s)", num, NUM_BASE_STRINGS, index, filename);
        return;
    }

    langtexts[index] = scalloc(sizeof(char *), NUM_BASE_STRINGS + 1);
    if (num > NUM_BASE_STRINGS)
        num = NUM_BASE_STRINGS;
    langtexts[index][0] = data = smalloc(size+4);
    *((u_int32_t *)data) = size;
    data += 4;
    if (fread(data, size, 1, f->fp) != 1) {
        slog(logsvc, "Failed to read language data for language %d (%s)",
                index, filename);
        goto fail;
    }
    for (i = 0; i < num; i++) {
        int32_t pos;
        if (read_int32(&pos, f) < 0) {
            slog(logsvc, "Failed to read entry %d in language %d (%s) TOC",
                    i, index, filename);
            goto fail;
        }
        if (pos == -1) {
            langtexts[index][i+1] = NULL;
        } else {
            langtexts[index][i+1] = data + pos;
        }
    }
    close_db(f);
    is_loaded[index] = 1;
    return;

  fail:
    sfree(data);
    sfree(langtexts[index]);
    langtexts[index] = NULL;
    return;
}

/*************************************************************************/

/* Initialize list of lists. */

int lang_init()
{
    int i, j, n = 0;

    num_strings = NUM_BASE_STRINGS;

    /* Load language files */
    memset(is_loaded, 0, sizeof(is_loaded));
    for (i = 0; i < lenof(langorder); i++) {
        for (j = 0; filenames[j].num >= 0; j++) {
            if (filenames[j].num == langorder[i])
                break;
        }
        if (filenames[j].num >= 0) {
            load_lang(langorder[i], filenames[j].filename);
        } else {
            slog(logsvc, "BUG: lang_init(): no filename entry for language %d!",
                    langorder[i]);
        }
    }

    /* Make sure the default language has all strings available */
    if (!langtexts[DEF_LANGUAGE]) {
        slog(logsvc, "Unable to load default language");
        return 0;
    }

    for (i = 0; i < num_strings; i++) {
        if (!langtexts[DEF_LANGUAGE][i+1]) {
            if (is_loaded[LANG_ES] && langtexts[DEF_LANGUAGE][i+1]) {
                langtexts[langorder[DEF_LANGUAGE]][i+1] = langtexts[LANG_ES][i+1];
            } else {
                slog(logsvc, "String %s missing from default language", base_langstrs[i]);
                return 0;
            }
        }
    }

    /* Set up the list of available languages in langlist[] */
    for (i = 0; i < lenof(langorder); i++) {
        if (is_loaded[langorder[i]]) {
            langlist[n++] = langorder[i];
            for (j = 0; j < num_strings; j++) {
                if (!langtexts[langorder[i]][j+1]) {
                    langtexts[langorder[i]][j+1] = langtexts[LANG_ES][j+1];
                 }
            }
        }
    }
    while (n < lenof(langlist))
        langlist[n++] = -1;

    return 1;

}

/*************************************************************************/

/* Clean up language data. */

void lang_cleanup(void)
{
    int i;

    for (i = 0; i < NUM_LANGS; i++) {
        if (langtexts[i]) {
            sfree(langtexts[i][0]);
            sfree(langtexts[i]);
            langtexts[i] = NULL;
        }
    }
}

/*************************************************************************/
/*************************************************************************/

/* Return the language number for the given language name.  If the language
 * is not found, returns -1.
 */

int lookup_language(const char *name)
{
    int i;

    for (i = 0; filenames[i].num >= 0; i++) {
        if (strcasecmp(filenames[i].filename, name) == 0)
            return filenames[i].num;
    }
    return -1;
}

/*************************************************************************/

/* Return true if the given language is loaded, false otherwise. */

int have_language(int language)
{
    return language >= 0 && language < NUM_LANGS && is_loaded[language];
}

/*************************************************************************/

/* Return the index of the given string name.  If the name is not found,
 * returns -1.  Note that string names are case sensitive.
 */

int lookup_string(const char *name)
{
    int i;

    for (i = 0; i < num_strings; i++) {
        if (strcmp(base_langstrs[i], name) == 0)
            return i;
    }
    return -1;
}

/*************************************************************************/

/* Retrieve a message text using the language selected for the given
 * struct NickInfo (if NickInfo is NULL, use DEF_LANGUAGE).
 */

char *getstring_old(const struct NickInfo *ni, int index)
{
    return langtexts[((ni)?(ni)->language:DEF_LANGUAGE)][(index+1)];
}

char *getstring2(const struct NickInfo *ni, int index)
{
    int language;
    char *text;

    if (index < 0 || index >= num_strings) {
        slog(logsvc, "getstring(): BUG: index (%d) out of range!", index);
        return NULL;
    }

    language = (ni && ni->language != LANG_DEFAULT)
               ? ni->language
               : DEF_LANGUAGE;
    text = langtexts[language][index+1];
    if (!text)
        text = langtexts[DEF_LANGUAGE][index+1];
    return text;
}

/*************************************************************************/

/* Retrieve a message text using the given language. */

char *getstring_lang(int language, int index)
{
    char *text;

    if (language < 0 || language >= NUM_LANGS) {
        slog(logsvc, "getstring_lang(): BUG: language (%d) out of range!", language);
        language = DEF_LANGUAGE;
    } else if (index < 0 || index >= num_strings) {
        slog(logsvc, "getstring_lang(): BUG: index (%d) out of range!", index);
        return NULL;
    }
    text = langtexts[language][index+1];
    if (!text)
        text = langtexts[DEF_LANGUAGE][index+1];
    return text;
}

/*************************************************************************/
/*************************************************************************/

/* Format a string in a strftime()-like way, but heed the user's language
 * setting for month and day names.
 * Assumption: No month or day name has a length (including trailing null)
 * greater than BUFSIZE.
 */

int strftime_lang(char *buf, int size, const struct NickInfo *ni, int format, time_t time)
{
    int language = ni ? ni->language : DEF_LANGUAGE;
    char tmpbuf[BUFSIZE], buf2[BUFSIZE];
    char *s;
    int i, ret;
    struct tm *tm;

    if ((s = langtexts[language][STRFTIME_DAYS_SHORT+1]) != NULL) {
        for (i = 0; i < tm->tm_wday; i++)
            s += strcspn(s, "\n")+1;
        i = strcspn(s, "\n");
        strncpy(buf2, s, i);
        buf2[i] = 0;
        strnrepl(tmpbuf, sizeof(tmpbuf), "%a", buf2);
    }
    if ((s = langtexts[language][STRFTIME_DAYS_LONG+1]) != NULL) {
        for (i = 0; i < tm->tm_wday; i++)
            s += strcspn(s, "\n")+1;
        i = strcspn(s, "\n");
        strncpy(buf2, s, i);
        buf2[i] = 0;
        strnrepl(tmpbuf, sizeof(tmpbuf), "%A", buf2);
    }
    if ((s = langtexts[language][STRFTIME_MONTHS_SHORT+1]) != NULL) {
        for (i = 0; i < tm->tm_mon; i++)
            s += strcspn(s, "\n")+1;
        i = strcspn(s, "\n");
        strncpy(buf2, s, i);
        buf2[i] = 0;
        strnrepl(tmpbuf, sizeof(tmpbuf), "%b", buf2);
    }
    if ((s = langtexts[language][STRFTIME_MONTHS_LONG+1]) != NULL) {
        for (i = 0; i < tm->tm_mon; i++)
            s += strcspn(s, "\n")+1;
        i = strcspn(s, "\n");
        strncpy(buf2, s, i);
        buf2[i] = 0;
        strnrepl(tmpbuf, sizeof(tmpbuf), "%B", buf2);
    }
    ret = strftime(buf, size, tmpbuf, tm);
    if (ret >= size)  // buffer overflow, should be impossible
        return 0;
    return ret;
}

/*************************************************************************/

/* Generates a string describing the given length of time to one unit
 * (e.g. "3 days" or "10 hours"), or two units (e.g. "5 hours 25 minutes")
 * if the MT_DUALUNIT flag is specified.  The minimum resolution is one
 * minute, unless the MT_SECONDS flag is specified; the returned time is
 * rounded up if in the minimum unit, else rounded to the nearest integer.
 * The returned buffer is a static buffer which will be overwritten on the
 * next call to this routine.
 *
 * The MT_* flags (passed in the `flags' parameter) are defined in
 * language.h.
 */

char *maketime(const struct NickInfo *ni, time_t time, int flags)
{
    static char buf[BUFSIZE];
    int unit;

    if (time < 1)  /* Enforce a minimum of one second */
        time = 1;

    if ((flags & MT_SECONDS) && time <= 59) {

        unit = (time==1 ? STR_SECOND : STR_SECONDS);
        snprintf(buf, sizeof(buf), "%ld%s", (long)time, getstring(ni, unit));

    } else if (!(flags & MT_SECONDS) && time <= 59*60) {

        time = (time+59) / 60;
        unit = (time==1 ? STR_MINUTE : STR_MINUTES);
        snprintf(buf, sizeof(buf), "%ld%s", (long)time, getstring(ni, unit));


    } else if (flags & MT_DUALUNIT) {

        time_t time2;
        int unit2;

        if (time <= 59*60+59) {  /* 59 minutes, 59 seconds */
            time2 = time % 60;
            unit2 = (time2==1 ? STR_SECOND : STR_SECONDS);
            time = time / 60;
            unit = (time==1 ? STR_MINUTE : STR_MINUTES);
        } else if (time <= (23*60+59)*60+30) {  /* 23 hours, 59.5 minutes */
            time = (time+30) / 60;
            time2 = time % 60;
            unit2 = (time2==1 ? STR_MINUTE : STR_MINUTES);
            time = time / 60;
            unit = (time==1 ? STR_HOUR : STR_HOURS);
        } else {
            time = (time+(30*60)) / (60*60);
            time2 = time % 24;
            unit2 = (time2==1 ? STR_HOUR : STR_HOURS);
            time = time / 24;
            unit = (time==1 ? STR_DAY : STR_DAYS);
        }
        if (time2)
            snprintf(buf, sizeof(buf), "%ld%s%s%ld%s", (long)time,
                     getstring(ni, unit), getstring(ni, STR_TIMESEP),
                     (long)time2, getstring(ni, unit2));
        else
            snprintf(buf, sizeof(buf), "%ld%s", (long)time,
                     getstring(ni, unit));

    } else {  /* single unit */

        if (time <= 59*60+30) {  /* 59 min 30 sec; MT_SECONDS known true */
            time = (time+30) / 60;
            unit = (time==1 ? STR_MINUTE : STR_MINUTES);
        } else if (time <= (23*60+30)*60) {  /* 23 hours, 30 minutes */
            time = (time+(30*60)) / (60*60);
            unit = (time==1 ? STR_HOUR : STR_HOURS);
        } else {
            time = (time+(12*60*60)) / (24*60*60);
            unit = (time==1 ? STR_DAY : STR_DAYS);
        }
        snprintf(buf, sizeof(buf), "%ld%s", (long)time, getstring(ni, unit));

    }

    return buf;
}

/*************************************************************************/

/* Generates a description for the given expiration time in the form of
 * days, hours, minutes, seconds and/or a combination thereof.  May also
 * return "does not expire" or "already expired" messages if the expiration
 * time given is zero or earlier than the current time, respectively.
 * String is truncated if it would exceed `size' bytes (including trailing
 * null byte).
 */

void expires_in_lang(char *buf, int size, const struct NickInfo *ni, time_t expires)
{
    time_t seconds = expires - time(NULL);

    if (expires == 0) {
        strscpy(buf, getstring(ni, EXPIRES_NONE), size);
    } else if (seconds <= 0) {
        strscpy(buf, getstring(ni, EXPIRES_NOW), size);
    } else {
        if (seconds <= 0) {
            /* Already expired--it will be cleared out by the next get() */
            seconds = 1;
        }
        snprintf(buf, size, getstring(ni, EXPIRES_IN),
                 maketime(ni, seconds,MT_DUALUNIT));
    }
}

/*************************************************************************/

/* Send a syntax-error message to the user. */

void syntax_error(struct NetClient *service, struct NetClient *nc, const char *command, int msgnum)
{
    const char *str = getstring(nc->user->ni, msgnum);
    msg_lang(service, nc, SYNTAX_ERROR, str);
    msg_lang(service, nc, MORE_INFO, service->name, command);
}

/*************************************************************************/
/*************************************************************************/

