/*
 * ServicesIRCh - Services for IRCh, log.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2019 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "log.h"

#include "language.h"
#include "operserv.h"
#include "send.h"
#include "services.h"
#include "svc_memory.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/*************************************************************************/

static struct LogInfo *logs = NULL;
/* Log general de Services */
struct LogInfo *logsvc = NULL;
#if defined(DEBUGMODE)
/* Log modo debug */
static struct LogInfo *logdbg = NULL;
#endif

static int log_rotate(struct LogInfo *li);

/*************************************************************************/
/****************************** Statistics *******************************/
/*************************************************************************/

/* Return information on memory use.  Assumes pointers are valid. */

void get_log_stats(long *nrec, long *memuse)
{
    struct LogInfo *li;
    int numlogs = 0;
    long mem = 0;

    for (li = logs; li; li = li->next) {
        numlogs++;
        mem += sizeof(struct LogInfo);
        mem += strlen(li->name)+1;
        mem += strlen(li->filename)+1;
    }
    *nrec = numlogs;
    *memuse = mem;
}

/*************************************************************************/
/************************** Internal functions ***************************/
/*************************************************************************/

/* Open the log file.  Return -1 if the log file could not be opened, else
 * return 0. */

static int log_open(struct LogInfo *li)
{
    char filename[256];

    if (!li || li->fdlog)
        return 0;

    li->datalog = time(NULL);

    sprintf(filename, "%s/%s", SERVICES_LOGS_PATH, li->filename);
    li->fdlog = fopen(filename, "a");

    if (li->fdlog)
        setbuf(li->fdlog, NULL);

    return li->fdlog != NULL ? 0 : -1;
}

/* Close the log file. */

static void log_close(struct LogInfo *li)
{
    if (!li || !li->fdlog)
        return;

    fclose(li->fdlog);
    li->fdlog = NULL;
}

/*************************************************************************/
/************************** External functions ***************************/
/*************************************************************************/

struct LogInfo *log_add(char *name, char *filename)
{
    struct LogInfo *li;

    li = smalloc(sizeof(struct LogInfo));
    li->name = sstrdup(name);
    li->filename = sstrdup(filename);
    li->fdlog = NULL;
    li->datalog = time(NULL);

    li->next = logs;
    logs = li;

    log_open(li);

    return li;
}

/*************************************************************************/

int log_init(void)
{
    logsvc = log_add("services", log_filename);
    if (!logsvc)
        return -1;

#if defined(DEBUGMODE)
    if (debug)
        logdbg = log_add("debug", "debug.log");
#endif

    return logsvc->fdlog != NULL ? 0 : -1;
}

/*************************************************************************/

void log_openall()
{
    struct LogInfo *li;

    for (li = logs; li; li = li->next) {
        log_open(li);
    }
}

/*************************************************************************/

void log_closeall()
{
    struct LogInfo *li;

    for (li = logs; li; li = li->next) {
        log_close(li);
    }
}

/*************************************************************************/

int log_rotateall(struct NetClient *nc)
{
    struct LogInfo *li;

    for (li = logs; li; li = li->next) {
        log_rotate(li);
    }
}

/*************************************************************************/

static int log_rotate(struct LogInfo *li)
{
    struct tm tm;
    char old_filename[256];
    char new_filename[256];
    char new_name[25];
    FILE *tmplog;

    tm = *localtime(&li->datalog);
    strftime(new_name, sizeof(new_name)-1, "%Y%m%d", &tm);
    sprintf(new_filename, "%s/%s-%s.log", SERVICES_LOGS_PATH, li->name, new_name);
    sprintf(old_filename, "%s/%s", SERVICES_LOGS_PATH, li->filename);

    if ((tmplog = fopen(new_filename, "r"))) {
        fclose(tmplog);
        time(&li->datalog);
        slog(logsvc, "Warning: Cannot rotate %s. %s already exists", li->name, new_name);
        return 0;
    }

    log_close(li);

    if (rename(old_filename, new_filename) != 0) {
        wallops(NULL, "Error rotando fichero de log de %s.", li->name);
    }

    if (log_open(li) == -1) {
        wallops(NULL, "Error. No puede reabrirse el fichero de log de %s.", li->name);
        return 0;
    }

    return 1;
}

/*************************************************************************/

/* Rename the log file. Errors are wallop'ed if *u is NULL. Returns 1 if the
 * log file is successfully renamed otherwise 0 is returned. newname should 
 * not be user-supplied - rather use the date or something. Pass NULL for *u 
 * if called automatically. 
 * There is lots of room for improvement in the use of this function!
 *
 * -TheShadow (15 May 1999)
 */

int rename_log(struct NetClient *nc, struct LogInfo *li, const char *newname)
{
    FILE *temp_fp;
    int success = 1;

    if ((temp_fp = fopen(newname, "r"))) {
         fclose(temp_fp);
        if (nc) {
            msg_lang(ncOperServ, nc, OPER_ROTATELOG_FILE_EXISTS, newname);
        } else {
            wallops(NULL, "WARNING: Could not rename logfile, a file with "
                        "the name \2%s\2 already exists!", newname);
        }
        slog(logsvc, "ERROR: Could not rename logfile, a file with the name \2%s\2 "
                        "already exists!", newname);
        return 0;
    }

    Debug((1, "Renaming logfile from %s to %s ...", log_filename, newname));

    log_close(li);

    /* Note: If something goes wrong here, there is no way for us to log it.
     * We have to rely on the notice or an oper seeing the wallop. -TheShadow
     */
    if (rename(li->filename, newname) == 0) {
        if (nc) {
            msg_lang(ncOperServ, nc, OPER_ROTATELOG_RENAME_DONE, newname);
        }

    } else {
        if (nc) {
            msg_lang(ncOperServ, nc, OPER_ROTATELOG_RENAME_FAIL,
                        strerror(errno));
        } else {
            wallops(NULL, "WARNING: Logfile could not be renamed: %s",
                        strerror(errno));
        }
        success = 0;
    }

    if (log_open(li) == -1) {
        if (nc) {
            msg_lang(ncOperServ, nc, OPER_ROTATELOG_OPEN_LOG_FAIL,
                        strerror(errno));
        } else {
            /* If we ever get here, then we're pretty stuffed. Services is
             * probably going to have to be restarted. All we can do is yell
             * for help and tell who ever is listening to find the services
             * administrator (root) and notify them. -TheShadow */

            wallops(NULL, "WARNING: Logging could not be restarted: \2%s\2",
                        strerror(errno));
            wallops(NULL, "Please notify your services administrator, \2%s\2, "
                        "and include the above error message.", ServicesRoot);
            success = 0;
        }
    }

    if (debug && !success)
        slog(logsvc, "Logfile could not be renamed: %s", strerror(errno));

    if (success) {
        slog(logsvc, "Fresh logfile started. Previous logfile has been renamed to: %s",
                         newname);
    } else if (debug) {
        /* pointless if open_log() fails */
        slog(logsvc, "ERROR: Logfile could not be renamed: %s", strerror(errno));
    }

    return success;
}

/* This is the function that should be called by a user, and currently is :)
 * Basically we make a nice new log filename and then call the rename_log
 * function. Due to the nature of life, we have to pass the User structure
 * through to the rename code. This is the only sain way of being able to
 * notify users of problems while renaming the log file - we might not be able
 * to log the problem.
 * -TheShadow (15 May 1999)
 */

void rotate_log(struct NetClient *nc)
{
    struct LogInfo *li;
    time_t t;
    struct tm tm;
    char newname[23];

    /* TODO: Adaptar */
    return;

    time(&t);
    tm = *localtime(&t);

    for (li = logs; li; li = li->next) {
        strftime(newname, sizeof(newname)-1, "services-%Y%m%d.log", &tm);

        rename_log(nc, li, newname);
    }
}

/*************************************************************************/

/* Log stuff to the log file with a datestamp.  Note that errno is
 * preserved by this routine and log_perror().
 */

void slog(struct LogInfo *li, const char *fmt, ...)
{
    va_list args;
    time_t t;
    struct tm tm;
    char buf[256];
    int errno_save = errno;

    va_start(args, fmt);
    time(&t);
    tm = *localtime(&t);
#if HAVE_GETTIMEOFDAY
    if (debug) {
        char *s;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S", &tm);
        s = buf + strlen(buf);
        s += snprintf(s, sizeof(buf)-(s-buf), ".%06d", tv.tv_usec);
        strftime(s, sizeof(buf)-(s-buf)-1, " %Y] ", &tm);
    } else {
#endif
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S %Y] ", &tm);
#if HAVE_GETTIMEOFDAY
    }
#endif
    if (li->fdlog) {
        fputs(buf, li->fdlog);
        vfprintf(li->fdlog, fmt, args);
        fputc('\n', li->fdlog);
    }
    if (nofork) {
        fputs(buf, stderr);
        vfprintf(stderr, fmt, args);
        fputc('\n', stderr);
    }
    errno = errno_save;
}

/* Log stuff to the log file with a datestamp. */
#if defined(DEBUGMODE)
void logdebug(int level, const char *fmt, ...)
{
    va_list args;
    time_t t;
    struct tm tm;
    char buf[256];
    int errno_save = errno;

    if (debug < level)
        return;

    if (!logdbg)
        logdbg = log_add("debug", "debug.log");

    if (!logdbg)
        return;

    if (!logdbg || (debug < level))
        return;

    va_start(args, fmt);
    time(&t);
    tm = *localtime(&t);
#if HAVE_GETTIMEOFDAY
    if (debug) {
        char *s;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S", &tm);
        s = buf + strlen(buf);
        s += snprintf(s, sizeof(buf)-(s-buf), ".%06d", tv.tv_usec);
        strftime(s, sizeof(buf)-(s-buf)-1, " %Y] ", &tm);
    } else {
#endif
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S %Y] ", &tm);
#if HAVE_GETTIMEOFDAY
    }
#endif
    if (logdbg->fdlog) {
        fputs(buf, logdbg->fdlog);
        vfprintf(logdbg->fdlog, fmt, args);
        fputc('\n', logdbg->fdlog);
    }
    if (nofork) {
        fputs(buf, stderr);
        vfprintf(stderr, fmt, args);
        fputc('\n', stderr);
    }
    errno = errno_save;
}
#endif /* DEBUGMODE */

/* Like log(), but tack a ": " and a system error message (as returned by
 * strerror()) onto the end.
 */

void log_perror(const char *fmt, ...)
{
    va_list args;
    time_t t;
    struct tm tm;
    char buf[256];
    int errno_save = errno;

    va_start(args, fmt);
    time(&t);
    tm = *localtime(&t);
#if HAVE_GETTIMEOFDAY
    if (debug) {
        char *s;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S", &tm);
        s = buf + strlen(buf);
        s += snprintf(s, sizeof(buf)-(s-buf), ".%06d", tv.tv_usec);
        strftime(s, sizeof(buf)-(s-buf)-1, " %Y] ", &tm);
    } else {
#endif
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S %Y] ", &tm);
#if HAVE_GETTIMEOFDAY
    }
#endif
    if (logsvc && logsvc->fdlog) {
        fputs(buf, logsvc->fdlog);
        vfprintf(logsvc->fdlog, fmt, args);
        fprintf(logsvc->fdlog, ": %s\n", strerror(errno_save));
    }
    if (nofork) {
        fputs(buf, stderr);
        vfprintf(stderr, fmt, args);
        fprintf(stderr, ": %s\n", strerror(errno_save));
    }
    errno = errno_save;
}

/*************************************************************************/

/* We've hit something we can't recover from.  Let people know what
 * happened, then go down.
 */

void fatal(const char *fmt, ...)
{
    va_list args;
    time_t t;
    struct tm tm;
    char buf[256], buf2[4096];

    va_start(args, fmt);
    time(&t);
    tm = *localtime(&t);
#if HAVE_GETTIMEOFDAY
    if (debug) {
        char *s;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S", &tm);
        s = buf + strlen(buf);
        s += snprintf(s, sizeof(buf)-(s-buf), ".%06d", tv.tv_usec);
        strftime(s, sizeof(buf)-(s-buf)-1, " %Y] ", &tm);
    } else {
#endif
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S %Y] ", &tm);
#if HAVE_GETTIMEOFDAY
    }
#endif
    vsnprintf(buf2, sizeof(buf2), fmt, args);
    if (logsvc && logsvc->fdlog)
        fprintf(logsvc->fdlog, "%sFATAL: %s\n", buf, buf2);
    if (nofork)
        fprintf(stderr, "%sFATAL: %s\n", buf, buf2);
    if (servsock >= 0)
        wallops(NULL, "FATAL ERROR!  %s", buf2);
    exit(1);
}


/* Same thing, but do it like perror(). */

void fatal_perror(const char *fmt, ...)
{
    va_list args;
    time_t t;
    struct tm tm;
    char buf[256], buf2[4096];
    int errno_save = errno;

    va_start(args, fmt);
    time(&t);
    tm = *localtime(&t);
#if HAVE_GETTIMEOFDAY
    if (debug) {
        char *s;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S", &tm);
        s = buf + strlen(buf);
        s += snprintf(s, sizeof(buf)-(s-buf), ".%06d", tv.tv_usec);
        strftime(s, sizeof(buf)-(s-buf)-1, " %Y] ", &tm);
    } else {
#endif
        strftime(buf, sizeof(buf)-1, "[%b %d %H:%M:%S %Y] ", &tm);
#if HAVE_GETTIMEOFDAY
    }
#endif
    vsnprintf(buf2, sizeof(buf2), fmt, args);
    if (logsvc && logsvc->fdlog)
        fprintf(logsvc->fdlog, "%sFATAL: %s: %s\n", buf, buf2, strerror(errno_save));
    if (stderr)
        fprintf(stderr, "%sFATAL: %s: %s\n", buf, buf2, strerror(errno_save));
    if (servsock >= 0)
        wallops(NULL, "FATAL ERROR!  %s: %s", buf2, strerror(errno_save));
    exit(1);
}

/*************************************************************************/
