/*
 * ServicesIRCh - Services for IRCh, match.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1998 Andy Church <achurch@dragonfire.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "match.h"

#include "charset.h"
#include "network.h"

#include <limits.h>
#include <netinet/in.h>
#include <string.h>

/*************************************************************************/

/* match_wild:  Attempt to match a string to a pattern which might contain
 *              '*' or '?' wildcards.  Return 1 if the string matches the
 *              pattern, 0 if not.
 */

static int do_match_wild(const char *pattern, const char *str, int docase)
{
    char c;
    const char *s;

    /* This WILL eventually terminate: either by *pattern == 0, or by a
     * trailing '*'. */

    for (;;) {
        switch (c = *pattern++) {
          case 0:
            if (!*str)
                return 1;
            return 0;
          case '?':
            if (!*str)
                return 0;
            str++;
            break;
          case '*':
            if (!*pattern)
                return 1;   /* trailing '*' matches everything else */
            s = str;
            while (*s) {
                if ((docase ? (*s==*pattern) : (ToLower(*s)==ToLower(*pattern)))
                                        && do_match_wild(pattern, s, docase))
                    return 1;
                s++;
            }
            break;
          default:
            if (docase ? (*str++ != c) : (ToLower(*str++) != ToLower(c)))
                return 0;
            break;
        } /* switch */
    }
}


int match_wild(const char *pattern, const char *str)
{
    return do_match_wild(pattern, str, 1);
}

int match_wild_nocase(const char *pattern, const char *str)
{
    return do_match_wild(pattern, str, 0);
}

int match_wild_x_case(const char *pattern, const char *str, int docase)
{
    return do_match_wild(pattern, str, docase);
}

/*************************************************************************/

/** Test whether an address matches the most significant bits of a mask.
 * @param[in] addr Address to test.
 * @param[in] mask Address to test against.
 * @param[in] bits Number of bits to test.
 * @return 0 on mismatch, 1 if bits < 128 and all bits match; -1 if
 * bits == 128 and all bits match.
 *
 * Codigo procedente de Undernet
 */
int ipmask_check(const struct irc_in_addr *addr, const struct irc_in_addr *mask, unsigned char bits)
{
  int k;

  for (k = 0; k < 8; k++) {
    if (bits < 16)
      return !(htons(addr->in6_16[k] ^ mask->in6_16[k]) >> (16-bits));
    if (addr->in6_16[k] != mask->in6_16[k])
      return 0;
    if (!(bits -= 16))
      return 1;
  }
  return -1;
}
