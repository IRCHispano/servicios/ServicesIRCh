/*
 * ServicesIRCh - Services for IRCh, memoserv.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "memoserv.h"

#include "chanserv.h"
#include "commands.h"
#include "datafiles.h"
#include "hash.h"
#include "language.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "nickserv.h"
#include "send.h"
#include "service.h"
#include "services.h"
#include "svc_memory.h"
#include "users.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* provisional */
#include "operserv.h"

/*************************************************************************/

struct NetClient *ncMemoServ;

static int delmemo(struct MemoInfo *mi, int num);

static void do_help(struct NetClient *nc);
static void do_send(struct NetClient *nc);
static void do_list(struct NetClient *nc);
static void do_read(struct NetClient *nc);
static void do_del(struct NetClient *nc);
static void do_set(struct NetClient *nc);
static void do_set_notify(struct NetClient *nc, struct MemoInfo *mi, char *param);
static void do_set_limit(struct NetClient *nc, struct MemoInfo *mi, char *param);
static void do_info(struct NetClient *nc);

/*************************************************************************/

static struct Command cmds[] = {
    { "HELP",       do_help, NULL,  -1,                      -1,-1,-1,-1 },
    { "SEND",       do_send, NULL,  MEMO_HELP_SEND,          -1,-1,-1,-1 },
    { "LIST",       do_list, NULL,  MEMO_HELP_LIST,          -1,-1,-1,-1 },
    { "READ",       do_read, NULL,  MEMO_HELP_READ,          -1,-1,-1,-1 },
    { "DEL",        do_del,  NULL,  MEMO_HELP_DEL,           -1,-1,-1,-1 },
    { "SET",        do_set,  NULL,  MEMO_HELP_SET,           -1,-1,-1,-1 },
    { "SET NOTIFY", NULL,    NULL,  MEMO_HELP_SET_NOTIFY,    -1,-1,-1,-1 },
    { "SET LIMIT",  NULL,    NULL,  MEMO_HELP_SET_LIMIT,     -1,-1,-1,-1 },
    { "INFO",       do_info, NULL,  -1,
                MEMO_HELP_INFO, MEMO_SERVADMIN_HELP_INFO,
                MEMO_SERVADMIN_HELP_INFO, MEMO_SERVADMIN_HELP_INFO },
    { NULL }
};

/*************************************************************************/
/*************************************************************************/

/* MemoServ initialization. */

void ms_init(void)
{
    struct Command *cmd;

    ncMemoServ = service_create(s_MemoServ, NULL, NULL, desc_MemoServ);
    ncMemoServ->service->has_priv = NULL;
    ncMemoServ->service->func = memoserv;
    set_user_modes(&myService, ncMemoServ, "+Bd");
    service_introduce(ncMemoServ);

    cmd = lookup_cmd(cmds, "SET LIMIT");
    if (cmd)
        cmd->help_param1 = (char *)(long)MSMaxMemos;
}

/*************************************************************************/

/* memoserv:  Main MemoServ routine.
 *            Note that the User structure passed to the do_* routines will
 *            always be valid (non-NULL) and will always have a valid
 *            NickInfo pointer in the `ni' field.
 */

void memoserv(struct NetClient *source, char *buf)
{
    char *cmd, *s;

    cmd = strtok(buf, " ");
    if (!cmd) {
        return;
    } else if (strcasecmp(cmd, "\1PING") == 0) {
        if (!(s = strtok(NULL, "")))
            s = "\1";
        notice(ncMemoServ, source, "\1PING %s", s);
    } else if (skeleton) {
        msg_lang(ncMemoServ, source, SERVICE_OFFLINE, s_MemoServ);
    } else {
        if (!source->user->ni && strcasecmp(cmd, "HELP") != 0)
            msg_lang(ncMemoServ, source, NICK_NOT_REGISTERED_HELP, s_NickServ);
        else
            run_cmd(ncMemoServ, source, cmds, cmd);
    }
}

/*************************************************************************/

/* Compatibility memo load routine. */

#define SAFE(x) do {                                        \
    if ((x) < 0) {                                        \
        if (!forceload)                                        \
            fatal("Read error on memo.db");                \
        failed = 1;                                        \
        break;                                                \
    }                                                        \
} while (0)

void load_old_ms_dbase(void)
{
    struct dbFILE *f;
    int ver, i, j, c;
    struct NickInfo *ni;
    struct Memo *memos;
    struct memolist_ {
        struct memolist_ *next, *prev;
        char nick[NICKMAX];
        long n_memos;
        struct Memo *memos;
        long reserved[4];
    } old_memolist;
    struct {
        char sender[NICKMAX];
        long number;
        time_t time;
        char *text;
        short flags;
        short reserved_s;
        long reserved[3];
    } oldmemo;
    int failed = 0;

    if (!(f = open_db(s_MemoServ, "memo.db", "r")))
        return;
    switch (ver = get_file_version(f)) {
      case 4:
      case 3:
      case 2:
      case 1:
        for (i = 33; i < 256 && !failed; ++i) {
            while ((c = getc_db(f)) != 0) {
                if (c != 1)
                    fatal("Invalid format in memo.db");
                SAFE(read_variable(old_memolist, f));
                if (debug >= 3)
                    slog(logsvc, "debug: load_old_ms_dbase: got memolist for %s",
                                old_memolist.nick);
                old_memolist.memos = memos =
                                smalloc(sizeof(struct Memo) * old_memolist.n_memos);
                for (j = 0; j < old_memolist.n_memos; j++, memos++) {
                    SAFE(read_variable(oldmemo, f));
                    strscpy(memos->sender, oldmemo.sender, NICKMAX);
                    memos->number = oldmemo.number;
                    memos->time = oldmemo.time;
                    memos->flags = oldmemo.flags;
                }
                memos = old_memolist.memos;
                for (j = 0; j < old_memolist.n_memos; j++) {
                    if (read_string(&memos[j].text, f) < 0)
                        fatal("Read error on memo.db");
                }
                ni = findnick(old_memolist.nick);
                if (ni) {
                    ni->memos.memocount = old_memolist.n_memos;
                    ni->memos.memos = old_memolist.memos;
                }
            }
        }
        break;
      default:
        fatal("Unsupported version number (%d) on memo.db", ver);
    } /* switch (version) */
    close_db(f);
}

/*************************************************************************/

/* check_memos:  See if the given user has any unread memos, and send a
 *               NOTICE to that user if so (and if the appropriate flag is
 *               set).
 */

void check_memos(struct NetClient *nc)
{
    struct NickInfo *ni;
    int i, newcnt = 0;

    if (!(ni = nc->user->ni) || !nick_recognized(nc) ||
                         !(ni->flags & NI_MEMO_SIGNON))
        return;

    for (i = 0; i < ni->memos.memocount; i++) {
        if (ni->memos.memos[i].flags & MF_UNREAD)
            newcnt++;
    }
    if (newcnt > 0) {
        msg_lang(ncMemoServ, nc,
                newcnt==1 ? MEMO_HAVE_NEW_MEMO : MEMO_HAVE_NEW_MEMOS, newcnt);
        if (newcnt == 1 && (ni->memos.memos[i-1].flags & MF_UNREAD)) {
            msg_lang(ncMemoServ, nc, MEMO_TYPE_READ_LAST, s_MemoServ);
        } else if (newcnt == 1) {
            for (i = 0; i < ni->memos.memocount; i++) {
                if (ni->memos.memos[i].flags & MF_UNREAD)
                    break;
            }
            msg_lang(ncMemoServ, nc, MEMO_TYPE_READ_NUM, s_MemoServ,
                        ni->memos.memos[i].number);
        } else {
            msg_lang(ncMemoServ, nc, MEMO_TYPE_LIST_NEW, s_MemoServ);
        }
    }
    if (ni->memos.memomax > 0 && ni->memos.memocount >= ni->memos.memomax) {
        if (ni->memos.memocount > ni->memos.memomax)
            msg_lang(ncMemoServ, nc, MEMO_OVER_LIMIT, ni->memos.memomax);
        else
            msg_lang(ncMemoServ, nc, MEMO_AT_LIMIT, ni->memos.memomax);
    }
}

/*************************************************************************/
/*********************** MemoServ private routines ***********************/
/*************************************************************************/

/* Return the MemoInfo corresponding to the given nick or channel name.
 * Return in `ischan' 1 if the name was a channel name, else 0.
 */

static struct MemoInfo *getmemoinfo(const char *name, int *ischan)
{
    if (*name == '#') {
        struct ChannelInfo *ci;
        if (ischan)
            *ischan = 1;
        ci = cs_findchan(name);
        if (ci)
            return &ci->memos;
        else
            return NULL;
    } else {
        struct NickInfo *ni;
        if (ischan)
            *ischan = 0;
        ni = findnick(name);
        if (ni)
            return &getlink(ni)->memos;
        else
            return NULL;
    }
}

/*************************************************************************/

/* Delete a memo by number.  Return 1 if the memo was found, else 0. */

static int delmemo(struct MemoInfo *mi, int num)
{
    int i;

    for (i = 0; i < mi->memocount; i++) {
        if (mi->memos[i].number == num)
            break;
    }
    if (i < mi->memocount) {
        free(mi->memos[i].text); /* Deallocate memo text memory */
        mi->memocount--;         /* One less memo now */
        if (i < mi->memocount)         /* Move remaining memos down a slot */
            memmove(mi->memos + i, mi->memos + i+1,
                                sizeof(struct Memo) * (mi->memocount - i));
        if (mi->memocount == 0) { /* If no more memos, free array */
            free(mi->memos);
            mi->memos = NULL;
        }
        return 1;
    } else {
        return 0;
    }
}

/*************************************************************************/
/*********************** MemoServ command routines ***********************/
/*************************************************************************/

/* Return a help message. */

static void do_help(struct NetClient *nc)
{
    char *cmd = strtok(NULL, "");

    if (!cmd) {
        msg_help(ncMemoServ, nc, MEMO_HELP, s_ChanServ);
    } else {
        help_cmd(ncMemoServ, nc, cmds, cmd);
    }
}

/*************************************************************************/

/* Send a memo to a nick/channel. */

static void do_send(struct NetClient *nc)
{
    char *source = nc->name;
    int ischan;
    struct MemoInfo *mi;
    struct Memo *m;
    char *name = strtok(NULL, " ");
    char *text = strtok(NULL, "");
    time_t now = time(NULL);
    int is_servadmin = is_services_admin(nc);

    if (!text) {
        syntax_error(ncMemoServ, nc, "SEND", MEMO_SEND_SYNTAX);

    } else if (!nick_recognized(nc)) {
        msg_lang(ncMemoServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);

    } else if (!(mi = getmemoinfo(name, &ischan))) {
        msg_lang(ncMemoServ, nc,
                ischan ? CHAN_X_NOT_REGISTERED : NICK_X_NOT_REGISTERED, name);

    } else if (MSSendDelay > 0 &&
                nc && nc->user->lastmemosend+MSSendDelay > now && !is_servadmin) {
        nc->user->lastmemosend = now;
        msg_lang(ncMemoServ, nc, MEMO_SEND_PLEASE_WAIT, MSSendDelay);

    } else if (mi->memomax == 0 && !is_servadmin) {
        msg_lang(ncMemoServ, nc, MEMO_X_GETS_NO_MEMOS, name);

    } else if (mi->memomax > 0 && mi->memocount >= mi->memomax
                && !is_servadmin) {
        msg_lang(ncMemoServ, nc, MEMO_X_HAS_TOO_MANY_MEMOS, name);

    } else {
        nc->user->lastmemosend = now;
        mi->memocount++;
        mi->memos = srealloc(mi->memos, sizeof(struct Memo) * mi->memocount);
        m = &mi->memos[mi->memocount-1];
        strscpy(m->sender, source, NICKMAX);
        if (mi->memocount > 1) {
            m->number = m[-1].number + 1;
            if (m->number < 1) {
                int i;
                for (i = 0; i < mi->memocount; i++)
                    mi->memos[i].number = i+1;
            }
        } else {
            m->number = 1;
        }
        m->time = time(NULL);
        m->text = sstrdup(text);
        m->flags = MF_UNREAD;
        msg_lang(ncMemoServ, nc, MEMO_SENT, name);
        if (!ischan) {
            struct NickInfo *ni = getlink(findnick(name));
            if (ni->flags & NI_MEMO_RECEIVE) {
                if (MSNotifyAll) {
                    for (nc = firstnc(); nc; nc = nextnc()) {
                        if (!IsUser(nc))
                            continue;

                        if (nc->user->real_ni == ni) {
                            msg_lang(ncMemoServ, nc, MEMO_NEW_MEMO_ARRIVED,
                                        source, s_MemoServ, m->number);
                        }
                    }
                } else {
                    nc = FindUser(name);
                    if (nc) {
                        msg_lang(ncMemoServ, nc, MEMO_NEW_MEMO_ARRIVED,
                                        source, s_MemoServ, m->number);
                    }
                } /* if (MSNotifyAll) */
            } /* if (flags & MEMO_RECEIVE) */
        } /* if (!ischan) */
    } /* if command is valid */
}

/*************************************************************************/

/* Display a single memo entry, possibly printing the header first. */

static int list_memo(struct NetClient *nc, int index, struct MemoInfo *mi, int *sent_header,
                        int new, const char *chan)
{
    struct Memo *m;
    char timebuf[64];

    if (index < 0 || index >= mi->memocount)
        return 0;
    if (!*sent_header) {
        if (chan) {
            msg_lang(ncMemoServ, nc,
                        new ? MEMO_LIST_CHAN_NEW_MEMOS : MEMO_LIST_CHAN_MEMOS,
                        chan, s_MemoServ, chan);
        } else {
            msg_lang(ncMemoServ, nc,
                        new ? MEMO_LIST_NEW_MEMOS : MEMO_LIST_MEMOS,
                        nc->name, s_MemoServ);
        }
        msg_lang(ncMemoServ, nc, MEMO_LIST_HEADER);
        *sent_header = 1;
    }
    m = &mi->memos[index];
    strftime_lang(timebuf, sizeof(timebuf),
                nc->user->ni, STRFTIME_DATE_TIME_FORMAT, m->time);
    timebuf[sizeof(timebuf)-1] = 0;        /* just in case */
    msg_lang(ncMemoServ, nc, MEMO_LIST_FORMAT,
                (m->flags & MF_UNREAD) ? '*' : ' ',
                m->number, m->sender, timebuf);
    return 1;
}

static int list_memo_callback(struct NetClient *nc, int num, va_list args)
{
    struct MemoInfo *mi = va_arg(args, struct MemoInfo *);
    int *sent_header = va_arg(args, int *);
    const char *chan = va_arg(args, const char *);
    int i;

    for (i = 0; i < mi->memocount; i++) {
        if (mi->memos[i].number == num)
            break;
    }
    /* Range checking done by list_memo() */
    return list_memo(nc, i, mi, sent_header, 0, chan);
}


/* List the memos (if any) for the source nick or given channel. */

static void do_list(struct NetClient *nc)
{
    char *param = strtok(NULL, " "), *chan = NULL;
    struct ChannelInfo *ci;
    struct MemoInfo *mi;
    struct Memo *m;
    int i;

    if (param && *param == '#') {
        chan = param;
        param = strtok(NULL, " ");
        if (!(ci = cs_findchan(chan))) {
            msg_lang(ncMemoServ, nc, CHAN_X_NOT_REGISTERED, chan);
            return;
        } else if (!check_access(nc, ci, CA_MEMO)) {
            msg_lang(ncMemoServ, nc, ACCESS_DENIED);
            return;
        }
        mi = &ci->memos;
    } else {
        if (!nick_identified(nc)) {
            msg_lang(ncMemoServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
            return;
        }
        mi = &nc->user->ni->memos;
    }
    if (param && !isdigit(*param) && strcasecmp(param, "NEW") != 0) {
        syntax_error(ncMemoServ, nc, "LIST", MEMO_LIST_SYNTAX);
    } else if (mi->memocount == 0) {
        if (chan)
            msg_lang(ncMemoServ, nc, MEMO_X_HAS_NO_MEMOS, chan);
        else
            msg_lang(ncMemoServ, nc, MEMO_HAVE_NO_MEMOS);
    } else {
        int sent_header = 0;
        if (param && isdigit(*param)) {
            process_numlist(param, NULL, list_memo_callback, nc,
                                        mi, &sent_header, chan);
        } else {
            if (param) {
                for (i = 0, m = mi->memos; i < mi->memocount; i++, m++) {
                    if (m->flags & MF_UNREAD)
                        break;
                }
                if (i == mi->memocount) {
                    if (chan)
                        msg_lang(ncMemoServ, nc, MEMO_X_HAS_NO_NEW_MEMOS,
                                        chan);
                    else
                        msg_lang(ncMemoServ, nc, MEMO_HAVE_NO_NEW_MEMOS);
                    return;
                }
            }
            for (i = 0, m = mi->memos; i < mi->memocount; i++, m++) {
                if (param && !(m->flags & MF_UNREAD))
                    continue;
                list_memo(nc, i, mi, &sent_header, param != NULL, chan);
            }
        }
    }
}

/*************************************************************************/

/* Send a single memo to the given user. */

static int read_memo(struct NetClient *nc, int index, struct MemoInfo *mi, const char *chan)
{
    struct Memo *m;
    char timebuf[64];

    if (index < 0 || index >= mi->memocount)
        return 0;
    m = &mi->memos[index];
    strftime_lang(timebuf, sizeof(timebuf),
                nc->user->ni, STRFTIME_DATE_TIME_FORMAT, m->time);
    timebuf[sizeof(timebuf)-1] = 0;
    if (chan)
        msg_lang(ncMemoServ, nc, MEMO_CHAN_HEADER, m->number,
                m->sender, timebuf, s_MemoServ, chan, m->number);
    else
        msg_lang(ncMemoServ, nc, MEMO_HEADER, m->number,
                m->sender, timebuf, s_MemoServ, m->number);
    msg_lang(ncMemoServ, nc, MEMO_TEXT, m->text);
    m->flags &= ~MF_UNREAD;
    return 1;
}

static int read_memo_callback(struct NetClient *nc, int num, va_list args)
{
    struct MemoInfo *mi = va_arg(args, struct MemoInfo *);
    const char *chan = va_arg(args, const char *);
    int i;

    for (i = 0; i < mi->memocount; i++) {
        if (mi->memos[i].number == num)
            break;
    }
    /* Range check done in read_struct Memo */
    return read_memo(nc, i, mi, chan);
}


/* Read memos. */

static void do_read(struct NetClient *nc)
{
    struct MemoInfo *mi;
    struct ChannelInfo *ci;
    char *numstr = strtok(NULL, " "), *chan = NULL;
    int num, count;

    if (numstr && *numstr == '#') {
        chan = numstr;
        numstr = strtok(NULL, " ");
        if (!(ci = cs_findchan(chan))) {
            msg_lang(ncMemoServ, nc, CHAN_X_NOT_REGISTERED, chan);
            return;
        } else if (!check_access(nc, ci, CA_MEMO)) {
            msg_lang(ncMemoServ, nc, ACCESS_DENIED);
            return;
        }
        mi = &ci->memos;
    } else {
        if (!nick_identified(nc)) {
            msg_lang(ncMemoServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
            return;
        }
        mi = &nc->user->ni->memos;
    }
    num = numstr ? atoi(numstr) : -1;
    if (!numstr || (strcasecmp(numstr,"LAST") != 0 && strcasecmp(numstr,"NEW") != 0
                    && num <= 0)) {
        syntax_error(ncMemoServ, nc, "READ", MEMO_READ_SYNTAX);

    } else if (mi->memocount == 0) {
        if (chan)
            msg_lang(ncMemoServ, nc, MEMO_X_HAS_NO_MEMOS, chan);
        else
            msg_lang(ncMemoServ, nc, MEMO_HAVE_NO_MEMOS);

    } else {
        int i;

        if (strcasecmp(numstr, "NEW") == 0) {
            int readcount = 0;
            for (i = 0; i < mi->memocount; i++) {
                if (mi->memos[i].flags & MF_UNREAD) {
                    read_memo(nc, i, mi, chan);
                    readcount++;
                }
            }
            if (!readcount) {
                if (chan)
                    msg_lang(ncMemoServ, nc, MEMO_X_HAS_NO_NEW_MEMOS, chan);
                else
                    msg_lang(ncMemoServ, nc, MEMO_HAVE_NO_NEW_MEMOS);
            }
        } else if (strcasecmp(numstr, "LAST") == 0) {
            for (i = 0; i < mi->memocount-1; i++)
                ;
            read_memo(nc, i, mi, chan);
        } else {        /* number[s] */
            if (!process_numlist(numstr, &count, read_memo_callback, nc,
                                                                mi, chan)) {
                if (count == 1)
                    msg_lang(ncMemoServ, nc, MEMO_DOES_NOT_EXIST, num);
                else
                    msg_lang(ncMemoServ, nc, MEMO_LIST_NOT_FOUND, numstr);
            }
        }

    }
}

/*************************************************************************/

/* Delete a single memo from a MemoInfo. */

static int del_memo_callback(struct NetClient *nc, int num, va_list args)
{
    struct MemoInfo *mi = va_arg(args, struct MemoInfo *);
    int *last = va_arg(args, int *);
    int *last0 = va_arg(args, int *);
    char **end = va_arg(args, char **);
    int *left = va_arg(args, int *);

    if (delmemo(mi, num)) {
        if (num != (*last)+1) {
            if (*last != -1) {
                int len;
                if (*last0 != *last)
                    len = snprintf(*end, *left, ",%d-%d", *last0, *last);
                else
                    len = snprintf(*end, *left, ",%d", *last);
                *end += len;
                *left -= len;
            }
            *last0 = num;
        }
        *last = num;
        return 1;
    } else {
        return 0;
    }
}


/* Delete memos. */

static void do_del(struct NetClient *nc)
{
    struct MemoInfo *mi;
    struct ChannelInfo *ci;
    char *numstr = strtok(NULL, ""), *chan = NULL;
    int last, last0, i;
    char buf[BUFSIZE], *end;
    int delcount, count, left;

    if (numstr && *numstr == '#') {
        chan = strtok(numstr, " ");
        numstr = strtok(NULL, "");
        if (!(ci = cs_findchan(chan))) {
            msg_lang(ncMemoServ, nc, CHAN_X_NOT_REGISTERED, chan);
            return;
        } else if (!check_access(nc, ci, CA_MEMO)) {
            msg_lang(ncMemoServ, nc, ACCESS_DENIED);
            return;
        }
        mi = &ci->memos;
    } else {
        if (!nick_identified(nc)) {
            msg_lang(ncMemoServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
            return;
        }
        mi = &nc->user->ni->memos;
    }
    if (!numstr || (!isdigit(*numstr) && strcasecmp(numstr, "ALL") != 0)) {
        syntax_error(ncMemoServ, nc, "DEL", MEMO_DEL_SYNTAX);
    } else if (mi->memocount == 0) {
        if (chan)
            msg_lang(ncMemoServ, nc, MEMO_X_HAS_NO_MEMOS, chan);
        else
            msg_lang(ncMemoServ, nc, MEMO_HAVE_NO_MEMOS);
    } else {
        if (isdigit(*numstr)) {
            /* Delete a specific memo or memos. */
            last = -1;   /* Last memo deleted */
            last0 = -1;  /* Beginning of range of last memos deleted */
            end = buf;
            left = sizeof(buf);
            delcount = process_numlist(numstr, &count, del_memo_callback, nc, mi,
                                                &last, &last0, &end, &left);
            if (last != -1) {
                /* Some memos got deleted; tell them which ones. */
                if (delcount > 1) {
                    if (last0 != last)
                        end += snprintf(end, sizeof(buf)-(end-buf),
                                ",%d-%d", last0, last);
                    else
                        end += snprintf(end, sizeof(buf)-(end-buf),
                                ",%d", last);
                    /* "buf+1" here because *buf == ',' */
                    msg_lang(ncMemoServ, nc, MEMO_DELETED_SEVERAL, buf+1);
                } else {
                    msg_lang(ncMemoServ, nc, MEMO_DELETED_ONE, last);
                }
            } else {
                /* No memos were deleted.  Tell them so. */
                if (count == 1)
                    msg_lang(ncMemoServ, nc, MEMO_DOES_NOT_EXIST,
                                atoi(numstr));
                else
                    msg_lang(ncMemoServ, nc, MEMO_DELETED_NONE);
            }
        } else {
            /* Delete all memos. */
            for (i = 0; i < mi->memocount; i++)
                free(mi->memos[i].text);
            free(mi->memos);
            mi->memos = NULL;
            mi->memocount = 0;
            msg_lang(ncMemoServ, nc, MEMO_DELETED_ALL);
        }
    }
}

/*************************************************************************/

static void do_set(struct NetClient *nc)
{
    char *cmd    = strtok(NULL, " ");
    char *param  = strtok(NULL, "");
    struct MemoInfo *mi = &nc->user->ni->memos;

    if (readonly) {
        msg_lang(ncMemoServ, nc, MEMO_SET_DISABLED);
        return;
    }
    if (!param) {
        syntax_error(ncMemoServ, nc, "SET", MEMO_SET_SYNTAX);
    } else if (!nick_identified(nc)) {
        msg_lang(ncMemoServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
        return;
    } else if (strcasecmp(cmd, "NOTIFY") == 0) {
        do_set_notify(nc, mi, param);
    } else if (strcasecmp(cmd, "LIMIT") == 0) {
        do_set_limit(nc, mi, param);
    } else {
        msg_lang(ncMemoServ, nc, MEMO_SET_UNKNOWN_OPTION, strupper(cmd));
        msg_lang(ncMemoServ, nc, MORE_INFO, s_MemoServ, "SET");
    }
}

/*************************************************************************/

static void do_set_notify(struct NetClient *nc, struct MemoInfo *mi, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        nc->user->ni->flags |= NI_MEMO_SIGNON | NI_MEMO_RECEIVE;
        msg_lang(ncMemoServ, nc, MEMO_SET_NOTIFY_ON, s_MemoServ);
    } else if (strcasecmp(param, "LOGON") == 0) {
        nc->user->ni->flags |= NI_MEMO_SIGNON;
        nc->user->ni->flags &= ~NI_MEMO_RECEIVE;
        msg_lang(ncMemoServ, nc, MEMO_SET_NOTIFY_LOGON, s_MemoServ);
    } else if (strcasecmp(param, "NEW") == 0) {
        nc->user->ni->flags &= ~NI_MEMO_SIGNON;
        nc->user->ni->flags |= NI_MEMO_RECEIVE;
        msg_lang(ncMemoServ, nc, MEMO_SET_NOTIFY_NEW, s_MemoServ);
    } else if (strcasecmp(param, "OFF") == 0) {
        nc->user->ni->flags &= ~(NI_MEMO_SIGNON | NI_MEMO_RECEIVE);
        msg_lang(ncMemoServ, nc, MEMO_SET_NOTIFY_OFF, s_MemoServ);
    } else {
        syntax_error(ncMemoServ, nc, "SET NOTIFY", MEMO_SET_NOTIFY_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_limit(struct NetClient *nc, struct MemoInfo *mi, char *param)
{
    char *p1 = strtok(param, " ");
    char *p2 = strtok(NULL, " ");
    char *p3 = strtok(NULL, " ");
    char *user = NULL, *chan = NULL;
    int32_t limit;
    struct NickInfo *ni = nc->user->ni;
    struct ChannelInfo *ci = NULL;
    int is_servadmin = is_services_admin(nc);

    if (p1 && *p1 == '#') {
        chan = p1;
        p1 = p2;
        p2 = p3;
        p3 = strtok(NULL, " ");
        if (!(ci = cs_findchan(chan))) {
            msg_lang(ncMemoServ, nc, CHAN_X_NOT_REGISTERED, chan);
            return;
        } else if (!is_servadmin && !check_access(nc, ci, CA_MEMO)) {
            msg_lang(ncMemoServ, nc, ACCESS_DENIED);
            return;
        }
        mi = &ci->memos;
    }
    if (is_servadmin) {
        if (p2 && strcasecmp(p2, "HARD") != 0 && !chan) {
            if (!(ni = findnick(p1))) {
                msg_lang(ncMemoServ, nc, NICK_X_NOT_REGISTERED, p1);
                return;
            }
            ni = getlink(ni);
            user = p1;
            mi = &ni->memos;
            p1 = p2;
            p2 = p3;
        } else if (!p1) {
            syntax_error(ncMemoServ, nc, "SET LIMIT",
                                        MEMO_SET_LIMIT_SERVADMIN_SYNTAX);
            return;
        }
        if ((!isdigit(*p1) && strcasecmp(p1, "NONE") != 0) ||
                        (p2 && strcasecmp(p2, "HARD") != 0)) {
            syntax_error(ncMemoServ, nc, "SET LIMIT",
                                        MEMO_SET_LIMIT_SERVADMIN_SYNTAX);
            return;
        }
        if (chan) {
            if (p2)
                ci->flags |= CI_MEMO_HARDMAX;
            else
                ci->flags &= ~CI_MEMO_HARDMAX;
        } else {
            if (p2)
                ni->flags |= NI_MEMO_HARDMAX;
            else
                ni->flags &= ~NI_MEMO_HARDMAX;
        }
        limit = atoi(p1);
        if (limit < 0 || limit > 32767) {
            msg_lang(ncMemoServ, nc, MEMO_SET_LIMIT_OVERFLOW, 32767);
            limit = 32767;
        }
        if (strcasecmp(p1, "NONE") == 0)
            limit = -1;
    } else {
        if (!p1 || p2 || !isdigit(*p1)) {
            syntax_error(ncMemoServ, nc, "SET LIMIT", MEMO_SET_LIMIT_SYNTAX);
            return;
        }
        if (chan && (ci->flags & CI_MEMO_HARDMAX)) {
            msg_lang(ncMemoServ, nc, MEMO_SET_LIMIT_FORBIDDEN, chan);
            return;
        } else if (!chan && (ni->flags & NI_MEMO_HARDMAX)) {
            msg_lang(ncMemoServ, nc, MEMO_SET_YOUR_LIMIT_FORBIDDEN);
            return;
        }
        limit = atoi(p1);
        /* The first character is a digit, but we could still go negative
         * from overflow... watch out! */
        if (limit < 0 || (MSMaxMemos > 0 && limit > MSMaxMemos)) {
            if (chan) {
                msg_lang(ncMemoServ, nc, MEMO_SET_LIMIT_TOO_HIGH,
                        chan, MSMaxMemos);
            } else {
                msg_lang(ncMemoServ, nc, MEMO_SET_YOUR_LIMIT_TOO_HIGH,
                        MSMaxMemos);
            }
            return;
        } else if (limit > 32767) {
            msg_lang(ncMemoServ, nc, MEMO_SET_LIMIT_OVERFLOW, 32767);
            limit = 32767;
        }
    }
    mi->memomax = limit;
    if (limit > 0) {
        if (!chan && ni == nc->user->ni)
            msg_lang(ncMemoServ, nc, MEMO_SET_YOUR_LIMIT, limit);
        else
            msg_lang(ncMemoServ, nc, MEMO_SET_LIMIT,
                        chan ? chan : user, limit);
    } else if (limit == 0) {
        if (!chan && ni == nc->user->ni)
            msg_lang(ncMemoServ, nc, MEMO_SET_YOUR_LIMIT_ZERO);
        else
            msg_lang(ncMemoServ, nc, MEMO_SET_LIMIT_ZERO, chan ? chan : user);
    } else {
        if (!chan && ni == nc->user->ni)
            msg_lang(ncMemoServ, nc, MEMO_UNSET_YOUR_LIMIT);
        else
            msg_lang(ncMemoServ, nc, MEMO_UNSET_LIMIT, chan ? chan : user);
    }
}

/*************************************************************************/

static void do_info(struct NetClient *nc)
{
    struct MemoInfo *mi;
    struct NickInfo *ni = NULL;
    struct ChannelInfo *ci = NULL;
    char *name = strtok(NULL, " ");
    int is_servadmin = is_services_admin(nc);
    int hardmax = 0;

    if (is_servadmin && name && *name != '#') {
        ni = findnick(name);
        if (!ni) {
            msg_lang(ncMemoServ, nc, NICK_X_NOT_REGISTERED, name);
            return;
        }
        ni = getlink(ni);
        mi = &ni->memos;
        hardmax = ni->flags & NI_MEMO_HARDMAX ? 1 : 0;
    } else if (name && *name == '#') {
        ci = cs_findchan(name);
        if (!ci) {
            msg_lang(ncMemoServ, nc, CHAN_X_NOT_REGISTERED, name);
            return;
        } else if (!check_access(nc, ci, CA_MEMO)) {
            msg_lang(ncMemoServ, nc, ACCESS_DENIED);
            return;
        }
        mi = &ci->memos;
        hardmax = ci->flags & CI_MEMO_HARDMAX ? 1 : 0;
    } else { /* !name */
        if (!nick_identified(nc)) {
            msg_lang(ncMemoServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
            return;
        }
        mi = &nc->user->ni->memos;
    }

    if (name && ni != nc->user->ni) {

        if (!mi->memocount) {
            msg_lang(ncMemoServ, nc, MEMO_INFO_X_NO_MEMOS, name);
        } else if (mi->memocount == 1) {
            if (mi->memos[0].flags & MF_UNREAD)
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_MEMO_UNREAD, name);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_MEMO, name);
        } else {
            int count = 0, i;
            for (i = 0; i < mi->memocount; i++) {
                if (mi->memos[i].flags & MF_UNREAD)
                    count++;
            }
            if (count == mi->memocount)
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_MEMOS_ALL_UNREAD,
                        name, count);
            else if (count == 0)
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_MEMOS,
                        name, mi->memocount);
            else if (count == 0)
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_MEMOS_ONE_UNREAD,
                        name, mi->memocount);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_MEMOS_SOME_UNREAD,
                        name, mi->memocount, count);
        }
        if (mi->memomax >= 0) {
            if (hardmax)
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_HARD_LIMIT,
                        name, mi->memomax);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_X_LIMIT,
                        name, mi->memomax);
        } else {
            msg_lang(ncMemoServ, nc, MEMO_INFO_X_NO_LIMIT, name);
        }

    } else { /* !name || ni == nc->user->ni */

        if (!mi->memocount) {
            msg_lang(ncMemoServ, nc, MEMO_INFO_NO_MEMOS);
        } else if (mi->memocount == 1) {
            if (mi->memos[0].flags & MF_UNREAD)
                msg_lang(ncMemoServ, nc, MEMO_INFO_MEMO_UNREAD);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_MEMO);
        } else {
            int count = 0, i;
            for (i = 0; i < mi->memocount; i++) {
                if (mi->memos[i].flags & MF_UNREAD)
                    count++;
            }
            if (count == mi->memocount)
                msg_lang(ncMemoServ, nc, MEMO_INFO_MEMOS_ALL_UNREAD, count);
            else if (count == 0)
                msg_lang(ncMemoServ, nc, MEMO_INFO_MEMOS, mi->memocount);
            else if (count == 1)
                msg_lang(ncMemoServ, nc, MEMO_INFO_MEMOS_ONE_UNREAD,
                        mi->memocount);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_MEMOS_SOME_UNREAD,
                        mi->memocount, count);
        }
        if (mi->memomax == 0) {
            if (!is_servadmin && hardmax)
                msg_lang(ncMemoServ, nc, MEMO_INFO_HARD_LIMIT_ZERO);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_LIMIT_ZERO);
        } else if (mi->memomax > 0) {
            if (!is_servadmin && hardmax)
                msg_lang(ncMemoServ, nc, MEMO_INFO_HARD_LIMIT, mi->memomax);
            else
                msg_lang(ncMemoServ, nc, MEMO_INFO_LIMIT, mi->memomax);
        } else {
            msg_lang(ncMemoServ, nc, MEMO_INFO_NO_LIMIT);
        }

    } /* if (name && ni != nc->user->ni) */
}

/*************************************************************************/
