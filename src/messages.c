/*
 * ServicesIRCh - Services for IRCh, messages.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "messages.h"

#include "channels.h"
#include "chanserv.h"
#include "config.h"
#include "hash.h"
#include "language.h"
#include "log.h"
#include "helpserv.h"
#include "memoserv.h"
#include "netclient.h"
#include "newsserv.h"
#include "nickserv.h"
#include "numerics.h"
#include "operserv.h"
#include "process.h" /* Ignore */
#include "send.h"
#include "servers.h"
#include "service.h"
#include "services.h"
#include "sockutil.h"
#include "stats.h"
#include "users.h"
#include "version.h"

#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>

/* List of messages is at the bottom of the file. */

/*************************************************************************/
/*************************************************************************/

static void m_ignore(struct NetClient *source, int ac, char **av)
{
}

/*************************************************************************/

static void m_info(struct NetClient *source, int ac, char **av)
{
    const char **text = info_text;
    char buf[BUFSIZE];

    if (IsServer(source))
        return;

    while (*text)
        send_numirc(source, 371, ":%s", *text++);

    send_numirc(source, 371, ": ");
    send_numirc(source, 371, ":Birth Date: %s, compile # %s", version_creation, version_build);
    strftime_lang(buf, sizeof(buf), source->user->ni, STRFTIME_DATE_TIME_FORMAT, start_time);
    send_numirc(source, 371, ":On-line since %s", buf);
    send_numirc(source, 374, ":End of /INFO list.");
}

/*************************************************************************/

/* Handle a PING command
 *
 *  source = Server
 *   av[0] = Dest server
 *
 */
static void m_ping(struct NetClient *source, int ac, char **av)
{
    if (ac < 1) {
        slog(logsvc, "-- WRONG PING!!");
        return;
    }

        send_cmd(&myService, "Z :%s", av[0]);
}

/*************************************************************************/

/* Handle a MOTD command
 *
 *  source = User
 *   av[0] = Dest server
 *
 */
static void m_motd(struct NetClient *source, int ac, char **av)
{
    FILE *f;
    char buf[BUFSIZE];

    f = fopen(MOTDFilename, "r");
    if (f) {
        struct stat sb;
        struct tm tmfile;

        send_numirc(source, 375, ":- %s Message of the Day - ", ServerName);
        stat(MOTDFilename, &sb);
        tmfile = *localtime((time_t *) &sb.st_mtime);

        send_numirc(source, 372, ":- %d-%d-%d %d:%02d",
                    tmfile.tm_year + 1900, tmfile.tm_mon + 1,
                    tmfile.tm_mday, tmfile.tm_hour, tmfile.tm_min);

        while (fgets(buf, sizeof(buf), f)) {
            buf[strlen(buf)-1] = 0;
            send_numirc(source, 372, ":- %s", buf);
        }
        fclose(f);


       /* Look, people.  I'm not asking for payment, praise, or anything like
         * that for using Services... is it too much to ask that I at least get
         * some recognition for my work?  Please don't remove the copyright
         * message below.
         */
        send_numirc(source, 372, ":-");
        send_numirc(source, 372, ":- Services is Copyright (C) "
                    "1996-1998 Andy Church.");
        send_numirc(source, 372, ":- ServicesIRCh is Copyright (C) "
                    "2000-2019 Toni Garcia - zoltan.");
        send_numirc(source, 376, ":End of /MOTD command.");
    } else {
        send_numirc(source, 422, ":MOTD File is missing");
    }
}

/*************************************************************************/

static void m_privmsg(struct NetClient *source, int ac, char **av)
{
    struct NetClient *ncBot;
    struct timeval start, stop; /* When processing started and finished */
    char *s;
    int32_t diff, directmsg;

    if (ac != 2)
        return;

    /* Check if we should ignore.  Operators always get through. */
    if (allow_ignore && !is_oper(source)) {
        struct IgnoreData *ign = get_ignore(source->name);
        if (ign && ign->time > time(NULL)) {
            slog(logsvc, "Ignored message from %s: \"%s\"", source->name, inbuf);
            return;
        }
    }

    /* If a server is specified (nick@server format), make sure it matches
     * us, and strip it off. */
    s = strchr(av[0], '@');
    if (s) {
        *s++ = 0;
        if (strcasecmp(s, ServerName) != 0)
            return;
        directmsg = 1;
    } else {
        directmsg = 0;
    }

    gettimeofday(&start, NULL);

    if (!directmsg)
        ncBot = FindNUser(av[0]);
    else
        ncBot = FindUser(av[0]);

    if (!ncBot || (ncBot && !ncBot->service->func))
        return;

    if (ncBot->service->has_priv == NULL)
        ncBot->service->func(source, av[1]);
    else if (ncBot->service->has_priv(source))
        ncBot->service->func(source, av[1]);
    else {
        msg_lang(ncBot, source, ACCESS_DENIED);
        if (WallBadOS)
            wallops(ncOperServ, "Denied access to %s from %s (non-oper)",
                    ncBot->name, source->name);
    }

    gettimeofday(&stop, NULL);
    diff = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);

    /* Add to ignore list if the command took a significant amount of time. */
    if (allow_ignore) {
        if (diff >= 1000000 && !is_oper(source) && !IsServer(source))
            add_ignore(source->name, diff/1000000);
    }
}

/*************************************************************************/

void m_time(struct NetClient *source, int ac, char **av)
{
    time_t t;
    struct tm *tm;
    char buf[64];

    time(&t);
    tm = localtime(&t);
    strftime(buf, sizeof(buf), "%a %b %d %H:%M:%S %Y %Z", tm);
    send_numirc(source, 391, ":%s", buf);
}

/*************************************************************************/

void m_version(struct NetClient *source, int ac, char **av)
{
    send_numirc(source, 351, "%s %s :%s",
                version_number, ServerName, version_protocol);
}

/*************************************************************************/

void m_whois(struct NetClient *source, int ac, char **av)
{
    struct NetClient *nc;

    if (ac < 2)
        return;

    nc = FindUser(av[1]);
    if (nc) {
        send_numirc(source, 311, "%s %s %s * :%s", av[1], nc->user->username,
                    nc->user->host, nc->description);

        send_numirc(source, 312, "%s %s :%s", av[1], nc->user->server->name, nc->user->server->description);

        if (nc->user->away)
            send_numirc(source, 301, "%s :%s", av[1], nc->user->away);

        if (nc->user->modes & UMODE_o)
            send_numirc(source, 313, "%s :is an IRC Operator", av[1]);

        if (MyService(nc))
            send_numirc(source, 317, "%s 0 %lu :seconds idle, signon time", av[1], nc->user->ts_connect);
    } else {
        send_numirc(source, 401, "%s :No such nick", av[1]);
    }

    send_numirc(source, 318, "%s :End of /WHOIS list.", av[1]);
}

/*************************************************************************/
/*************************************************************************/

/* Ordenados por mayor frecuencia por optimizacion */
struct Message messages[] = {
    { "AWAY",         "A",    m_away },
    { "JOIN",         "J",    m_join },
    { "KICK",         "K",    m_kick },
    { "KILL",         "D",    m_kill },
    { "MODE",         "M",    m_mode },
    { "MOTD",         "MO",   m_motd },
    { "NICK",         "N",    m_nick },
    { "NOTICE",       "O",    NULL },
    { "PART",         "L",    m_part },
    { "PASS",         "PA",   m_ignore },
    { "PING",         "G",    m_ping },
    { "PRIVMSG",      "P",    m_privmsg },
    { "QUIT",         "Q",    m_quit },
    { "SERVER",       "S",    m_server },
    { "SQUIT",        "SQ",   m_squit },
    { "STATS",        "R",    m_stats },
    { "TOPIC",        "T",    m_topic },
    { "OPMODE",       "OM",   m_opmode },
    { "USER",         "USER", NULL },
    { "VERSION",      "V",    m_version },
    { "INFO",         "F",    m_info },
    { "WALLOPS",      "WA",   NULL },
    { "WHOIS",        "W",    m_whois },
    { "INVITE",       "I",    m_invite },
    { "TIME",         "TIME", m_time },
    { "SILENCE",      "U",    NULL },
    { "GLINE",        "GL",   m_ignore },
    { "BURST",        "B",    m_burst },
    { "CREATE",       "C",    m_create },
    { "EOB_ACK",      "EA",   m_end_of_burst_ack },
    { "END_OF_BURST", "EB",   m_end_of_burst },
    { "351",          "351",  m_351 }, /* Version */
    { "401",          "401",  NULL },  // No such nick
    { "436",          "436",  m_436 }, /* Colision nick */
    { "441",          "441",  NULL },  // They are'nt on that channel
    { "443",          "443",  NULL },  // Is already on channel
    { "510",          "510",  NULL },  // You can't talk with xx, (S)he is silencing you
    { "441",          "441",  NULL },  // They are'nt on that channel
    { "443",          "443",  NULL },  // Is already on channel
    { "510",          "510",  NULL },  // You can't talk with xx, (S)he is silencing you
    { NULL }
};

/*************************************************************************/

struct Message *find_message_name(const char *name)
{
    struct Message *m;

    for (m = messages; m->name; m++) {
        if (strcasecmp(name, m->name) == 0)
            return m;
    }
    return NULL;
}

struct Message *find_message_tok(const char *tok)
{
    struct Message *m;

    for (m = messages; m->tok; m++) {
        if (strcasecmp(tok, m->tok) == 0)
            return m;
    }
    return NULL;
}

/*************************************************************************/
