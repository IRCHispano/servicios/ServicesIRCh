/*
 * ServicesIRCh - Services for IRCh, netclient.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2012-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "netclient.h"

#include "config.h"
#include "hash.h"
#include "language.h"
#include "log.h"
#include "misc.h"
#include "numerics.h"
#include "send.h"
#include "servers.h"
#include "services.h" //Prov arranque
#include "stats.h"
#include "svc_memory.h"
#include "svc_string.h"
#include "users.h"
#include "version.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct NetClient myService;
struct NetClient *myHub;
struct NetClient *ncLocalList = &myService;
struct NetClient *ncGlobalList = &myService;

#define HASHNETCSIZE    1024
#define HASHNETC(name)  (((name)[0]&31)<<5 | ((name)[1]&31))
static struct NetClient *netclientlist[HASHNETCSIZE];

/*************************************************************************/

/* Statistics. Return memory total */

void get_netclient_stats(struct NetClient *service, struct NetClient *user, long *memuse)
{
    struct NetClient *nc;
    struct u_chaninfolist *uci;
    int users = 0;
    int servers = 0;
    long musers = 0;
    long mservers = 0;
    long mtotal = 0;

    for (nc = firstnc(); nc; nc = nextnc()) {
        if (IsUser(nc)) {
            users++;
            musers += sizeof(*nc);
            musers += sizeof(*nc->user);
            if (nc->name)
                musers += strlen(nc->name)+1;
            if (nc->description)
                musers += strlen(nc->description)+1;
            if (nc->user->username)
                musers += strlen(nc->user->username)+1;
            if (nc->user->host)
                musers += strlen(nc->user->host)+1;
            if (nc->user->virtualhost)
                musers += strlen(nc->user->virtualhost)+1;
            if (nc->user->away)
                musers += strlen(nc->user->away)+1;

//            for (uci = nc->user->founder_chans; uci; uci = uci->next)
  //              musers += sizeof(*uci);

        } else {
            servers++;

            mservers += sizeof(*nc);
            mservers += sizeof(*nc->serv);
            if (nc->name)
                mservers += strlen(nc->name)+1;
            if (nc->description)
                mservers += strlen(nc->description)+1;
            if (nc->serv->version)
                mservers += strlen(nc->serv->version)+1;
            if (nc->serv->options)
                mservers += strlen(nc->serv->options)+1;
        }
    }

    mtotal = musers + mservers;

    //msg_lang(service, user, ADMIN_MEMSTATS_SERVER_MEM,
    //         servers, (mservers+512) / 1024);
    //msg_lang(service, user, ADMIN_MEMSTATS_USER_MEM,
    //         users, (musers+512) / 1024);

    *memuse = mtotal;
}


/*************************************************************************/
/*************************************************************************/

/* Iterate over all netclients in the nc list.  Return NULL at end of list. */

static struct NetClient *current;
static int next_index;

struct NetClient *firstnc(void)
{
    next_index = 0;
    while (next_index < HASHNETCSIZE && current == NULL)
        current = netclientlist[next_index++];
    Debug((3, "firstnc() returning %s",
               current ? current->name : "NULL (end of list)"));
    return current;
}

struct NetClient *nextnc(void)
{
    if (current)
        current = current->next;
    if (!current && next_index < HASHNETCSIZE) {
        while (next_index < HASHNETCSIZE && current == NULL)
            current = netclientlist[next_index++];
    }
    Debug((3, "nextnc() returning %s",
               current ? current->name : "NULL (end of list)"));
    return current;
}

void netclient_add_list(struct NetClient *nc)
{
    struct NetClient **list;

    list = &netclientlist[HASHNETC(nc->name)];
    nc->next = *list;
    nc->prev = NULL;
    if (*list)
        (*list)->prev = nc;
    *list = nc;
}

void netclient_del_list(struct NetClient *nc)
{
    if (nc->prev)
        nc->prev->next = nc->next;
    else
        netclientlist[HASHNETC(nc->name)] = nc->next;
    if (nc->next)
        nc->next->prev = nc->prev;
}

/*************************************************************************/
/*************************************************************************/

/* Inicializa el netcliente del Service */
void netclient_init()
{
    init_hash();

    /* Inicializamos con la introduccion del Service */
    memset(&myService, 0, sizeof(myService));
    (&myService)->name = sstrdup(ServerName);
    (&myService)->description = sstrdup(ServerDesc);
    SetMe(&myService);
    SetServer(&myService);

    (&myService)->ts_create = time(NULL);

    /* Creamos la estructura de servidor */
    (&myService)->serv = servers_create();
    (&myService)->serv->protocol = 10;
    (&myService)->serv->up = &myService;
    (&myService)->serv->down = NULL;
    (&myService)->serv->flags |= (SFLAG_HUB | SFLAG_SERVICE | SFLAG_IPV6);
    (&myService)->serv->version = sstrdup(version_number);
    (&myService)->serv->options = sstrdup(version_build);

    /* Ajustamos al MyService el numeric P10 y la mascara de capacidad P10 */
    SetYXXMyServer(&myService, ServerNumeric, 200);

    (&myService)->serv->start_time = (&myService)->serv->link_time = time(NULL);

     netclient_add_list(&myService);
     hAddNetClient(&myService);
     ++IRCStats.pservers;
}

struct NetClient *netclient_create(int type)
{
    struct NetClient *nc;

    nc = smalloc(sizeof(struct NetClient));
    assert(nc);
    memset(nc, 0, sizeof(struct NetClient));
    nc->status = type;

    if (type == NETCLIENT_USER)
        nc->user = users_create();
    else if (type == NETCLIENT_SERVER)
        nc->serv = servers_create();
    else
        assert(0); /* Hay que llamar la funcion de forma adecuada */

    return nc;
}

void netclient_delete(struct NetClient *nc)
{
    assert(nc);

    if (nc->name)
      sfree(nc->name);
    if (nc->description)
      sfree(nc->description);

    if (nc->user)
        users_delete(nc->user);

    if (nc->serv)
        servers_delete(nc->serv);

    sfree(nc);
}

/*************************************************************************/
