/*
 * ServicesIRCh - Services for IRCh, newsserv.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2011-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
   Copyright (C) 1999 Andrew Kempe - TheShadow <theshadow@shadowfire.org>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* News functions.
 * Based on code by Andrew Kempe (TheShadow)
 *     E-mail: <theshadow@shadowfire.org>
 *
 * Services is copyright (c) 1996-1999 Andy Church.
 *     E-mail: <achurch@dragonfire.net>
 * This program is free but copyrighted software; see the file COPYING for
 * details.
 */
#include "sysconf.h"
#include "newsserv.h"

#include "config.h"
#include "datafiles.h"
#include "language.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "send.h"
#include "services.h"
#include "svc_memory.h"
#include "users.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

/* provisional */
#include "operserv.h"

/*************************************************************************/

struct NewsItem {
    int16_t type;
    int32_t num;                /* Numbering is separate for login and oper news */
    char *text;
    char who[NICKMAX];
    time_t time;
};

static int32_t nnews = 0;
static int32_t news_size = 0;
static struct NewsItem *news = NULL;

/*************************************************************************/

/* List of messages for each news type.  This simplifies message sending. */

#define MSG_SYNTAX        0
#define MSG_LIST_HEADER        1
#define MSG_LIST_ENTRY        2
#define MSG_LIST_NONE        3
#define MSG_ADD_SYNTAX        4
#define MSG_ADD_FULL        5
#define MSG_ADDED        6
#define MSG_DEL_SYNTAX        7
#define MSG_DEL_NOT_FOUND 8
#define MSG_DELETED        9
#define MSG_DEL_NONE        10
#define MSG_DELETED_ALL        11
#define MSG_MAX                11

struct NewsMsgs {
    int16_t type;
    char *name;
    int msgs[MSG_MAX+1];
};
struct NewsMsgs msgarray[] = {
    { NEWS_LOGON, "LOGON",
        { NEWS_LOGON_SYNTAX,
          NEWS_LOGON_LIST_HEADER,
          NEWS_LOGON_LIST_ENTRY,
          NEWS_LOGON_LIST_NONE,
          NEWS_LOGON_ADD_SYNTAX,
          NEWS_LOGON_ADD_FULL,
          NEWS_LOGON_ADDED,
          NEWS_LOGON_DEL_SYNTAX,
          NEWS_LOGON_DEL_NOT_FOUND,
          NEWS_LOGON_DELETED,
          NEWS_LOGON_DEL_NONE,
          NEWS_LOGON_DELETED_ALL
        }
    },
    { NEWS_OPER, "OPER",
        { NEWS_OPER_SYNTAX,
          NEWS_OPER_LIST_HEADER,
          NEWS_OPER_LIST_ENTRY,
          NEWS_OPER_LIST_NONE,
          NEWS_OPER_ADD_SYNTAX,
          NEWS_OPER_ADD_FULL,
          NEWS_OPER_ADDED,
          NEWS_OPER_DEL_SYNTAX,
          NEWS_OPER_DEL_NOT_FOUND,
          NEWS_OPER_DELETED,
          NEWS_OPER_DEL_NONE,
          NEWS_OPER_DELETED_ALL
        }
    }
};

static int *findmsgs(int16_t type, char **typename) {
    int i;
    for (i = 0; i < lenof(msgarray); i++) {
        if (msgarray[i].type == type) {
            if (typename)
                *typename = msgarray[i].name;
            return msgarray[i].msgs;
        }
    }
    return NULL;
}

/*************************************************************************/

/* Called by the main OperServ routine in response to a NEWS command. */
static void do_news(struct NetClient *nc, int16_t type);

/* Lists all a certain type of news. */
static void do_news_list(struct NetClient *nc, int16_t type, int *msgs);

/* Add news items. */ 
static void do_news_add(struct NetClient *nc, int16_t type, int *msgs, const char *typename);
static int add_newsitem(struct NetClient *nc, const char *text, int16_t type);

/* Delete news items. */
static void do_news_del(struct NetClient *nc, int16_t type, int *msgs, const char *typename);
static int del_newsitem(int num, int16_t type);

/*************************************************************************/
/****************************** Statistics *******************************/
/*************************************************************************/

void get_news_stats(long *nrec, long *memuse)
{
    long mem;
    int i;

    mem = sizeof(struct NewsItem) * news_size;
    for (i = 0; i < nnews; i++)
        mem += strlen(news[i].text)+1;
    *nrec = nnews;
    *memuse = mem;
}

/*************************************************************************/
/*********************** News item loading/saving ************************/
/*************************************************************************/

#define SAFE(x) do {                                        \
    if ((x) < 0) {                                        \
        if (!forceload)                                        \
            fatal("Read error on %s", NewsDBName);        \
        nnews = i;                                        \
        break;                                                \
    }                                                        \
} while (0)

void load_news()
{
    struct dbFILE *f;
    int i;
    int16_t n;
    int32_t tmp32;

    if (!(f = open_db(s_OperServ, NewsDBName, "r")))
        return;
    switch (i = get_file_version(f)) {
      case 7:
        SAFE(read_int16(&n, f));
        nnews = n;
        if (nnews < 8)
            news_size = 16;
        else if (nnews >= 16384)
            news_size = 32767;
        else
            news_size = 2*nnews;
        news = smalloc(sizeof(*news) * news_size);
        if (!nnews) {
            close_db(f);
            return;
        }
        for (i = 0; i < nnews; i++) {
            SAFE(read_int16(&news[i].type, f));
            SAFE(read_int32(&news[i].num, f));
            SAFE(read_string(&news[i].text, f));
            SAFE(read_buffer(news[i].who, f));
            SAFE(read_int32(&tmp32, f));
            news[i].time = tmp32;
        }
        break;

      default:
        fatal("Unsupported version (%d) on %s", i, NewsDBName);
    } /* switch (ver) */

    close_db(f);
}

#undef SAFE

/*************************************************************************/

#define SAFE(x) do {                                                \
    if ((x) < 0) {                                                \
        restore_db(f);                                                \
        log_perror("Write error on %s", NewsDBName);                \
        if (time(NULL) - lastwarn > WarningTimeout) {                \
            wallops(NULL, "Write error on %s: %s", NewsDBName,        \
                        strerror(errno));                        \
            lastwarn = time(NULL);                                \
        }                                                        \
        return;                                                        \
    }                                                                \
} while (0)

void save_news()
{
    struct dbFILE *f;
    int i;
    static time_t lastwarn = 0;

    if (!(f = open_db(s_OperServ, NewsDBName, "w")))
        return;
    SAFE(write_int16(nnews, f));
    for (i = 0; i < nnews; i++) {
        SAFE(write_int16(news[i].type, f));
        SAFE(write_int32(news[i].num, f));
        SAFE(write_string(news[i].text, f));
        SAFE(write_buffer(news[i].who, f));
        SAFE(write_int32(news[i].time, f));
    }
    close_db(f);
}

#undef SAFE

/*************************************************************************/
/***************************** News display ******************************/
/*************************************************************************/

void display_news(struct NetClient *nc, int16_t type)
{
    int i;
    int count = 0;        /* Number we're going to show--not more than 3 */
    int msg;

    if (type == NEWS_LOGON) {
        msg = NEWS_LOGON_TEXT;
    } else if (type == NEWS_OPER) {
        msg = NEWS_OPER_TEXT;
    } else {
        slog(logsvc, "news: Invalid type (%d) to display_news()", type);
        return;
    }

    for (i = nnews-1; i >= 0; i--) {
        if (count >= 3)
            break;
        if (news[i].type == type)
            count++;
    }
    while (++i < nnews) {
        if (news[i].type == type) {
            char timebuf[64];

            strftime_lang(timebuf, sizeof(timebuf), nc->user->ni,
                                STRFTIME_SHORT_DATE_FORMAT, news[i].time);
            msg_lang(ncOperServ, nc, msg, timebuf, news[i].text);
        }
    }
}

/*************************************************************************/
/***************************** News editing ******************************/
/*************************************************************************/

/* Declared in extern.h */
void do_logonnews(struct NetClient *nc)
{
    do_news(nc, NEWS_LOGON);
}

/* Declared in extern.h */
void do_opernews(struct NetClient *nc)
{
    do_news(nc, NEWS_OPER);
}

/*************************************************************************/

/* Main news command handling routine. */
void do_news(struct NetClient *nc, short type)
{
    int is_servadmin = is_services_admin(nc);
    char *cmd = strtok(NULL, " ");
    char *typename;
    int *msgs;

    msgs = findmsgs(type, &typename);
    if (!msgs) {
        slog(logsvc, "news: Invalid type to do_news()");
        return;
    }

    if (!cmd)
        cmd = "";

    if (strcasecmp(cmd, "LIST") == 0) {
        do_news_list(nc, type, msgs);

    } else if (strcasecmp(cmd, "ADD") == 0) {
        if (is_servadmin)
            do_news_add(nc, type, msgs, typename);
        else
            msg_lang(ncOperServ, nc, PERMISSION_DENIED);

    } else if (strcasecmp(cmd, "DEL") == 0) {
        if (is_servadmin)
            do_news_del(nc, type, msgs, typename);
        else
            msg_lang(ncOperServ, nc, PERMISSION_DENIED);

    } else {
        char buf[32];
        snprintf(buf, sizeof(buf), "%sNEWS", typename);
        syntax_error(ncOperServ, nc, buf, msgs[MSG_SYNTAX]);
    }
}

/*************************************************************************/

/* Handle a {LOGON,OPER}NEWS LIST command. */

static void do_news_list(struct NetClient *nc, int16_t type, int *msgs)
{
    int i, count = 0;
    char timebuf[64];

    for (i = 0; i < nnews; i++) {
        if (news[i].type == type) {
            if (count == 0)
                msg_lang(ncOperServ, nc, msgs[MSG_LIST_HEADER]);
            strftime_lang(timebuf, sizeof(timebuf), nc->user->ni,
                                STRFTIME_DATE_TIME_FORMAT, news[i].time);
            msg_lang(ncOperServ, nc, msgs[MSG_LIST_ENTRY],
                                news[i].num, timebuf,
                                *news[i].who ? news[i].who : "<unknown>",
                                news[i].text);
            count++;
        }
    }
    if (count == 0)
        msg_lang(ncOperServ, nc, msgs[MSG_LIST_NONE]);
}

/*************************************************************************/

/* Handle a {LOGON,OPER}NEWS ADD command. */

static void do_news_add(struct NetClient *nc, int16_t type, int *msgs, const char *typename)
{
    char *text = strtok(NULL, "");

    if (!text) {
        char buf[32];
        snprintf(buf, sizeof(buf), "%sNEWS", typename);
        syntax_error(ncOperServ, nc, buf, msgs[MSG_ADD_SYNTAX]);
    } else {
        int n = add_newsitem(nc, text, type);
        if (n < 0)
            msg_lang(ncOperServ, nc, msgs[MSG_ADD_FULL]);
        else
            msg_lang(ncOperServ, nc, msgs[MSG_ADDED], n);
        if (readonly)
            msg_lang(ncOperServ, nc, READ_ONLY_MODE);
    }
}


/* Actually add a news item.  Return the number assigned to the item, or -1
 * if the news list is full (32767 items).
 */

static int add_newsitem(struct NetClient *nc, const char *text, short type)
{
    int i, num;

    if (nnews >= 32767)
        return -1;

    if (nnews >= news_size) {
        if (news_size < 8)
            news_size = 8;
        else
            news_size *= 2;
        news = srealloc(news, sizeof(*news) * news_size);
    }
    num = 0;
    for (i = nnews-1; i >= 0; i--) {
        if (news[i].type == type) {
            num = news[i].num;
            break;
        }
    }
    news[nnews].type = type;
    news[nnews].num = num+1;
    news[nnews].text = sstrdup(text);
    news[nnews].time = time(NULL);
    strscpy(news[nnews].who, nc->name, NICKMAX);
    nnews++;
    return num+1;
}

/*************************************************************************/

/* Handle a {LOGON,OPER}NEWS DEL command. */

static void do_news_del(struct NetClient *nc, int16_t type, int *msgs, const char *typename)
{
    char *text = strtok(NULL, " ");

    if (!text) {
        char buf[32];
        snprintf(buf, sizeof(buf), "%sNEWS", typename);
        syntax_error(ncOperServ, nc, buf, msgs[MSG_DEL_SYNTAX]);
    } else {
        if (strcasecmp(text, "ALL") != 0) {
            int num = atoi(text);
            if (num > 0 && del_newsitem(num, type))
                msg_lang(ncOperServ, nc, msgs[MSG_DELETED], num);
            else
                msg_lang(ncOperServ, nc, msgs[MSG_DEL_NOT_FOUND], num);
        } else {
            if (del_newsitem(0, type))
                msg_lang(ncOperServ, nc, msgs[MSG_DELETED_ALL]);
            else
                msg_lang(ncOperServ, nc, msgs[MSG_DEL_NONE]);
        }
        if (readonly)
            msg_lang(ncOperServ, nc, READ_ONLY_MODE);
    }
}


/* Actually delete a news item.  If `num' is 0, delete all news items of
 * the given type.  Returns the number of items deleted.
 */

static int del_newsitem(int num, short type)
{
    int i;
    int count = 0;

    for (i = 0; i < nnews; i++) {
        if (news[i].type == type && (num == 0 || news[i].num == num)) {
            free(news[i].text);
            count++;
            nnews--;
            if (i < nnews)
                memcpy(news+i, news+i+1, sizeof(*news) * (nnews-i));
            i--;
        }
    }
    return count;
}

/*************************************************************************/
