/*
 * ServicesIRCh - Services for IRCh, nickserv.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * Note that with the addition of nick links, most functions in Services
 * access the "effective nick" (nc->user->ni) to determine privileges and such.
 * The only functions which access the "real nick" (nc->user->real_ni) are:
 *        various functions which set/check validation flags
 *            (validate_user, cancel_user, nick_{identified,recognized},
 *             do_identify)
 *        validate_user (adding a collide timeout on the real nick)
 *        cancel_user (deleting a timeout on the real nick)
 *        do_register (checking whether the real nick is registered)
 *        do_drop (dropping the real nick)
 *        do_link (linking the real nick to another)
 *        do_unlink (unlinking the real nick)
 *        chanserv.c/do_register (setting the founder to the real nick)
 * plus a few functions in users.c relating to nick creation/changing.
 */
#include "sysconf.h"
#include "nickserv.h"

#include "actions.h"
#include "chanserv.h"
#include "commands.h"
#include "datafiles.h"
#include "encrypt.h"
#include "hash.h"
#include "language.h"
#include "log.h"
#include "match.h"
#include "memoserv.h"
#include "misc.h"
#include "netclient.h"
#include "send.h"
#include "service.h"
#include "services.h"
#include "svc_memory.h"
#include "timeout.h"
#include "users.h"

#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/* provisional */
#include "operserv.h"

/*************************************************************************/

struct NetClient *ncNickServ;

static struct NickInfo *nicklists[256];        /* One for each initial character */

#define TO_COLLIDE   0                        /* Collide the user with this nick */
#define TO_RELEASE   1                        /* Release a collided nick */

/*************************************************************************/

static int is_on_access(struct NetClient *nc, struct NickInfo *ni);
static void alpha_insert_nick(struct NickInfo *ni);
static struct NickInfo *makenick(const char *nick);
static int delnick(struct NickInfo *ni);
static void remove_links(struct NickInfo *ni);
static void delink(struct NickInfo *ni);

static void collide(struct NickInfo *ni, int from_timeout);
static void release(struct NickInfo *ni, int from_timeout);
static void add_ns_timeout(struct NickInfo *ni, int type, time_t delay);
static void del_ns_timeout(struct NickInfo *ni, int type);

static void do_help(struct NetClient *nc);
static void do_register(struct NetClient *nc);
static void do_identify(struct NetClient *nc);
static void do_drop(struct NetClient *nc);
static void do_set(struct NetClient *nc);
static void do_set_password(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_language(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_url(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_email(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_kill(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_secure(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_private(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_hide(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_set_noexpire(struct NetClient *nc, struct NickInfo *ni, char *param);
static void do_access(struct NetClient *nc);
static void do_link(struct NetClient *nc);
static void do_unlink(struct NetClient *nc);
static void do_listlinks(struct NetClient *nc);
static void do_info(struct NetClient *nc);
static void do_list(struct NetClient *nc);
static void do_recover(struct NetClient *nc);
static void do_release(struct NetClient *nc);
static void do_ghost(struct NetClient *nc);
static void do_status(struct NetClient *nc);
static void do_getpass(struct NetClient *nc);
static void do_forbid(struct NetClient *nc);

/*************************************************************************/

static struct Command cmds[] = {
    { "HELP",     do_help,     NULL,  -1,                     -1,-1,-1,-1 },
    { "REGISTER", do_register, NULL,  NICK_HELP_REGISTER,     -1,-1,-1,-1 },
    { "IDENTIFY", do_identify, NULL,  NICK_HELP_IDENTIFY,     -1,-1,-1,-1 },
    { "DROP",     do_drop,     NULL,  -1,
                NICK_HELP_DROP, NICK_SERVADMIN_HELP_DROP,
                NICK_SERVADMIN_HELP_DROP, NICK_SERVADMIN_HELP_DROP },
    { "ACCESS",   do_access,   NULL,  NICK_HELP_ACCESS,       -1,-1,-1,-1 },
    { "LINK",     do_link,     NULL,  NICK_HELP_LINK,         -1,-1,-1,-1 },
    { "UNLINK",   do_unlink,   NULL,  NICK_HELP_UNLINK,       -1,-1,-1,-1 },
    { "SET",      do_set,      NULL,  NICK_HELP_SET,
                -1, NICK_SERVADMIN_HELP_SET,
                NICK_SERVADMIN_HELP_SET, NICK_SERVADMIN_HELP_SET },
    { "SET PASSWORD", NULL,    NULL,  NICK_HELP_SET_PASSWORD, -1,-1,-1,-1 },
    { "SET URL",      NULL,    NULL,  NICK_HELP_SET_URL,      -1,-1,-1,-1 },
    { "SET EMAIL",    NULL,    NULL,  NICK_HELP_SET_EMAIL,    -1,-1,-1,-1 },
    { "SET KILL",     NULL,    NULL,  NICK_HELP_SET_KILL,     -1,-1,-1,-1 },
    { "SET SECURE",   NULL,    NULL,  NICK_HELP_SET_SECURE,   -1,-1,-1,-1 },
    { "SET PRIVATE",  NULL,    NULL,  NICK_HELP_SET_PRIVATE,  -1,-1,-1,-1 },
    { "SET HIDE",     NULL,    NULL,  NICK_HELP_SET_HIDE,     -1,-1,-1,-1 },
    { "SET NOEXPIRE", NULL,    NULL,  -1, -1,
                NICK_SERVADMIN_HELP_SET_NOEXPIRE,
                NICK_SERVADMIN_HELP_SET_NOEXPIRE,
                NICK_SERVADMIN_HELP_SET_NOEXPIRE },
    { "RECOVER",  do_recover,  NULL,  NICK_HELP_RECOVER,      -1,-1,-1,-1 },
    { "RELEASE",  do_release,  NULL,  NICK_HELP_RELEASE,      -1,-1,-1,-1 },
    { "GHOST",    do_ghost,    NULL,  NICK_HELP_GHOST,        -1,-1,-1,-1 },
    { "INFO",     do_info,     NULL,  NICK_HELP_INFO,
                -1, NICK_HELP_INFO, NICK_SERVADMIN_HELP_INFO,
                NICK_SERVADMIN_HELP_INFO },
    { "LIST",     do_list,     NULL,  -1,
                NICK_HELP_LIST, NICK_SERVADMIN_HELP_LIST,
                NICK_SERVADMIN_HELP_LIST, NICK_SERVADMIN_HELP_LIST },
    { "STATUS",   do_status,   NULL,  NICK_HELP_STATUS,       -1,-1,-1,-1 },
    { "LISTLINKS",do_listlinks,is_services_admin, -1,
                -1, NICK_SERVADMIN_HELP_LISTLINKS,
                NICK_SERVADMIN_HELP_LISTLINKS, NICK_SERVADMIN_HELP_LISTLINKS },
    { "GETPASS",  do_getpass,  is_services_admin,  -1,
                -1, NICK_SERVADMIN_HELP_GETPASS,
                NICK_SERVADMIN_HELP_GETPASS, NICK_SERVADMIN_HELP_GETPASS },
    { "FORBID",   do_forbid,   is_services_admin,  -1,
                -1, NICK_SERVADMIN_HELP_FORBID,
                NICK_SERVADMIN_HELP_FORBID, NICK_SERVADMIN_HELP_FORBID },
    { NULL }
};

/*************************************************************************/
/*************************************************************************/

/* Return information on memory use.  Assumes pointers are valid. */

void get_nickserv_stats(long *nrec, long *memuse)
{
    long count = 0, mem = 0;
    int i, j;
    struct NickInfo *ni;
    char **accptr;

    for (i = 0; i < 256; i++) {
        for (ni = nicklists[i]; ni; ni = ni->next) {
            count++;
            mem += sizeof(*ni);
            if (ni->url)
                mem += strlen(ni->url)+1;
            if (ni->email)
                mem += strlen(ni->email)+1;
            if (ni->last_usermask)
                mem += strlen(ni->last_usermask)+1;
            if (ni->last_realname)
                mem += strlen(ni->last_realname)+1;
            if (ni->last_quit)
                mem += strlen(ni->last_quit)+1;
            mem += sizeof(char *) * ni->accesscount;
            for (accptr=ni->access, j=0; j < ni->accesscount; accptr++, j++) {
                if (*accptr)
                    mem += strlen(*accptr)+1;
            }
            mem += ni->memos.memocount * sizeof(struct Memo);
            for (j = 0; j < ni->memos.memocount; j++) {
                if (ni->memos.memos[j].text)
                    mem += strlen(ni->memos.memos[j].text)+1;
            }
        }
    }
    *nrec = count;
    *memuse = mem;
}

/*************************************************************************/
/*************************************************************************/

/* NickServ initialization. */

void ns_init(void)
{
    ncNickServ = service_create(s_NickServ, NULL, NULL, desc_NickServ);
    ncNickServ->service->has_priv = NULL;
    ncNickServ->service->func = nickserv;
    set_user_modes(&myService, ncNickServ, "+Bd");
    service_introduce(ncNickServ);
}

/*************************************************************************/

/* Main NickServ routine. */

void nickserv(struct NetClient *source, char *buf)
{
    char *cmd, *s;

    cmd = strtok(buf, " ");

    if (!cmd) {
        return;
    } else if (strcasecmp(cmd, "\1PING") == 0) {
        if (!(s = strtok(NULL, "")))
            s = "\1";
        notice(ncNickServ, source, "\1PING %s", s);
    } else if (skeleton) {
        msg_lang(ncNickServ, source, SERVICE_OFFLINE, s_NickServ);
    } else {
        run_cmd(ncNickServ, source, cmds, cmd);
    }

}

/*************************************************************************/

/* Load/save data files. */


#define SAFE(x) do {                                        \
    if ((x) < 0) {                                        \
        if (!forceload)                                        \
            fatal("Read error on %s", NickDBName);        \
        failed = 1;                                        \
        break;                                                \
    }                                                        \
} while (0)


static void load_old_ns_dbase(struct dbFILE *f, int ver)
{
    struct nickinfo_ {
        struct NickInfo *next, *prev;
        char nick[NICKMAX];
        char pass[PASSMAX];
        char *last_usermask;
        char *last_realname;
        time_t time_registered;
        time_t last_seen;
        long accesscount;
        char **access;
        long flags;
        time_t id_timestamp;
        unsigned short memomax;
        unsigned short channelcount;
        char *url;
        char *email;
    } old_nickinfo;

    int i, j, c;
    struct NickInfo *ni, **last, *prev;
    int failed = 0;

    for (i = 33; i < 256 && !failed; i++) {
        last = &nicklists[i];
        prev = NULL;
        while ((c = getc_db(f)) != 0) {
            if (c != 1)
                fatal("Invalid format in %s", NickDBName);
            SAFE(read_variable(old_nickinfo, f));
            if (debug >= 3)
                slog(logsvc, "debug: load_old_ns_dbase read nick %s", old_nickinfo.nick);
            ni = scalloc(1, sizeof(struct NickInfo));
            *last = ni;
            last = &ni->next;
            ni->prev = prev;
            prev = ni;
            strscpy(ni->nick, old_nickinfo.nick, NICKMAX);
            strscpy(ni->pass, old_nickinfo.pass, PASSMAX);
            ni->time_registered = old_nickinfo.time_registered;
            ni->last_seen = old_nickinfo.last_seen;
            ni->accesscount = old_nickinfo.accesscount;
            ni->flags = old_nickinfo.flags;
            if (ver < 3)        /* Memo max field created in ver 3 */
                ni->memos.memomax = MSMaxMemos;
            else if (old_nickinfo.memomax)
                ni->memos.memomax = old_nickinfo.memomax;
            else
                ni->memos.memomax = -1;  /* Unlimited is now -1 */
            /* Reset channel count because counting was broken in old
             * versions; load_old_cs_dbase() will calculate the count */
            ni->channelcount = 0;
            ni->channelmax = CSMaxReg;
            ni->language = DEF_LANGUAGE;
            /* ENCRYPTEDPW and VERBOTEN moved from ni->flags to ni->status */
            if (ni->flags & 4)
                ni->status |= NS_VERBOTEN;
            if (ni->flags & 8)
                ni->status |= NS_ENCRYPTEDPW;
            ni->flags &= ~0xE000000C;
#ifdef USE_ENCRYPTION
            if (!(ni->status & (NS_ENCRYPTEDPW | NS_VERBOTEN))) {
                if (debug)
                    slog(logsvc, "debug: %s: encrypting password for `%s' on load",
                                s_NickServ, ni->nick);
                if (encrypt_in_place(ni->pass, PASSMAX) < 0)
                    fatal("%s: Can't encrypt `%s' nickname password!",
                                s_NickServ, ni->nick);
                ni->status |= NS_ENCRYPTEDPW;
            }
#else
            if (ni->status & NS_ENCRYPTEDPW) {
                /* Bail: it makes no sense to continue with encrypted
                 * passwords, since we won't be able to verify them */
                fatal("%s: load database: password for %s encrypted "
                          "but encryption disabled, aborting",
                          s_NickServ, ni->nick);
            }
#endif
            if (old_nickinfo.url)
                SAFE(read_string(&ni->url, f));
            if (old_nickinfo.email)
                SAFE(read_string(&ni->email, f));
            SAFE(read_string(&ni->last_usermask, f));
            if (!ni->last_usermask)
                ni->last_usermask = sstrdup("@");
            SAFE(read_string(&ni->last_realname, f));
            if (!ni->last_realname)
                ni->last_realname = sstrdup("");
            if (ni->accesscount) {
                char **access, *s;
                if (ni->accesscount > NSAccessMax)
                    ni->accesscount = NSAccessMax;
                access = smalloc(sizeof(char *) * ni->accesscount);
                ni->access = access;
                for (j = 0; j < ni->accesscount; j++, access++)
                    SAFE(read_string(access, f));
                while (j < old_nickinfo.accesscount) {
                    SAFE(read_string(&s, f));
                    if (s)
                        free(s);
                    j++;
                }
            }
            ni->id_timestamp = 0;
            if (ver < 3) {
                ni->flags |= NI_MEMO_SIGNON | NI_MEMO_RECEIVE;
            } else if (ver == 3) {
                if (!(ni->flags & (NI_MEMO_SIGNON | NI_MEMO_RECEIVE)))
                    ni->flags |= NI_MEMO_SIGNON | NI_MEMO_RECEIVE;
            }
        } /* while (getc_db(f) != 0) */
        *last = NULL;
    } /* for (i) */
    if (debug >= 2)
        slog(logsvc, "debug: load_old_ns_dbase(): loading memos");
    load_old_ms_dbase();
}


void load_ns_dbase(void)
{
    struct dbFILE *f;
    int ver, i, j, c;
    struct NickInfo *ni, **last, *prev;
    int failed = 0;

    if (!(f = open_db(s_NickServ, NickDBName, "r")))
        return;

    switch (ver = get_file_version(f)) {

      case 7:
      case 6:
      case 5:
        for (i = 0; i < 256 && !failed; i++) {
            int32_t tmp32;
            last = &nicklists[i];
            prev = NULL;
            while ((c = getc_db(f)) == 1) {
                if (c != 1)
                    fatal("Invalid format in %s", NickDBName);
                ni = scalloc(sizeof(struct NickInfo), 1);
                *last = ni;
                last = &ni->next;
                ni->prev = prev;
                prev = ni;
                SAFE(read_buffer(ni->nick, f));
                SAFE(read_buffer(ni->pass, f));
                SAFE(read_string(&ni->url, f));
                SAFE(read_string(&ni->email, f));
                SAFE(read_string(&ni->last_usermask, f));
                if (!ni->last_usermask)
                    ni->last_usermask = sstrdup("@");
                SAFE(read_string(&ni->last_realname, f));
                if (!ni->last_realname)
                    ni->last_realname = sstrdup("");
                SAFE(read_string(&ni->last_quit, f));
                SAFE(read_int32(&tmp32, f));
                ni->time_registered = tmp32;
                SAFE(read_int32(&tmp32, f));
                ni->last_seen = tmp32;
                SAFE(read_int16(&ni->status, f));
                ni->status &= ~NS_TEMPORARY;
#ifdef USE_ENCRYPTION
                if (!(ni->status & (NS_ENCRYPTEDPW | NS_VERBOTEN))) {
                    if (debug)
                        slog(logsvc, "debug: %s: encrypting password for `%s' on load",
                                s_NickServ, ni->nick);
                    if (encrypt_in_place(ni->pass, PASSMAX) < 0)
                        fatal("%s: Can't encrypt `%s' nickname password!",
                                s_NickServ, ni->nick);
                    ni->status |= NS_ENCRYPTEDPW;
                }
#else
                if (ni->status & NS_ENCRYPTEDPW) {
                    /* Bail: it makes no sense to continue with encrypted
                     * passwords, since we won't be able to verify them */
                    fatal("%s: load database: password for %s encrypted "
                          "but encryption disabled, aborting",
                          s_NickServ, ni->nick);
                }
#endif
                /* Store the _name_ of the link target in ni->link for now;
                 * we'll resolve it after we've loaded all the nicks */
                SAFE(read_string((char **)&ni->link, f));
                SAFE(read_int16(&ni->linkcount, f));
                if (ni->link) {
                    SAFE(read_int16(&ni->channelcount, f));
                    /* No other information saved for linked nicks, since
                     * they get it all from their link target */
                    ni->flags = 0;
                    ni->accesscount = 0;
                    ni->access = NULL;
                    ni->memos.memocount = 0;
                    ni->memos.memomax = MSMaxMemos;
                    ni->memos.memos = NULL;
                    ni->channelmax = CSMaxReg;
                    ni->language = DEF_LANGUAGE;
                } else {
                    SAFE(read_int32(&ni->flags, f));
                    if (!NSAllowKillImmed)
                        ni->flags &= ~NI_KILL_IMMED;
                    SAFE(read_int16(&ni->accesscount, f));
                    if (ni->accesscount) {
                        char **access;
                        access = smalloc(sizeof(char *) * ni->accesscount);
                        ni->access = access;
                        for (j = 0; j < ni->accesscount; j++, access++)
                            SAFE(read_string(access, f));
                    }
                    SAFE(read_int16(&ni->memos.memocount, f));
                    SAFE(read_int16(&ni->memos.memomax, f));
                    if (ni->memos.memocount) {
                        struct Memo *memos;
                        memos = smalloc(sizeof(struct Memo) * ni->memos.memocount);
                        ni->memos.memos = memos;
                        for (j = 0; j < ni->memos.memocount; j++, memos++) {
                            SAFE(read_int32(&memos->number, f));
                            SAFE(read_int16(&memos->flags, f));
                            SAFE(read_int32(&tmp32, f));
                            memos->time = tmp32;
                            SAFE(read_buffer(memos->sender, f));
                            SAFE(read_string(&memos->text, f));
                        }
                    }
                    SAFE(read_int16(&ni->channelcount, f));
                    SAFE(read_int16(&ni->channelmax, f));
                    if (ver == 5) {
                        /* Fields not initialized properly for new nicks */
                        /* These will be updated by load_cs_dbase() */
                        ni->channelcount = 0;
                        ni->channelmax = CSMaxReg;
                    }
                    SAFE(read_int16(&ni->language, f));
                }
                ni->id_timestamp = 0;
            } /* while (getc_db(f) != 0) */
            *last = NULL;
        } /* for (i) */

        /* Now resolve links */
        for (i = 0; i < 256; i++) {
            for (ni = nicklists[i]; ni; ni = ni->next) {
                if (ni->link)
                    ni->link = findnick((char *)ni->link);
            }
        }

        break;

      case 4:
      case 3:
      case 2:
      case 1:
        load_old_ns_dbase(f, ver);
        break;

      default:
        fatal("Unsupported version number (%d) on %s", ver, NickDBName);

    } /* switch (version) */

    close_db(f);
}

#undef SAFE

/*************************************************************************/

#define SAFE(x) do {                                                \
    if ((x) < 0) {                                                \
        restore_db(f);                                                \
        log_perror("Write error on %s", NickDBName);                \
        if (time(NULL) - lastwarn > WarningTimeout) {                \
            wallops(NULL, "Write error on %s: %s", NickDBName,        \
                        strerror(errno));                        \
            lastwarn = time(NULL);                                \
        }                                                        \
        return;                                                        \
    }                                                                \
} while (0)

void save_ns_dbase(void)
{
    struct dbFILE *f;
    int i, j;
    struct NickInfo *ni;
    char **access;
    struct Memo *memos;
    static time_t lastwarn = 0;

    if (!(f = open_db(s_NickServ, NickDBName, "w")))
        return;
    for (i = 0; i < 256; i++) {
        for (ni = nicklists[i]; ni; ni = ni->next) {
            SAFE(write_int8(1, f));
            SAFE(write_buffer(ni->nick, f));
            SAFE(write_buffer(ni->pass, f));
            SAFE(write_string(ni->url, f));
            SAFE(write_string(ni->email, f));
            SAFE(write_string(ni->last_usermask, f));
            SAFE(write_string(ni->last_realname, f));
            SAFE(write_string(ni->last_quit, f));
            SAFE(write_int32(ni->time_registered, f));
            SAFE(write_int32(ni->last_seen, f));
            SAFE(write_int16(ni->status, f));
            if (ni->link) {
                SAFE(write_string(ni->link->nick, f));
                SAFE(write_int16(ni->linkcount, f));
                SAFE(write_int16(ni->channelcount, f));
            } else {
                SAFE(write_string(NULL, f));
                SAFE(write_int16(ni->linkcount, f));
                SAFE(write_int32(ni->flags, f));
                SAFE(write_int16(ni->accesscount, f));
                for (j=0, access=ni->access; j<ni->accesscount; j++, access++)
                    SAFE(write_string(*access, f));
                SAFE(write_int16(ni->memos.memocount, f));
                SAFE(write_int16(ni->memos.memomax, f));
                memos = ni->memos.memos;
                for (j = 0; j < ni->memos.memocount; j++, memos++) {
                    SAFE(write_int32(memos->number, f));
                    SAFE(write_int16(memos->flags, f));
                    SAFE(write_int32(memos->time, f));
                    SAFE(write_buffer(memos->sender, f));
                    SAFE(write_string(memos->text, f));
                }
                SAFE(write_int16(ni->channelcount, f));
                SAFE(write_int16(ni->channelmax, f));
                SAFE(write_int16(ni->language, f));
            }
        } /* for (ni) */
        SAFE(write_int8(0, f));
    } /* for (i) */
    close_db(f);
}

#undef SAFE

/*************************************************************************/

/* Check whether a user is on the access list of the nick they're using, or
 * if they're the same user who last identified for the nick.  If not, send
 * warnings as appropriate.  If so (and not NI_SECURE), update last seen
 * info.  Return 1 if the user is valid and recognized, 0 otherwise (note
 * that this means an NI_SECURE nick will return 0 from here unless the
 * user's timestamp matches the last identify timestamp).  If the user's
 * nick is not registered, 0 is returned.
 */

int validate_user(struct NetClient *nc)
{
    struct NickInfo *ni;
    int on_access;

    if (!(ni = nc->user->real_ni))
        return 0;

    if (ni->status & NS_VERBOTEN) {
        msg_lang(ncNickServ, nc, NICK_MAY_NOT_BE_USED);
        if (NSForceNickChange)
            msg_lang(ncNickServ, nc, FORCENICKCHANGE_IN_1_MINUTE);
        else
            msg_lang(ncNickServ, nc, DISCONNECT_IN_1_MINUTE);
        add_ns_timeout(ni, TO_COLLIDE, 60);
        return 0;
    }

    if (!NoSplitRecovery) {
        /* XXX: This code should be checked to ensure it can't be fooled */
        if (ni->id_timestamp != 0 && nc->user->ts_connect == ni->id_timestamp) {
            char buf[256];
            snprintf(buf, sizeof(buf), "%s@%s", nc->user->username, nc->user->host);
            if (strcmp(buf, ni->last_usermask) == 0) {
                ni->status |= NS_IDENTIFIED;
                return 1;
            }
        }
    }

    on_access = is_on_access(nc, nc->user->ni);
    if (on_access)
        ni->status |= NS_ON_ACCESS;

    if (!(nc->user->ni->flags & NI_SECURE) && on_access) {
        ni->status |= NS_RECOGNIZED;
        ni->last_seen = time(NULL);
        if (ni->last_usermask)
            free(ni->last_usermask);
        ni->last_usermask = smalloc(strlen(nc->user->username)+strlen(nc->user->host)+2);
        sprintf(ni->last_usermask, "%s@%s", nc->user->username, nc->user->host);
        if (ni->last_realname)
            free(ni->last_realname);
        ni->last_realname = sstrdup(nc->description);
        return 1;
    }

    if (on_access || !(nc->user->ni->flags & NI_KILL_IMMED)) {
        if (nc->user->ni->flags & NI_SECURE)
            msg_lang(ncNickServ, nc, NICK_IS_SECURE, s_NickServ);
        else
            msg_lang(ncNickServ, nc, NICK_IS_REGISTERED, s_NickServ);
    }

    if ((nc->user->ni->flags & NI_KILLPROTECT) && !on_access) {
        if (nc->user->ni->flags & NI_KILL_IMMED) {
            collide(ni, 0);
        } else if (nc->user->ni->flags & NI_KILL_QUICK) {
            if (NSForceNickChange)
                    msg_lang(ncNickServ, nc, FORCENICKCHANGE_IN_20_SECONDS);
            else
                    msg_lang(ncNickServ, nc, DISCONNECT_IN_20_SECONDS);
            add_ns_timeout(ni, TO_COLLIDE, 20);
        } else {
            if (NSForceNickChange)
                    msg_lang(ncNickServ, nc, FORCENICKCHANGE_IN_1_MINUTE);
            else
                    msg_lang(ncNickServ, nc, DISCONNECT_IN_1_MINUTE);
            add_ns_timeout(ni, TO_COLLIDE, 60);
        }
    }

    return 0;
}

/*************************************************************************/

/* Cancel validation flags for a nick (i.e. when the user with that nick
 * signs off or changes nicks).  Also cancels any impending collide. */

void cancel_user(struct NetClient *nc)
{
    struct NickInfo *ni = nc->user->real_ni;
    if (ni) {

        if (ni->status & NS_GUESTED) {
            struct NetClient *ncenforce;
            char realname[NICKMAX+16];

            snprintf(realname, sizeof(realname), "%s Enforcement", s_NickServ);

            ncenforce = service_create(ni->nick, NSEnforcerUser, NSEnforcerHost, realname);
            ncenforce->service->has_priv = NULL;
            ncenforce->service->func = NULL;
            service_introduce(ncenforce);

            add_ns_timeout(ni, TO_RELEASE, NSReleaseTimeout);
            ni->status &= ~NS_TEMPORARY;
            ni->status |= NS_KILL_HELD;
        } else {
            ni->status &= ~NS_TEMPORARY;
        }
        del_ns_timeout(ni, TO_COLLIDE);
    }
}

/*************************************************************************/

/* Return whether a user has identified for their nickname. */

int nick_identified(struct NetClient *nc)
{
    return nc->user->real_ni && (nc->user->real_ni->status & NS_IDENTIFIED);
}

/*************************************************************************/

/* Return whether a user is recognized for their nickname. */

int nick_recognized(struct NetClient *nc)
{
    return nc->user->real_ni && (nc->user->real_ni->status & (NS_IDENTIFIED | NS_RECOGNIZED));
}

/*************************************************************************/

/* Remove all nicks which have expired.  Also update last-seen time for all
 * nicks.
 */

void expire_nicks()
{
    struct NetClient *nc;
    struct NickInfo *ni, *next;
    int i;
    time_t now = time(NULL);

    /* Assumption: this routine takes less than NSExpire seconds to run.
     * If it doesn't, some users may end up with invalid user->ni pointers. */
    for (nc = firstnc(); nc; nc = nextnc()) {
        if (!IsUser(nc))
            continue;

        if (nc->user->real_ni) {
            if (debug >= 2)
                slog(logsvc, "debug: NickServ: updating last seen time for %s", nc->name);
            nc->user->real_ni->last_seen = time(NULL);
        }
    }
    if (!NSExpire)
        return;
    for (i = 0; i < 256; i++) {
        for (ni = nicklists[i]; ni; ni = next) {
            next = ni->next;
            if (now - ni->last_seen >= NSExpire
                        && !(ni->status & (NS_VERBOTEN | NS_NO_EXPIRE))) {
                slog(logsvc, "Expiring nickname %s", ni->nick);
                delnick(ni);
            }
        }
    }
}

/*************************************************************************/
/*************************************************************************/

/* Return the NickInfo structure for the given nick, or NULL if the nick
 * isn't registered. */

struct NickInfo *findnick(const char *nick)
{
    struct NickInfo *ni;

    for (ni = nicklists[tolower(*nick)]; ni; ni = ni->next) {
        if (strcasecmp(ni->nick, nick) == 0)
            return ni;
    }
    return NULL;
}

/*************************************************************************/

/* Return the "master" nick for the given nick; i.e., trace the linked list
 * through the `link' field until we find a nickname with a NULL `link'
 * field.  Assumes `ni' is not NULL.
 *
 * Note that we impose an arbitrary limit of 512 nested links.  This is to
 * prevent infinite loops in case someone manages to create a circular
 * link.  If we pass this limit, we arbitrarily cut off the link at the
 * initial nick.
 */

struct NickInfo *getlink(struct NickInfo *ni)
{
    struct NickInfo *orig = ni;
    int i = 0;

    while (ni->link && ++i < 512)
        ni = ni->link;
    if (i >= 512) {
        slog(logsvc, "%s: Infinite loop(?) found at nick %s for nick %s, cutting link",
                s_NickServ, ni->nick, orig->nick);
        orig->link = NULL;
        ni = orig;
        /* FIXME: we should sanitize the data fields */
    }
    return ni;
}

/*************************************************************************/
/*********************** NickServ private routines ***********************/
/*************************************************************************/

/* Is the given user's address on the given nick's access list?  Return 1
 * if so, 0 if not. */

static int is_on_access(struct NetClient *nc, struct NickInfo *ni)
{
    int i;
    char *buf;

    if (ni->accesscount == 0)
        return 0;
    i = strlen(nc->user->username);
    buf = smalloc(i + strlen(nc->user->host) + 2);
    sprintf(buf, "%s@%s", nc->user->username, nc->user->host);
    strlower(buf+i+1);
    for (i = 0; i < ni->accesscount; i++) {
        if (match_wild_nocase(ni->access[i], buf)) {
            free(buf);
            return 1;
        }
    }
    free(buf);
    return 0;
}

/*************************************************************************/

/* Insert a nick alphabetically into the database. */

static void alpha_insert_nick(struct NickInfo *ni)
{
    struct NickInfo *ptr, *prev;
    char *nick = ni->nick;

    for (prev = NULL, ptr = nicklists[tolower(*nick)];
                        ptr && strcasecmp(ptr->nick, nick) < 0;
                        prev = ptr, ptr = ptr->next)
        ;
    ni->prev = prev;
    ni->next = ptr;
    if (!prev)
        nicklists[tolower(*nick)] = ni;
    else
        prev->next = ni;
    if (ptr)
        ptr->prev = ni;
}

/*************************************************************************/

/* Add a nick to the database.  Returns a pointer to the new NickInfo
 * structure if the nick was successfully registered, NULL otherwise.
 * Assumes nick does not already exist.
 */

static struct NickInfo *makenick(const char *nick)
{
    struct NickInfo *ni;

    ni = scalloc(sizeof(struct NickInfo), 1);
    strscpy(ni->nick, nick, NICKMAX);
    alpha_insert_nick(ni);
    return ni;
}

/*************************************************************************/

/* Remove a nick from the NickServ database.  Return 1 on success, 0
 * otherwise.  Also deletes the nick from any ChanServ/OperServ lists it is
 * on.
 */

static int delnick(struct NickInfo *ni)
{
    int i;

    cs_remove_nick(ni);
    os_remove_nick(ni);
    if (ni->linkcount)
        remove_links(ni);
    if (ni->link)
        ni->link->linkcount--;
    if (ni->next)
        ni->next->prev = ni->prev;
    if (ni->prev)
        ni->prev->next = ni->next;
    else
        nicklists[tolower(*ni->nick)] = ni->next;
    if (ni->last_usermask)
        free(ni->last_usermask);
    if (ni->last_realname)
        free(ni->last_realname);
    if (ni->access) {
        for (i = 0; i < ni->accesscount; i++) {
            if (ni->access[i])
                free(ni->access[i]);
        }
        free(ni->access);
    }
    if (ni->memos.memos) {
        for (i = 0; i < ni->memos.memocount; i++) {
            if (ni->memos.memos[i].text)
                free(ni->memos.memos[i].text);
        }
        free(ni->memos.memos);
    }
    free(ni);
    return 1;
}

/*************************************************************************/

/* Remove any links to the given nick (i.e. prior to deleting the nick).
 * Note this is currently linear in the number of nicks in the database--
 * that's the tradeoff for the nice clean method of keeping a single parent
 * link in the data structure.
 */

static void remove_links(struct NickInfo *ni)
{
    int i;
    struct NickInfo *ptr;

    for (i = 0; i < 256; i++) {
        for (ptr = nicklists[i]; ptr; ptr = ptr->next) {
            if (ptr->link == ni) {
                if (ni->link) {
                    ptr->link = ni->link;
                    ni->link->linkcount++;
                } else
                    delink(ptr);
            }
        }
    }
}

/*************************************************************************/

/* Break a link from the given nick to its parent. */

static void delink(struct NickInfo *ni)
{
    struct NickInfo *link;

    link = ni->link;
    ni->link = NULL;
    do {
        link->channelcount -= ni->channelcount;
        if (link->link)
            link = link->link;
    } while (link->link);
    ni->status = link->status;
    link->status &= ~NS_TEMPORARY;
    ni->flags = link->flags;
    ni->channelmax = link->channelmax;
    ni->memos.memomax = link->memos.memomax;
    ni->language = link->language;
    if (link->accesscount > 0) {
        char **access;
        int i;

        ni->accesscount = link->accesscount;
        access = smalloc(sizeof(char *) * ni->accesscount);
        ni->access = access;
        for (i = 0; i < ni->accesscount; i++, access++)
            *access = sstrdup(link->access[i]);
    }
    link->linkcount--;
}

/*************************************************************************/

/* Marks a user as having identified for the given nick. */

void set_identified(struct NetClient *nc, int server)
{
    struct NickInfo *ni = nc->user->ni;

    if (!ni)
        return;

    ni->status |= NS_IDENTIFIED;
    ni->id_timestamp = nc->user->ts_connect;
    ni->last_seen = time(NULL);
    if (!(ni->status & NS_RECOGNIZED)) {
        if (ni->last_usermask)
            sfree(ni->last_usermask);
        ni->last_usermask = smalloc(strlen(nc->user->username)+strlen(nc->user->host)+2);
        sprintf(ni->last_usermask, "%s@%s", nc->user->username, nc->user->host);
        //memcpy(&ni->last_ip, &nc->user->ip, sizeof(struct irc_in_addr));
        if (ni->last_realname)
            sfree(ni->last_realname);
        ni->last_realname = sstrdup(nc->description);
    }
   if (server) {
       slog(logsvc, "%s: %s!%s@%s AUTO-identified for nick %s", s_NickServ,
               nc->name, nc->user->username, nc->user->host, nc->name);
//       msg_lang(ncNickServ, nc, NICK_IDENTIFY_SERVER);
   } else {
       slog(logsvc, "%s: %s!%s@%s identified for nick %s", s_NickServ,
               nc->name, nc->user->username, nc->user->host, nc->name);
       msg_lang(ncNickServ, nc, NICK_IDENTIFY_SUCCEEDED);
   }
   if (!(ni->status & NS_RECOGNIZED)) {
       check_memos(nc);
       ni->status |= NS_RECOGNIZED;
   }
   strcpy(ni->nick, nc->name);
}

/*************************************************************************/
/*************************************************************************/

/* Collide a nick. 
 *
 * When connected to a network using DALnet servers, version 4.4.15 and above, 
 * Services is now able to force a nick change instead of killing the user. 
 * The new nick takes the form "Guest######". If a nick change is forced, we
 * do not introduce the enforcer nick until the user's nick actually changes. 
 * This is watched for and done in cancel_user(). -TheShadow 
 */

static void collide(struct NickInfo *ni, int from_timeout)
{
    struct NetClient *nc;

    nc = FindUser(ni->nick);

    if (!from_timeout)
        del_ns_timeout(ni, TO_COLLIDE);

    if (NSForceNickChange) {
        struct timeval tv;
        char guestnick[NICKMAX];

        gettimeofday(&tv, NULL);
        snprintf(guestnick, sizeof(guestnick), "%s%ld%ld", NSGuestNickPrefix,
                        tv.tv_usec / 10000, tv.tv_sec % (60*60*24));

        msg_lang(ncNickServ, nc, FORCENICKCHANGE_NOW, guestnick);

        send_cmd(&myService, "SN %s %s", ni->nick, guestnick);
        ni->status |= NS_GUESTED;
    } else {
        char realname[NICKMAX+16]; /* Long enough for s_NickServ + " Enforcement" */

        msg_lang(ncNickServ, nc, DISCONNECT_NOW);

            kill_user(ncNickServ, nc, "Nick kill enforced");

        snprintf(realname, sizeof(realname), "%s Enforcement", s_NickServ);

        service_create(ni->nick, NSEnforcerUser, NSEnforcerHost, realname);
        ni->status |= NS_KILL_HELD;
        add_ns_timeout(ni, TO_RELEASE, NSReleaseTimeout);
    }
}

/*************************************************************************/

/* Release hold on a nick. */

static void release(struct NickInfo *ni, int from_timeout)
{
    struct NetClient *ncUser;

    if (!from_timeout)
        del_ns_timeout(ni, TO_RELEASE);

    ncUser = FindService(ni->nick);
    if (ncUser)
        service_remove(ncUser);

    ni->status &= ~NS_KILL_HELD;
}

/*************************************************************************/
/*************************************************************************/

static struct my_timeout {
    struct my_timeout *next, *prev;
    struct NickInfo *ni;
    struct Timeout *to;
    int type;
} *my_timeouts;

/*************************************************************************/

/* Remove a collide/release timeout from our private list. */

static void rem_ns_timeout(struct NickInfo *ni, int type)
{
    struct my_timeout *t, *t2;

    t = my_timeouts;
    while (t) {
        if (t->ni == ni && t->type == type) {
            t2 = t->next;
            if (t->next)
                t->next->prev = t->prev;
            if (t->prev)
                t->prev->next = t->next;
            else
                my_timeouts = t->next;
            free(t);
            t = t2;
        } else {
            t = t->next;
        }
    }
}

/*************************************************************************/

/* Collide a nick on timeout. */

static void timeout_collide(struct Timeout *t)
{
    struct NickInfo *ni = t->data;
    struct NetClient *nc;

    rem_ns_timeout(ni, TO_COLLIDE);
    /* If they identified or don't exist anymore, don't kill them. */
    if ((ni->status & NS_IDENTIFIED)
                || !(nc = FindUser(ni->nick))
                || nc->user->my_signon > t->settime)
        return;
    /* The RELEASE timeout will always add to the beginning of the
     * list, so we won't see it.  Which is fine because it can't be
     * triggered yet anyway. */
    collide(ni, 1);
}

/*************************************************************************/

/* Release a nick on timeout. */

static void timeout_release(struct Timeout *t)
{
    struct NickInfo *ni = t->data;

    rem_ns_timeout(ni, TO_RELEASE);
    release(ni, 1);
}

/*************************************************************************/

/* Add a collide/release timeout. */

void add_ns_timeout(struct NickInfo *ni, int type, time_t delay)
{
    struct Timeout *to;
    struct my_timeout *t;
    void (*timeout_routine)(struct Timeout *);

    if (type == TO_COLLIDE)
        timeout_routine = timeout_collide;
    else if (type == TO_RELEASE)
        timeout_routine = timeout_release;
    else {
        slog(logsvc, "NickServ: unknown timeout type %d!  ni=%p (%s), delay=%ld",
                type, ni, ni->nick, delay);
        return;
    }
    to = add_timeout(delay, timeout_routine, 0);
    to->data = ni;
    t = smalloc(sizeof(*t));
    t->next = my_timeouts;
    my_timeouts = t;
    t->prev = NULL;
    t->ni = ni;
    t->to = to;
    t->type = type;
}

/*************************************************************************/

/* Delete a collide/release timeout. */

static void del_ns_timeout(struct NickInfo *ni, int type)
{
    struct my_timeout *t, *t2;

    t = my_timeouts;
    while (t) {
        if (t->ni == ni && t->type == type) {
            t2 = t->next;
            if (t->next)
                t->next->prev = t->prev;
            if (t->prev)
                t->prev->next = t->next;
            else
                my_timeouts = t->next;
            del_timeout(t->to);
            free(t);
            t = t2;
        } else {
            t = t->next;
        }
    }
}

/*************************************************************************/
/*********************** NickServ command routines ***********************/
/*************************************************************************/

/* Return a help message. */

static void do_help(struct NetClient *nc)
{
    char *cmd = strtok(NULL, "");

    if (!cmd) {
        if (NSExpire >= 86400)
            msg_help(ncNickServ, nc, NICK_HELP, NSExpire/86400);
        else
            msg_help(ncNickServ, nc, NICK_HELP_EXPIRE_ZERO);
        if (is_services_oper(nc))
            msg_help(ncNickServ, nc, NICK_SERVADMIN_HELP);
    } else if (strcasecmp(cmd, "SET LANGUAGE") == 0) {
        int i;
        msg_help(ncNickServ, nc, NICK_HELP_SET_LANGUAGE);
        for (i = 0; i < NUM_LANGS && langlist[i] >= 0; i++) {
            notice(ncNickServ, nc, "    %2d) %s",
                        i+1, getstring_lang(langlist[i], LANG_NAME));
        }
    } else {
        help_cmd(ncNickServ, nc, cmds, cmd);
    }
}

/*************************************************************************/

/* Register a nick. */

static void do_register(struct NetClient *nc)
{
    struct NickInfo *ni;
    char *pass = strtok(NULL, " ");

    if (readonly) {
        msg_lang(ncNickServ, nc, NICK_REGISTRATION_DISABLED);
        return;
    }

#ifdef IRC_DAL4_4_15
    /* Prevent "Guest" nicks from being registered. -TheShadow */
    if (NSForceNickChange) {
        int prefixlen = strlen(NSGuestNickPrefix);
        int nicklen = strlen(nc->name);

        /* A guest nick is defined as a nick...
         *         - starting with NSGuestNickPrefix
         *         - with a series of between, and including, 2 and 7 digits
         * -TheShadow
         */
        if (nicklen <= prefixlen+7 && nicklen >= prefixlen+2 &&
                        stristr(nc->name, NSGuestNickPrefix) == nc->name &&
                        strspn(nc->name+prefixlen, "1234567890") ==
                                                        nicklen-prefixlen) {
            msg_lang(ncNickServ, nc, NICK_CANNOT_BE_REGISTERED, nc->name);
            return;
        }
    }
#endif

    if (!pass || (strcasecmp(pass, nc->name) == 0 && strtok(NULL, " "))) {
        syntax_error(ncNickServ, nc, "REGISTER", NICK_REGISTER_SYNTAX);

    } else if (time(NULL) < nc->user->lastnickreg + NSRegDelay) {
        msg_lang(ncNickServ, nc, NICK_REG_PLEASE_WAIT, NSRegDelay);

    } else if (nc->user->real_ni) {        /* i.e. there's already such a nick regged */
        if (nc->user->real_ni->status & NS_VERBOTEN) {
            slog(logsvc, "%s: %s@%s tried to register FORBIDden nick %s", s_NickServ,
                        nc->user->username, nc->user->host, nc->name);
            msg_lang(ncNickServ, nc, NICK_CANNOT_BE_REGISTERED, nc->name);
        } else {
            msg_lang(ncNickServ, nc, NICK_ALREADY_REGISTERED, nc->name);
        }

    } else if (strcasecmp(nc->name, pass) == 0
                || (StrictPasswords && strlen(pass) < 5)
    ) {
        msg_lang(ncNickServ, nc, MORE_OBSCURE_PASSWORD);

    } else {
        ni = makenick(nc->name);
        if (ni) {
#ifdef USE_ENCRYPTION
            int len = strlen(pass);
            if (len > PASSMAX) {
                len = PASSMAX;
                pass[len] = 0;
                msg_lang(ncNickServ, nc, PASSWORD_TRUNCATED, PASSMAX);
            }
            if (encrypt(pass, len, ni->pass, PASSMAX) < 0) {
                memset(pass, 0, strlen(pass));
                slog(logsvc, "%s: Failed to encrypt password for %s (register)",
                        s_NickServ, nc->name);
                msg_lang(ncNickServ, nc, NICK_REGISTRATION_FAILED);
                return;
            }
            memset(pass, 0, strlen(pass));
            ni->status = NS_ENCRYPTEDPW | NS_IDENTIFIED | NS_RECOGNIZED;
#else
            if (strlen(pass) > PASSMAX-1) /* -1 for null byte */
                msg_lang(ncNickServ, nc, PASSWORD_TRUNCATED, PASSMAX-1);
            strscpy(ni->pass, pass, PASSMAX);
            ni->status = NS_IDENTIFIED | NS_RECOGNIZED;
#endif
            ni->flags = 0;
            if (NSDefKill)
                ni->flags |= NI_KILLPROTECT;
            if (NSDefKillQuick)
                ni->flags |= NI_KILL_QUICK;
            if (NSDefSecure)
                ni->flags |= NI_SECURE;
            if (NSDefPrivate)
                ni->flags |= NI_PRIVATE;
            if (NSDefHideEmail)
                ni->flags |= NI_HIDE_EMAIL;
            if (NSDefHideUsermask)
                ni->flags |= NI_HIDE_MASK;
            if (NSDefHideQuit)
                ni->flags |= NI_HIDE_QUIT;
            if (NSDefMemoSignon)
                ni->flags |= NI_MEMO_SIGNON;
            if (NSDefMemoReceive)
                ni->flags |= NI_MEMO_RECEIVE;
            ni->memos.memomax = MSMaxMemos;
            ni->channelcount = 0;
            ni->channelmax = CSMaxReg;
            ni->last_usermask = smalloc(strlen(nc->user->username)+strlen(nc->user->host)+2);
            sprintf(ni->last_usermask, "%s@%s", nc->user->username, nc->user->host);
            ni->last_realname = sstrdup(nc->description);
            ni->time_registered = ni->last_seen = time(NULL);
            ni->accesscount = 1;
            ni->access = smalloc(sizeof(char *));
            ni->access[0] = create_mask_access(nc->user->username, nc->user->host);
            ni->language = DEF_LANGUAGE;
            ni->link = NULL;
            nc->user->ni = nc->user->real_ni = ni;
            slog(logsvc, "%s: `%s' registered by %s@%s", s_NickServ,
                        nc->name, nc->user->username, nc->user->host);
            msg_lang(ncNickServ, nc, NICK_REGISTERED, nc->name, ni->access[0]);
#ifndef USE_ENCRYPTION
            msg_lang(ncNickServ, nc, NICK_PASSWORD_IS, ni->pass);
#endif
            nc->user->lastnickreg = time(NULL);
#ifdef IRC_DAL4_4_15
            send_cmd(ServerName, "SVSMODE %s +r", nc->name);
#endif
        } else {
            slog(logsvc, "%s: makenick(%s) failed", s_NickServ, nc->name);
            msg_lang(ncNickServ, nc, NICK_REGISTRATION_FAILED);
        }

    }

}

/*************************************************************************/

static void do_identify(struct NetClient *nc)
{
    char *pass = strtok(NULL, " ");
    struct NickInfo *ni;
    int res;

    if (!pass) {
        syntax_error(ncNickServ, nc, "IDENTIFY", NICK_IDENTIFY_SYNTAX);

    } else if (!(ni = nc->user->real_ni)) {
        notice(ncNickServ, nc, "Your nick isn't registered.");

    } else if (!(res = check_password(pass, ni->pass))) {
        slog(logsvc, "%s: Failed IDENTIFY for %s!%s@%s",
                s_NickServ, nc->name, nc->user->username, nc->user->host);
        msg_lang(ncNickServ, nc, PASSWORD_INCORRECT);
        bad_password(nc);

    } else if (res == -1) {
        msg_lang(ncNickServ, nc, NICK_IDENTIFY_FAILED);

    } else {
        ni->status |= NS_IDENTIFIED;
        ni->id_timestamp = nc->user->ts_connect;
        if (!(ni->status & NS_RECOGNIZED)) {
            ni->last_seen = time(NULL);
            if (ni->last_usermask)
                free(ni->last_usermask);
            ni->last_usermask = smalloc(strlen(nc->user->username)+strlen(nc->user->host)+2);
            sprintf(ni->last_usermask, "%s@%s", nc->user->username, nc->user->host);
            if (ni->last_realname)
                free(ni->last_realname);
            ni->last_realname = sstrdup(nc->description);
        }
#ifdef IRC_DAL4_4_15
        send_cmd(ServerName, "SVSMODE %s +r", nc->name);
#endif
        slog(logsvc, "%s: %s!%s@%s identified for nick %s", s_NickServ,
                        nc->name, nc->user->username, nc->user->host, nc->name);
        msg_lang(ncNickServ, nc, NICK_IDENTIFY_SUCCEEDED);
        if (!(ni->status & NS_RECOGNIZED))
            check_memos(nc);

    }
}

/*************************************************************************/

static void do_drop(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    struct NickInfo *ni;
    struct NetClient *nc2;

    if (readonly && !is_services_admin(nc)) {
        msg_lang(ncNickServ, nc, NICK_DROP_DISABLED);
        return;
    }

    if (!is_services_admin(nc) && nick) {
        syntax_error(ncNickServ, nc, "DROP", NICK_DROP_SYNTAX);

    } else if (!(ni = (nick ? findnick(nick) : nc->user->real_ni))) {
        if (nick)
            msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);
        else
            msg_lang(ncNickServ, nc, NICK_NOT_REGISTERED);

    } else if (NSSecureAdmins && nick && nick_is_services_admin(ni) && 
                                                            !is_services_root(nc)) {
        msg_lang(ncNickServ, nc, PERMISSION_DENIED);
        
    } else if (!nick && !nick_identified(nc)) {
        msg_lang(ncNickServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);

    } else {
        if (readonly)
            msg_lang(ncNickServ, nc, READ_ONLY_MODE);
#ifdef IRC_DAL4_4_15
        send_cmd(ServerName, "SVSMODE %s -r", ni->nick);
#endif
        delnick(ni);
        slog(logsvc, "%s: %s!%s@%s dropped nickname %s", s_NickServ,
                nc->name, nc->user->username, nc->user->host, nick ? nick : nc->name);
        if (nick)
            msg_lang(ncNickServ, nc, NICK_X_DROPPED, nick);
        else
            msg_lang(ncNickServ, nc, NICK_DROPPED);
        if (nick && (nc2 = FindUser(nick)))
            nc2->user->ni = nc2->user->real_ni = NULL;
        else if (!nick)
            nc->user->ni = nc->user->real_ni = NULL;
    }
}

/*************************************************************************/

static void do_set(struct NetClient *nc)
{
    char *cmd    = strtok(NULL, " ");
    char *param  = strtok(NULL, " ");
    struct NickInfo *ni;
    int is_servadmin = is_services_admin(nc);
    int set_nick = 0;

    if (readonly) {
        msg_lang(ncNickServ, nc, NICK_SET_DISABLED);
        return;
    }

    if (is_servadmin && cmd && (ni = findnick(cmd))) {
        cmd = param;
        param = strtok(NULL, " ");
        set_nick = 1;
    } else {
        ni = nc->user->ni;
    }
    if (!param && (!cmd || (strcasecmp(cmd,"URL")!=0 && strcasecmp(cmd,"EMAIL")!=0))){
        if (is_servadmin) {
            syntax_error(ncNickServ, nc, "SET", NICK_SET_SERVADMIN_SYNTAX);
        } else {
            syntax_error(ncNickServ, nc, "SET", NICK_SET_SYNTAX);
        }
        msg_lang(ncNickServ, nc, MORE_INFO, s_NickServ, "SET");
    } else if (!ni) {
        msg_lang(ncNickServ, nc, NICK_NOT_REGISTERED);
    } else if (!is_servadmin && !nick_identified(nc)) {
        msg_lang(ncNickServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
    } else if (strcasecmp(cmd, "PASSWORD") == 0) {
        do_set_password(nc, set_nick ? ni : nc->user->real_ni, param);
    } else if (strcasecmp(cmd, "LANGUAGE") == 0) {
        do_set_language(nc, ni, param);
    } else if (strcasecmp(cmd, "URL") == 0) {
        do_set_url(nc, set_nick ? ni : nc->user->real_ni, param);
    } else if (strcasecmp(cmd, "EMAIL") == 0) {
        do_set_email(nc, set_nick ? ni : nc->user->real_ni, param);
    } else if (strcasecmp(cmd, "KILL") == 0) {
        do_set_kill(nc, ni, param);
    } else if (strcasecmp(cmd, "SECURE") == 0) {
        do_set_secure(nc, ni, param);
    } else if (strcasecmp(cmd, "PRIVATE") == 0) {
        do_set_private(nc, ni, param);
    } else if (strcasecmp(cmd, "HIDE") == 0) {
        do_set_hide(nc, ni, param);
    } else if (strcasecmp(cmd, "NOEXPIRE") == 0) {
        do_set_noexpire(nc, ni, param);
    } else {
        if (is_servadmin)
            msg_lang(ncNickServ, nc, NICK_SET_UNKNOWN_OPTION_OR_BAD_NICK,
                        strupper(cmd));
        else
            msg_lang(ncNickServ, nc, NICK_SET_UNKNOWN_OPTION, strupper(cmd));
    }
}

/*************************************************************************/

static void do_set_password(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    int len = strlen(param);

    if (NSSecureAdmins && nc->user->real_ni != ni && nick_is_services_admin(ni) && 
                                                            !is_services_root(nc)) {
        msg_lang(ncNickServ, nc, PERMISSION_DENIED);
        return;
    } else if (strcasecmp(ni->nick, param) == 0 || (StrictPasswords && len < 5)) {
        msg_lang(ncNickServ, nc, MORE_OBSCURE_PASSWORD);
        return;
    }

#ifdef USE_ENCRYPTION
    if (len > PASSMAX) {
        len = PASSMAX;
        param[len] = 0;
        msg_lang(ncNickServ, nc, PASSWORD_TRUNCATED, PASSMAX);
    }
    if (encrypt(param, len, ni->pass, PASSMAX) < 0) {
        memset(param, 0, strlen(param));
        slog(logsvc, "%s: Failed to encrypt password for %s (set)",
                s_NickServ, ni->nick);
        msg_lang(ncNickServ, nc, NICK_SET_PASSWORD_FAILED);
        return;
    }
    memset(param, 0, strlen(param));
    msg_lang(ncNickServ, nc, NICK_SET_PASSWORD_CHANGED);
#else
    if (strlen(param) > PASSMAX-1) /* -1 for null byte */
        msg_lang(ncNickServ, nc, PASSWORD_TRUNCATED, PASSMAX-1);
    strscpy(ni->pass, param, PASSMAX);
    msg_lang(ncNickServ, nc, NICK_SET_PASSWORD_CHANGED_TO, ni->pass);
#endif
    if (nc->user->real_ni != ni) {
        slog(logsvc, "%s: %s!%s@%s used SET PASSWORD as Services admin on %s",
                s_NickServ, nc->name, nc->user->username, nc->user->host, ni->nick);
        if (WallSetpass) {
            wallops(ncNickServ, "\2%s\2 used SET PASSWORD as Services admin "
                        "on \2%s\2", nc->name, ni->nick);
        }
    }
}

/*************************************************************************/

static void do_set_language(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    int langnum;

    if (param[strspn(param, "0123456789")] != 0) {  /* i.e. not a number */
        syntax_error(ncNickServ, nc, "SET LANGUAGE", NICK_SET_LANGUAGE_SYNTAX);
        return;
    }
    langnum = atoi(param)-1;
    if (langnum < 0 || langnum >= NUM_LANGS || langlist[langnum] < 0) {
        msg_lang(ncNickServ, nc, NICK_SET_LANGUAGE_UNKNOWN,
                langnum+1, s_NickServ);
        return;
    }
    ni->language = langlist[langnum];
    msg_lang(ncNickServ, nc, NICK_SET_LANGUAGE_CHANGED);
}

/*************************************************************************/

static void do_set_url(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    if (ni->url)
        free(ni->url);
    if (param) {
        ni->url = sstrdup(param);
        msg_lang(ncNickServ, nc, NICK_SET_URL_CHANGED, param);
    } else {
        ni->url = NULL;
        msg_lang(ncNickServ, nc, NICK_SET_URL_UNSET);
    }
}

/*************************************************************************/

static void do_set_email(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    if (ni->email)
        free(ni->email);
    if (param) {
        ni->email = sstrdup(param);
        msg_lang(ncNickServ, nc, NICK_SET_EMAIL_CHANGED, param);
    } else {
        ni->email = NULL;
        msg_lang(ncNickServ, nc, NICK_SET_EMAIL_UNSET);
    }
}

/*************************************************************************/

static void do_set_kill(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ni->flags |= NI_KILLPROTECT;
        ni->flags &= ~(NI_KILL_QUICK | NI_KILL_IMMED);
        msg_lang(ncNickServ, nc, NICK_SET_KILL_ON);
    } else if (strcasecmp(param, "QUICK") == 0) {
        ni->flags |= NI_KILLPROTECT | NI_KILL_QUICK;
        ni->flags &= ~NI_KILL_IMMED;
        msg_lang(ncNickServ, nc, NICK_SET_KILL_QUICK);
    } else if (strcasecmp(param, "IMMED") == 0) {
        if (NSAllowKillImmed) {
            ni->flags |= NI_KILLPROTECT | NI_KILL_IMMED;
            ni->flags &= ~NI_KILL_QUICK;
            msg_lang(ncNickServ, nc, NICK_SET_KILL_IMMED);
        } else {
            msg_lang(ncNickServ, nc, NICK_SET_KILL_IMMED_DISABLED);
        }
    } else if (strcasecmp(param, "OFF") == 0) {
        ni->flags &= ~(NI_KILLPROTECT | NI_KILL_QUICK | NI_KILL_IMMED);
        msg_lang(ncNickServ, nc, NICK_SET_KILL_OFF);
    } else {
        syntax_error(ncNickServ, nc, "SET KILL",
                NSAllowKillImmed ? NICK_SET_KILL_IMMED_SYNTAX
                                 : NICK_SET_KILL_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_secure(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ni->flags |= NI_SECURE;
        msg_lang(ncNickServ, nc, NICK_SET_SECURE_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ni->flags &= ~NI_SECURE;
        msg_lang(ncNickServ, nc, NICK_SET_SECURE_OFF);
    } else {
        syntax_error(ncNickServ, nc, "SET SECURE", NICK_SET_SECURE_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_private(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    if (strcasecmp(param, "ON") == 0) {
        ni->flags |= NI_PRIVATE;
        msg_lang(ncNickServ, nc, NICK_SET_PRIVATE_ON);
    } else if (strcasecmp(param, "OFF") == 0) {
        ni->flags &= ~NI_PRIVATE;
        msg_lang(ncNickServ, nc, NICK_SET_PRIVATE_OFF);
    } else {
        syntax_error(ncNickServ, nc, "SET PRIVATE", NICK_SET_PRIVATE_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_hide(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    int flag, onmsg, offmsg;

    if (strcasecmp(param, "EMAIL") == 0) {
        flag = NI_HIDE_EMAIL;
        onmsg = NICK_SET_HIDE_EMAIL_ON;
        offmsg = NICK_SET_HIDE_EMAIL_OFF;
    } else if (strcasecmp(param, "USERMASK") == 0) {
        flag = NI_HIDE_MASK;
        onmsg = NICK_SET_HIDE_MASK_ON;
        offmsg = NICK_SET_HIDE_MASK_OFF;
    } else if (strcasecmp(param, "QUIT") == 0) {
        flag = NI_HIDE_QUIT;
        onmsg = NICK_SET_HIDE_QUIT_ON;
        offmsg = NICK_SET_HIDE_QUIT_OFF;
    } else {
        syntax_error(ncNickServ, nc, "SET HIDE", NICK_SET_HIDE_SYNTAX);
        return;
    }
    param = strtok(NULL, " ");
    if (!param) {
        syntax_error(ncNickServ, nc, "SET HIDE", NICK_SET_HIDE_SYNTAX);
    } else if (strcasecmp(param, "ON") == 0) {
        ni->flags |= flag;
        msg_lang(ncNickServ, nc, onmsg, s_NickServ);
    } else if (strcasecmp(param, "OFF") == 0) {
        ni->flags &= ~flag;
        msg_lang(ncNickServ, nc, offmsg, s_NickServ);
    } else {
        syntax_error(ncNickServ, nc, "SET HIDE", NICK_SET_HIDE_SYNTAX);
    }
}

/*************************************************************************/

static void do_set_noexpire(struct NetClient *nc, struct NickInfo *ni, char *param)
{
    if (!is_services_admin(nc)) {
        msg_lang(ncNickServ, nc, PERMISSION_DENIED);
        return;
    }
    if (!param) {
        syntax_error(ncNickServ, nc, "SET NOEXPIRE", NICK_SET_NOEXPIRE_SYNTAX);
        return;
    }
    if (strcasecmp(param, "ON") == 0) {
        ni->status |= NS_NO_EXPIRE;
        msg_lang(ncNickServ, nc, NICK_SET_NOEXPIRE_ON, ni->nick);
    } else if (strcasecmp(param, "OFF") == 0) {
        ni->status &= ~NS_NO_EXPIRE;
        msg_lang(ncNickServ, nc, NICK_SET_NOEXPIRE_OFF, ni->nick);
    } else {
        syntax_error(ncNickServ, nc, "SET NOEXPIRE", NICK_SET_NOEXPIRE_SYNTAX);
    }
}

/*************************************************************************/

static void do_access(struct NetClient *nc)
{
    char *cmd = strtok(NULL, " ");
    char *mask = strtok(NULL, " ");
    struct NickInfo *ni;
    int i;
    char **access;

    if (cmd && strcasecmp(cmd, "LIST") == 0 && mask && is_services_admin(nc)
                        && (ni = findnick(mask))) {
        ni = getlink(ni);
        msg_lang(ncNickServ, nc, NICK_ACCESS_LIST_X, mask);
        mask = strtok(NULL, " ");
        for (access = ni->access, i = 0; i < ni->accesscount; access++, i++) {
            if (mask && !match_wild(mask, *access))
                continue;
            notice(ncNickServ, nc, "    %s", *access);
        }

    } else if (!cmd || ((strcasecmp(cmd,"LIST")==0) ? !!mask : !mask)) {
        syntax_error(ncNickServ, nc, "ACCESS", NICK_ACCESS_SYNTAX);

    } else if (mask && !strchr(mask, '@')) {
        msg_lang(ncNickServ, nc, BAD_USERHOST_MASK);
        msg_lang(ncNickServ, nc, MORE_INFO, s_NickServ, "ACCESS");

    } else if (!(ni = nc->user->ni)) {
        msg_lang(ncNickServ, nc, NICK_NOT_REGISTERED);

    } else if (!nick_identified(nc)) {
        msg_lang(ncNickServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);

    } else if (strcasecmp(cmd, "ADD") == 0) {
        if (ni->accesscount >= NSAccessMax) {
            msg_lang(ncNickServ, nc, NICK_ACCESS_REACHED_LIMIT, NSAccessMax);
            return;
        }
        for (access = ni->access, i = 0; i < ni->accesscount; access++, i++) {
            if (strcmp(*access, mask) == 0) {
                msg_lang(ncNickServ, nc,
                        NICK_ACCESS_ALREADY_PRESENT, *access);
                return;
            }
        }
        ni->accesscount++;
        ni->access = srealloc(ni->access, sizeof(char *) * ni->accesscount);
        ni->access[ni->accesscount-1] = sstrdup(mask);
        msg_lang(ncNickServ, nc, NICK_ACCESS_ADDED, mask);

    } else if (strcasecmp(cmd, "DEL") == 0) {
        /* First try for an exact match; then, a case-insensitive one. */
        for (access = ni->access, i = 0; i < ni->accesscount; access++, i++) {
            if (strcmp(*access, mask) == 0)
                break;
        }
        if (i == ni->accesscount) {
            for (access = ni->access, i = 0; i < ni->accesscount;
                                                        access++, i++) {
                if (strcasecmp(*access, mask) == 0)
                    break;
            }
        }
        if (i == ni->accesscount) {
            msg_lang(ncNickServ, nc, NICK_ACCESS_NOT_FOUND, mask);
            return;
        }
        msg_lang(ncNickServ, nc, NICK_ACCESS_DELETED, *access);
        free(*access);
        ni->accesscount--;
        if (i < ni->accesscount)        /* if it wasn't the last entry... */
            memmove(access, access+1, (ni->accesscount-i) * sizeof(char *));
        if (ni->accesscount)                /* if there are any entries left... */
            ni->access = srealloc(ni->access, ni->accesscount * sizeof(char *));
        else {
            free(ni->access);
            ni->access = NULL;
        }

    } else if (strcasecmp(cmd, "LIST") == 0) {
        msg_lang(ncNickServ, nc, NICK_ACCESS_LIST);
        for (access = ni->access, i = 0; i < ni->accesscount; access++, i++) {
            if (mask && !match_wild(mask, *access))
                continue;
            notice(ncNickServ, nc, "    %s", *access);
        }

    } else {
        syntax_error(ncNickServ, nc, "ACCESS", NICK_ACCESS_SYNTAX);

    }
}

/*************************************************************************/

static void do_link(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    struct NickInfo *ni = nc->user->real_ni, *target;
    int res;

    if (NSDisableLinkCommand) {
        msg_lang(ncNickServ, nc, NICK_LINK_DISABLED);
        return;
    }

    if (!pass) {
        syntax_error(ncNickServ, nc, "LINK", NICK_LINK_SYNTAX);

    } else if (!ni) {
        msg_lang(ncNickServ, nc, NICK_NOT_REGISTERED);

    } else if (!nick_identified(nc)) {
        msg_lang(ncNickServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);

    } else if (!(target = findnick(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);

    } else if (target == ni) {
        msg_lang(ncNickServ, nc, NICK_NO_LINK_SAME, nick);

    } else if (target->status & NS_VERBOTEN) {
        msg_lang(ncNickServ, nc, NICK_X_FORBIDDEN, nick);

    } else if (!(res = check_password(pass, target->pass))) {
        slog(logsvc, "%s: LINK: bad password for %s by %s!%s@%s",
                s_NickServ, nick, nc->name, nc->user->username, nc->user->host);
        msg_lang(ncNickServ, nc, PASSWORD_INCORRECT);
        bad_password(nc);

    } else if (res == -1) {
        msg_lang(ncNickServ, nc, NICK_LINK_FAILED);

    } else {
        struct NickInfo *tmp;

        /* Make sure they're not trying to make a circular link */
        for (tmp = target; tmp; tmp = tmp->link) {
            if (tmp == ni) {
                msg_lang(ncNickServ, nc, NICK_LINK_CIRCULAR, nick);
                return;
            }
        }

        /* If this nick already has a link, break it */
        if (ni->link)
            delink(ni);

        ni->link = target;
        target->linkcount++;
        do {
            target->channelcount += ni->channelcount;
            if (target->link)
                target = target->link;
        } while (target->link);
        if (ni->access) {
            int i;
            for (i = 0; i < ni->accesscount; i++) {
                if (ni->access[i])
                    free(ni->access[i]);
            }
            free(ni->access);
            ni->access = NULL;
            ni->accesscount = 0;
        }
        if (ni->memos.memos) {
            int i, num;
            struct Memo *memo;
            if (target->memos.memos) {
                num = 0;
                for (i = 0; i < target->memos.memocount; i++) {
                    if (target->memos.memos[i].number > num)
                        num = target->memos.memos[i].number;
                }
                num++;
                target->memos.memos = srealloc(target->memos.memos,
                        sizeof(struct Memo) * (ni->memos.memocount +
                                        target->memos.memocount));
            } else {
                num = 1;
                target->memos.memos = smalloc(sizeof(struct Memo)*ni->memos.memocount);
                target->memos.memocount = 0;
            }
            memo = target->memos.memos + target->memos.memocount;
            for (i = 0; i < ni->memos.memocount; i++, memo++) {
                *memo = ni->memos.memos[i];
                memo->number = num++;
            }
            target->memos.memocount += ni->memos.memocount;
            ni->memos.memocount = 0;
            free(ni->memos.memos);
            ni->memos.memos = NULL;
            ni->memos.memocount = 0;
        }
        nc->user->ni = target;
        msg_lang(ncNickServ, nc, NICK_LINKED, nick);
        /* They gave the password, so they might as well have IDENTIFY'd */
        target->status |= NS_IDENTIFIED;
    }
}

/*************************************************************************/

static void do_unlink(struct NetClient *nc)
{
    struct NickInfo *ni;
    char *linkname;
    char *nick = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    int res = 0;

    if (nick) {
        int is_servadmin = is_services_admin(nc);
        ni = findnick(nick);
        if (!ni) {
            msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);
        } else if (!ni->link) {
            msg_lang(ncNickServ, nc, NICK_X_NOT_LINKED, nick);
        } else if (!is_servadmin && !pass) {
            syntax_error(ncNickServ, nc, "UNLINK", NICK_UNLINK_SYNTAX);
        } else if (!is_servadmin &&
                                !(res = check_password(pass, ni->pass))) {
            slog(logsvc, "%s: LINK: bad password for %s by %s!%s@%s",
                s_NickServ, nick, nc->name, nc->user->username, nc->user->host);
            msg_lang(ncNickServ, nc, PASSWORD_INCORRECT);
            bad_password(nc);
        } else if (res == -1) {
            msg_lang(ncNickServ, nc, NICK_UNLINK_FAILED);
        } else {
            linkname = ni->link->nick;
            delink(ni);
            msg_lang(ncNickServ, nc, NICK_X_UNLINKED, ni->nick, linkname);
            /* Adjust user record if user is online */
            /* FIXME: probably other cases we need to consider here */
            for (nc = firstnc(); nc; nc = nextnc()) {
                if (!IsUser(nc))
                    continue;

                if (nc->user->real_ni == ni) {
                    nc->user->ni = ni;
                    break;
                }
            }
        }
    } else {
        ni = nc->user->real_ni;
        if (!ni)
            msg_lang(ncNickServ, nc, NICK_NOT_REGISTERED);
        else if (!nick_identified(nc))
            msg_lang(ncNickServ, nc, NICK_IDENTIFY_REQUIRED, s_NickServ);
        else if (!ni->link)
            msg_lang(ncNickServ, nc, NICK_NOT_LINKED);
        else {
            linkname = ni->link->nick;
            nc->user->ni = ni;  /* Effective nick now the same as real nick */
            delink(ni);
            msg_lang(ncNickServ, nc, NICK_UNLINKED, linkname);
        }
    }
}

/*************************************************************************/

static void do_listlinks(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    char *param = strtok(NULL, " ");
    struct NickInfo *ni, *ni2;
    int count = 0, i;

    if (!nick || (param && strcasecmp(param, "ALL") != 0)) {
        syntax_error(ncNickServ, nc, "LISTLINKS", NICK_LISTLINKS_SYNTAX);

    } else if (!(ni = findnick(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);

    } else if (ni->status & NS_VERBOTEN) {
        msg_lang(ncNickServ, nc, NICK_X_FORBIDDEN, ni->nick);

    } else {
        msg_lang(ncNickServ, nc, NICK_LISTLINKS_HEADER, ni->nick);
        if (param)
            ni = getlink(ni);
        for (i = 0; i < 256; i++) {
            for (ni2 = nicklists[i]; ni2; ni2 = ni2->next) {
                if (ni2 == ni)
                    continue;
                if (param ? getlink(ni2) == ni : ni2->link == ni) {
                    notice(ncNickServ, nc, "    %s", ni2->nick);
                    count++;
                }
            }
        }
        msg_lang(ncNickServ, nc, NICK_LISTLINKS_FOOTER, count);
    }
}

/*************************************************************************/

/* Show hidden info to nick owners and sadmins when the "ALL" parameter is
 * supplied. If a nick is online, the "Last seen address" changes to "Is 
 * online from".
 * Syntax: INFO <nick> {ALL}
 * -TheShadow (13 Mar 1999)
 */

static void do_info(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    char *param = strtok(NULL, " ");
    struct NickInfo *ni, *real;
    int is_servadmin = is_services_admin(nc);

    if (!nick) {
            syntax_error(ncNickServ, nc, "INFO", NICK_INFO_SYNTAX);

    } else if (!(ni = findnick(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);

    } else if (ni->status & NS_VERBOTEN) {
        msg_lang(ncNickServ, nc, NICK_X_FORBIDDEN, nick);

    } else {
        char buf[BUFSIZE], *end;
        const char *commastr = getstring(nc->user->ni, COMMA_SPACE);
        int need_comma = 0;
        int nick_online = 0;
        int show_hidden = 0;

        /* Is the real owner of the nick we're looking up online? -TheShadow */
        if (ni->status & NS_IDENTIFIED)
            nick_online = 1;

        /* Only show hidden fields to owner and sadmins and only when the ALL
         * parameter is used. -TheShadow */
        if (param && strcasecmp(param, "ALL") == 0 && 
                        ((nick_online && (strcasecmp(nc->name, nick) == 0)) ||
                                is_services_admin(nc)))
            show_hidden = 1;

        real = getlink(ni);

        msg_lang(ncNickServ, nc, NICK_INFO_REALNAME,
                nick, ni->last_realname);

        if (nick_online) {
            if (show_hidden || !(real->flags & NI_HIDE_MASK))
                msg_lang(ncNickServ, nc, NICK_INFO_ADDRESS_ONLINE,
                        ni->last_usermask);
            else
                msg_lang(ncNickServ, nc, NICK_INFO_ADDRESS_ONLINE_NOHOST,
                        ni->nick);

        } else {
            if (show_hidden || !(real->flags & NI_HIDE_MASK))
                msg_lang(ncNickServ, nc, NICK_INFO_ADDRESS,
                        ni->last_usermask);

            strftime_lang(buf, sizeof(buf), nc->user->ni, STRFTIME_DATE_TIME_FORMAT,
                          ni->last_seen);
            msg_lang(ncNickServ, nc, NICK_INFO_LAST_SEEN, buf);
        }
        strftime_lang(buf, sizeof(buf), nc->user->ni, STRFTIME_DATE_TIME_FORMAT,
                      ni->time_registered);
        msg_lang(ncNickServ, nc, NICK_INFO_TIME_REGGED, buf);
        if (ni->last_quit && (show_hidden || !(real->flags & NI_HIDE_QUIT)))
            msg_lang(ncNickServ, nc, NICK_INFO_LAST_QUIT, ni->last_quit);
        if (ni->url)
            msg_lang(ncNickServ, nc, NICK_INFO_URL, ni->url);
        if (ni->email && (show_hidden || !(real->flags & NI_HIDE_EMAIL)))
            msg_lang(ncNickServ, nc, NICK_INFO_EMAIL, ni->email);
        *buf = 0;
        end = buf;
        if (real->flags & NI_KILLPROTECT) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s",
                        getstring(nc->user->ni, NICK_INFO_OPT_KILL));
            need_comma = 1;
        }
        if (real->flags & NI_SECURE) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, NICK_INFO_OPT_SECURE));
            need_comma = 1;
        }
        if (real->flags & NI_PRIVATE) {
            end += snprintf(end, sizeof(buf)-(end-buf), "%s%s",
                        need_comma ? commastr : "",
                        getstring(nc->user->ni, NICK_INFO_OPT_PRIVATE));
            need_comma = 1;
        }
        msg_lang(ncNickServ, nc, NICK_INFO_OPTIONS,
                *buf ? buf : getstring(nc->user->ni, NICK_INFO_OPT_NONE));
        if ((ni->status & NS_NO_EXPIRE) && (real == nc->user->ni || is_servadmin))
            msg_lang(ncNickServ, nc, NICK_INFO_NO_EXPIRE);

    }
}

/*************************************************************************/

/* SADMINS can search for nicks based on their NS_VERBOTEN and NS_NO_EXPIRE
 * status. The keywords FORBIDDEN and NOEXPIRE represent these two states
 * respectively. These keywords should be included after the search pattern.
 * Multiple keywords are accepted and should be separated by spaces. Only one
 * of the keywords needs to match a nick's state for the nick to be displayed.
 * Forbidden nicks can be identified by "[Forbidden]" appearing in the last
 * seen address field. Nicks with NOEXPIRE set are preceeded by a "!". Only
 * SADMINS will be shown forbidden nicks and the "!" indicator.
 * Syntax for sadmins: LIST pattern [FORBIDDEN] [NOEXPIRE]
 * -TheShadow
 */

static void do_list(struct NetClient *nc)
{
    char *pattern = strtok(NULL, " ");
    char *keyword;
    struct NickInfo *ni;
    int nnicks, i;
    char buf[BUFSIZE];
    int is_servadmin = is_services_admin(nc);
    int16_t matchflags = 0; /* NS_ flags a nick must match one of to qualify */

    if (NSListOpersOnly && !(nc->user->modes & UMODE_o)) {
        msg_lang(ncNickServ, nc, PERMISSION_DENIED);
        return;
    }

    if (!pattern) {
        syntax_error(ncNickServ, nc, "LIST",
                is_servadmin ? NICK_LIST_SERVADMIN_SYNTAX : NICK_LIST_SYNTAX);
    } else {
        nnicks = 0;

        while (is_servadmin && (keyword = strtok(NULL, " "))) {
            if (strcasecmp(keyword, "FORBIDDEN") == 0)
                matchflags |= NS_VERBOTEN;
            if (strcasecmp(keyword, "NOEXPIRE") == 0)
                matchflags |= NS_NO_EXPIRE;
        }

        msg_lang(ncNickServ, nc, NICK_LIST_HEADER, pattern);
        for (i = 0; i < 256; i++) {
            for (ni = nicklists[i]; ni; ni = ni->next) {
                if (!is_servadmin && ((ni->flags & NI_PRIVATE)
                                                || (ni->status & NS_VERBOTEN)))
                    continue;
                if ((matchflags != 0) && !(ni->status & matchflags))
                    continue;

                /* We no longer compare the pattern against the output buffer.
                 * Instead we build a nice nick!user@host buffer to compare.
                 * The output is then generated separately. -TheShadow */
                snprintf(buf, sizeof(buf), "%s!%s", ni->nick,
                                ni->last_usermask ? ni->last_usermask : "*@*");
                if (strcasecmp(pattern, ni->nick) == 0 ||
                                        match_wild_nocase(pattern, buf)) {
                    if (++nnicks <= NSListMax) {
                        char noexpire_char = ' ';
                        if (is_servadmin && (ni->status & NS_NO_EXPIRE))
                            noexpire_char = '!';
                        if (!is_servadmin && (ni->flags & NI_HIDE_MASK)) {
                            snprintf(buf, sizeof(buf), "%-20s  [Hidden]",
                                                ni->nick);
                        } else if (ni->status & NS_VERBOTEN) {
                            snprintf(buf, sizeof(buf), "%-20s  [Forbidden]",
                                                ni->nick);
                        } else {
                            snprintf(buf, sizeof(buf), "%-20s  %s",
                                                ni->nick, ni->last_usermask);
                        }
                        notice(ncNickServ, nc, "   %c%s",
                                                noexpire_char, buf);
                    }
                }
            }
        }
        msg_lang(ncNickServ, nc, NICK_LIST_RESULTS,
                        nnicks>NSListMax ? NSListMax : nnicks, nnicks);
    }
}

/*************************************************************************/

static void do_recover(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    struct NickInfo *ni;
    struct NetClient *nc2;

    if (!nick) {
        syntax_error(ncNickServ, nc, "RECOVER", NICK_RECOVER_SYNTAX);
    } else if (!(nc2 = FindUser(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_IN_USE, nick);
    } else if (!(ni = nc2->user->real_ni)) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);
    } else if (strcasecmp(nick, nc->name) == 0) {
        msg_lang(ncNickServ, nc, NICK_NO_RECOVER_SELF);
    } else if (pass) {
        int res = check_password(pass, ni->pass);
        if (res == 1) {
            collide(ni, 0);
            msg_lang(ncNickServ, nc, NICK_RECOVERED, s_NickServ, nick);
        } else {
            msg_lang(ncNickServ, nc, ACCESS_DENIED);
            if (res == 0) {
                slog(logsvc, "%s: RECOVER: invalid password for %s by %s!%s@%s",
                        s_NickServ, nick, nc->name, nc->user->username, nc->user->host);
                bad_password(nc);
            }
        }
    } else {
        if (!(ni->flags & NI_SECURE) && is_on_access(nc, ni)) {
            collide(ni, 0);
            msg_lang(ncNickServ, nc, NICK_RECOVERED, s_NickServ, nick);
        } else {
            msg_lang(ncNickServ, nc, ACCESS_DENIED);
        }
    }
}

/*************************************************************************/

static void do_release(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    struct NickInfo *ni;

    if (!nick) {
        syntax_error(ncNickServ, nc, "RELEASE", NICK_RELEASE_SYNTAX);
    } else if (!(ni = findnick(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);
    } else if (!(ni->status & NS_KILL_HELD)) {
        msg_lang(ncNickServ, nc, NICK_RELEASE_NOT_HELD, nick);
    } else if (pass) {
        int res = check_password(pass, ni->pass);
        if (res == 1) {
            release(ni, 0);
            msg_lang(ncNickServ, nc, NICK_RELEASED);
        } else {
            msg_lang(ncNickServ, nc, ACCESS_DENIED);
            if (res == 0) {
                slog(logsvc, "%s: RELEASE: invalid password for %s by %s!%s@%s",
                        s_NickServ, nick, nc->name, nc->user->username, nc->user->host);
                bad_password(nc);
            }
        }
    } else {
        if (!(ni->flags & NI_SECURE) && is_on_access(nc, ni)) {
            release(ni, 0);
            msg_lang(ncNickServ, nc, NICK_RELEASED);
        } else {
            msg_lang(ncNickServ, nc, ACCESS_DENIED);
        }
    }
}

/*************************************************************************/

static void do_ghost(struct NetClient *nc)
{
    char *nick = strtok(NULL, " ");
    char *pass = strtok(NULL, " ");
    struct NickInfo *ni;
    struct NetClient *nc2;

    if (!nick) {
        syntax_error(ncNickServ, nc, "GHOST", NICK_GHOST_SYNTAX);
    } else if (!(nc2 = FindUser(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_IN_USE, nick);
    } else if (!(ni = nc2->user->real_ni)) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);
    } else if (strcasecmp(nick, nc->name) == 0) {
        msg_lang(ncNickServ, nc, NICK_NO_GHOST_SELF);
    } else if (pass) {
        int res = check_password(pass, ni->pass);
        if (res == 1) {
            char buf[NICKMAX+32];
            snprintf(buf, sizeof(buf), "GHOST command used by %s", nc->name);
            kill_user(ncNickServ, nc2, buf);
            msg_lang(ncNickServ, nc, NICK_GHOST_KILLED, nick);
        } else {
            msg_lang(ncNickServ, nc, ACCESS_DENIED);
            if (res == 0) {
                slog(logsvc, "%s: RELEASE: invalid password for %s by %s!%s@%s",
                        s_NickServ, nick, nc->name, nc->user->username, nc->user->host);
                bad_password(nc);
            }
        }
    } else {
        if (!(ni->flags & NI_SECURE) && is_on_access(nc, ni)) {
            char buf[NICKMAX+32];
            snprintf(buf, sizeof(buf), "GHOST command used by %s", nc->name);
            kill_user(ncNickServ, nc2, buf);
            msg_lang(ncNickServ, nc, NICK_GHOST_KILLED, nick);
        } else {
            msg_lang(ncNickServ, nc, ACCESS_DENIED);
        }
    }
}

/*************************************************************************/

static void do_status(struct NetClient *nc)
{
    char *nick;
    struct NetClient *nc2;
    int i = 0;

    while ((nick = strtok(NULL, " ")) && (i++ < 16)) {
        if (!(nc2 = FindUser(nick)))
            notice(ncNickServ, nc, "STATUS %s 0", nick);
        else if (nick_identified(nc2))
            notice(ncNickServ, nc, "STATUS %s 3", nick);
        else if (nick_recognized(nc2))
            notice(ncNickServ, nc, "STATUS %s 2", nick);
        else
            notice(ncNickServ, nc, "STATUS %s 1", nick);
    }
}

/*************************************************************************/

static void do_getpass(struct NetClient *nc)
{
#ifndef USE_ENCRYPTION
    char *nick = strtok(NULL, " ");
    struct NickInfo *ni;
#endif

    /* Assumes that permission checking has already been done. */
#ifdef USE_ENCRYPTION
    msg_lang(ncNickServ, nc, NICK_GETPASS_UNAVAILABLE);
#else
    if (!nick) {
        syntax_error(ncNickServ, nc, "GETPASS", NICK_GETPASS_SYNTAX);
    } else if (!(ni = findnick(nick))) {
        msg_lang(ncNickServ, nc, NICK_X_NOT_REGISTERED, nick);
    } else if (NSSecureAdmins && nick_is_services_admin(ni) && 
                                                            !is_services_root(nc)) {
        msg_lang(ncNickServ, nc, PERMISSION_DENIED);
    } else {
        slog(logsvc, "%s: %s!%s@%s used GETPASS on %s",
                s_NickServ, nc->name, nc->user->username, nc->user->host, nick);
        if (WallGetpass)
            wallops(ncNickServ, "\2%s\2 used GETPASS on \2%s\2", nc->name, nick);
        msg_lang(ncNickServ, nc, NICK_GETPASS_PASSWORD_IS, nick, ni->pass);
    }
#endif
}

/*************************************************************************/

static void do_forbid(struct NetClient *nc)
{
    struct NickInfo *ni;
    char *nick = strtok(NULL, " ");

    /* Assumes that permission checking has already been done. */
    if (!nick) {
        syntax_error(ncNickServ, nc, "FORBID", NICK_FORBID_SYNTAX);
        return;
    }
    if (readonly)
        msg_lang(ncNickServ, nc, READ_ONLY_MODE);
    if ((ni = findnick(nick)) != NULL)
        delnick(ni);
    ni = makenick(nick);
    if (ni) {
        ni->status |= NS_VERBOTEN;
        slog(logsvc, "%s: %s set FORBID for nick %s", s_NickServ, nc->name, nick);
        msg_lang(ncNickServ, nc, NICK_FORBID_SUCCEEDED, nick);
    } else {
        slog(logsvc, "%s: Valid FORBID for %s by %s failed", s_NickServ,
                nick, nc->name);
        msg_lang(ncNickServ, nc, NICK_FORBID_FAILED, nick);
    }
}

/*************************************************************************/
