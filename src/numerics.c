/*
 * ServicesIRCh - Services for IRCh, numerics.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * Modulo Undernet P10 hecho por zoltan <zolty@ctv.es>
 * 23-09-2000
 *
 * Adaptado para P10 real por el mismo autor zoltan <toni@tonigarcia.es>
 *
 * El codigo ha sido tomado del ircu de Undernet
 * Web para pillar ircus de undernet <http://coder-com.undernet.org>
*/
#include "sysconf.h"
#include "numerics.h"

#include "config.h"
#include "log.h"
#include "netclient.h"
#include "network.h"
#include "servers.h"
#include "svc_memory.h"
#include "users.h"

#include <assert.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>

static int numerics_ext = 0;

/** Number of bits encoded in one numnick character. */
#define NUMNICKLOG 6
#define NUMNICKBASE 64           /* (2 << NUMNICKLOG) */
/** Bitmask to select value of next numnick character. */
#define NUMNICKMASK 63           /* (NUMNICKBASE-1) */
/** Number of servers representable in a numnick. */
#define NN_MAX_SERVER 4096       /* (NUMNICKBASE * NUMNICKBASE) */
/** Number of clients representable in a numnick. */
#define NN_MAX_CLIENT_SHORT 4096 /* (NUMNICKBASE * NUMNICKBASE) */
#define NN_MAX_CLIENT_EXT 262144 /* NUMNICKBASE ^ 3 */

/** Array of servers indexed by numnick. */
static struct NetClient *server_list[NN_MAX_SERVER];
/** Maximum used server numnick, plus one. */
static unsigned int last_numeric = 0;

/**
 * Converts a numeric to the corresponding character.
 * The following characters are currently known to be forbidden:
 *
 * '\\0' : Because we use '\\0' as end of line.
 *
 * ' '  : Because parse_*() uses this as parameter separator.
 *
 * ':'  : Because parse_server() uses this to detect if a prefix is a
 *        numeric or a name.
 *
 * '+'  : Because m_nick() uses this to determine if parv[6] is a
 *        umode or not.
 *
 * '&', '#', '$', '@' and '%' :
 *        Because m_message() matches these characters to detect special cases.
 */
static const char convert2y[] = {
  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
  'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
  'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
  'w','x','y','z','0','1','2','3','4','5','6','7','8','9','[',']'
};

/** Converts a character to its (base64) numnick value. */
static const unsigned int convert2n[] = {
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  52,53,54,55,56,57,58,59,60,61, 0, 0, 0, 0, 0, 0,
   0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
  15,16,17,18,19,20,21,22,23,24,25,62, 0,63, 0, 0,
   0,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
  41,42,43,44,45,46,47,48,49,50,51, 0, 0, 0, 0, 0,

   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/** Convert a string to its value as a numnick.
 * @param[in] s Numnick string to decode.
 * @return %Numeric nickname value.
 */
unsigned int base64toint(const char* s)
{
  unsigned int i = convert2n[(unsigned char) *s++];
  while (*s) {
    i <<= NUMNICKLOG;
    i += convert2n[(unsigned char) *s++];
  }
  return i;
}

/** Encode a number as a numnick.
 * @param[out] buf Output buffer.
 * @param[in] v Value to encode.
 * @param[in] count Number of numnick digits to write to \a buf.
 */
const char* inttobase64(char* buf, unsigned int v, unsigned int count)
{
  buf[count] = '\0';
  while (count > 0) {
    buf[--count] = convert2y[(v & NUMNICKMASK)];
    v >>= NUMNICKLOG;
  }
  return buf;
}

/** Look up a server by numnick string.
 * See @ref numnicks for more details.
 * @param[in] numeric %Numeric nickname of server (may contain trailing junk).
 * @return %Server with that numnick (or NULL).
 */
static struct NetClient* FindXNServer(const char* numeric)
{
    char buf[3];
    buf[0] = *numeric++;
    buf[1] = *numeric;
    buf[2] = '\0';
    Debug((3, "FindXNServer: %s(%d)", buf, base64toint(buf)));
    return server_list[base64toint(buf)];
}

/** Look up a server by numnick string.
 * See @ref numnicks for more details.
 * @param[in] numeric %Numeric nickname of server.
 * @return %Server with that numnick (or NULL).
 */
struct NetClient* FindNServer(const char* numeric)
{
    unsigned int len = strlen(numeric);

    if (len < 3) {
        Debug((3, "FindNServer: %s(%d)", numeric, base64toint(numeric)));
        return server_list[base64toint(numeric)];
    }
    else if (len == 3) {
        Debug((3, "FindNServer: %c(%d)", *numeric,
               convert2n[(unsigned char) *numeric]));
        return server_list[convert2n[(unsigned char) *numeric]];
    }
    return FindXNServer(numeric);
}

/** Look up a user by numnick string.
 * See @ref numnicks for more details.
 * @param[in] yxx %Numeric nickname of user.
 * @return %User with that numnick (or NULL).
 */
struct NetClient* FindNUser(const char* yxx)
{
    struct NetClient* server = 0;
    if (5 == strlen(yxx)) {
        if (0 != (server = FindXNServer(yxx))) {
            Debug((3, "findNUser: %s(%d)", yxx,
                  base64toint(yxx + 2) & server->serv->nn_mask));
            return server->serv->client_list[base64toint(yxx + 2) & server->serv->nn_mask];
        }
   }
   else if (0 != (server = FindNServer(yxx))) {
       Debug((3, "findNUser: %s(%d)",
              yxx, base64toint(yxx + 1) & server->serv->nn_mask));
       return server->serv->client_list[base64toint(yxx + 1) & server->serv->nn_mask];
   }
   return 0;
}

/** Set a server's numeric and capacity.
 * See @ref numnicks for more details.
 * @param[in] c %Server that is being assigned a numnick.
 * @param[in] numeric Numnick value for server.
 * @param[in] capacity Maximum number of clients the server supports.
 */
void SetYXXMyServer(struct NetClient* nc, unsigned int numeric, unsigned int capacity)
{
    unsigned int max_clients = 16;
    unsigned int nn_max_client;

    assert(0 != nc);
    assert(numeric < NN_MAX_SERVER);

    if (numeric >= NUMNICKBASE) {
        inttobase64(nc->yxx, numeric, 2);
        nn_max_client = NN_MAX_CLIENT_EXT;
        numerics_ext = 1;
    } else {
        nc->yxx[0] = convert2y[numeric];
        nn_max_client = NN_MAX_CLIENT_SHORT;
        numerics_ext = 0;
    }

    if (numeric >= last_numeric)
        last_numeric = numeric + 1;
    server_list[numeric] = nc;

    /*
     * Calculate mask to be used for the maximum number of clients
     */
    while (max_clients < capacity)
        max_clients <<= 1;

    /*
     * Sanity checks
     */
    if (max_clients > nn_max_client)
        max_clients = nn_max_client;

    --max_clients;
    if (numerics_ext)
        inttobase64(nc->serv->nn_capacity, max_clients, 3);
    else
        inttobase64(nc->serv->nn_capacity, max_clients, 2);
    nc->serv->nn_mask = max_clients;       /* Our Numeric Nick mask */
    nc->serv->client_list = scalloc(max_clients + 1, sizeof(struct NetClient*));
    server_list[base64toint(nc->yxx)] = nc;
}

/** Set a server's numeric nick.
 * @param[in,out] server %Server that is being assigned a numnick.
 * @param[in] yxx %Numeric nickname for server.
 */
void SetServerYXX(struct NetClient *server, const char *yxx)
{
    unsigned int index;

    if (strlen(yxx) == 5) {
        strncpy(server->yxx, yxx, 2);
        strncpy(server->serv->nn_capacity, yxx + 2, 3);
    } else {
        server->yxx[0]               = yxx[0];
        server->serv->nn_capacity[0] = yxx[1];
        server->serv->nn_capacity[1] = yxx[2];
    }
    server->serv->nn_mask = base64toint(server->serv->nn_capacity);

    index = base64toint(server->yxx);
    if (index >= last_numeric)
        last_numeric = index + 1;
    server_list[index] = server;

    server->serv->client_list = scalloc(server->serv->nn_mask + 1, sizeof(struct NetClient *));
}

/** Unassign a server's numnick.
 * @param[in] server %Server that should be removed from the numnick table.
 */
void ClearServerYXX(const struct NetClient *server)
{
    unsigned int index = base64toint(server->yxx);
    if (server_list[index] == server)     /* Sanity check */
        server_list[index] = 0;
}

/** Register numeric of new (local) client.
 * See @ref numnicks for more details.
 * Assign a numnick and add it to our client_list.
 * @param[in] cptr %User being registered.
 */
int SetLocalNumNick(struct NetClient *nc)
{
    static unsigned int last_nn  = 0;
    struct NetClient **client_list      = (&myService)->serv->client_list;
    unsigned int mask            = (&myService)->serv->nn_mask;
    unsigned int count           = 0;
    int max_client               = NN_MAX_CLIENT_SHORT;

//    assert(netclient->user->serv == &myService);

    if (numerics_ext)
        max_client = NN_MAX_CLIENT_EXT;

    while (client_list[last_nn & mask]) {
        if (++count == max_client) {
            assert(count < max_client);
            return 0;
        }
        if (++last_nn == max_client)
            last_nn = 0;
    }
    /* Reserva del numerico */
    client_list[last_nn & mask] = nc;

    if (numerics_ext)
        inttobase64(nc->yxx, last_nn, 3);
    else
        inttobase64(nc->yxx, last_nn, 2);

    if (++last_nn == max_client)
        last_nn = 0;
    return 1;
}

/** Register numeric of new (remote) client.
 * See @ref numnicks for more details.
 * Add it to the appropriate client_list.
 * @param[in] acptr %User being registered.
 * @param[in] yxx User's numnick.
 */
void SetRemoteNumNick(struct NetClient* nc, const char *yxx)
{
    struct NetClient **arclient;
    struct NetClient *server = nc->user->server;

    if (strlen(yxx) == 5) {
        strcpy(nc->yxx, yxx + 2);
    } else {
       nc->yxx[0] = *++yxx;
       nc->yxx[1] = *++yxx;
       nc->yxx[2] = 0;
    }

    Debug((3, "SetRemoteNumNick: %s(%d)", nc->yxx,
          base64toint(nc->yxx) & server->serv->nn_mask));

    arclient = &(server)->serv->client_list[base64toint(nc->yxx) & server->serv->nn_mask];
    if (*arclient) {
        ; /** @todo Expulsion del nick por numeric nick collision por ghost */
    }
    *arclient = nc;
}

/** Remove a client from a server's user array.
 * @param[in] server %Server that owns the user to remove.
 * @param[in] yxx Numnick of client to remove.
 */
void RemoveYXXClient(struct NetClient* server, const char* yxx)
{
    assert(0 != server);
    assert(0 != yxx);

    if (*yxx) {
        Debug((3, "RemoveYXXClient: %s(%d)", yxx,
               base64toint(yxx) & server->serv->nn_mask));
        server->serv->client_list[base64toint(yxx) & server->serv->nn_mask] = 0;
    }
}


/** Encode an IP address in the base64 used by numnicks.
 * For IPv4 addresses (including IPv4-mapped and IPv4-compatible IPv6
 * addresses), the 32-bit host address is encoded directly as six
 * characters.
 *
 * For IPv6 addresses, each 16-bit address segment is encoded as three
 * characters, but the longest run of zero segments is encoded using an
 * underscore.
 * @param[out] buf Output buffer to write to.
 * @param[in] addr IP address to encode.
 * @param[in] count Number of bytes writable to \a buf.
 * @param[in] v6_ok If non-zero, peer understands base-64 encoded IPv6 addresses.
 */
const char* iptobase64(char* buf, const struct irc_in_addr* addr, unsigned int count, int v6_ok)
{
  if (irc_in_addr_is_ipv4(addr)) {
    assert(count >= 6);
    inttobase64(buf, (ntohs(addr->in6_16[6]) << 16) | ntohs(addr->in6_16[7]), 6);
  } else if (!v6_ok) {
    assert(count >= 6);
    if (addr->in6_16[0] == htons(0x2002))
        inttobase64(buf, (ntohs(addr->in6_16[1]) << 16) | ntohs(addr->in6_16[2]), 6);
    else
        strcpy(buf, "AAAAAA");
  } else {
    unsigned int max_start, max_zeros, curr_zeros, zero, ii;
    char *output = buf;

    assert(count >= 25);
    /* Can start by printing out the leading non-zero parts. */
    for (ii = 0; (addr->in6_16[ii]) && (ii < 8); ++ii) {
      inttobase64(output, ntohs(addr->in6_16[ii]), 3);
      output += 3;
    }
    /* Find the longest run of zeros. */
    for (max_start = zero = ii, max_zeros = curr_zeros = 0; ii < 8; ++ii) {
      if (!addr->in6_16[ii])
        curr_zeros++;
      else if (curr_zeros > max_zeros) {
        max_start = ii - curr_zeros;
        max_zeros = curr_zeros;
        curr_zeros = 0;
      }
    }
    if (curr_zeros > max_zeros) {
      max_start = ii - curr_zeros;
      max_zeros = curr_zeros;
      curr_zeros = 0;
    }
    /* Print the rest of the address */
    for (ii = zero; ii < 8; ) {
      if ((ii == max_start) && max_zeros) {
        *output++ = '_';
        ii += max_zeros;
      } else {
        inttobase64(output, ntohs(addr->in6_16[ii]), 3);
        output += 3;
        ii++;
      }
    }
    *output = '\0';
  }
  return buf;
}

/** Decode an IP address from base64.
 * @param[in] input Input buffer to decode.
 * @param[out] addr IP address structure to populate.
 */
void base64toip(const char* input, struct irc_in_addr* addr)
{
  memset(addr, 0, sizeof(*addr));
  if (strlen(input) == 6) {
    unsigned int in = base64toint(input);
    /* An all-zero address should stay that way. */
    if (in) {
      addr->in6_16[5] = htons(65535);
      addr->in6_16[6] = htons(in >> 16);
      addr->in6_16[7] = htons(in & 65535);
    }
  } else {
    unsigned int pos = 0;
    do {
      if (*input == '_') {
        unsigned int left;
        for (left = (25 - strlen(input)) / 3 - pos; left; left--)
          addr->in6_16[pos++] = 0;
        input++;
      } else {
        unsigned short accum = convert2n[(unsigned char)*input++];
        accum = (accum << NUMNICKLOG) | convert2n[(unsigned char)*input++];
        accum = (accum << NUMNICKLOG) | convert2n[(unsigned char)*input++];
        addr->in6_16[pos++] = ntohs(accum);
      }
    } while (pos < 8);
  }
}
