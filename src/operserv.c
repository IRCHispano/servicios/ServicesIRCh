/*
 * ServicesIRCh - Services for IRCh, operserv.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2009-2016 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "operserv.h"

#include "actions.h"
#include "akill.h"
#include "channels.h"
#include "chanserv.h"
#include "commands.h"
#include "hash.h"
#include "language.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "newsserv.h"
#include "nickserv.h"
#include "send.h"
#include "service.h"
#include "services.h"
#include "stats.h"
#include "svc_memory.h"
#include "users.h"

#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/* provisional */
#include "datafiles.h"
#include "process.h"
#include "sockutil.h"

/*************************************************************************/

/* Services admin list */
static struct NickInfo *services_admins[MAX_SERVADMINS];

/* Services operator list */
static struct NickInfo *services_opers[MAX_SERVOPERS];


struct clone {
    char *host;
    long time;
};

/* List of most recent users - statically initialized to zeros */
static struct clone clonelist[CLONE_DETECT_SIZE];

/* Which hosts have we warned about, and when?  This is used to keep us
 * from sending out notices over and over for clones from the same host. */
static struct clone warnings[CLONE_DETECT_SIZE];

/*************************************************************************/

struct NetClient *ncOperServ;
struct NetClient *ncDevNull;

static void do_help(struct NetClient *nc);
static void do_global(struct NetClient *nc);
static void do_stats(struct NetClient *nc);
static void do_admin(struct NetClient *nc);
static void do_oper(struct NetClient *nc);
static void do_os_mode(struct NetClient *nc);
static void do_clearmodes(struct NetClient *nc);
static void do_os_kick(struct NetClient *nc);
static void do_set(struct NetClient *nc);
static void do_jupe(struct NetClient *nc);
static void do_raw(struct NetClient *nc);
static void do_update(struct NetClient *nc);
static void do_os_quit(struct NetClient *nc);
static void do_shutdown(struct NetClient *nc);
static void do_restart(struct NetClient *nc);
static void do_listignore(struct NetClient *nc);
static void do_killclones(struct NetClient *nc);

#ifdef DEBUG_COMMANDS
static void send_clone_lists(struct NetClient *nc);
static void do_matchwild(struct NetClient *nc);
#endif

/*************************************************************************/

static struct Command cmds[] = {
    { "HELP",       do_help,       NULL,  -1,                   -1,-1,-1,-1 },
    { "GLOBAL",     do_global,     NULL,  OPER_HELP_GLOBAL,     -1,-1,-1,-1 },
    { "STATS",      do_stats,      NULL,  OPER_HELP_STATS,      -1,-1,-1,-1 },
    { "UPTIME",     do_stats,      NULL,  OPER_HELP_STATS,      -1,-1,-1,-1 },

    /* Anyone can use the LIST option to the ADMIN and OPER commands; those
     * routines check privileges to ensure that only authorized users
     * modify the list. */
    { "ADMIN",      do_admin,      NULL,  OPER_HELP_ADMIN,      -1,-1,-1,-1 },
    { "OPER",       do_oper,       NULL,  OPER_HELP_OPER,       -1,-1,-1,-1 },
    /* Similarly, anyone can use *NEWS LIST, but *NEWS {ADD,DEL} are
     * reserved for Services admins. */
    { "LOGONNEWS",  do_logonnews,  NULL,  NEWS_HELP_LOGON,      -1,-1,-1,-1 },
    { "OPERNEWS",   do_opernews,   NULL,  NEWS_HELP_OPER,       -1,-1,-1,-1 },

    /* Commands for Services opers: */
    { "MODE",       do_os_mode,    is_services_oper,
        OPER_HELP_MODE, -1,-1,-1,-1 },
    { "CLEARMODES", do_clearmodes, is_services_oper,
        OPER_HELP_CLEARMODES, -1,-1,-1,-1 },
    { "KICK",       do_os_kick,    is_services_oper,
        OPER_HELP_KICK, -1,-1,-1,-1 },
    { "AKILL",      do_akill,      is_services_oper,
        OPER_HELP_AKILL, -1,-1,-1,-1 },

    /* Commands for Services admins: */
    { "SET",        do_set,        is_services_admin,
        OPER_HELP_SET, -1,-1,-1,-1 },
    { "SET READONLY",0,0,  OPER_HELP_SET_READONLY, -1,-1,-1,-1 },
    { "SET DEBUG",0,0,     OPER_HELP_SET_DEBUG, -1,-1,-1,-1 },
    { "JUPE",       do_jupe,       is_services_admin,
        OPER_HELP_JUPE, -1,-1,-1,-1 },
    { "RAW",        do_raw,        is_services_admin,
        OPER_HELP_RAW, -1,-1,-1,-1 },
    { "UPDATE",     do_update,     is_services_admin,
        OPER_HELP_UPDATE, -1,-1,-1,-1 },
    { "QUIT",       do_os_quit,    is_services_admin,
        OPER_HELP_QUIT, -1,-1,-1,-1 },
    { "SHUTDOWN",   do_shutdown,   is_services_admin,
        OPER_HELP_SHUTDOWN, -1,-1,-1,-1 },
    { "RESTART",    do_restart,    is_services_admin,
        OPER_HELP_RESTART, -1,-1,-1,-1 },
    { "LISTIGNORE", do_listignore, is_services_admin,
        -1,-1,-1,-1, -1 },
    { "KILLCLONES", do_killclones, is_services_admin,
        OPER_HELP_KILLCLONES, -1,-1,-1, -1 },
#ifndef STREAMLINED
//    { "SESSION",    do_session,    is_services_admin,
  //      OPER_HELP_SESSION, -1,-1,-1, -1 },
//    { "EXCEPTION",  do_exception,  is_services_admin,
  //      OPER_HELP_EXCEPTION, -1,-1,-1, -1 },
#endif

    /* Commands for Services root: */

    { "ROTATELOG",  rotate_log,  is_services_root, -1,-1,-1,-1,
        OPER_HELP_ROTATELOG },

#ifdef DEBUG_COMMANDS
    { "LISTCHANS",  send_channel_list,  is_services_root, -1,-1,-1,-1,-1 },
    { "LISTCHAN",   send_channel_users, is_services_root, -1,-1,-1,-1,-1 },
    { "LISTUSERS",  send_user_list,     is_services_root, -1,-1,-1,-1,-1 },
    { "LISTUSER",   send_user_info,     is_services_root, -1,-1,-1,-1,-1 },
    { "LISTTIMERS", send_timeout_list,  is_services_root, -1,-1,-1,-1,-1 },
    { "MATCHWILD",  do_matchwild,       is_services_root, -1,-1,-1,-1,-1 },
    { "LISTCLONES", send_clone_lists,   is_services_root, -1,-1,-1,-1,-1 },
#endif

    /* Fencepost: */
    { NULL }
};

/*************************************************************************/
/*************************************************************************/

/* OperServ initialization. */

void os_init(void)
{
    struct Command *cmd;

    ncOperServ = service_create(s_OperServ, NULL, NULL, desc_OperServ);
    ncOperServ->service->has_priv = is_oper;
    ncOperServ->service->func = operserv;
    set_user_modes(&myService, ncOperServ, "+ioBd");
    service_introduce(ncOperServ);

    if (s_DevNull) {
        ncOperServ = service_create(s_DevNull, NULL, NULL, desc_DevNull);
        ncOperServ->service->has_priv = NULL;
        ncOperServ->service->func = NULL;
        set_user_modes(&myService, ncDevNull, "+Bd");
        service_introduce(ncDevNull);
    }

    cmd = lookup_cmd(cmds, "GLOBAL");
    if (cmd)
        cmd->help_param1 = s_GlobalNoticer;
    cmd = lookup_cmd(cmds, "ADMIN");
    if (cmd)
        cmd->help_param1 = s_NickServ;
    cmd = lookup_cmd(cmds, "OPER");
    if (cmd)
        cmd->help_param1 = s_NickServ;
}

/*************************************************************************/

/* Main OperServ routine. */

void operserv(struct NetClient *source, char *buf)
{
    char *cmd;
    char *s;

    slog(logsvc, "%s: %s: %s", s_OperServ, source, buf);

    cmd = strtok(buf, " ");
    if (!cmd) {
        return;
    } else if (strcasecmp(cmd, "\1PING") == 0) {
        if (!(s = strtok(NULL, "")))
            s = "\1";
        notice(ncOperServ, source, "\1PING %s", s);
    } else {
        run_cmd(ncOperServ, source, cmds, cmd);
    }
}

/*************************************************************************/

struct NetClient *mode_sender()
{
//    if (OSServerModeSender)
//        return &myService;
//    else
        return ncOperServ;
}

/*************************************************************************/
/**************************** Privilege checks ***************************/
/*************************************************************************/

/* Load OperServ data. */

#define SAFE(x) do {                                        \
    if ((x) < 0) {                                        \
        if (!forceload)                                        \
            fatal("Read error on %s", OperDBName);        \
        failed = 1;                                        \
        break;                                                \
    }                                                        \
} while (0)

void load_os_dbase(void)
{
    struct dbFILE *f;
    int16_t i, n, ver;
    char *s;
    int failed = 0;

    if (!(f = open_db(s_OperServ, OperDBName, "r")))
        return;
    switch (ver = get_file_version(f)) {
      case 7:
      case 6:
      case 5:
        SAFE(read_int16(&n, f));
        for (i = 0; i < n && !failed; i++) {
            SAFE(read_string(&s, f));
            if (s && i < MAX_SERVADMINS)
                services_admins[i] = findnick(s);
            if (s)
                free(s);
        }
        if (!failed)
            SAFE(read_int16(&n, f));
        for (i = 0; i < n && !failed; i++) {
            SAFE(read_string(&s, f));
            if (s && i < MAX_SERVOPERS)
                services_opers[i] = findnick(s);
            if (s)
                free(s);
        }
        if (ver >= 7) {
            int32_t tmp32;
            SAFE(read_int32(&IRCStats.max_users, f));
            SAFE(read_int32(&tmp32, f));
            IRCStats.ts_max_users = tmp32;
        }
        break;

      case 4:
      case 3:
        SAFE(read_int16(&n, f));
        for (i = 0; i < n && !failed; i++) {
            SAFE(read_string(&s, f));
            if (s && i < MAX_SERVADMINS)
                services_admins[i] = findnick(s);
            if (s)
                free(s);
        }
        break;

      default:
        fatal("Unsupported version (%d) on %s", ver, OperDBName);
    } /* switch (version) */
    close_db(f);
}

#undef SAFE

/*************************************************************************/

/* Save OperServ data. */

#define SAFE(x) do {                                                \
    if ((x) < 0) {                                                \
        restore_db(f);                                                \
        log_perror("Write error on %s", OperDBName);                \
        if (time(NULL) - lastwarn > WarningTimeout) {                \
            wallops(NULL, "Write error on %s: %s", OperDBName,        \
                        strerror(errno));                        \
            lastwarn = time(NULL);                                \
        }                                                        \
        return;                                                        \
    }                                                                \
} while (0)

void save_os_dbase(void)
{
    struct dbFILE *f;
    int16_t i, count = 0;
    static time_t lastwarn = 0;

    if (!(f = open_db(s_OperServ, OperDBName, "w")))
        return;
    for (i = 0; i < MAX_SERVADMINS; i++) {
        if (services_admins[i])
            count++;
    }
    SAFE(write_int16(count, f));
    for (i = 0; i < MAX_SERVADMINS; i++) {
        if (services_admins[i])
            SAFE(write_string(services_admins[i]->nick, f));
    }
    count = 0;
    for (i = 0; i < MAX_SERVOPERS; i++) {
        if (services_opers[i])
            count++;
    }
    SAFE(write_int16(count, f));
    for (i = 0; i < MAX_SERVOPERS; i++) {
        if (services_opers[i])
            SAFE(write_string(services_opers[i]->nick, f));
    }
    SAFE(write_int32(IRCStats.max_users, f));
    SAFE(write_int32(IRCStats.ts_max_users, f));
    close_db(f);
}

#undef SAFE

/*************************************************************************/

/* Does the given user have Services root privileges? */

int is_services_root(struct NetClient *nc)
{
    if (!(nc->user->modes & UMODE_o) || strcasecmp(nc->name, ServicesRoot) != 0)
        return 0;
    if (skeleton || nick_identified(nc))
        return 1;
    return 0;
}

/*************************************************************************/

/* Does the given user have Services admin privileges? */

int is_services_admin(struct NetClient *nc)
{
    int i;

    if (!(nc->user->modes & UMODE_o))
        return 0;
    if (is_services_root(nc))
        return 1;
    if (skeleton)
        return 1;
    for (i = 0; i < MAX_SERVADMINS; i++) {
        if (services_admins[i] && nc->user->ni == getlink(services_admins[i])) {
            if (nick_identified(nc))
                return 1;
            return 0;
        }
    }
    return 0;
}

/*************************************************************************/

/* Does the given user have Services oper privileges? */

int is_services_oper(struct NetClient *nc)
{
    int i;

    if (!(nc->user->modes & UMODE_o))
        return 0;
    if (is_services_admin(nc))
        return 1;
    if (skeleton)
        return 1;
    for (i = 0; i < MAX_SERVOPERS; i++) {
        if (services_opers[i] && nc->user->ni == getlink(services_opers[i])) {
            if (nick_identified(nc))
                return 1;
            return 0;
        }
    }
    return 0;
}

/*************************************************************************/

/* Is the given nick a Services admin/root nick? */

/* NOTE: Do not use this to check if a user who is online is a services admin
 * or root. This function only checks if a user has the ABILITY to be a 
 * services admin. Rather use is_services_admin(struct NetClient *nc). -TheShadow */

int nick_is_services_admin(struct NickInfo *ni)
{
    int i;

    if (!ni)
        return 0;
    if (strcasecmp(ni->nick, ServicesRoot) == 0)
        return 1;
    for (i = 0; i < MAX_SERVADMINS; i++) {
        if (services_admins[i] && getlink(ni) == getlink(services_admins[i]))
            return 1;
    }
    return 0;
}

/*************************************************************************/


/* Expunge a deleted nick from the Services admin/oper lists. */

void os_remove_nick(const struct NickInfo *ni)
{
    int i;

    for (i = 0; i < MAX_SERVADMINS; i++) {
        if (services_admins[i] == ni)
            services_admins[i] = NULL;
    }
    for (i = 0; i < MAX_SERVOPERS; i++) {
        if (services_opers[i] == ni)
            services_opers[i] = NULL;
    }
}

/*************************************************************************/
/**************************** Clone detection ****************************/
/*************************************************************************/

/* We just got a new user; does it look like a clone?  If so, send out a
 * wallops.
 */

void check_clones(struct NetClient *nc)
{
#ifndef STREAMLINED
    int i, clone_count;
    long last_time;

    if (!CheckClones)
        return;

    if (clonelist[0].host)
        free(clonelist[0].host);
    i = CLONE_DETECT_SIZE-1;
    memmove(clonelist, clonelist+1, sizeof(struct clone) * i);
    clonelist[i].host = sstrdup(nc->user->host);
    last_time = clonelist[i].time = time(NULL);
    clone_count = 1;
    while (--i >= 0 && clonelist[i].host) {
        if (clonelist[i].time < last_time - CloneMaxDelay)
            break;
        if (strcasecmp(clonelist[i].host, nc->user->host) == 0) {
            ++clone_count;
            last_time = clonelist[i].time;
            if (clone_count >= CloneMinUsers)
                break;
        }
    }
    if (clone_count >= CloneMinUsers) {
        /* Okay, we have clones.  Check first to see if we already know
         * about them. */
        for (i = CLONE_DETECT_SIZE-1; i >= 0 && warnings[i].host; --i) {
            if (strcasecmp(warnings[i].host, nc->user->host) == 0)
                break;
        }
        if (i < 0 || warnings[i].time < nc->user->ts_connect - CloneWarningDelay) {
            /* Send out the warning, and note it. */
            wallops(ncOperServ,
                "\2WARNING\2 - possible clones detected from %s", nc->user->host);
            slog(logsvc, "%s: possible clones detected from %s",
                s_OperServ, nc->user->host);
            i = CLONE_DETECT_SIZE-1;
            if (warnings[0].host)
                free(warnings[0].host);
            memmove(warnings, warnings+1, sizeof(struct clone) * i);
            warnings[i].host = sstrdup(nc->user->host);
            warnings[i].time = clonelist[i].time;
            if (KillClones)
                kill_user(ncOperServ, nc, "Clone kill");
        }
    }
#endif        /* !STREAMLINED */
}

/*************************************************************************/

#ifdef DEBUG_COMMANDS

/* Send clone arrays to given nick. */

static void send_clone_lists(struct NetClient *nc)
{
    int i;

    if (!CheckClones) {
        notice(ncOperServ, nc, "CheckClones not enabled.");
        return;
    }

    notice(ncOperServ, nc, "clonelist[]");
    for (i = 0; i < CLONE_DETECT_SIZE; i++) {
        if (clonelist[i].host)
            notice(ncOperServ, nc, "    %10ld  %s", clonelist[i].time, clonelist[i].host ? clonelist[i].host : "(null)");
    }
    notice(ncOperServ, nc, "warnings[]");
    for (i = 0; i < CLONE_DETECT_SIZE; i++) {
        if (clonelist[i].host)
            notice(ncOperServ, nc, "    %10ld  %s", warnings[i].time, warnings[i].host ? warnings[i].host : "(null)");
    }
}

#endif        /* DEBUG_COMMANDS */

/*************************************************************************/
/*********************** OperServ command functions **********************/
/*************************************************************************/

/* HELP command. */

static void do_help(struct NetClient *nc)
{
    const char *cmd = strtok(NULL, "");

    if (!cmd) {
        msg_help(ncOperServ, nc, OPER_HELP);
    } else {
        help_cmd(ncOperServ, nc, cmds, cmd);
    }
}

/*************************************************************************/

/* Global notice sending via GlobalNoticer. */

static void do_global(struct NetClient *nc)
{
    char *msg = strtok(NULL, "");
    struct NetClient *ncBot;

    if (!msg) {
        syntax_error(ncOperServ, nc, "GLOBAL", OPER_GLOBAL_SYNTAX);
        return;
    }

    /* Agregamos el bot */
    ncBot = service_create(s_GlobalNoticer, NULL, NULL, desc_GlobalNoticer);
    set_user_modes(&myService, ncBot, "+Bok");
    service_introduce(ncBot);


#if HAVE_ALLWILD_NOTICE
    send_cmd(ncBot, "P $* :%s", msg);
#else
# ifdef NETWORK_DOMAIN
    send_cmd(ncBot, "P $*.%s :%s", NETWORK_DOMAIN, msg);
# else
    /* Go through all common top-level domains.  If you have others,
     * add them here.
     */
    send_cmd(ncBot, "P $*.com :%s", msg);
    send_cmd(ncBot, "P $*.net :%s", msg);
    send_cmd(ncBot, "P $*.org :%s", msg);
    send_cmd(ncBot, "P $*.edu :%s", msg);
# endif
#endif
    service_remove(ncBot);
}

/*************************************************************************/

/* STATS command. */

static void do_stats(struct NetClient *nc)
{
    time_t uptime = time(NULL) - start_time;
    char *extra = strtok(NULL, "");
    int days = uptime/86400, hours = (uptime/3600)%24,
        mins = (uptime/60)%60, secs = uptime%60;
    char timebuf[64];

    if (extra && strcasecmp(extra, "ALL") != 0) {
        if (strcasecmp(extra, "AKILL") == 0) {
            int timeout = AutokillExpiry+59;
            msg_lang(ncOperServ, nc, OPER_STATS_AKILL_COUNT, num_akills());
            if (timeout >= 172800)
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_DAYS,
                        timeout/86400);
            else if (timeout >= 86400)
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_DAY);
            else if (timeout >= 7200)
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_HOURS,
                        timeout/3600);
            else if (timeout >= 3600)
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_HOUR);
            else if (timeout >= 120)
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_MINS,
                        timeout/60);
            else if (timeout >= 60)
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_MIN);
            else
                msg_lang(ncOperServ, nc, OPER_STATS_AKILL_EXPIRE_NONE);
            return;
        } else {
            msg_lang(ncOperServ, nc, OPER_STATS_UNKNOWN_OPTION,
                        strupper(extra));
        }
    }

    msg_lang(ncOperServ, nc, OPER_STATS_CURRENT_USERS, IRCStats.users, IRCStats.ircops);
    strftime_lang(timebuf, sizeof(timebuf), nc->user->ni, STRFTIME_DATE_TIME_FORMAT,
                  IRCStats.ts_max_users);
    msg_lang(ncOperServ, nc, OPER_STATS_MAX_USERS, IRCStats.max_users, timebuf);
    if (days > 1) {
        msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_DHMS,
                days, hours, mins, secs);
    } else if (days == 1) {
        msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1DHMS,
                days, hours, mins, secs);
    } else {
        if (hours > 1) {
            if (mins != 1) {
                if (secs != 1) {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_HMS,
                                hours, mins, secs);
                } else {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_HM1S,
                                hours, mins, secs);
                }
            } else {
                if (secs != 1) {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_H1MS,
                                hours, mins, secs);
                } else {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_H1M1S,
                                hours, mins, secs);
                }
            }
        } else if (hours == 1) {
            if (mins != 1) {
                if (secs != 1) {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1HMS,
                                hours, mins, secs);
                } else {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1HM1S,
                                hours, mins, secs);
                }
            } else {
                if (secs != 1) {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1H1MS,
                                hours, mins, secs);
                } else {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1H1M1S,
                                hours, mins, secs);
                }
            }
        } else {
            if (mins != 1) {
                if (secs != 1) {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_MS,
                                mins, secs);
                } else {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_M1S,
                                mins, secs);
                }
            } else {
                if (secs != 1) {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1MS,
                                mins, secs);
                } else {
                    msg_lang(ncOperServ, nc, OPER_STATS_UPTIME_1M1S,
                                mins, secs);
                }
            }
        }
    }

    if (extra && strcasecmp(extra, "ALL") == 0 && is_services_admin(nc)) {
        long count, mem, count2, mem2;
        int i;

        msg_lang(ncOperServ, nc, OPER_STATS_BYTES_READ, total_read / 1024);
        msg_lang(ncOperServ, nc, OPER_STATS_BYTES_WRITTEN, 
                        total_written / 1024);
/*
        get_user_stats(&count, &mem);
        msg_lang(ncOperServ, nc, OPER_STATS_USER_MEM,
                        count, (mem+512) / 1024);*/
        get_channel_stats(&count, &mem);
        msg_lang(ncOperServ, nc, OPER_STATS_CHANNEL_MEM,
                        count, (mem+512) / 1024);
        get_nickserv_stats(&count, &mem);
        msg_lang(ncOperServ, nc, OPER_STATS_NICKSERV_MEM,
                        count, (mem+512) / 1024);
        get_chanserv_stats(&count, &mem);
        msg_lang(ncOperServ, nc, OPER_STATS_CHANSERV_MEM,
                        count, (mem+512) / 1024);
        count = 0;
        if (CheckClones) {
            mem = sizeof(struct clone) * CLONE_DETECT_SIZE * 2;
            for (i = 0; i < CLONE_DETECT_SIZE; i++) {
                if (clonelist[i].host) {
                    count++;
                    mem += strlen(clonelist[i].host)+1;
                }
                if (warnings[i].host) {
                    count++;
                    mem += strlen(warnings[i].host)+1;
                }
            }
        }
        get_akill_stats(&count2, &mem2);
        count += count2;
        mem += mem2;
        get_news_stats(&count2, &mem2);
        count += count2;
        mem += mem2;
/*
        get_exception_stats(&count2, &mem2);
        count += count2;
        mem += mem2;
        msg_lang(ncOperServ, nc, OPER_STATS_OPERSERV_MEM,
                        count, (mem+512) / 1024);

        get_session_stats(&count, &mem);
        msg_lang(ncOperServ, nc, OPER_STATS_SESSIONS_MEM,
                        count, (mem+512) / 1024);
*/
    }
}

/*************************************************************************/

/* Channel mode changing (MODE command). */

static void do_os_mode(struct NetClient *nc)
{
    int argc;
    char **argv;
    char *s = strtok(NULL, "");
    char *chan, *modes;
    struct Channel *c;

    if (!s) {
        syntax_error(ncOperServ, nc, "MODE", OPER_MODE_SYNTAX);
        return;
    }
    chan = s;
    s += strcspn(s, " ");
    if (!*s) {
        syntax_error(ncOperServ, nc, "MODE", OPER_MODE_SYNTAX);
        return;
    }
    *s = 0;
    modes = (s+1) + strspn(s+1, " ");
    if (!*modes) {
        syntax_error(ncOperServ, nc, "MODE", OPER_MODE_SYNTAX);
        return;
    }
    if (!(c = FindChannel(chan))) {
        msg_lang(ncOperServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (c->bouncy_modes) {
        msg_lang(ncOperServ, nc, OPER_BOUNCY_MODES_U_LINE);
        return;
    } else {
        send_cmd(ncOperServ, "M %s %s", chan, modes);
        send_cmd(ncOperServ, "MODE %s %s", chan, modes);
        if (WallOSMode)
            wallops(ncOperServ, "%s used MODE %s on %s", nc->name, modes, chan);
        *s = ' ';
        argc = split_buf(chan, &argv, 1);
        m_mode(ncOperServ, argc, argv);
    }
}

/*************************************************************************/

/* Clear all modes from a channel. */

static void do_clearmodes(struct NetClient *nc)
{
    char *s;
    int i;
    char *argv[3];
    char *chan = strtok(NULL, " ");
    struct Channel *c;
    int all = 0;
    struct Membership *member;
    struct Ban *ban, *next;

    if (!chan) {
        syntax_error(ncOperServ, nc, "CLEARMODES", OPER_CLEARMODES_SYNTAX);
    } else if (!(c = FindChannel(chan))) {
        msg_lang(ncOperServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (c->bouncy_modes) {
        msg_lang(ncOperServ, nc, OPER_BOUNCY_MODES_U_LINE);
        return;
    } else {
        s = strtok(NULL, " ");
        if (s) {
            if (strcmp(s, "ALL") == 0) {
                all = 1;
            } else {
                syntax_error(ncOperServ,nc,"CLEARMODES",OPER_CLEARMODES_SYNTAX);
                return;
            }
        }
        if (WallOSClearmodes)
            wallops(ncOperServ, "%s used CLEARMODES%s on %s",
                        nc->name, all ? " ALL" : "", chan);
        if (all) {
            /* Clear mode +ov */
            for (member = c->members; member; member = member->next_member) {
                if (member->status & CUMODE_OP) {
                    member->status &= ~CUMODE_OP;
                    send_cmd(mode_sender(), "M %s -o :%s%s", c->name, NumNick(member->user));
                }
                if (member->status & CUMODE_VOICE) {
                    member->status &= ~CUMODE_VOICE;
                    send_cmd(mode_sender(), "M %s -v :%s%s", c->name, NumNick(member->user));
                }
            }
        }

        /* Clear modes */
        send_cmd(mode_sender(), "MODE %s -iklmnpstMRCNPuz :%s", chan,
                c->key ? c->key : "");
        argv[0] = sstrdup(chan);
        argv[1] = sstrdup("-iklmnpstMRCNPuz");
        argv[2] = c->key ? c->key : sstrdup("");
        m_mode(ncOperServ, 2, argv);
        free(argv[0]);
        free(argv[1]);
        free(argv[2]);
        c->key = NULL;
        c->limit = 0;

        /* Clear bans */
        for (ban = c->banlist; ban; ban = next) {
            next = ban->next;

            send_cmd(mode_sender(), "MODE %s -b :%s", c->name, ban->banstr);
            free(ban);

            c->bancount--;
        }
        c->banlist = NULL;
    }
}

/*************************************************************************/

/* Kick a user from a channel (KICK command). */

static void do_os_kick(struct NetClient *nc)
{
    char *argv[3];
    char *chan, *nick, *s, *dest;
    struct NetClient *nc2;
    struct Channel *c;

    chan = strtok(NULL, " ");
    nick = strtok(NULL, " ");
    s = strtok(NULL, "");
    if (!chan || !nick || !s) {
        syntax_error(ncOperServ, nc, "KICK", OPER_KICK_SYNTAX);
        return;
    }
    if (!(c = FindChannel(chan))) {
        msg_lang(ncOperServ, nc, CHAN_X_NOT_IN_USE, chan);
    } else if (!(nc2 = FindUser(nick))) {
        msg_lang(ncOperServ, nc, NICK_X_NOT_IN_USE, nick);
    } else if (c->bouncy_modes) {
        msg_lang(ncOperServ, nc, OPER_BOUNCY_MODES_U_LINE);
        return;
    }
    send_cmd(&myService, "K %s %s%s :%s (%s)", chan, NumNick(nc2), nc->name, s);
    remove_user_from_channel(nc2, c);
    if (WallOSKick)
        wallops(ncOperServ, "%s used KICK on %s/%s", nc->name, nick, chan);
}

/*************************************************************************/

/* Services admin list viewing/modification. */

static void do_admin(struct NetClient *nc)
{
    char *cmd, *nick;
    struct NickInfo *ni;
    int i;

    if (skeleton) {
        msg_lang(ncOperServ, nc, OPER_ADMIN_SKELETON);
        return;
    }
    cmd = strtok(NULL, " ");
    if (!cmd)
        cmd = "";

    if (strcasecmp(cmd, "ADD") == 0) {
        if (!is_services_root(nc)) {
            msg_lang(ncOperServ, nc, PERMISSION_DENIED);
            return;
        }
        nick = strtok(NULL, " ");
        if (nick) {
            if (!(ni = findnick(nick))) {
                msg_lang(ncOperServ, nc, NICK_X_NOT_REGISTERED, nick);
                return;
            }
            for (i = 0; i < MAX_SERVADMINS; i++) {
                if (!services_admins[i] || services_admins[i] == ni)
                    break;
            }
            if (services_admins[i] == ni) {
                msg_lang(ncOperServ, nc, OPER_ADMIN_EXISTS, ni->nick);
            } else if (i < MAX_SERVADMINS) {
                services_admins[i] = ni;
                msg_lang(ncOperServ, nc, OPER_ADMIN_ADDED, ni->nick);
            } else {
                msg_lang(ncOperServ, nc, OPER_ADMIN_TOO_MANY, MAX_SERVADMINS);
            }
            if (readonly)
                msg_lang(ncOperServ, nc, READ_ONLY_MODE);
        } else {
            syntax_error(ncOperServ, nc, "ADMIN", OPER_ADMIN_ADD_SYNTAX);
        }

    } else if (strcasecmp(cmd, "DEL") == 0) {
        if (!is_services_root(nc)) {
            msg_lang(ncOperServ, nc, PERMISSION_DENIED);
            return;
        }
        nick = strtok(NULL, " ");
        if (nick) {
            if (!(ni = findnick(nick))) {
                msg_lang(ncOperServ, nc, NICK_X_NOT_REGISTERED, nick);
                return;
            }
            for (i = 0; i < MAX_SERVADMINS; i++) {
                if (services_admins[i] == ni)
                    break;
            }
            if (i < MAX_SERVADMINS) {
                services_admins[i] = NULL;
                msg_lang(ncOperServ, nc, OPER_ADMIN_REMOVED, ni->nick);
                if (readonly)
                    msg_lang(ncOperServ, nc, READ_ONLY_MODE);
            } else {
                msg_lang(ncOperServ, nc, OPER_ADMIN_NOT_FOUND, ni->nick);
            }
        } else {
            syntax_error(ncOperServ, nc, "ADMIN", OPER_ADMIN_DEL_SYNTAX);
        }

    } else if (strcasecmp(cmd, "LIST") == 0) {
        msg_lang(ncOperServ, nc, OPER_ADMIN_LIST_HEADER);
        for (i = 0; i < MAX_SERVADMINS; i++) {
            if (services_admins[i])
                notice(ncOperServ, nc, "%s", services_admins[i]->nick);
        }

    } else {
        syntax_error(ncOperServ, nc, "ADMIN", OPER_ADMIN_SYNTAX);
    }
}

/*************************************************************************/

/* Services oper list viewing/modification. */

static void do_oper(struct NetClient *nc)
{
    char *cmd, *nick;
    struct NickInfo *ni;
    int i;

    if (skeleton) {
        msg_lang(ncOperServ, nc, OPER_OPER_SKELETON);
        return;
    }
    cmd = strtok(NULL, " ");
    if (!cmd)
        cmd = "";

    if (strcasecmp(cmd, "ADD") == 0) {
        if (!is_services_admin(nc)) {
            msg_lang(ncOperServ, nc, PERMISSION_DENIED);
            return;
        }
        nick = strtok(NULL, " ");
        if (nick) {
            if (!(ni = findnick(nick))) {
                msg_lang(ncOperServ, nc, NICK_X_NOT_REGISTERED, nick);
                return;
            }
            for (i = 0; i < MAX_SERVOPERS; i++) {
                if (!services_opers[i] || services_opers[i] == ni)
                    break;
            }
            if (services_opers[i] == ni) {
                msg_lang(ncOperServ, nc, OPER_OPER_EXISTS, ni->nick);
            } else if (i < MAX_SERVOPERS) {
                services_opers[i] = ni;
                msg_lang(ncOperServ, nc, OPER_OPER_ADDED, ni->nick);
            } else {
                msg_lang(ncOperServ, nc, OPER_OPER_TOO_MANY, MAX_SERVOPERS);
            }
            if (readonly)
                msg_lang(ncOperServ, nc, READ_ONLY_MODE);
        } else {
            syntax_error(ncOperServ, nc, "OPER", OPER_OPER_ADD_SYNTAX);
        }

    } else if (strcasecmp(cmd, "DEL") == 0) {
        if (!is_services_admin(nc)) {
            msg_lang(ncOperServ, nc, PERMISSION_DENIED);
            return;
        }
        nick = strtok(NULL, " ");
        if (nick) {
            if (!(ni = findnick(nick))) {
                msg_lang(ncOperServ, nc, NICK_X_NOT_REGISTERED, nick);
                return;
            }
            for (i = 0; i < MAX_SERVOPERS; i++) {
                if (services_opers[i] == ni)
                    break;
            }
            if (i < MAX_SERVOPERS) {
                services_opers[i] = NULL;
                msg_lang(ncOperServ, nc, OPER_OPER_REMOVED, ni->nick);
                if (readonly)
                    msg_lang(ncOperServ, nc, READ_ONLY_MODE);
            } else {
                msg_lang(ncOperServ, nc, OPER_OPER_NOT_FOUND, ni->nick);
            }
        } else {
            syntax_error(ncOperServ, nc, "OPER", OPER_OPER_DEL_SYNTAX);
        }

    } else if (strcasecmp(cmd, "LIST") == 0) {
        msg_lang(ncOperServ, nc, OPER_OPER_LIST_HEADER);
        for (i = 0; i < MAX_SERVOPERS; i++) {
            if (services_opers[i])
                notice(ncOperServ, nc, "%s", services_opers[i]->nick);
        }

    } else {
        syntax_error(ncOperServ, nc, "OPER", OPER_OPER_SYNTAX);
    }
}

/*************************************************************************/

/* Set various Services runtime options. */

static void do_set(struct NetClient *nc)
{
    char *option = strtok(NULL, " ");
    char *setting = strtok(NULL, " ");

    if (!option || !setting) {
        syntax_error(ncOperServ, nc, "SET", OPER_SET_SYNTAX);

    } else if (strcasecmp(option, "IGNORE") == 0) {
        if (strcasecmp(setting, "on") == 0) {
            allow_ignore = 1;
            msg_lang(ncOperServ, nc, OPER_SET_IGNORE_ON);
        } else if (strcasecmp(setting, "off") == 0) {
            allow_ignore = 0;
            msg_lang(ncOperServ, nc, OPER_SET_IGNORE_OFF);
        } else {
            msg_lang(ncOperServ, nc, OPER_SET_IGNORE_ERROR);
        }

    } else if (strcasecmp(option, "READONLY") == 0) {
        if (strcasecmp(setting, "on") == 0) {
            readonly = 1;
            slog(logsvc, "Read-only mode activated");
            log_closeall();
            msg_lang(ncOperServ, nc, OPER_SET_READONLY_ON);
        } else if (strcasecmp(setting, "off") == 0) {
            readonly = 0;
            log_openall();
            slog(logsvc, "Read-only mode deactivated");
            msg_lang(ncOperServ, nc, OPER_SET_READONLY_OFF);
        } else {
            msg_lang(ncOperServ, nc, OPER_SET_READONLY_ERROR);
        }

    } else if (strcasecmp(option, "DEBUG") == 0) {
        if (strcasecmp(setting, "on") == 0) {
            debug = 1;
            slog(logsvc, "Debug mode activated");
            msg_lang(ncOperServ, nc, OPER_SET_DEBUG_ON);
        } else if (strcasecmp(setting, "off") == 0 ||
                                (*setting == '0' && atoi(setting) == 0)) {
            slog(logsvc, "Debug mode deactivated");
            debug = 0;
            msg_lang(ncOperServ, nc, OPER_SET_DEBUG_OFF);
        } else if (isdigit(*setting) && atoi(setting) > 0) {
            debug = atoi(setting);
            slog(logsvc, "Debug mode activated (level %d)", debug);
            msg_lang(ncOperServ, nc, OPER_SET_DEBUG_LEVEL, debug);
        } else {
            msg_lang(ncOperServ, nc, OPER_SET_DEBUG_ERROR);
        }

    } else {
        msg_lang(ncOperServ, nc, OPER_SET_UNKNOWN_OPTION, option);
    }
}

/*************************************************************************/

static void do_jupe(struct NetClient *nc)
{
    char *jserver = strtok(NULL, " ");
    char *reason = strtok(NULL, "");
    char buf[NICKMAX+16];

    if (!jserver) {
        syntax_error(ncOperServ, nc, "JUPE", OPER_JUPE_SYNTAX);
    } else {
        wallops(ncOperServ, "\2Juping\2 %s by request of \2%s\2.",
                jserver, nc->name);
        if (!reason) {
            snprintf(buf, sizeof(buf), "Jupitered by %s", nc->name);
            reason = buf;
        }
        send_cmd(NULL, "SERVER %s 1 %lu %lu P09 :%s",
                jserver, time(NULL), time(NULL), reason);
    }
}

/*************************************************************************/

static void do_raw(struct NetClient *nc)
{
    char *text = strtok(NULL, "");

    if (!text)
        syntax_error(ncOperServ, nc, "RAW", OPER_RAW_SYNTAX);
    else
        send_cmd(NULL, "%s", text);
}

/*************************************************************************/

static void do_update(struct NetClient *nc)
{
    msg_lang(ncOperServ, nc, OPER_UPDATING);
    save_data = 1;
}

/*************************************************************************/

static void do_os_quit(struct NetClient *nc)
{
    quitmsg = malloc(28 + strlen(nc->name));
    if (!quitmsg)
        quitmsg = "QUIT command received, but out of memory!";
    else
        sprintf(quitmsg, "QUIT command received from %s", nc->name);
    quitting = 1;
}

/*************************************************************************/

static void do_shutdown(struct NetClient *nc)
{
    quitmsg = malloc(32 + strlen(nc->name));
    if (!quitmsg)
        quitmsg = "SHUTDOWN command received, but out of memory!";
    else
        sprintf(quitmsg, "SHUTDOWN command received from %s", nc->name);
    save_data = 1;
    delayed_quit = 1;
}

/*************************************************************************/

static void do_restart(struct NetClient *nc)
{
#ifdef SERVICES_BIN
    quitmsg = malloc(31 + strlen(nc->name));
    if (!quitmsg)
        quitmsg = "RESTART command received, but out of memory!";
    else
        sprintf(quitmsg, "RESTART command received from %s", nc->name);
    raise(SIGHUP);
#else
    msg_lang(ncOperServ, nc, OPER_CANNOT_RESTART);
#endif
}

/*************************************************************************/

/* XXX - this function is broken!! */

static void do_listignore(struct NetClient *nc)
{
    int sent_header = 0;
    struct IgnoreData *id;
    int i;

    notice(ncOperServ, nc, "Command disabled - it's broken.");
    return;
    
    for (i = 0; i < 256; i++) {
        for (id = ignore[i]; id; id = id->next) {
            if (!sent_header) {
                msg_lang(ncOperServ, nc, OPER_IGNORE_LIST);
                sent_header = 1;
            }
            notice(ncOperServ, nc, "%ld %s", id->time, id->who);
        }
    }
    if (!sent_header)
        msg_lang(ncOperServ, nc, OPER_IGNORE_LIST_EMPTY);
}

/*************************************************************************/

#ifdef DEBUG_COMMANDS

static void do_matchwild(struct NetClient *nc)
{
    char *pat = strtok(NULL, " ");
    char *str = strtok(NULL, " ");
    if (pat && str)
        notice(ncOperServ, nc, "%d", match_wild(pat, str));
    else
        notice(ncOperServ, nc, "Syntax error.");
}

#endif        /* DEBUG_COMMANDS */

/*************************************************************************/

/* Kill all users matching a certain host. The host is obtained from the
 * supplied nick. The raw hostmsk is not supplied with the command in an effort
 * to prevent abuse and mistakes from being made - which might cause *.com to
 * be killed. It also makes it very quick and simple to use - which is usually
 * what you want when someone starts loading numerous clones. In addition to
 * killing the clones, we add a temporary AKILL to prevent them from 
 * immediately reconnecting.
 * Syntax: KILLCLONES nick
 * -TheShadow (29 Mar 1999) 
 */

static void do_killclones(struct NetClient *nc)
{
#if 0
    char *clonenick = strtok(NULL, " ");
    int count=0;
    struct NetClient *cloneuser, *user, *tempuser;
    char *clonemask, *akillmask;
    char killreason[NICKMAX+32];
    char akillreason[] = "Temporary KILLCLONES akill.";

    if (!clonenick) {
        msg_lang(ncOperServ, nc, OPER_KILLCLONES_SYNTAX);

    } else if (!(cloneuser = FindUser(clonenick))) {
        msg_lang(ncOperServ, nc, OPER_KILLCLONES_UNKNOWN_NICK, clonenick);

    } else {
        clonemask = smalloc(strlen(cloneuser->user->host) + 5);
        sprintf(clonemask, "*!*@%s", cloneuser->user->host);

        akillmask = smalloc(strlen(cloneuser->user->host) + 3);
        sprintf(akillmask, "*@%s", strlower(cloneuser->user->host));
        
        user = firstuser();
        while (user) {
            if (match_usermask(clonemask, user) != 0) {
                tempuser = nextuser();
                count++;
                snprintf(killreason, sizeof(killreason), 
                                        "Cloning [%d]", count);
                kill_user(NULL, user->name, killreason);
                user = tempuser;
            } else {
                user = nextuser();
            }
        }

        add_akill(akillmask, akillreason, nc->name, 
                        time(NULL) + KillClonesAkillExpire);

        wallops(ncOperServ, "\2%s\2 used KILLCLONES for \2%s\2 killing "
                        "\2%d\2 clones. A temporary AKILL has been added "
                        "for \2%s\2.", nc->name, clonemask, count, akillmask);

        slog(logsvc, "%s: KILLCLONES: %d clone(s) matching %s killed.",
                        s_OperServ, count, clonemask);

        free(akillmask);
        free(clonemask);
    }
#endif
}

/*************************************************************************/
