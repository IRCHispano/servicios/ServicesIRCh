/*
 * ServicesIRCh - Services for IRCh, process.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "process.h"

#include "hash.h"
#include "language.h"
#include "log.h"
#include "messages.h"
#include "misc.h"
#include "netclient.h"
#include "nickserv.h"
#include "numerics.h"
#include "send.h"
#include "services.h"
#include "svc_memory.h"
#include "svc_string.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/*************************************************************************/
/*************************************************************************/

/* Use ignore code? */
int allow_ignore = 1;

/* People to ignore (hashed by first character of nick). */
struct IgnoreData *ignore[256];

/*************************************************************************/

/* add_ignore: Add someone to the ignorance list for the next `delta'
 *             seconds.
 */

void add_ignore(const char *nick, time_t delta)
{
    struct IgnoreData *ign;
    char who[NICKMAX];
    time_t now = time(NULL);
    struct IgnoreData **whichlist = &ignore[tolower(nick[0])];

    strscpy(who, nick, NICKMAX);
    for (ign = *whichlist; ign; ign = ign->next) {
        if (strcasecmp(ign->who, who) == 0)
            break;
    }
    if (ign) {
        if (ign->time > now)
            ign->time += delta;
        else
            ign->time = now + delta;
    } else {
        ign = smalloc(sizeof(*ign));
        strscpy(ign->who, who, sizeof(ign->who));
        ign->time = now + delta;
        ign->next = *whichlist;
        *whichlist = ign;
    }
}

/*************************************************************************/

/* get_ignore: Retrieve an ignorance record for a nick.  If the nick isn't
 *             being ignored, return NULL and flush the record from the
 *             in-core list if it exists (i.e. ignore timed out).
 */

struct IgnoreData *get_ignore(const char *nick)
{
    struct IgnoreData *ign, *prev;
    time_t now = time(NULL);
    struct IgnoreData **whichlist = &ignore[tolower(nick[0])];

    for (ign = *whichlist, prev = NULL; ign; prev = ign, ign = ign->next) {
        if (strcasecmp(ign->who, nick) == 0)
            break;
    }
    if (ign && ign->time <= now) {
        if (prev)
            prev->next = ign->next;
        else
            *whichlist = ign->next;
        free(ign);
        ign = NULL;
    }
    return ign;
}

/*************************************************************************/
/*************************************************************************/

/* split_buf:  Split a buffer into arguments and store the arguments in an
 *             argument vector pointed to by argv (which will be malloc'd
 *             as necessary); return the argument count.  If colon_special
 *             is non-zero, then treat a parameter with a leading ':' as
 *             the last parameter of the line, per the IRC RFC.  Destroys
 *             the buffer by side effect.
 */

int split_buf(char *buf, char ***argv, int colon_special)
{
    int argvsize = 8;
    int argc;
    char *s;

    *argv = smalloc(sizeof(char *) * argvsize);
    argc = 0;
    while (*buf) {
        if (argc == argvsize) {
            argvsize += 8;
            *argv = srealloc(*argv, sizeof(char *) * argvsize);
        }
        if (*buf == ':') {
            (*argv)[argc++] = buf+1;
            buf = "";
        } else {
            s = strpbrk(buf, " ");
            if (s) {
                *s++ = 0;
                while (isspace(*s))
                    s++;
            } else {
                s = buf + strlen(buf);
            }
            (*argv)[argc++] = buf;
            buf = s;
        }
    }
    return argc;
}

/*************************************************************************/

/* process:  Main processing routine.  Takes the string in inbuf (global
 *           variable) and does something appropriate with it. */

void process()
{
    char source[64];
    char cmd[64];
    char buf[512];                /* Longest legal IRC command line */
    char *s;
    int ac;                        /* Parameters for the command */
    char **av;
    struct Message *m;
    struct NetClient *nc;

    /* If debugging, log the buffer. */
    Debug((1, "Received: %s", inbuf));

    /* First make a copy of the buffer so we have the original in case we
     * crash - in that case, we want to know what we crashed on. */
    strscpy(buf, inbuf, sizeof(buf));

    /* Split the buffer into pieces. */
    if (*buf == ':') {
        /* P09 */
        s = strpbrk(buf, " ");
        if (!s)
            return;
        *s = 0;
        while (isspace(*++s))
            ;
        strscpy(source, buf+1, sizeof(source));
        memmove(buf, s, strlen(s)+1);
        nc = SeekNetClient(source);

    } else if (myHub) {
        /* P10 */
        s = strpbrk(buf, " ");
        if (!s)
            return;
        *s = 0;
        while (isspace(*++s))
            ;
        strscpy(source, buf, sizeof(source));
        memmove(buf, s, strlen(s)+1);

        if (strlen(source) < 3)
            nc = FindNServer(source);
        else
            nc = FindNUser(source);

    } else {
        /* Proceso de conexion PASS+SERVER */
        nc = NULL;
    }

    if (!*buf)
        return;
    s = strpbrk(buf, " ");
    if (s) {
        *s = 0;
        while (isspace(*++s))
            ;
    } else
        s = buf + strlen(buf);
    strscpy(cmd, buf, sizeof(cmd));
    ac = split_buf(s, &av, 1);

    /* Do something with the message. */
    m = find_message_tok(cmd);
    if (!m)
        m = find_message_name(cmd);

    if (m) {
        if (!nc && m->func && (!irc_strcmp(m->name, "SERVER") || !irc_strcmp(m->name, "PASS")))
            m->func(NULL, ac, av);
        else if (nc && m->func)
            m->func(nc, ac, av);
        else if (!nc) {
            if (!irc_strcmp(m->name, "PRIVMSG")) {
                slog(logsvc, "%s: user record for %s not found", av[0], source);
                send_cmd(&myService, "P %s :%s", source,
                         getstring((struct NickInfo *)NULL, USER_RECORD_NOT_FOUND));
            } else {
                slog(logsvc, "unknown source from server (%s) %s", inbuf, source);
            }
        }
    } else {
        slog(logsvc, "unknown message from server (%s) %s", inbuf, cmd);
    }
}

/*************************************************************************/
