/*
 * ServicesIRCh - Services for IRCh, send.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "send.h"

#include "config.h"
#include "language.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "nickserv.h"
#include "services.h"
#include "sockutil.h"
#include "users.h"

#include <string.h>

/*************************************************************************/

/* Send a command to the server.  The two forms here are like
 * printf()/vprintf() and friends. */

void send_cmd(struct NetClient *source, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    vsend_cmd(source, fmt, args);
    va_end(args);
}

void vsend_cmd(struct NetClient *source, const char *fmt, va_list args)
{
    char buf[BUFSIZE];

    vsnprintf(buf, sizeof(buf), fmt, args);
    if (source) {
        if (IsUser(source)) {
            sockprintf(servsock, "%s%s %s\r\n", NumNick(source), buf);
            Debug((1, "Sent: %s%s(%s) %s", NumNick(source), source->name, buf));
        } else {
            sockprintf(servsock, "%s %s\r\n", NumServ(source), buf);
            Debug((1, "Sent: %s(%s) %s", NumServ(source), source->name, buf));
        }
    } else {
        sockprintf(servsock, "%s\r\n", buf);
        Debug((1, "Sent: %s", buf));
    }
}

/*************************************************************************/

/* Send a numeric message to the server. */

void send_numirc(struct NetClient *dest, int numeric, const char *fmt, ...)
{
    va_list args;
    char buf[BUFSIZE];

    va_start(args, fmt);
    snprintf(buf, sizeof(buf), "%d %s%s %s", numeric, NumNick(dest), fmt);
    vsend_cmd(&myService, buf, args);
    va_end(args);

}

/*************************************************************************/

/* Send out a WALLOPS (a GLOBOPS on ircd.dal). */

void wallops(struct NetClient *source, const char *fmt, ...)
{
    va_list args;
    char buf[BUFSIZE];

    va_start(args, fmt);
    snprintf(buf, sizeof(buf), "WA :%s", fmt);
    vsend_cmd(source ? source : &myService, buf, args);
}

/*************************************************************************/

/* Send a NOTICE from the given source to the given nick. */
void notice(struct NetClient *source, struct NetClient *dest, const char *fmt, ...)
{
    va_list args;
    char buf[BUFSIZE];

    va_start(args, fmt);
    snprintf(buf, sizeof(buf), "O %s%s :%s", NumNick(dest), fmt);
    vsend_cmd(source, buf, args);
}

/* Send a NOTICE from the given source to the given channel. */
void msg_chan(struct NetClient *source, const char *chan, const char *fmt, ...)
{
    va_list args;
    char buf[BUFSIZE];

    va_start(args, fmt);
    snprintf(buf, sizeof(buf), "O %s :%s", chan, fmt);
    vsend_cmd(source, buf, args);
}

/* Send a NULL-terminated array of text as NOTICEs. */
void notice_list(struct NetClient *source, struct NetClient *dest, const char **text)
{
    while (*text) {
        /* Have to kludge around an ircII bug here: if a notice includes
         * no text, it is ignored, so we replace blank lines by lines
         * with a single space.
         */
        if (**text)
            notice(source, dest, *text);
        else
            notice(source, dest, " ");
        text++;
    }
}


/* Send a message in the user's selected language to the user using NOTICE. */
void msg_lang(struct NetClient *source, struct NetClient *dest, int message, ...)
{
    va_list args;
    char buf[4096];        /* because messages can be really big */
    char *s, *t;
    const char *fmt;

    if (!dest)
        return;
    va_start(args, message);
    fmt = getstring(dest->user->ni, message);
    if (!fmt)
        return;
    vsnprintf(buf, sizeof(buf), fmt, args);
    s = buf;
    while (*s) {
        t = s;
        s += strcspn(s, "\n");
        if (*s)
            *s++ = 0;
        send_cmd(source, "O %s%s :%s", NumNick(dest), *t ? t : " ");
    }
}


/* Like msg_lang(), but replace %S by the source.  This is an ugly hack
 * to simplify letting help messages display the name of the pseudoclient
 * that's sending them.
 */
void msg_help(struct NetClient *source, struct NetClient *dest, int message, ...)
{
    va_list args;
    char buf[4096], buf2[4096], outbuf[BUFSIZE];
    char *s, *t;
    const char *fmt;

    if (!dest)
        return;
    va_start(args, message);
    fmt = getstring(dest->user->ni, message);
    if (!fmt)
        return;
    /* Some sprintf()'s eat %S or turn it into just S, so change all %S's
     * into \1\1... we assume this doesn't occur anywhere else in the
     * string. */
    strscpy(buf2, fmt, sizeof(buf2));
    strnrepl(buf2, sizeof(buf2), "%S", "\1\1");
    vsnprintf(buf, sizeof(buf), buf2, args);
    s = buf;
    while (*s) {
        t = s;
        s += strcspn(s, "\n");
        if (*s)
            *s++ = 0;
        strscpy(outbuf, t, sizeof(outbuf));
        strnrepl(outbuf, sizeof(outbuf), "\1\1", source->name);
        send_cmd(source, "O %s%s :%s", NumNick(dest), *outbuf ? outbuf : " ");
    }
}

/*************************************************************************/

/* Send a PRIVMSG from the given source to the given nick. */
void privmsg(struct NetClient *source, struct NetClient *dest, const char *fmt, ...)
{
    va_list args;
    char buf[BUFSIZE];

    va_start(args, fmt);
    snprintf(buf, sizeof(buf), "P %s%s :%s", NumNick(dest), fmt);
    vsend_cmd(source, buf, args);
}

/*************************************************************************/
