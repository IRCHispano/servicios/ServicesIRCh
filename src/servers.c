/*
 * ServicesIRCh - Services for IRCh, servers.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2012-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "servers.h"

#include "channels.h"
#include "hash.h"
#include "log.h"
#include "netclient.h"
#include "numerics.h"
#include "operserv.h"
#include "send.h"
#include "services.h"
#include "stats.h"
#include "svc_memory.h"
#include "users.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

/*************************************************************************/

struct Server *servers_create(void)
{
    struct Server *serv;

    serv = smalloc(sizeof(struct Server));
    assert(serv);
    memset(serv, 0, sizeof(struct Server));

    return serv;
}

void servers_delete(struct Server *serv)
{
    assert(serv);

    if (serv->version)
        sfree(serv->version);
    if (serv->options)
        sfree(serv->options);

    if (serv->client_list)
        sfree(serv->client_list);

    sfree(serv);
}

/*************************************************************************/

struct ServerLink *serverlink_add(struct ServerLink **slp, struct NetClient *nc)
{
    struct ServerLink *sl = smalloc(sizeof(struct ServerLink));
    sl->nc = nc;
    sl->prev = 0;
    if ((sl->next = *slp))
        sl->next->prev = sl;
    *slp = sl;
    return sl;
}

void serverlink_del(struct ServerLink **slp, struct ServerLink *sl)
{
    if (sl->prev) {
        if ((sl->prev->next = sl->next))
            sl->next->prev = sl->prev;
    }
    else if ((*slp = sl->next))
        sl->next->prev = NULL;
    sfree(sl);
}

/*************************************************************************/

/* Da de baja un servidor */

static void server_remove(struct NetClient *nc, char *reason)
{
    if (nc->serv && nc->serv->client_list)
        ClearServerYXX(nc);

    serverlink_del(&nc->serv->up->serv->down, nc->serv->updown);
    nc->serv->updown = 0;

    Count_quitserver(IRCStats, nc);
    netclient_del_list(nc);
    hRemNetClient(nc);

    netclient_delete(nc);
}

/*************************************************************************/

/* Borra servidores recursivamente */
static void squit_recursive(struct NetClient *nc, char *reason)
{
    struct NetClient *tmpnc;
    struct ServerLink *next;
    struct ServerLink *sl;
    struct NetClient **usersnc;
    unsigned int i;

    for (sl = nc->serv->down; sl; sl = next) {
        next = sl->next;
        tmpnc = sl->nc;

        /* Borramos recursivamente */
        squit_recursive(tmpnc, reason);
        server_remove(tmpnc, reason);
    }

    /* Borrar todos los clientes del servidor */
    usersnc = nc->serv->client_list;
    for (i = 0; i <= nc->serv->nn_mask; ++usersnc, i++) {
        if (*usersnc)
            user_remove(*usersnc, reason);
    }
}


/*************************************************************************/
/******************* PARSEO DE RED - SERVIDORES **************************/
/*************************************************************************/

/* Handle a END_OF_BURST command
 *
 *  source = Servidor
 *    av = Se ignora
 */
void m_end_of_burst(struct NetClient *source, int ac, char **av)
{
    /*
     * Dado que solo tenemos un servidor en Services,
     * no tenemos que propagarlo.
     */
   ClearBurst(source);
   SetBurstAck(source);

   //if (source == myHub)
     //  send_cmd(&myService, "EA");
}

/*************************************************************************/

/* Handle a END_OF_BURST_ACK command
 *
 *  source = Servidor
 *    av = Se ignora
 */
void m_end_of_burst_ack(struct NetClient *source, int ac, char **av)
{
    /*
     * Dado que solo tenemos un servidor en Services,
     * no tenemos que propagarlo.
     */
    ClearBurstAck(source);
//    if (WallNetwork && (source == myHub))
  //      wallops(ncOperServ, "[NET-ACK] Finalizado el burst con %d servidores y %d usuarios",
    //            IRCStats.servers, IRCStats.users);
}

/*************************************************************************/

/* Handle a SERVER command
 *
 *  source = NULL (hub uplink) o Servidor
 *    av[0] = Nombre del servidor
 *    av[1] = Hopcount
 *    av[2] = Timestamp arranque
 *    av[3] = Timestamp link
 *    av[4] = Protocolo del servidor
 *    av[5] = Numerico + Capacidad
 *    av[6] = Flags del servidor (+6 IPv6, +h hub, +s service)
 *    av[7] = Descripcion servidor
 *
 */
void m_server(struct NetClient *source, int ac, char **av)
{
    struct NetClient *nc;

    if (ac < 8) {
        Debug((1, "SERVER message: expecting 8 or more parameters after "
                  "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    /* No vamos a comprobar flags, protocolo, nombre, descripcion
     * porque ya lo hacen los propios ircds al linkar uno de los
     * otros.
     * Si se casca por una mala implementacion del ircd, no es
     * mi problema, que hagan ircds como dios manda.
     */

    /* Comprobamos la existencia del servidor. */
    if (FindServer(av[0]))
        return;

    /* Creamos el NetClient asociado */
    nc = netclient_create(NETCLIENT_SERVER);

    nc->name = sstrdup(av[0]);
    nc->description = sstrdup(av[7]);
    nc->hopcount = atoi(av[1]);

    nc->serv->protocol = atoi(av[4] + 1);
    nc->serv->start_time = atoi(av[2]);
    nc->serv->link_time = atoi(av[3]);

    if (*av[6] == '+') {
        while (*av[6]) {
            switch (*av[6]++) {
                case 'h' : SetHub(nc); break;
                case 's' : SetService(nc); break;
                case '6' : SetIPv6(nc); break;
            }
        }
    }
    /* Agregamos en la lista de numericos
     * y tabla hash de nombres.
     */
    SetServerYXX(nc, av[5]);
    netclient_add_list(nc);
    hAddNetClient(nc);

    if (!source) {
        /* Es el servidor que conecta a los services */
        nc->serv->up = &myService;
        myHub = nc;
    } else {
        /* Otro servidor de la red */
        nc->serv->up = source;
        nc->serv->updown = serverlink_add(&(source->serv->down), nc);
    }

    if (*av[4] == 'J') {
        SetBurst(nc);
//        if (WallNetwork)
  //          wallops(ncOperServ, "[NET-JOIN] %s", nc->name);
    }

    Count_newserver(IRCStats, nc);
    slog(logsvc, "NETJOIN de %s (%s)", nc->name, nc->yxx);

    /* Enviamos una version */
   // send_cmd(ncOperServ, "V %s", nc->yxx);
}

/*************************************************************************/

/* Handle a SQUIT command
 *
 *  source = Servidor
 *    av[0] = Numerico/Nombre del servidor
 *    av[1] = Timestamp link (o 0 para forzar)
 *    av[2] = Mensaje de squit
 *
 */
void m_squit(struct NetClient *source, int ac, char **av)
{
    struct NetClient *nc;
    char *servername;
    time_t ts = 0;
    int users, servs, services, pservs;

    if (ac < 2) {
        Debug((1, "SQUIT message: expecting 2 or more parameters after "
                  "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    /* Primero buscamos por numerico
     * y si no existe, por nombre.
     */
    nc = FindNServer(av[0]);
    if (!nc)
        nc = FindServer(av[0]);

    if (!nc)
        return;

    /* Si es para mi, lo paso a mi hub */
    if (IsMe(nc))
        nc = myHub;

    /* Si lo que hace squit es el enlace hub<->services
     * se hace un cierre controlado.
     */
    if (nc == myHub)
    {
        save_data = 1;
        delayed_quit = 1;
        return;
    }

    if (ac > 2)
        ts = atoi(av[1]);
//    else
  //      send_cmd(&myService, "DS :Protocol Violation from %s: SQUIT with no timestamp/reason",
    //             source->name);

    if (ts && ts != nc->serv->link_time)
      return;

    servername = sstrdup(nc->name);
    users = IRCStats.users;
    servs = IRCStats.servers;
    services = IRCStats.services;
    pservs = IRCStats.pservers;

    slog(logsvc, "Netbreak %s. Serv: %u - Users: %u Reason: %s",
            servername, IRCStats.servers, IRCStats.users, av[2]);

    squit_recursive(nc, av[2]);
    server_remove(nc, av[2]);

    slog(logsvc, "Netbreak %s OK. Serv: %u - Users: %u Reason: %s",
            servername, IRCStats.servers, IRCStats.users, av[2]);

    users -= IRCStats.users;
    servs -= IRCStats.servers;
    services -= IRCStats.services;
    pservs -= IRCStats.pservers;

//    if (WallNetwork)
  //      wallops(ncOperServ, "[NET-BREAK] %s, %u servidores, %u services, %u bots y %u usuarios afectados",
    //            servername, servs, pservs, services, users);

    sfree(servername);
}

/*************************************************************************/

/* Handle a 351 numeric
 *
 *  source = Servidor
 *    av[0] = Nick que manda consulta
 *    av[1] = Version del servidor
 *    av[2] = Nombre del servidor
 *    av[3] = Opciones del servidor
 *
 */
void m_351(struct NetClient *source, int ac, char **av)
{
    if (ac < 4) {
        Debug((1, "351 VERSION message: expecting 4 or more parameters after "
                  "parsing; got %d, source='%s'", ac, source->name));
        return;
    }

    if (source->serv->version)
        sfree(source->serv->version);
    source->serv->version = sstrdup(av[1]);

    if (source->serv->options)
        sfree(source->serv->options);
    source->serv->options = sstrdup(av[3]);
}
