/*
 * ServicesIRCh - Services for IRCh, service.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "service.h"

#include "channels.h"
#include "chanserv.h"
#include "config.h"
#include "hash.h"
#include "log.h"
#include "misc.h"
#include "netclient.h"
#include "numerics.h"
#include "send.h"
#include "servers.h"
#include "stats.h"
#include "svc_memory.h"
#include "svc_string.h"
#include "users.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct NetClient *service_create(const char *nick, const char *user, const char *host, const char *realname)
{
    struct NetClient *nc;
    struct Service *sv;

    Debug((1, "new service: %s", nick));

    /* Creamos el NetClient asociado */
    nc = netclient_create(NETCLIENT_USER);
    nc->status |= NETCLIENT_SERVICE;

    nc->user->server = &myService;
    nc->name = sstrdup(nick);
    nc->hopcount = 0;
    nc->user->ts_connect = nc->user->ts_lastnick = nc->user->my_signon = time(NULL);
    if (user)
        nc->user->username = sstrdup(user);
    else
        nc->user->username = sstrdup(ServiceUser);
    if (host)
        nc->user->host = sstrdup(host);
    else
        nc->user->host = sstrdup(ServiceHost);
    nc->description = sstrdup(realname);
//    memcpy(&(nc->user->ip), &ServiceIP, sizeof(struct irc_in_addr));

    SetLocalNumNick(nc);
    netclient_add_list(nc);
    hAddNetClient(nc);

    /* Contadores */
    ++nc->user->server->serv->clients;
    ++IRCStats.local_services;
    ++IRCStats.services;

    /* Agregamos a la lista local */



    /* Creamos el Service asociado */
    sv = smalloc(sizeof(struct Service));
    assert(sv);
    memset(sv, 0, sizeof(struct Service));

    nc->service = sv;
    sv->nc = nc;

    return nc;
}

void service_remove(struct NetClient *nc)
{
    struct Service *sv;

//    send_cmd(nc, "Q");

    /* Ajustando contadores */
    --nc->user->server->serv->clients;
    --IRCStats.local_services;
    --IRCStats.services;
    if (nc->user->modes & UMODE_o)
        IRCStats.ircops--;

    /* Borrar canales */
//    remove_user_from_all_channels(nc);

    sv = nc->service;
    nc->service = NULL;
    sfree(sv);

    if (nc->user)
        RemoveYXXClient(nc->user->server, nc->yxx);

    netclient_del_list(nc);
    hRemNetClient(nc);
    netclient_delete(nc);
}


void service_introduce(struct NetClient *nc)
{
    if (myHub) {
        struct Membership *chan;

        /* Solo mandamos si esta conectado, si no esta lo hacemos
         * por el burst */
        send_cmd(&myService, "N %s 1 %lu %s %s +%s %s %s%s :%s",
                 nc->name, nc->user->ts_lastnick, nc->user->username, nc->user->host,
                 umode_str(nc->user->modes, 1), irc_ntoa(&nc->user->ip), NumNick(nc), nc->description);

        chan = nc->user->channel;
        while (chan) {
            send_cmd(nc, "J %s", chan->channel->name);
            if (chan->status & (CUMODE_OP|CUMODE_VOICE))
                send_cmd(&myService, "M %s +%s%s %s%s %s%s", chan->channel->name,
                         (chan->status & CUMODE_OP) ? "o" : "",
                         (chan->status & CUMODE_VOICE) ? "v" : "",
                         (chan->status & CUMODE_OP) ? nc->user->server->yxx : "",
                         (chan->status & CUMODE_OP) ? nc->yxx : "",
                         (chan->status & CUMODE_VOICE) ? nc->user->server->yxx : "",
                         (chan->status & CUMODE_VOICE) ? nc->yxx : "");
            chan = chan->next_channel;
        }
    }
}

void service_join(struct NetClient *nc, char *channels)
{
    struct Channel *c;
    char *name, *p;

    for (name = irc_strtok(&p, channels, ","); name;
        name = irc_strtok(&p, 0, ",")) {

        if (!(c = FindChannel(name))) {
            if (!(c = get_channel(nc, name, 1))) {
                 continue;
            }
            c->creation_time = 915274800;
        }
        add_user_to_channel(c, nc, CUMODE_OP);

        if (myHub) {
            /* Solo mandamos si esta conectado, si no esta lo hacemos
             * por el burst */
            send_cmd(nc, "J %s", name);
            send_cmd(&myService, "M %s +o %s%s", name, NumNick(nc));
        }
    }
}

void services_burst()
{
    struct NetClient *nc;
    struct Channel *c;
    char bufburst[512];

    for (nc = firstnc(); nc; nc = nextnc())
    {
        if (!IsUser(nc))
            continue;

        send_cmd(&myService, "N %s 1 %lu %s %s +%s %s %s%s :%s",
                 nc->name, nc->user->ts_lastnick, nc->user->username, nc->user->host,
                 umode_str(nc->user->modes, 1), irc_ntoa(&nc->user->ip), NumNick(nc), nc->description);
    }

    for (c = firstchan(); c; c = nextchan())
    {
        struct Membership *member;
        char *mlock;
        int mode = 0;

        *bufburst = 0;

        /* Store ChannelInfo pointer in channel record */
        c->ci = cs_findchan(c->name);
        /* Store return pointer in ChannelInfo record */
        if (c->ci)
            c->ci->c = c;

        for (member = c->members; member; member = member->next_member) {
             if (*bufburst)
                 strcat(bufburst, ",");
            strcat(bufburst, member->user->user->server->yxx);
            strcat(bufburst, member->user->yxx);
            if (!mode) {
                strcat(bufburst, ":o");
                mode = 1;
            }
        }

        if (c->ci) {
            int modetemp;

            mlock = get_mlock(c->ci->mlock_on, 0, c->ci->mlock_key, c->ci->mlock_limit, 1);
            modetemp = ~c->mode & (c->ci->mlock_on | CMODE_r);
            c->mode |= modetemp;
        } else
            mlock = NULL;

        send_cmd(&myService, "B %s %ld %s %s", c->name, c->creation_time, (mlock || mlock && mlock[0] != '\0') ? mlock : "+", bufburst);
        restore_topic(c);
    }

    /* Enviamos fin de Burst */
    send_cmd(&myService, "EB");
}
