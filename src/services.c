/*
 * ServicesIRCh - Services for IRCh, services.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "services.h"

#include "akill.h"
#include "chanserv.h"
#include "log.h"
#include "netclient.h"
#include "newsserv.h"
#include "nickserv.h"
#include "operserv.h"
#include "process.h"
#include "send.h"
#include "sockutil.h"
#include "timeout.h"

#include <errno.h>
#include <setjmp.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/******** Global variables! ********/

/* Command-line options: (note that configuration variables are in config.c) */
char *services_dir = SERVICES_DATA_PATH;/* -dir dirname */
char *log_filename = LOG_FILENAME;        /* -log filename */
int   debug        = 0;                        /* -debug */
int   readonly     = 0;                        /* -readonly */
int   skeleton     = 0;                        /* -skeleton */
int   nofork       = 0;                        /* -nofork */
int   forceload    = 0;                        /* -forceload */

/* Set to 1 if we are to quit */
int quitting = 0;

/* Set to 1 if we are to quit after saving databases */
int delayed_quit = 0;

/* Contains a message as to why services is terminating */
char *quitmsg = NULL;

/* Input buffer - global, so we can dump it if something goes wrong */
char inbuf[BUFSIZE];

/* Socket for talking to server */
int servsock = -1;

/* Should we update the databases now? */
int save_data = 0;

/* At what time were we started? */
time_t start_time;


/******** Local variables! ********/

/* Set to 1 if we are waiting for input */
static int waiting = 0;

/* Set to 1 after we've set everything up */
static int started = 0;

/* If we get a signal, use this to jump out of the main loop. */
static jmp_buf panic_jmp;

/*************************************************************************/

/* If we get a weird signal, come here. */

void sighandler(int signum)
{
    if (started) {
        if (signum == SIGHUP) {  /* SIGHUP = save databases and restart */
            save_data = -2;
            signal(SIGHUP, SIG_IGN);
            slog(logsvc, "Received SIGHUP, restarting.");
            if (!quitmsg)
                quitmsg = "Restarting on SIGHUP";
            longjmp(panic_jmp, 1);
        } else if (signum == SIGTERM) {
            save_data = 1;
            delayed_quit = 1;
            signal(SIGTERM, SIG_IGN);
            signal(SIGHUP, SIG_IGN);
            slog(logsvc, "Received SIGTERM, exiting.");
            quitmsg = "Shutting down on SIGTERM";
            longjmp(panic_jmp, 1);
        } else if (signum == SIGINT || signum == SIGQUIT) {
            /* nothing -- terminate below */
        } else if (!waiting) {
            slog(logsvc, "PANIC! buffer = %s", inbuf);
            /* Cut off if this would make IRC command >510 characters. */
            if (strlen(inbuf) > 448) {
                inbuf[446] = '>';
                inbuf[447] = '>';
                inbuf[448] = 0;
            }
            wallops(NULL, "PANIC! buffer = %s\r\n", inbuf);
        } else if (waiting < 0) {
            /* This is static on the off-chance we run low on stack */
            static char buf[BUFSIZE];
            switch (waiting) {
                case  -1: snprintf(buf, sizeof(buf), "in timed_update");
                          break;
                case -11: snprintf(buf, sizeof(buf), "saving %s", NickDBName);
                          break;
                case -12: snprintf(buf, sizeof(buf), "saving %s", ChanDBName);
                          break;
                case -14: snprintf(buf, sizeof(buf), "saving %s", OperDBName);
                          break;
                case -15: snprintf(buf, sizeof(buf), "saving %s", AutokillDBName);
                          break;
                case -16: snprintf(buf, sizeof(buf), "saving %s", NewsDBName);
                          break;
                case -21: snprintf(buf, sizeof(buf), "expiring nicknames");
                          break;
                case -22: snprintf(buf, sizeof(buf), "expiring channels");
                          break;
                case -25: snprintf(buf, sizeof(buf), "expiring autokills");
                          break;
                default : snprintf(buf, sizeof(buf), "waiting=%d", waiting);
            }
            wallops(NULL, "PANIC! %s (%s)", buf, strsignal(signum));
            slog(logsvc, "PANIC! %s (%s)", buf, strsignal(signum));
        }
    }
    if (signum == SIGUSR1 || !(quitmsg = malloc(BUFSIZE))) {
        quitmsg = "Out of memory!";
        quitting = 1;
    } else {
#if HAVE_STRSIGNAL
        snprintf(quitmsg, BUFSIZE, "Services terminating: %s", strsignal(signum));
#else
        snprintf(quitmsg, BUFSIZE, "Services terminating on signal %d", signum);
#endif
        quitting = 1;
    }
    if (started)
        longjmp(panic_jmp, 1);
    else {
        slog(logsvc, "%s", quitmsg);
        if (isatty(2))
            fprintf(stderr, "%s\n", quitmsg);
        exit(1);
    }
}

/*************************************************************************/

/* Main routine.  (What does it look like? :-) ) */

int main(int ac, char **av, char **envp)
{
    volatile time_t last_update; /* When did we last update the databases? */
    volatile time_t last_expire; /* When did we last expire nicks/channels? */
    volatile time_t last_check;  /* When did we last check timeouts? */
    int i;
    char *progname;


    /* Find program name. */
    if ((progname = strrchr(av[0], '/')) != NULL)
        progname++;
    else
        progname = av[0];

    /* Initialization stuff. */
    if ((i = init(ac, av)) != 0)
        return i;


    /* We have a line left over from earlier, so process it first. */
    process();

    /* Set up timers. */
    last_update = time(NULL);
    last_expire = time(NULL);
    last_check  = time(NULL);

    /* The signal handler routine will drop back here with quitting != 0
     * if it gets called. */
    setjmp(panic_jmp);

    started = 1;


    /*** Main loop. ***/

    while (!quitting) {
        time_t t = time(NULL);

        Debug((2, "Top of main loop"));
        if (!readonly && (save_data || t-last_expire >= ExpireTimeout)) {
            waiting = -3;
            Debug((1, "Running expire routines"));
            if (!skeleton) {
                waiting = -21;
                expire_nicks();
                waiting = -22;
                expire_chans();
            }
            waiting = -25;
            expire_akills();
#ifndef STREAMLINED
//            expire_exceptions();
#endif
            last_expire = t;
        }
        if (!readonly && (save_data || t-last_update >= UpdateTimeout)) {
            waiting = -2;
            Debug((1, "Saving databases"));
            if (!skeleton) {
                waiting = -11;
                save_ns_dbase();
                waiting = -12;
                save_cs_dbase();
            }
            waiting = -14;
            save_os_dbase();
            waiting = -15;
            save_akill();
            waiting = -16;
            save_news();
  //          waiting = -17;
//            save_exceptions();
            if (save_data < 0)
                break;        /* out of main loop */

            save_data = 0;
            last_update = t;
        }
        if (delayed_quit)
            break;
        waiting = -1;
        if (t-last_check >= TimeoutCheck) {
            check_timeouts();
            last_check = t;
        }
        waiting = 1;
        i = (int)(long)sgets2(inbuf, sizeof(inbuf), servsock);
        waiting = 0;
        if (i > 0) {
            process();
        } else if (i == 0) {
            int errno_save = errno;
            quitmsg = malloc(BUFSIZE);
            if (quitmsg) {
                snprintf(quitmsg, BUFSIZE,
                        "Read error from server: %s", strerror(errno_save));
            } else {
                quitmsg = "Read error from server";
            }
            quitting = 1;
        }
        waiting = -4;
    }


    /* Check for restart instead of exit */
    if (save_data == -2) {
#ifdef SERVICES_BIN
        slog(logsvc, "Restarting");
        if (!quitmsg)
            quitmsg = "Restarting";
        send_cmd(&myService, "SQ %s 0 :%s", myService.yxx, quitmsg);
        disconn(servsock);
        close_slog(logsvc, );
        execve(SERVICES_BIN, av, envp);
        if (!readonly) {
            open_slog(logsvc, );
            log_perror("Restart failed");
            close_slog(logsvc, );
        }
        return 1;
#else
        quitmsg = "Restart attempt failed--SERVICES_BIN not defined (rerun configure)";
#endif
    }

    /* Disconnect and exit */
    if (!quitmsg)
        quitmsg = "Terminating, reason unknown";
    slog(logsvc, "%s", quitmsg);
    if (started)
        send_cmd(&myService, "SQ %s 0 :%s", myService.yxx, quitmsg);
    disconn(servsock);
    return 0;
}

/*************************************************************************/
