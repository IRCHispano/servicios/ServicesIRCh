/*
 * ServicesIRCh - Services for IRCh, stats.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "stats.h"

#include "charset.h"
#include "config.h"
#include "log.h"
#include "netclient.h"
#include "send.h"
#include "services.h"
#include "svc_string.h"

#include <assert.h>
#include <limits.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

/*************************************************************************/

struct IRCStatistics IRCStats;

/* Maps from characters to statistics descriptors. */
static struct StatsCmds *statsmap[256];
/* Number of statistics descriptors. */
static int statscount;
static int stats_cmp(const void *a_, const void *b_);

static void stats_void(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_connect(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_engine(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_access(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_links(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_modules(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_operators(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_mappings(struct NetClient *nc, const struct StatsCmds *sc, char *params);
static void stats_uptime(struct NetClient *nc, const struct StatsCmds *sc, char *params);

/*************************************************************************/

static struct StatsCmds statscmds[] = {
 { 'a', "nameservers",  0,                               stats_void,      0 },
 { 'b', "ddb",          0,                               stats_void,      0 },
 { 'c', "connect",      0,                               stats_connect,   0 },
 { 'd', "maskrules",    STATS_CASESENS,                  stats_void,      0 },
 { 'D', "crules",       STATS_CASESENS,                  stats_void,      0 },
 { 'e', "engine",       0,                               stats_engine,    0 },
 { 'f', "features",     0,                               stats_void,      0 },
 { 'g', "glines",       0,                               stats_void,      0 },
 { 'i', "access",       STATS_VARPARAM,                  stats_access,    0 },
 { 'j', "histogram",    STATS_CASESENS,                  stats_void,      0 },
 { 'J', "jupes",        STATS_CASESENS,                  stats_void,      0 },
 { 'k', "klines",       STATS_VARPARAM,                  stats_void,      0 },
 { 'l', "links",        STATS_VARPARAM | STATS_CASESENS, stats_links,     0 },
 { 'L', "modules",      STATS_CASESENS,                  stats_modules,   0 },
 { 'm', "commands",     STATS_CASESENS,                  stats_void,      0 },
 { 'o', "operators",    0,                               stats_operators, 0 },
 { 'p', "ports",        STATS_VARPARAM,                  stats_void,      0 },
 { 'q', "quarantines",  STATS_VARPARAM,                  stats_void,      0 },
 { 'R', "mappings",     STATS_CASESENS,                  stats_mappings,  0 },
 { 'r', "usage",        STATS_CASESENS,                  stats_void,      0 },
 { 'T', "motds",        STATS_CASESENS,                  stats_void,      0 },
 { 't', "locals",       STATS_CASESENS,                  stats_void,      0 },
 { 'U', "uworld",       STATS_CASESENS,                  stats_void,      0 },
 { 'u', "uptime",       STATS_CASESENS,                  stats_uptime,    0 },
 { 'v', "vservers",     STATS_VARPARAM | STATS_CASESENS, stats_void,      1 },
 { 'V', "vserversmach", STATS_VARPARAM | STATS_CASESENS, stats_void,      0 },
 { 'w', "userload",     0,                               stats_void,      0 },
 { 'x', "memusage",     0,                               stats_void,      0 },
 { 'y', "classes",      0,                               stats_void,      0 },
 { 'z', "memory",       0,                               stats_void,      0 },
 { '\0', NULL,          0,                               NULL, 0 }
};

/*************************************************************************/

/* Inicializar contadores y sistema de stats */
void stats_init(void)
{
    struct StatsCmds *sc;

    memset(&IRCStats, 0, sizeof(IRCStats));
    IRCStats.pservers = 1;

    for (statscount = 0, sc = statscmds; sc->name; sc++, statscount++) {}
        qsort(statscmds, statscount, sizeof(statscmds[0]), stats_cmp);

    for (sc = statscmds; sc->name; sc++)
    {
        if (!sc->character)
            continue;
        else if (sc->flags & STATS_CASESENS)
            /* case sensitive character... */
            statsmap[sc->character - CHAR_MIN] = sc;
        else
        {
            /* case insensitive--make sure to put in two entries */
            statsmap[ToLower(sc->character) - CHAR_MIN] = sc;
            statsmap[ToUpper(sc->character) - CHAR_MIN] = sc;
        }
    }
}

/*************************************************************************/

void stats_newrecord_users(void)
{
    IRCStats.max_users = IRCStats.users;
    IRCStats.ts_max_users = time(NULL);

    if (LogMaxUsers)
        slog(logsvc, "user: New maximum user count: %d", IRCStats.max_users);
}

void stats_newrecord_chans(void)
{
    IRCStats.max_chans = IRCStats.channels;
    IRCStats.ts_max_chans = time(NULL);
}


/* Stats vacio  */
static void stats_void(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
}


/* Stats c / connect */
static void stats_connect(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    send_numirc(nc, 213, "C %s * %d 1 * Servers",
                myHub ? myHub->name : "<null>" , RemotePort);
}

/* Stats e / engine */
static void stats_engine(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    send_numirc(nc, 237, "select() :Event loop engine");
}

/* Stats i / access */
static void stats_access(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
//    send_numirc(nc, 215, "I *@* %d * 0 Users", CLSDefLimit);
    send_numirc(nc, 215, "I *@* 1 * 0 Servers");
}

/* Stats l / links */
static void stats_links(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    send_numirc(nc, 211, "Connection SendQ SendM SendKBytes RcveM "
                         "RcveKBytes :Open since");
#if 0
    send_numirc(nc, 211, "%s %d %d %d %d %d :%ld",
                RemoteServer, read_buffer_len(), total_read, -1,
                write_buffer_len(), total_written, time(NULL) - start_time);
#endif
}

/* Stats L / modules */
static void stats_modules(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    send_numirc(nc, 241, "Module  Description      Entry Point");

    /* En el futuro, modulos aqui */
}

/* Stats o / operators */
static void stats_operators(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    send_numirc(nc, 243, "O *@* * %s Opers", ServicesRoot);
}

/* Stats R / mappings */
static void stats_mappings(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    send_numirc(nc, 276, "Command   Name      Prepend    Target");
}

/* Stats u / uptime */
static void stats_uptime(struct NetClient *nc, const struct StatsCmds *sc, char *params)
{
    int uptime = time(NULL) - start_time;

    send_numirc(nc, 242, ":Services up %d day%s, %d:%02d:%02d",
                uptime/86400, (uptime/86400 == 1) ? "" : "s",
                (uptime/3600) % 24, (uptime/60) % 60, uptime % 60);
    send_numirc(nc, 250, ":Current users: %d (%d ops); maximum %d",
                IRCStats.users, IRCStats.opers, IRCStats.max_users);
}


/* Stats v / vservers */

/* Stats V / vserversmach */


/*************************************************************************/

static int stats_cmp(const void *a_, const void *b_)
{
    const struct StatsCmds *a = a_;
    const struct StatsCmds *b = b_;
    return irc_strcmp(a->name, b->name);
}

static int stats_search(const void *key, const void *sc_)
{
    const struct StatsCmds *sc = sc_;
    return irc_strcmp(key, sc->name);
}

static const struct StatsCmds *stats_find(const char *name_or_char)
{
    if (!name_or_char[1])
        return statsmap[name_or_char[0] - CHAR_MIN];
    else
        return bsearch(name_or_char, statscmds, statscount, sizeof(statscmds[0]), stats_search);
}

/*************************************************************************/
/******************* PARSEO DE RED - ESTADISTICAS*************************/
/*************************************************************************/

/* Handle a STATS command
 *
 *  source = Usuario
 *    av[0] = Seleccion de estadisticas
 *    av[1] = Servidor a consultar (opcional)
 *    av[2] = Parametro extra para algunos stats (opcional)
 *
 *  En stats l/links, av[2] es la mascara de servidores.
 *  En stats p/ports, av[2] es la mascara de puertos.
 *  En stats k/klines y i/access, av[2] es el hostname para filtrar
 */
void m_stats(struct NetClient *source, int ac, char **av)
{
    const struct StatsCmds *sc;
    char *param;

    if ((ac < 1) || !(sc = stats_find(av[0]))) {
        send_numirc(source, 219, "%s :End of /STATS report", ac < 1 ? "*" : av[0]);
        return;
    }

    assert(sc != 0);

    if ((sc->flags & STATS_VARPARAM) && ac > 2 && av[2])
        param = av[2];
    else
        param = NULL;

    assert(sc->func != 0);

    /* Lanzamos la funcion */
    (*sc->func)(source, sc, param);

    send_numirc(source, 219, "%s :End of /STATS report", av[0]);
}
