## AutoMake Makefile fragment for the ServicesIRCh sources
##
## Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
## Copyright (C) 2016-2017 Toni Garcia - zoltan <toni@tonigarcia.es>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

noinst_PROGRAMS = src/charset_gen
bin_PROGRAMS = services

AM_CPPFLAGS = -I. -I$(top_srcdir)/include -DSERVICES_CONF_PATH="\"$(sysconfdir)\"" \
	-DSERVICES_BIN_PATH="\"$(bindir)/services\"" -DSERVICES_CONFIG_PATH="\"services.conf\"" \
	-DSERVICES_DATA_PATH="\"$(datadir)\"" -DSERVICES_LOGS_PATH="\"logs\"" -DSERVICES_TEMP_PATH="\"temp\""


EXTRA_DIST += src/version.c.SH

DISTCLEANFILES = src/version.c

nodist_services_SOURCES = src/version.c
services_SOURCES = \
	src/actions.c \
	src/akill.c \
	src/channels.c \
	src/charset.c \
	src/commands.c \
	src/compat.c \
	src/config.c \
	src/datafiles.c \
	src/encrypt.c \
	src/hash.c \
	src/init.c \
	src/language.c \
	src/log.c \
	src/match.c \
	src/messages.c \
	src/misc.c \
	src/netclient.c \
	src/numerics.c \
	src/process.c \
	src/send.c \
	src/servers.c \
	src/service.c \
	src/services.c \
	src/sockutil.c \
	src/stats.c \
	src/svc_memory.c \
	src/svc_string.c \
	src/timeout.c \
	src/users.c

# Bots
services_SOURCES += \
	src/chanserv.c \
	src/helpserv.c \
	src/memoserv.c \
	src/newsserv.c \
	src/nickserv.c \
	src/operserv.c

src/charset.c: src/charset_gen
	./src/charset_gen > src/charset.c

src/version.c: src/version.c.SH include/version.h .patches $(services_SOURCES)
	$(SHELL) $(srcdir)/src/version.c.SH $(top_srcdir)

include/language.h: include/langstrs.h
	cp -f src/lang/langstrs.h include/langstrs.h

include/langstrs.h: src/lang/langstrs.h
