/*
 * ServicesIRCh - Services for IRCh, svc_memory.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "svc_memory.h"

#include "log.h"

#include <signal.h>
#include <stdlib.h>
#include <string.h>

/*************************************************************************/
/*************************************************************************/

/* smalloc, scalloc, srealloc, sstrdup:
 *        Versions of the memory allocation functions which will cause the
 *        program to terminate with an "Out of memory" error if the memory
 *        cannot be allocated.  (Hence, the return value from these functions
 *        is never NULL.)
 */

void *smalloc(long size)
{
    void *buf;

    if (!size) {
        slog(logsvc, "smalloc: Illegal attempt to allocate 0 bytes");
        size = 1;
    }
    buf = malloc(size);
    if (!buf)
        raise(SIGUSR1);
    return buf;
}

void *scalloc(long elsize, long els)
{
    void *buf;

    if (!elsize || !els) {
        slog(logsvc, "scalloc: Illegal attempt to allocate 0 bytes");
        elsize = els = 1;
    }
    buf = calloc(elsize, els);
    if (!buf)
        raise(SIGUSR1);
    return buf;
}

void *srealloc(void *oldptr, long newsize)
{
    void *buf;

    if (!newsize) {
        slog(logsvc, "srealloc: Illegal attempt to allocate 0 bytes");
        newsize = 1;
    }
    buf = realloc(oldptr, newsize);
    if (!buf)
        raise(SIGUSR1);
    return buf;
}

char *sstrdup(const char *s)
{
    char *t = strdup(s);
    if (!t)
        raise(SIGUSR1);
    return t;
}

/*************************************************************************/
/*************************************************************************/

/* In the future: malloc() replacements that tell us if we're leaking and
 * maybe do sanity checks too... */

/*************************************************************************/
