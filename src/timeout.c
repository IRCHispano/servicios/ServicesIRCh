/*
 * ServicesIRCh - Services for IRCh, timeout.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "services.h"
#include "timeout.h"

#include "log.h"
#include "send.h"
#include "svc_memory.h"

#include <stdlib.h>

static struct Timeout *timeouts = NULL;

/*************************************************************************/

#ifdef DEBUG_COMMANDS

/* Send the timeout list to the given user. */

void send_timeout_list(struct User *nc)
{
    struct Timeout *to, *last;

    notice(s_OperServ, nc->nick, "Now: %ld", time(NULL));
    for (to = timeouts, last = NULL; to; last = to, to = to->next) {
        notice(s_OperServ, nc->nick, "%p: %ld: %p (%p)",
                        to, to->timeout, to->code, to->data);
        if (to->prev != last)
            notice(s_OperServ, nc->nick,
                        "    to->prev incorrect!  expected=%p seen=%p",
                        last, to->prev);
    }
}

#endif        /* DEBUG_COMMANDS */

/*************************************************************************/

/* Check the timeout list for any pending actions. */

void check_timeouts(void)
{
    struct Timeout *to, *to2;
    time_t t = time(NULL);

    Debug((2, "Checking timeouts at %ld", t));

    to = timeouts;
    while (to) {
        if (t < to->timeout) {
            to = to->next;
            continue;
        }
        Debug((4, "Running timeout %p (code=%p repeat=%d)",
                   to, to->code, to->repeat));
        to->code(to);
        if (to->repeat) {
            to = to->next;
            continue;
        }
        to2 = to->next;
        if (to->next)
            to->next->prev = to->prev;
        if (to->prev)
            to->prev->next = to->next;
        else
            timeouts = to->next;
        free(to);
        to = to2;
    }
    Debug((2, "Finished timeout list"));
}

/*************************************************************************/

/* Add a timeout to the list to be triggered in `delay' seconds.  If
 * `repeat' is nonzero, do not delete the timeout after it is triggered.
 * This must maintain the property that timeouts added from within a
 * timeout routine do not get checked during that run of the timeout list.
 */

struct Timeout *add_timeout(int delay, void (*code)(struct Timeout *), int repeat)
{
    struct Timeout *t = smalloc(sizeof(struct Timeout));
    t->settime = time(NULL);
    t->timeout = t->settime + delay;
    t->code = code;
    t->repeat = repeat;
    t->next = timeouts;
    t->prev = NULL;
    if (timeouts)
        timeouts->prev = t;
    timeouts = t;
    return t;
}

/*************************************************************************/

/* Remove a timeout from the list (if it's there). */

void del_timeout(struct Timeout *t)
{
    struct Timeout *ptr;

    for (ptr = timeouts; ptr; ptr = ptr->next) {
        if (ptr == t)
            break;
    }
    if (!ptr)
        return;
    if (t->prev)
        t->prev->next = t->next;
    else
        timeouts = t->next;
    if (t->next)
        t->next->prev = t->prev;
    free(t);
}

/*************************************************************************/
