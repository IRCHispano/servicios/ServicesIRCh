/*
 * ServicesIRCh - Services for IRCh, users.c
 *
 * Copyright (C) 2019-2021 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 2012-2013 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "sysconf.h"
#include "users.h"

#include "akill.h"
#include "channels.h"
#include "chanserv.h"
#include "log.h"
#include "hash.h"
#include "match.h"
#include "misc.h"
#include "netclient.h"
#include "nickserv.h"
#include "newsserv.h"
#include "numerics.h"
#include "send.h"
#include "servers.h"
#include "service.h"
#include "services.h"
#include "stats.h"
#include "svc_memory.h"
#include "svc_string.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*************************************************************************/
/*************************************************************************/

struct User *users_create(void)
{
    struct User *user;

    user = smalloc(sizeof(struct User));
    assert(user);
    memset(user, 0, sizeof(struct User));

    user->my_signon = 0;
    user->invalid_pw_count = 0;
    user->invalid_pw_time = 0;
    user->lastmemosend = 0;
    user->lastnickreg = 0;

    return user;
}

/*************************************************************************/

void users_delete(struct User *user)
{
    assert(user);

    if (user->username)
        sfree(user->username);
    if (user->host)
        sfree(user->host);
    if (user->away)
        sfree(user->away);

    sfree(user);
}

/*************************************************************************/

void user_remove(struct NetClient *nc, char *reason)
{
    struct NickInfo *ni;
    struct u_chaninfolist *ci, *ci2;

    /* Nick registrado */
    if ((ni = nc->user->real_ni) != NULL) {
        /* Services root privilege paranoia */
//        ni->status &= ~NS_SERVROOT;
        if ((!(ni->status & NS_VERBOTEN)) &&
              (ni->status & (NS_IDENTIFIED | NS_RECOGNIZED)) //&&
//             !(ni->status & NS_SUSPEND)) {
             ) {
         ni->last_seen = time(NULL);
         if (ni->last_quit)
             sfree(ni->last_quit);
         ni->last_quit = *reason ? sstrdup(reason) : NULL;
         ni->nc = NULL;
         //ni->acl = NULL;
       }
    }

    /* Ajustar contadores */
    Count_quituser(IRCStats, nc);
    if (nc->user->modes & UMODE_o)
        IRCStats.ircops--;

    /* Llamada a timeouts de NickServ */
    cancel_user(nc);

    /* Borrar canales */
    Debug((2, "user_remove(): remove from channels"));
    remove_user_from_all_channels(nc);

    /* Borrar datos founders */
    Debug((2, "user_remove(): free founder data"));
    ci = nc->user->founder_chans;
    while (ci) {
        ci2 = ci->next;
        sfree(ci);
        ci = ci2;
    }

    Debug((2, "delete_user(): free user data"));

    if (nc->user)
        RemoveYXXClient(nc->user->server, nc->yxx);

    netclient_del_list(nc);
    hRemNetClient(nc);
    netclient_delete(nc);
}

/*************************************************************************/

/*************************************************************************/
/*********** GESTION DE MODOS DE USUARIO *********************************/
/*************************************************************************/

static int user_modes[] = {
    UMODE_o, 'o',
    UMODE_i, 'i',
    UMODE_w, 'w',
    UMODE_s, 's',
    UMODE_d, 'd',
    UMODE_k, 'k',
    UMODE_g, 'g',
    UMODE_r, 'r',
    UMODE_S, 'S',
    UMODE_a, 'a',
    UMODE_C, 'C',
    UMODE_h, 'h',
    UMODE_B, 'B',
    UMODE_R, 'R',
    UMODE_c, 'c',
    UMODE_x, 'x',
    UMODE_X, 'X',
    UMODE_z, 'z',
    UMODE_n, 'n',
    UMODE_K, 'K',
    UMODE_I, 'I',
    UMODE_W, 'W',
    UMODE_D, 'D',
    UMODE_P, 'P',
    UMODE_J, 'J',
    0,       0
};

int user_mode_to_int(char mode)
{
    int flag, *s;

    for (s = user_modes; (flag = *s); s += 2) {
        if (mode == (char)(*(s + 1)))
            return flag;
    }
    return 0;
}

/*************************************************************************/

void set_user_modes(struct NetClient *source, struct NetClient *nc, char *modes)
{
    char *m;
    int flag, *s;
    int add = 1;        /* 1 if adding modes, 0 if deleting */

    for (m = modes; *m; m++) {
        switch (*m)
        {
            case '+':
                add = 1;
                break;
            case '-':
                add = 0;
                break;

            case ' ':
            case '\n':
            case '\r':
            case '\t':
                break;

            case 'o':
                if (add) {
                    nc->user->modes |= UMODE_o;
                    ++IRCStats.ircops;
                } else {
                    nc->user->modes &= ~UMODE_o;
                    --IRCStats.ircops;
                }
                break;

            default:
                for (s = user_modes; (flag = *s); s += 2) {
                    if (*m == (char)(*(s + 1))) {
                        if (add)
                            nc->user->modes |= flag;
                        else
                            nc->user->modes &= ~flag;
                        break;
                    }
                }
                break;
        }
    }
}

/*************************************************************************/

static char umode_buf[2 * sizeof(user_modes) / sizeof(int)];
char *umode_str(int modes, int show_invisible)
{
    char *m = umode_buf;
    int flag, *s;

    for (s = user_modes; (flag = *s); s += 2) {
        if (modes & flag) {
            if (flag & (UMODE_W | UMODE_I)) {
                if (show_invisible)
                    *m++ = *(s + 1);
            } else
                *m++ = *(s + 1);
        }
    }
    *m = '\0';

    return umode_buf;
}

/*************************************************************************/
/******************* PARSEO DE RED - USUARIOS ****************************/
/*************************************************************************/


/* Handle a AWAY command
 *
 *  source = User
 *   av[0] = Away message (Optional)
 *
 */
void m_away(struct NetClient *source, int ac, char **av)
{
    if (IsServer(source)) {
        send_cmd(&myService, "DS :Protocol Violation from %s: Server trying to set itself away",
                              source->name);
        return;
    }

    if (ac == 0 || *av[0] == 0) /* un-away */
        check_memos(source);

    return;
}

/*************************************************************************/

/* Handle a GLINE command.
 *
 *  source = User/Server
 */
void m_gline(struct NetClient *source, int ac, char **av)
{
    /* No necesitamos las glines, las ignoramos */
}

/*************************************************************************/

/* Handle a KILL command
 *
 *     source = User/Server
 *      av[0] = Numeric/Nick being killed
 *      av[1] = Reason
 *
 */
void m_kill(struct NetClient *source, int ac, char **av)
{
    struct NetClient *nc;

    if (ac < 2) {
        send_cmd(&myService, "DS :Protocol Violation from %s: Too few arguments for KILL",
                              source->name);
        send_numirc(source, 461, "NICK :Not enough parameters");
        return;
    }

    nc = FindNUser(av[0]);
    if (!nc)
        nc = FindUser(av[0]);

    if (nc) {
        /** @todo
         * Que tal si le mandamos un kill por tocar los cojones? :)
         */
         if (MyService(nc)) {
             /* Recover if someone kills us. */
             if (!readonly && !skeleton)
                 service_introduce(nc);
             wallops(NULL, "El gilipollas de %s ha killeado a %s", source->name, nc->name);
             slog(logsvc, "El gilipollas de %s ha killeado a %s", source->name, nc->name);
             return;
         }
         Debug((1, "%s killed", av[0]));

         if (IsUser(source))
             wallops(NULL, "[KILL] %s por %s, motivo: %s", nc->name, source->name, av[1]);

        /* Salida del usuario */
        user_remove(nc, av[1]);
    } else {
        if (IsUser(source))
            send_cmd(&myService, "O %s%s :KILL target disconnected before I got him :(",
                                  NumNick(source));
    }
}

/*************************************************************************/

/* Handle a NICK command.
 *
 *  If source is user:
 *      av[0] = New nick
 *      av[1] = Time of change
 *      av[2] = User modes (optional)
 *
 *      If source is server:
 *      av[0] = New nick
 *      av[1] = Hop count
 *      av[2] = Signon time
 *      av[3] = Username
 *      av[4] = Hostname
 *      av[5] = User modes (optional)
 *      av[ac-3] = IP base64
 *      av[ac-2] = Numeric
 *      av[ac-1] = User's real name
 *
 */
void m_nick(struct NetClient *source, int ac, char **av)
{
    struct NetClient *nc;
    struct irc_in_addr ip;
    struct NickInfo *new_ni;   /* New master nick */

    if ((IsServer(source) && ac < 7) || ac < 2) {
        send_numirc(source, 461, "NICK :Not enough parameters");
        if (debug) {
            slog(logsvc, "debug: NICK message: expecting 2 or 7 parameters after "
                    "parsing; got %d, source=`%s'", ac, source->name);
        }
        return;
    }

    /* ircu sends the server as the source for a NICK message for a new
     * user. */
    if (IsServer(source)) {
        /* Introduccion de un nick en la red */

        Debug((1, "new user: %s", av[0]));

        /* Comprobamos si existe el usuario */
        nc = FindUser(av[1]);
        if (nc) {
            /* Hay colision */
            /** @todo Si el nick es de un service, hay que killearlo. */
            assert(0);
            return;
        }

        /* We used to ignore the ~ which a lot of ircd's use to indicate no
         * identd response.  That caused channel bans to break, so now we
         * just take what the server gives us.  People are still encouraged
         * to read the RFCs and stop doing anything to usernames depending
         * on the result of an identd lookup.
         */

        /* Cogemos la IP */
        base64toip(av[ac - 3], &ip);

        if (!IsMe(source)) {
            /* First check for AKILLs. */
            if (check_akill(av[ac-2], av[0], av[3], av[4], &ip))
                return;
        }


        /* Creamos el NetClient asociado */
        nc = netclient_create(NETCLIENT_USER);

        nc->user->server = source;
        nc->name = sstrdup(av[0]);
        nc->hopcount = atoi(av[1]);
        nc->user->ts_connect = nc->user->ts_lastnick = atol(av[2]);
        nc->user->my_signon = time(NULL);
        nc->user->username = sstrdup(av[3]);
        nc->user->host = sstrdup(av[4]);

        //memcpy(&nc->user->ip, &ip, sizeof(struct irc_in_addr));
        base64toip(av[ac - 3], &nc->user->ip);
        nc->description = sstrdup(av[ac - 1]);

        if (ac > 6 && *av[5] == '+')
            set_user_modes(source, nc, av[5]);

        /* Agregamos en la lista de numericos
         * y tabla hash de nombres.
         */
        SetRemoteNumNick(nc, av[ac - 2]);
        netclient_add_list(nc);
        hAddNetClient(nc);
        Count_newuser(IRCStats, nc);

        //MOSTRAR NOTICIAS
        display_news(nc, NEWS_LOGON);

    } else {
        struct u_chaninfolist *uc, *uc2;
        /* Cambio de nick */
        assert(source);
        nc = source;

        Debug((1, "%s changes nick to %s", source->name, av[0]));

        if (irc_strcmp(nc->name, av[0]) != 0)
            nc->user->my_signon = time(NULL);

        nc->user->ts_lastnick = atol(av[1]);
        if (ac > 2 && *av[2] == '+')
            set_user_modes(source, nc, av[2]);

        netclient_del_list(nc);
        hRemNetClient(nc);

        sfree(nc->name);
        nc->name = sstrdup(av[0]);

        netclient_add_list(nc);
        hAddNetClient(nc);

        /* Be extra careful with Services root privileges; validate_user()
         * will clear all NS_TEMPORARY flags the next time someone uses
         * the nick, but just in case... */
//        if (nc->user->real_ni)
//            nc->user->real_ni->status &= ~NS_SERVROOT;

        /* Limpiamos lista founders */
        uc = nc->user->founder_chans;
        nc->user->founder_chans = NULL;
        while (uc) {
            uc2 = uc->next;
            sfree(uc);
            uc = uc2;
        }

        new_ni = findnick(av[0]);
        if (new_ni)
            new_ni = getlink(new_ni);

        if (new_ni != nc->user->ni) {
            cancel_user(nc);
        } else if (new_ni) {
            if (!(nc->user->real_ni->status & NS_IDENTIFIED))
                cancel_user(nc);
        }
    }

    nc->user->real_ni = findnick(nc->name);
    if (nc->user->real_ni) {
        nc->user->ni = getlink(nc->user->real_ni);
        nc->user->real_ni->nc = nc;
    } else
        nc->user->ni = NULL;

    if (skeleton)
        return;

    /* Make sure NS_ON_ACCESS flag gets set for current nick */
    //check_on_access(nc);

    if (nc->user->ni) {
//        if (nc->user->modes & UMODE_r && !(nc->user->ni->status & NS_SUSPEND))
        if (nc->user->modes & UMODE_r)
            set_identified(nc, 1);
        else
            validate_user(nc);
 //   } else {
   //     if (nc->user->modes & UMODE_r)
     //       ddb_sync_nick(nc->name, NULL);
    }
}

/*************************************************************************/

/* Handle a PRIVS command.
 *
 *  source = User
 */
void m_privs(struct NetClient *source, int ac, char **av)
{
    /* Los services no tienen los mismos privilegios
     * que el ircd, lo ignoramos.
     */
     return;
}

/*************************************************************************/

/* Handle a QUIT command.
 *
 *  source = User
 *   av[0] = Exit message
 *
 */
void m_quit(struct NetClient *source, int ac, char **av)
{
    if (IsServer(source)) {
        send_cmd(&myService, "DS :Protocol Violation from %s: Server QUIT, not SQUIT?",
                              source->name);
        return;
    }

    if (ac != 1)
        return;

    Debug((1, "%s quits", source->name));

    /* Salida del usuario */
    user_remove(source, av[0]);
}

/*************************************************************************/

/* Handle a MODE command.
 *
 *   source = User
 *    av[0] = Nick/Numeric to change mode
 *    av[1] = Modes
 *
 */
void m_umode(struct NetClient *source, int ac, char **av)
{
    struct NetClient *nc;
    struct NickInfo *ni;
    int has_r = 0;
    int have_r = 0;

    nc = FindNUser(av[0]);
    if (!nc)
        nc = FindUser(av[0]);

    if (nc) {
        if ((source != nc) && (!IsServer(source))) {
            slog(logsvc, "user: MODE %s %s from different nick %s!", nc->name, av[1], source->name);
            wallops(&myService, "%s attempted to change mode %s for %s (buf=%s)",
                    source->name, av[1], nc->name, inbuf);
            return;
        }

        Debug((1, "Changing mode for %s to %s", nc->name, av[1]));

        if (nc->user->modes & UMODE_r)
            has_r = 1;

        set_user_modes(source, nc, av[1]);

        if (skeleton)
            return;

        if (nc->user->modes & UMODE_r)
            have_r = 1;

        ni = nc->user->ni;
        if (!has_r && have_r) {
/*
            if (!ni)
                ddb_sync_nick(nc->name, NULL);
            else if (!(ni->status & NS_SUSPEND))
*/
                set_identified(nc, 1);
        } else if (has_r && !have_r) {
            if (ni) {
                ni->status &= ~NS_IDENTIFIED;
            }
        }
    } else {
        slog(logsvc, "user: MODE %s %s user not found! (Buf: %s)", av[0], av[1], inbuf);
    }
}

/*************************************************************************/

/* Handle a 436 numeric
 *
 *  source = Server
 *   av[0] = Nick collision
 *
 */
void m_436(struct NetClient *source, int ac, char **av)
{
    if (ac < 1)
        return;

    if (MyService(source) && !skeleton && !readonly)
        service_introduce(source);
}

/*************************************************************************/
/*************************************************************************/

/* Is the given nick an oper? */

int is_oper(struct NetClient *nc)
{
    return nc && nc->user && (nc->user->modes & UMODE_o);
}

/*************************************************************************/
/*************************************************************************/

/* Does the user's usermask match the given mask (either nick!user@host or
 * just user@host)?
 */

int match_usermask(const char *mask, struct NetClient *nc)
{
    char *mask2 = sstrdup(mask);
    char *nick, *username, *host, *nick2, *host2;
    int result;

    if (strchr(mask2, '!')) {
        nick = strlower(strtok(mask2, "!"));
        username = strtok(NULL, "@");
    } else {
        nick = NULL;
        username = strtok(mask2, "@");
    }
    host = strtok(NULL, "");
    if (!username || !host) {
        free(mask2);
        return 0;
    }
    strlower(host);
    host2 = strlower(sstrdup(nc->user->host));
    if (nick) {
        nick2 = strlower(sstrdup(nc->name));
        result = match_wild_nocase(nick, nick2) &&
                 match_wild_nocase(username, nc->user->username) &&
                 match_wild_nocase(host, host2);
        sfree(nick2);
    } else {
        result = match_wild_nocase(username, nc->user->username) &&
                 match_wild_nocase(host, host2);
    }
    free(mask2);
    free(host2);
    return result;
}

/*************************************************************************/

/* Split a usermask up into its constitutent parts.  Returned strings are
 * malloc()'d, and should be free()'d when done with.  Returns "*" for
 * missing parts.
 */

void split_usermask(const char *mask, char **nick, char **user, char **host)
{
    char *mask2 = sstrdup(mask);

    *nick = strtok(mask2, "!");
    *user = strtok(NULL, "@");
    *host = strtok(NULL, "");
    /* Handle special case: mask == user@host */
    if (*nick && !*user && strchr(*nick, '@')) {
        *nick = NULL;
        *user = strtok(mask2, "@");
        *host = strtok(NULL, "");
    }
    if (!*nick)
        *nick = "*";
    if (!*user)
        *user = "*";
    if (!*host)
        *host = "*";
    *nick = sstrdup(*nick);
    *user = sstrdup(*user);
    *host = sstrdup(*host);
    free(mask2);
}

/*************************************************************************/

/* Given a user, return a mask that will most likely match any address the
 * user will have from that location.  For IP addresses, wildcards the
 * appropriate subnet mask (e.g. 35.1.1.1 -> 35.*; 128.2.1.1 -> 128.2.*);
 * for named addresses, wildcards the leftmost part of the name unless the
 * name only contains two parts.  If the username begins with a ~, delete
 * it.  The returned character string is malloc'd and should be free'd
 * when done with.
 */

char *create_mask_ban(struct NetClient *nc)
{
    char *mask, *s, *end;

    /* Get us a buffer the size of the username plus hostname.  The result
     * will never be longer than this (and will often be shorter), thus we
     * can use strcpy() and sprintf() safely.
     */
    end = mask = smalloc(strlen(nc->user->username) + strlen(nc->user->host) + 2);
    end += sprintf(end, "%s@", nc->user->username);
    if (strspn(nc->user->host, "0123456789.") == strlen(nc->user->host)
                && (s = strchr(nc->user->host, '.'))
                && (s = strchr(s+1, '.'))
                && (s = strchr(s+1, '.'))
                && (   !strchr(s+1, '.'))) {        /* IP addr */
        s = sstrdup(nc->user->host);
        *strrchr(s, '.') = 0;
        if (atoi(nc->user->host) < 192)
            *strrchr(s, '.') = 0;
        if (atoi(nc->user->host) < 128)
            *strrchr(s, '.') = 0;
        sprintf(end, "%s.*", s);
        free(s);
    } else {
        if ((s = strchr(nc->user->host, '.')) && strchr(s+1, '.')) {
            s = sstrdup(strchr(nc->user->host, '.')-1);
            *s = '*';
        } else {
            s = sstrdup(nc->user->host);
        }
        strcpy(end, s);
        free(s);
    }
    return mask;
}

char *create_mask_access(char *username, char *host)
{
    char *mask, *s, *end;

    /* Get us a buffer the size of the username plus hostname.  The result
     * will never be longer than this (and will often be shorter).
     */
    end = mask = smalloc(strlen(username) + strlen(host) + 2);
    end += sprintf(end, "%s@", username);
    if (strspn(host, "0123456789.") == strlen(host)
                && (s = strchr(host, '.'))
                && (s = strchr(s+1, '.'))
                && (s = strchr(s+1, '.'))
                && (   !strchr(s+1, '.'))) {    /* IP addr */
        s = sstrdup(host);
        *strrchr(s, '.') = 0;
        sprintf(end, "%s.*", s);
        sfree(s);
    } else if (strchr(host, ':')) {  /* IPv6 */
        /* TODO: Pasar a /64 */
        s = sstrdup(host);
        strcpy(end, s);
        sfree(s);
    } else {
        if ((s = strchr(host, '.')) && strchr(s+1, '.')) {
            s = sstrdup(strchr(host, '.')-1);
            *s = '*';
        } else {
            s = sstrdup(host);
        }
        strcpy(end, s);
        sfree(s);
    }
    return mask;
}

/*************************************************************************/
